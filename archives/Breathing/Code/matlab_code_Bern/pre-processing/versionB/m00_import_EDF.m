% function m01_preprocessing_Respiration(whichsubject)
clear all
close all
addpath(genpath('./matlab_code_Respiration/'));
addpath('./pre-processing/versionB/Respiration/');
addpath(genpath('./matlab_code_Respiration/Functions'));
addpath('./EEG/');
addpath('/home/fosco/toolboxes/eeglab2022.0')
eeglab nogui
addpath('/home/fosco/toolboxes/fieldtrip-master')
ft_defaults

whichsubject = [1:43];

% which subjects you want to process?
[who_idx,code_patient,patient,RBD,PSG_sess1,PSG_sess2,Gr,match,newID,age,gender,center,converted] = get_subjects_RBD(whichsubject);


for isub = 1:length(who_idx)

    % get cfgs
    cfg = get_cfg_RBD(code_patient{isub});
    EEG = [];

    % ---------------------------
    % Import parameters & path(s)
    % ---------------------------
    % Write a status message to the command line.
    fprintf('\n Now importing subject %s, (number %d of %d to process).\n\n',code_patient{isub},isub,length(who_idx));
    pause(1)

    %-------------
    % Import data
    %-------------

    if PSG_sess2(isub) == 1 % exlude the second session of patients that have 2

    else

        eegData = [cfg.dir_raw code_patient{isub} '.edf'];

        if ~exist(eegData,'file')
            fprintf('Does not exist!\n',eegData)
        else

            % Create output directory if necessary.
            if ~isdir(cfg.dir_eeg)
                mkdir(cfg.dir_eeg);
            end

            % get the reference:
            eegtmp = pop_biosig(eegData);
            ref    = eegtmp.ref;
            clear eegtmp

            data = [];
            [data, event] = edf2fieldtrip(eegData);
            % [hdr] = ft_read_header(eegData);

            % % upsample data:
            % cfg1 = [];
            % cfg1.resamplefs = 500;
            % data = ft_resampledata(cfg1,data);


            % data has one extrachan
            selchan = [];
            selchan = ft_channelselection({'all' '-DHR' '-EDF Annotations' '-Arm-L' '-Arm-R' ...
                '-ChinA' '-ChinL' '-ChinR' '-Elevation_DR' '-Flattening' '-Gravity X' '-Gravity Y' '-Mask Snore'...
                '-Snore_DR' '-Snoring Sensor' '-Thermistor' '-Tib-L' '-Tib-R'}, data.label);
            cfg1 = [];
            cfg1.channel = selchan;
            data = ft_selectdata(cfg1,data);

            % % remove muscle artifact:
            % % Select EEG channels
            % cfg = [];
            % cfg.channel = 'EEG*';
            % data_eeg = ft_selectdata(cfg, data);
            %
            % % Apply bandpass filter
            % % Define the filter parameters
            % fs = data.fsample; % Sampling frequency in Hz
            % freqRange = [110 140]; % Frequency range of interest in Hz
            % filterOrder = 9; % Butterworth filter order
            %
            % % Convert frequency range to normalized values
            % Wn = freqRange / (fs/2);
            %
            % % Apply bandpass filter
            % [b, a] = butter(filterOrder, Wn, 'bandpass');
            % filteredData = filtfilt(b, a, cell2mat(data_eeg.trial).');
            %
            % % Compute the Hilbert transform
            % hilbertData = abs(hilbert(filteredData));
            %
            % % Compute z-value of the Hilbert transformed data
            % meanValue = mean(hilbertData);
            % stdValue = std(hilbertData);
            % zValue = (hilbertData - meanValue) ./ stdValue;
            %
            % % Find time points exceeding the z-value threshold
            % threshold = 20;
            % artifactIndices = find(zValue > threshold);
            %
            % % Display the segments with muscle artifacts
            % segmentLength = 10; % Segment length in seconds
            % numSamples = fs * segmentLength;
            % numSegments = floor(size(data, 2) / numSamples);
            % for i = 1:numSegments
            %     segmentStart = (i-1) * numSamples + 1;
            %     segmentEnd = i * numSamples;
            %     segment = data(:, segmentStart:segmentEnd);
            %
            %     % Check if the segment contains any artifact indices
            %     if any(artifactIndices >= segmentStart & artifactIndices <= segmentEnd)
            %         disp(['Segment ' num2str(i) ' contains muscle artifacts']);
            %
            %         % Plot the segment with muscle artifacts
            %         segmentTime = (segmentStart:segmentEnd) / fs;
            %         figure;
            %         for j = 1:numel(eegChannels)
            %             subplot(numel(eegChannels), 1, j);
            %             plot(segmentTime, segment(j, :));
            %             hold on;
            %             plot(segmentTime(artifactIndices), segment(j, artifactIndices), 'ro');
            %             xlabel('Time (s)');
            %             ylabel(['EEG Channel ' num2str(eegChannels(j))]);
            %             legend('Data', 'Muscle Artifact');
            %         end
            %     end
            % end

            % fprintf('\n\n [stage 1] get breathing info \n\n')
            % cfg2 = [];
            % cfg2.xlim = [30.0 50.0];
            % cfg2.channel = 'Abdomen';
            % figure;ft_singleplotER(cfg2,data);
            % cfg.channel = 'Thorax';
            % figure;ft_singleplotER(cfg,data);


            % add chan info
            fprintf('\n\n [stage 0] add channels info \n\n')
            %             chaninfo = load("/home/fosco/Git_epfl/hep_rbd/Chaninfo.mat");
            EEG = fieldtrip2eeglab(data);
            EEG.chanlocs = struct(EEG.chanlocs);

            for i = 1:size(data.label,1)
                EEG.chanlocs(i).labels = data.label{i};
                EEG.chanlocs(i).ref = 'EEG REF';
                EEG.chanlocs(i).X = [];
                EEG.chanlocs(i).Y = [];
                EEG.chanlocs(i).Z = [];
                EEG.ref           = 'EEG REF';

                if contains(EEG.chanlocs(i).labels,'EEG') == 1
                    EEG.type = 'EEG';
                else
                    EEG.type = 'external';
                end% Oz)
            end

            clear data

            tmp      = [];
            tmp      = EEG.data;
            EEG.data = [];
            EEG.data = tmp;
            EEG      = eeg_checkset(EEG);

            clear tmp

            % downsample the data:
            if contains(code_patient{isub},'PSG') == 1
                % EEG = pop_resample(EEG,200);
                % EEG = eeg_checkset(EEG);
                 if EEG.srate ~= 200
                    EEG.srate = 200;
                    EEG.times = EEG.times*10;
                    EEG = eeg_checkset(EEG);
                 end
            else
                if EEG.srate ~= 200
                    EEG = pop_resample(EEG,200);
                end
            end

            % add chanlocs:
            EEG = pop_chanedit(EEG, 'lookup','/home/fosco/toolboxes/eeglab2022.0/plugins/dipfit/standard_BEM/elec/standard_1005.elc');
            EEG = eeg_checkset(EEG);

            for i = 1:EEG.nbchan
                EEG.chanlocs(i).ref = 'EEG REF';
                EEG.ref             = 'EEG REF';
            end
            pop_saveset(EEG,[code_patient{isub}  '_import.set'] , cfg.dir_eeg);

           
            % save also for mne sleep staging:
            EEG  = pop_select(EEG, 'nochannel',{'Abdomen', 'Activity_DR', 'Arm-L', 'Arm-R','ChinA', 'ChinL', 'ChinR',...
                'EOG E1', 'EOG E2', 'Elevation_DR', 'Flattening', 'Flow_DR', 'Gravity X', 'Gravity Y', 'Heart Rate_DR',...
                'Light_BU','Nasal Pressure', 'pCO2', 'Phase_DR',...
                'Plethysmogram', 'Position_DR', 'Pulse', 'RD-Quality',...
                'EEG REF', 'RMI_DR', 'RR_DR', 'Snore_DR', 'Snoring Sensor',...
                'SpO2 BB', 'SpO2-Quality_DR', 'SpO2', 'Thermistor', 'Thorax',...
                'Tib-L', 'Tib-R', 'Tidal Volume_DR', 'XFlow_DR',...
                'XSum_DR','EKG','Mask Pressure','Pneumoflow','REF','PC02 Tosca'});
            
            fprintf(['\n\n' code_patient{isub} '\n\n'])
            % Writing the data
            pop_writeeeg(EEG, [cfg.dir_eeg code_patient{isub}  '_mne.edf'] , 'TYPE','EDF');
            % pop_saveset(EEG,[code_patient{isub}  '_mne.set'] , cfg.dir_eeg);
            clear EEG
        end
    end
end
% end

