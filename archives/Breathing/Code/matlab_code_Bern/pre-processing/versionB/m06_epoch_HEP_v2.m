clear all
close all

addpath(genpath('./matlab_code_Respiration/'));
addpath(genpath('./pre-processing/versionB/Respiration/'));
addpath(genpath('./matlab_code_Respiration/Functions/'));
addpath('./EEG/');
addpath('/home/fosco/toolboxes/eeglab2022.0')
eeglab nogui
addpath('/home/fosco/toolboxes/fieldtrip-master')
ft_defaults

whichsubject = [1:37];
whichdata    = '_import_w_physio_1hz_MRef3.set'; % _ica_1hz_clean
whichdataout = '_HEP_1hz_MRef3.set';


doepoch   = 0; % need to epoch data?
dormepoch = 0; % remove visually bad epochs?
doamprej  = 0; % reject epoch based on amplitude
ref       = 'mastoid';

RBDcode = 0;
HCcode  = 0;


% which subjects you want to process?
[who_idx,code_patient,patient,RBD,PSG_sess1,PSG_sess2,Gr,match,newID,age,gender,center,converted] = get_subjects_RBD(whichsubject);

for isub = 1:length(who_idx)

    % -------------
    % Prepare data.
    % -------------

    cfg = get_cfg_RBD(code_patient{isub});

    % Write a status message to the command line.
    EEG = [];
    EEG = pop_loadset('filename',[cfg.subject_name whichdata],'filepath',cfg.dir_eeg,'loadmode','all');

    % % remove the first and last minutes which are often noisy:
    % fprintf('\n\n [stage 0 ] I cut the first and last minutes \n\n')
    % start = 5*60; % 5min in seconds
    % stop  = EEG.xmax-start;
    % EEG   = pop_select(EEG,'time',[start stop]);
    % EEG   = eeg_checkset(EEG);
    % % Calculate the number of samples to remove
    % samples_to_remove_start = 5 * 60 * EEG.srate; % 5 minutes
    % samples_to_remove_end  = 10 * 60 * EEG.srate; % 10 minutes
    % 
    % %   Calculate the new start and end indices
    % new_start_index = samples_to_remove_start + 1; % Add 1 to keep the next sample
    % new_end_index = length(EEG.data) - samples_to_remove_end;
    % 
    % EEG = pop_select(EEG, 'point', [new_start_index, new_end_index]);

    % if data epoch
    EEG = eeg_checkset(EEG);
    EEG = eeg_checkset(EEG, 'eventconsistency');

    fprintf(['\n\n [ Recodring duration ] ' num2str(EEG.times(end)/60)  ' minutes\n\n']);

    if doepoch == 1
        fprintf(['\n\n [ stage 2 ] epoch data at R-peak \n\n\n\n']);
        [EEG, ~, com] = pop_epoch(EEG,{'r-peak'},[cfg.epoch_ekg_tmin cfg.epoch_ekg_tmax], ...
            'newname', 'BDF file epochs', 'epochinfo', 'yes');
        EEG = eeg_checkset(EEG);
        EEG = eegh(com, EEG);
    end


    % keep only chans of interest:
    eegchans  = {'C3','C4','F3','F4','F7','F8','Fp1','Fp2','Fpz','Fz','O1','O2','P3','P4','Pz','T3','T4','T5','T6','Cz','ECG','REF'};
    chans     = [];
    chans     = find(ismember({EEG.chanlocs.labels},eegchans));
    EEG       = pop_select(EEG, 'channel',{EEG.chanlocs(chans).labels});

    % remove trials based on amplitued on specific EEG channels (wihouth ECG, etc that we still want in the dataset):

    % remove bad trials:
    if dormepoch == 1
        eegchanskeep = {'C3','C4','F3','F4','F7','F8','Fp1','Fp2','Fpz','Fz','O1','O2','P3','P4','Pz','T3','T4','T5','T6','Cz'}; % Oz
        eegchanskeepnum = find(ismember({EEG.chanlocs.labels},eegchanskeep));

        EEGtmp = pop_select(EEG, 'channel',{EEG.chanlocs(eegchanskeepnum).labels});
        data   = eeglab2fieldtrip(EEGtmp,'preprocessing');
        cfg1        = [];
        cfg1.method = 'summary';
        [dummy,badsegment] = ft_rejectvisual(cfg1, data);

        if ~isempty(badsegment)
            fprintf('\n\n Removing: %s \n\n',string(badsegment));
            [EEG, com] = pop_select(EEG,'rmtrial',badsegment);
            EEG = eegh(com, EEG);
        end
    end

    if doamprej == 1
        eegchanskeep2 = {'C3','C4','F3','F4','O1','O2'}; % Oz
        eegchanskeepnum2 = find(ismember({EEG.chanlocs.labels},eegchanskeep2));
        [EEG,com] = pop_eegthresh(EEG,1,eegchanskeepnum2,-300,300,EEG.xmin, EEG.xmax,[],1);
    end

    pause(2)


    % ----------
    % Save data.
    % ------------------------------------------------
    % select only the first part when they are awake:
    %-------------------------------------------------
    EEGtmp = [];
    EEGtmp = pop_selectevent(EEG,'type','r-peak','awake',1);


    filename_awake =[cfg.dir_eeg '/' cfg.subject_name '_HEPs_RBD_1hz_orRef_awake.csv'];
    % Check if the file exists before attempting to delete it
    if exist(filename_awake, 'file')
        % Use the delete function to remove the file
        delete(filename_awake);
        disp(['File "', filename_awake, '" has been deleted.']);
    end
    resultfileData_awake = fopen(filename_awake,'at');
    fprintf(resultfileData_awake, 'amplitude patient age converted chan time sleep HR IBI rmssd hrv_sdsd hrv_sdnn rrhrv pLF pHF LFHFratio trials \n');

    % for itrial = 1:EEG.trials
    for ichan = 1:EEGtmp.nbchan
        for itime = 1:length(EEG.times)
            fprintf(resultfileData_awake,'%f %d %d %s %s %d %s %f %f %f %f %f %f %f %f %f %d \n',...
                mean(EEGtmp.data(ichan,itime,:),3),...
                patient(isub), ...
                age(isub),...
                converted{isub},...
                EEGtmp.chanlocs(ichan).labels,...
                EEGtmp.times(itime),...
                'awake',...
                EEGtmp.ekg_HR,...
                EEGtmp.ekg_IBImean,...
                mean(EEGtmp.rmssd,'omitnan'),...
                mean(EEGtmp.hrv_sdsd,'omitnan'),...
                mean(EEGtmp.hrv_sdnn,'omitnan'),...
                mean(EEGtmp.rrhrv,'omitnan'),...
                mean(EEGtmp.pLF,'omitnan'),...
                mean(EEGtmp.pHF,'omitnan'),...
                mean(EEGtmp.LFHFratio,'omitnan'),...
                EEGtmp.trials);
        end
    end
    % end
    fclose(resultfileData_awake);


    %----------------------------------
    % select all the sleep stating:
    %-----------------------------------
    % identify who many sleep stages are present:
    lsleep = unique([EEG.event.sleep]);
    lsleep = lsleep +1; % to differentiate awake and wake

    for s = 1:length(lsleep)

        if s == 1
            EEGtmp = [];
            EEGtmp = pop_selectevent(EEG,'type','r-peak','sleep',lsleep(s),'awake',1);
        else
            EEGtmp = [];
            EEGtmp = pop_selectevent(EEG,'type','r-peak','sleep',lsleep(s),'awake',0);
        end

        % convert to .csv
        filename = [cfg.dir_eeg '/' cfg.subject_name '_HEPs_RBD_1hz_orRef_sleep_' num2str(lsleep(s)) '.csv'];

        % Check if the file exists before attempting to delete it
        if exist(filename, 'file')
            % Use the delete function to remove the file
            delete(filename);
            disp(['File "', filename, '" has been deleted.']);
        end


        resultfileData1 = fopen(filename,'at');
        fprintf(resultfileData1, 'amplitude patient age converted chan time sleep HR IBI rmssd hrv_sdsd hrv_sdnn rrhrv pLF pHF LFHFratio trials awake\n');

        fprintf(['\n\n processing Sleep stage:' num2str(lsleep(s)) '\n\n\n'])
        for ichan = 1:EEGtmp.nbchan
            for itime = 1:length(EEG.times)
                fprintf(resultfileData1,'%f %d %d %s %s %d %d %f %f %f %f %f %f %f %f %f %d \n',...
                    mean(EEGtmp.data(ichan,itime,:),3),...
                    patient(isub), ...
                    age(isub),...
                    converted{isub},...
                    EEGtmp.chanlocs(ichan).labels,...
                    EEGtmp.times(itime),...
                    lsleep(s),...
                    EEGtmp.ekg_HR,...
                    EEGtmp.ekg_IBImean,...
                    mean(EEGtmp.rmssd(ichan,itime,:),3,'omitnan'),...
                    mean(EEGtmp.hrv_sdsd(ichan,itime,:),3,'omitnan'),...
                    mean(EEGtmp.hrv_sdnn(ichan,itime,:),3,'omitnan'),...
                    mean(EEGtmp.rrhrv(ichan,itime,:),3,'omitnan'),...
                    mean(EEGtmp.pLF(ichan,itime,:),3,'omitnan'),...
                    mean(EEGtmp.pHF(ichan,itime,:),3,'omitnan'),...
                    mean(EEGtmp.LFHFratio(ichan,itime,:),3,'omitnan'),...
                    EEGtmp.trials,...
                    EEGtmp.awake(itime));
            end
        end
        fclose(resultfileData1);
    end
end

fprintf('\n DONE...\n')
