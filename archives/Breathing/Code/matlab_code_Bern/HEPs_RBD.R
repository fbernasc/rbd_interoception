rm(list=ls(all=TRUE))
library(lme4);
library(lmerTest) # mixed model stuff
library(afex)
library(tidyverse)
library(data.table)
library(cowplot)
library(perm)
library(broom)
library(ggiraphExtra)
library(sjPlot)
library(gridExtra)
library(viridis)
library(emmeans)
library(brms)
library(sjstats)
library(sjmisc)
library(ggeffects)
library(bayestestR)
library(bayesplot)
library(rstanarm)
library(formattable)
library(eegUtils)
library(here)
library(ggbeeswarm)


# rootdir=here("EEG","Preprocessed")
rootdir = '/media/fosco/T7/RBD/EEG/Preprocessed/'
csvlist  = list.files(path=paste(rootdir), pattern = '.*_HEPs_RBD_1hz_MRef3_sleep_.*\\.csv$', recursive = TRUE)

# import data
new_data <- purrr::map_df(paste(rootdir,csvlist,sep = '/'), function(x) {
  mydata <- read.csv(x,header = TRUE,sep = "")
  mydata %>%
    # count(Type) %>%
    mutate(ID =  strsplit(basename(x), "_")[[1]][1])
})


# housekeeping:
new_data$amplitude = as.numeric(new_data$amplitude)
new_data$age = as.numeric(new_data$age)
new_data$time = as.numeric(new_data$time)
new_data$time = as.numeric(new_data$time)
new_data$HR = as.numeric(new_data$HR)
new_data$IBI = as.numeric(new_data$IBI)
new_data$rmssd = as.numeric(new_data$rmssd)
new_data$hrv_sdsd = as.numeric(new_data$hrv_sdsd)
new_data$hrv_sdnn = as.numeric(new_data$hrv_sdnn)
new_data$hrv_sdnn = as.numeric(new_data$hrv_sdnn)
new_data$rrhrv = as.numeric(new_data$rrhrv)
new_data$pLF = as.numeric(new_data$pLF)
new_data$pHF = as.numeric(new_data$pHF)
new_data$LFHFratio = as.numeric(new_data$LFHFratio)
new_data$patient   = as.numeric(new_data$patient)

new_data$patient[new_data$patient == 1] = 'RBD'
new_data$patient[new_data$patient == 0] = 'HC'
new_data$sleep[new_data$sleep == 0] = 'Wake'
new_data$sleep[new_data$sleep == 1] = 'N1'
new_data$sleep[new_data$sleep == 2] = 'N2'
new_data$sleep[new_data$sleep == 3] = 'N3'
new_data$sleep[new_data$sleep == 4] = 'REM'

new_data = new_data %>% mutate(sleep2 = ifelse(sleep == 'Wake','Wake',
                                               ifelse(sleep == 'N1' | sleep == 'N2' | sleep == 'N3','NREM',
                                                                             ifelse(sleep == 'REM','REM',NA))))

# check number of trials:
trls = new_data %>% distinct(ID,sleep,.keep_all = TRUE) %>% select(ID,trials,patient,sleep)

# how many patients per electrodes
nbrpatchan = new_data %>% group_by(chan) %>% summarise(n=length(unique(ID)))


# # exclude based on ECG:
new_data = new_data %>%
   filter(chan %in% c('C3','C4','F3','F4','O1','O2','ECG'))

# find common electrodes:
# com_chans =  new_data %>%
#   group_by(chan) %>%
#   mutate(same_across_ids = n_distinct(ID) == 1) %>%
#   ungroup()


# check ECG signals:
colors <- c('black')
new_data %>% 
  filter(chan %in% c('ECG')) %>% 
  ggplot(aes(x=time,y = amplitude)) +
  stat_summary(fun = mean,geom = "line")+
  geom_vline(xintercept = 0,linetype = "dashed" )+
  geom_hline(yintercept = 0,linetype = "dashed") +
  scale_color_manual(values = colors)  + 
  scale_fill_manual(values = colors)  + 
  facet_wrap(~ID,scale = "free") +
  ylab(expression(paste("HEPs amplitude (",mu,"V)"))) +
  xlab('Time (msec)') +
  theme_cowplot(font_size = 12,line_size = 0.5, font_family = "sans-serif")  +
  ggtitle("ECG")


# check single subjects for noisy data
new_data %>% 
  filter(!patient %in% c("RBD")) %>% 
  filter(chan %in% c('C3')) %>% 
  ggplot(aes(x=time,y = amplitude,colour = ID)) +
  stat_summary(fun.data = mean_cl_boot,geom = "ribbon",aes(fill = ID),alpha = 0.2) +
  stat_summary(fun = mean,geom = "line")+
  geom_vline(xintercept = 0,linetype = "dashed" )+
  geom_hline(yintercept = 0,linetype = "dashed") +
  facet_wrap(~ID,scale = "free") +
  ylab(expression(paste("HEPs amplitude (",mu,"V)"))) +
  xlab('Time (msec)') +
  ggtitle("HEP all")

colors <- c('Darkorange','Purple')
new_data %>% 
  filter(!ID %in% c("sub-AA8486")) %>% # very noisy with Original REF: "sub-AA8486","39787","18473" | for M2 Ref: "sub-AA8486","40333" | for M1 Ref: same as M2
  ggplot(aes(x=time,y = amplitude,colour = patient)) +
  stat_summary(fun.data = mean_cl_boot,geom = "ribbon",aes(fill = patient),alpha = 0.2) +
  stat_summary(fun = mean,geom = "line")+
  geom_vline(xintercept = 0,linetype = "dashed" )+
  geom_hline(yintercept = 0,linetype = "dashed") +
  scale_color_manual(values = colors)  + 
  scale_fill_manual(values = colors)  + 
  facet_wrap(~chan,scale = "free",ncol=2) +
  ylab(expression(paste("HEPs amplitude (",mu,"V)"))) +
  xlab('Time (msec)') +
  # theme_cowplot(font_size = 12,line_size = 0.5, font_family = "sans-serif")  +
  ggtitle("HEP all")
  

new_data <- new_data %>% 
  filter(!ID %in% c("sub-AA8486")) %>% 
  group_by(ID,chan) %>% 
  mutate(z_amplitude = scale(amplitude)) %>% ungroup()

new_data$sleep2 <- factor(new_data$sleep2, levels = c("Wake", "NREM", "REM"))

new_data %>% 
  ggplot(aes(x=time,y = z_amplitude,colour = patient)) +
  stat_summary(fun.data = mean_cl_boot,geom = "ribbon",aes(fill = patient),alpha = 0.2) +
  stat_summary(fun = mean,geom = "line")+
  geom_vline(xintercept = 0,linetype = "dashed" )+
  geom_hline(yintercept = 0,linetype = "dashed") +
  scale_color_manual(values = colors)  + 
  scale_fill_manual(values = colors)  + 
  facet_wrap(~chan,scale = "free") +
  ylab(expression(paste("HEPs amplitude (",mu,"V)"))) +
  xlab('Time (msec)') +
  theme_cowplot(font_size = 12,line_size = 0.5, font_family = "sans-serif")  +
  ggtitle("HEP all")


# new_data %>% 
#   ggplot(aes(x=time,y = z_amplitude,colour = sleep2)) +
#   # stat_summary(fun.data = mean_cl_boot,geom = "ribbon",aes(fill = sleep2),alpha = 0.2) +
#   stat_summary(fun = mean,geom = "line")+
#   geom_vline(xintercept = 0,linetype = "dashed" )+
#   geom_hline(yintercept = 0,linetype = "dashed") +
#   facet_wrap(chan~patient,scale = "free",ncol=2) +
#   ylab(expression(paste("HEPs amplitude (",mu,"V)"))) +
#   xlab('Time (msec)') +
#   ggtitle("HEP as a function of sleep stage")

new_data %>% 
  filter(chan %in% c('C4','ECG')) %>% 
  ggplot(aes(x=time,y = z_amplitude,colour = patient)) +
  stat_summary(fun.data = mean_cl_boot,geom = "ribbon",aes(fill = patient),alpha = 0.2) +
  stat_summary(fun = mean,geom = "line")+
  geom_vline(xintercept = 0,linetype = "dashed" )+
  geom_hline(yintercept = 0,linetype = "dashed") +
  scale_color_manual(values = colors)  + 
  scale_fill_manual(values = colors)  + 
  facet_wrap(chan~sleep2,scale = "free",ncol=3) +
  ylab(expression(paste("HEPs amplitude (",mu,"V)"))) +
  theme_cowplot(font_size = 12,line_size = 0.5, font_family = "sans-serif")  +
  xlab('Time (msec)') +
  ggtitle("HEP as a function of sleep stage")


new_data %>% 
  filter(chan %in% c('C3','C4','F4','F3','ECG')) %>% 
  ggplot(aes(x=time,y = z_amplitude,colour = sleep)) +
  # stat_summary(fun.data = mean_cl_boot,geom = "ribbon",aes(fill = sleep),alpha = 0.2) +
  stat_summary(fun = mean,geom = "line")+
  geom_vline(xintercept = 0,linetype = "dashed" )+
  geom_hline(yintercept = 0,linetype = "dashed") +
  facet_wrap(chan~patient,scale = "free",ncol = 2) +
  ylab(expression(paste("HEPs amplitude (",mu,"V)"))) +
  xlab('Time (msec)') +
  # theme_cowplot(font_size = 12,line_size = 0.5, font_family = "sans-serif")  +
  ggtitle("HEP over sleep stages")

new_data %>% 
  filter(sleep %in% c('Wake')) %>% 
  ggplot(aes(x=time,y = z_amplitude,colour = patient)) +
  stat_summary(fun.data = mean_cl_boot,geom = "ribbon",aes(fill = patient),alpha = 0.2) +
  stat_summary(fun = mean,geom = "line")+
  geom_vline(xintercept = 0,linetype = "dashed" )+
  geom_hline(yintercept = 0,linetype = "dashed") +
  scale_color_manual(values = colors)  + 
  scale_fill_manual(values = colors)  + 
  facet_wrap(~chan,scale = "free") +
  ylab(expression(paste("z-score HEPs amplitude (",mu,"V)"))) +
  xlab('Time (msec)') +
  # theme_cowplot(font_size = 12,line_size = 0.5, font_family = "sans-serif") +
  ggtitle("HEP during awake")

new_data %>% 
  filter(sleep %in% c('N1')) %>% 
  ggplot(aes(x=time,y = z_amplitude,colour = patient)) +
  stat_summary(fun.data = mean_cl_boot,geom = "ribbon",aes(fill = patient),alpha = 0.2) +
  stat_summary(fun = mean,geom = "line")+
  geom_vline(xintercept = 0,linetype = "dashed" )+
  geom_hline(yintercept = 0,linetype = "dashed") +
  scale_color_manual(values = colors)  + 
  scale_fill_manual(values = colors)  + 
  facet_wrap(~chan,scale = "free") +
  ylab(expression(paste("HEPs amplitude (",mu,"V)"))) +
  xlab('Time (msec)') +
  theme_cowplot(font_size = 12,line_size = 0.5, font_family = "sans-serif") +
  ggtitle("HEP during N1")

new_data %>% 
  filter(sleep %in% c('N2')) %>% 
  ggplot(aes(x=time,y = z_amplitude,colour = patient)) +
  stat_summary(fun.data = mean_cl_boot,geom = "ribbon",aes(fill = patient),alpha = 0.2) +
  stat_summary(fun = mean,geom = "line")+
  geom_vline(xintercept = 0,linetype = "dashed" )+
  geom_hline(yintercept = 0,linetype = "dashed") +
  scale_color_manual(values = colors)  + 
  scale_fill_manual(values = colors)  + 
  facet_wrap(~chan,scale = "free") +
  ylab(expression(paste("HEPs amplitude (",mu,"V)"))) +
  xlab('Time (msec)') +
  theme_cowplot(font_size = 12,line_size = 0.5, font_family = "sans-serif") +
  ggtitle("HEP during N2")

new_data %>% 
  filter(sleep %in% c('N2')) %>% 
  ggplot(aes(x=time,y = z_amplitude,colour = patient)) +
  stat_summary(fun.data = mean_cl_boot,geom = "ribbon",aes(fill = patient),alpha = 0.2) +
  stat_summary(fun = mean,geom = "line")+
  geom_vline(xintercept = 0,linetype = "dashed" )+
  geom_hline(yintercept = 0,linetype = "dashed") +
  scale_color_manual(values = colors)  + 
  scale_fill_manual(values = colors)  + 
  facet_wrap(~chan,scale = "free") +
  ylab(expression(paste("HEPs amplitude (",mu,"V)"))) +
  xlab('Time (msec)') +
  theme_cowplot(font_size = 12,line_size = 0.5, font_family = "sans-serif") +
  ggtitle("HEP during N2")

new_data %>% 
  filter(sleep %in% c('N3')) %>% 
  ggplot(aes(x=time,y = z_amplitude,colour = patient)) +
  stat_summary(fun.data = mean_cl_boot,geom = "ribbon",aes(fill = patient),alpha = 0.2) +
  stat_summary(fun = mean,geom = "line")+
  geom_vline(xintercept = 0,linetype = "dashed" )+
  geom_hline(yintercept = 0,linetype = "dashed") +
  scale_color_manual(values = colors)  +
  scale_fill_manual(values = colors)  + 
  facet_wrap(~chan,scale = "free") +
  ylab(expression(paste("HEPs amplitude (",mu,"V)"))) +
  xlab('Time (msec)') +
  theme_cowplot(font_size = 12,line_size = 0.5, font_family = "sans-serif") +
  ggtitle("HEP during N3")

new_data %>% 
  filter(sleep %in% c('REM')) %>% 
  ggplot(aes(x=time,y = z_amplitude,colour = patient)) +
  stat_summary(fun.data = mean_cl_boot,geom = "ribbon",aes(fill = patient),alpha = 0.2) +
  stat_summary(fun = mean,geom = "line")+
  geom_vline(xintercept = 0,linetype = "dashed" )+
  geom_hline(yintercept = 0,linetype = "dashed") +
  scale_color_manual(values = colors)  + 
  scale_fill_manual(values = colors)  + 
  facet_wrap(~chan,scale = "free") +
  ylab(expression(paste("HEPs amplitude (",mu,"V)"))) +
  xlab('Time (msec)') +
  theme_cowplot(font_size = 12,line_size = 0.5, font_family = "sans-serif") +
  ggtitle("HEP during REM")


new_data %>% 
  ggplot(aes(x=time,y = z_amplitude,colour = sleep)) +
  stat_summary(fun.data = mean_cl_boot,geom = "ribbon",aes(fill = sleep),alpha = 0.2) +
  stat_summary(fun = mean,geom = "line")+
  geom_vline(xintercept = 0,linetype = "dashed" )+
  geom_hline(yintercept = 0,linetype = "dashed") +
  facet_wrap(~chan,scale = "free") +
  ylab(expression(paste("HEPs amplitude (",mu,"V)"))) +
  xlab('Time (msec)') +
  theme_cowplot(font_size = 12,line_size = 0.5, font_family = "sans-serif") +
  ggtitle("HEP as a function of sleep stage")


# check conversion
new_data %>% 
  filter(patient == 'RBD') %>% 
  filter(!converted %in% c('NA','unknown')) %>% 
  ggplot(aes(x=time,y = z_amplitude,colour = converted)) +
  stat_summary(fun.data = mean_cl_boot,geom = "ribbon",aes(fill = converted),alpha = 0.2) +
  stat_summary(fun = mean,geom = "line")+
  geom_vline(xintercept = 0,linetype = "dashed" )+
  geom_hline(yintercept = 0,linetype = "dashed") +
  facet_wrap(~chan,scale = "free") +
  ylab(expression(paste("HEPs amplitude (",mu,"V)"))) +
  xlab('Time (msec)') +
  ggtitle("HEP converted or not")


### do stats
library(permuco)
library(multidplyr)
chans = unique(new_data$chan)
times = unique(new_data$time)
allres= list()
timer <- seq(1,length(times),15)
# clust=new_cluster(2)
# clust = clust %>% cluster_library(c('dplyr','purrr','permuco'))

for (c in 1:length(times)) {
  print(paste('fitting timepoint = ',times[c],'ms',sep=''))
  if (c %in% timer & c<length(times)){
    print(paste('I re-start the clusters',sep=''))
    clust=new_cluster(2)
    clust = clust %>% cluster_library(c('dplyr','purrr','permuco'))
  }
  allres[[c]] = new_data %>%
    # dplyr::filter(nbchan == 6) %>% 
    dplyr::filter (time == times[c]) %>%
    dplyr::select(z_amplitude,patient,chan) %>% 
    group_by(chan) %>% 
    nest() %>% 
    partition(clust) %>%
    mutate(lme_out = map(data,~permuco::aovperm(z_amplitude~patient*sleep, np = 500, data = .))) %>%
    mutate(perf = map(lme_out,"table")) %>%
    mutate(pperm = map(perf,'resampled P(>F)')) %>%
    mutate(pparam = map(perf,'parametric P(>F)')) %>%
    mutate(F= map(perf,'F')) %>%
    dplyr::select(-perf,-data,-lme_out) %>%
    as.data.frame() %>%
    mutate(time =times[c])}

allres=bind_rows(allres) # concatenate the list allres
# save(allres,file='./allres_HEP_RBD_early.RData'

Fs   = as_tibble(matrix(unlist(allres$F),ncol=2,byrow = T)); names(Fs) = c('Group','NA')
ps   = as_tibble(matrix(unlist(allres$pparam),ncol=2,byrow = T)); names(ps) = c('pGroup','NA')
widestats = allres %>% select(-F,-pparam) %>% as_tibble()
widestats = cbind(widestats,Fs,ps)

longstats = widestats  %>% pivot_longer(names_to = 'effect', cols =  c(Group)) %>% select(chan,time,effect,F=value)
tmp  = widestats %>% pivot_longer(names_to = 'effect', cols =  c(pGroup))
longstats$p=tmp$value

longstats = longstats %>%
  filter(time>= -200 & time<=600) %>% 
  group_by(effect,chan) %>% 
  mutate(pfdr = p.adjust(p, method = 'fdr')) %>% 
  arrange(chan)

# search any electrodes with effects PERMU
# tmp = longstats %>% 
#   group_by(chan,effect) %>%
#   summarise(sig = any(pfdr<0.05)) %>% pivot_wider(names_from=effect,values_from=sig)
# Group_only   = tmp %>% filter(Group) %>% select(chan)

# plot p-values per each channels:
longstats %>% filter(effect == 'Group') %>%
  ggplot(aes(x=time,y=log(pfdr))) + geom_line() +
  geom_hline(yintercept = c(log(0.05),log(0.01),log(0.001)),linetype = 'dashed',alpha=0.5)+
  geom_vline(xintercept = 0,alpha=.2) + facet_wrap(~chan) +  ggtitle("Interaction")
