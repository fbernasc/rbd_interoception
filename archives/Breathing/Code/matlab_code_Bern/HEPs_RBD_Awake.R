rm(list=ls(all=TRUE))
library(lme4);
library(lmerTest) # mixed model stuff
library(afex)
library(tidyverse)
library(data.table)
library(cowplot)
library(perm)
library(broom)
library(ggiraphExtra)
library(sjPlot)
library(gridExtra)
library(viridis)
library(emmeans)
library(brms)
library(sjstats)
library(sjmisc)
library(ggeffects)
library(bayestestR)
library(bayesplot)
library(rstanarm)
library(formattable)
library(eegUtils)
library(here)
library(ggbeeswarm)


# rootdir=here("EEG","Preprocessed")
rootdir = '/media/fosco/T7/RBD/EEG/Preprocessed/'
csvlist  = list.files(path=paste(rootdir), pattern = '.*HEPs_RBD_1hz_awake.*\\.csv$', recursive = TRUE)

# import data
new_data <- purrr::map_df(paste(rootdir,csvlist,sep = '/'), function(x) {
  mydata <- read.csv(x,header = TRUE,sep = "")
  mydata %>%
    # count(Type) %>%
    mutate(ID =  strsplit(basename(x), "_")[[1]][1])
})


# housekeeping:
new_data$amplitude = as.numeric(new_data$amplitude)
new_data$age = as.numeric(new_data$age)
new_data$time = as.numeric(new_data$time)
new_data$time = as.numeric(new_data$time)
new_data$HR = as.numeric(new_data$HR)
new_data$IBI = as.numeric(new_data$IBI)
new_data$rmssd = as.numeric(new_data$rmssd)
new_data$hrv_sdsd = as.numeric(new_data$hrv_sdsd)
new_data$hrv_sdnn = as.numeric(new_data$hrv_sdnn)
new_data$hrv_sdnn = as.numeric(new_data$hrv_sdnn)
new_data$rrhrv = as.numeric(new_data$rrhrv)
new_data$pLF = as.numeric(new_data$pLF)
new_data$pHF = as.numeric(new_data$pHF)
new_data$LFHFratio = as.numeric(new_data$LFHFratio)
new_data$patient   = as.numeric(new_data$patient)

new_data$patient[new_data$patient == 1] = 'RBD'
new_data$patient[new_data$patient == 0] = 'HC'


# check number of trials:
trls = new_data %>% distinct(ID,.keep_all = TRUE) %>% select(ID,trials,patient)

# how many patients per electrodes
nbrpatchan = new_data %>% group_by(chan) %>% summarise(n=length(unique(ID)))


# new_data = new_data %>%
#   filter(!ID %in% c("06631")) %>% 
#   filter(!ID %in% c("06624","sub-AA8486","09255","34862")) 

# exclude based on ECG:
new_data = new_data %>%
  filter(!ID %in% c("36925","sub-Z 85.57","sub-dbcd3cb8-a6fa-46b9-977b-3c4d9fe23c7e")) 
  

# check ECG signals:
colors <- c('black')
new_data %>% 
  filter(chan %in% c('ECG')) %>% 
  ggplot(aes(x=time,y = amplitude)) +
  stat_summary(fun = mean,geom = "line")+
  geom_vline(xintercept = 0,linetype = "dashed" )+
  geom_hline(yintercept = 0,linetype = "dashed") +
  scale_color_manual(values = colors)  + 
  scale_fill_manual(values = colors)  + 
  facet_wrap(~ID,scale = "free") +
  ylab(expression(paste("HEPs amplitude (",mu,"V)"))) +
  xlab('Time (msec)') +
  theme_cowplot(font_size = 12,line_size = 0.5, font_family = "sans-serif")  +
  ggtitle("ECG")


colors <- c('black','red')
new_data %>% 
  filter(!trials < 200) %>% 
  filter(chan %in% c('F3','ECG')) %>% 
  ggplot(aes(x=time,y = amplitude,colour = chan)) +
  stat_summary(fun.data = mean_cl_boot,geom = "ribbon",aes(fill = chan),alpha = 0.2) +
  stat_summary(fun = mean,geom = "line")+
  geom_vline(xintercept = 0,linetype = "dashed" )+
  geom_hline(yintercept = 0,linetype = "dashed") +
  scale_color_manual(values = colors)  + 
  scale_fill_manual(values = colors)  + 
  facet_wrap(~ID,scale = "free") +
  ylab(expression(paste("HEPs amplitude (",mu,"V)"))) +
  xlab('Time (msec)') +
  theme_cowplot(font_size = 12,line_size = 0.5, font_family = "sans-serif")  +
  ggtitle("HEP awake")



colors <- c('black','red')
new_data %>% 
  filter(!trials < 200) %>% 
  filter(chan %in% c('C3','C4','F3','F4','O1','O2','ECG','REF')) %>% 
  ggplot(aes(x=time,y = amplitude,colour = patient)) +
  stat_summary(fun.data = mean_cl_boot,geom = "ribbon",aes(fill = patient),alpha = 0.2) +
  stat_summary(fun = mean,geom = "line")+
  geom_vline(xintercept = 0,linetype = "dashed" )+
  geom_hline(yintercept = 0,linetype = "dashed") +
  scale_color_manual(values = colors)  + 
  scale_fill_manual(values = colors)  + 
  facet_wrap(~chan,scale = "free") +
  ylab(expression(paste("HEPs amplitude (",mu,"V)"))) +
  xlab('Time (msec)') +
  theme_cowplot(font_size = 12,line_size = 0.5, font_family = "sans-serif")  +
  ggtitle("HEP all")
  

new_data <- new_data %>% 
  group_by(ID,chan) %>% 
  mutate(m_amp = mean(amplitude)) %>% 
  
  mutate(z_amplitude = scale(amplitude)) %>% ungroup()

new_data %>% 
  ggplot(aes(x=time,y = z_amplitude,colour = patient)) +
  stat_summary(fun.data = mean_cl_boot,geom = "ribbon",aes(fill = patient),alpha = 0.2) +
  stat_summary(fun = mean,geom = "line")+
  geom_vline(xintercept = 0,linetype = "dashed" )+
  geom_hline(yintercept = 0,linetype = "dashed") +
  scale_color_manual(values = colors)  + 
  scale_fill_manual(values = colors)  + 
  facet_wrap(~chan,scale = "free") +
  ylab(expression(paste("z-score HEPs amplitude (",mu,"V)"))) +
  xlab('Time (msec)') +
  theme_cowplot(font_size = 12,line_size = 0.5, font_family = "sans-serif") +
  ggtitle("HEP during awake")


### do stats
library(permuco)
library(multidplyr)
chans = unique(new_data$chan)
times = unique(new_data$time)
allres= list()
timer <- seq(1,length(times),15)
# clust=new_cluster(2)
# clust = clust %>% cluster_library(c('dplyr','purrr','permuco'))

for (c in 1:length(times)) {
  print(paste('fitting timepoint = ',times[c],'ms',sep=''))
  if (c %in% timer & c<length(times)){
    print(paste('I re-start the clusters',sep=''))
    clust=new_cluster(2)
    clust = clust %>% cluster_library(c('dplyr','purrr','permuco'))
  }
  allres[[c]] = new_data %>%
    # dplyr::filter(nbchan == 6) %>% 
    dplyr::filter (time == times[c]) %>%
    dplyr::select(z_amplitude,patient,chan) %>% 
    group_by(chan) %>% 
    nest() %>% 
    partition(clust) %>%
    mutate(lme_out = map(data,~permuco::aovperm(z_amplitude~patient, np = 500, data = .))) %>%
    mutate(perf = map(lme_out,"table")) %>%
    mutate(pperm = map(perf,'resampled P(>F)')) %>%
    mutate(pparam = map(perf,'parametric P(>F)')) %>%
    mutate(F= map(perf,'F')) %>%
    dplyr::select(-perf,-data,-lme_out) %>%
    as.data.frame() %>%
    mutate(time =times[c])}

allres=bind_rows(allres) # concatenate the list allres
# save(allres,file='./allres_HEP_RBD_early.RData'

Fs   = as_tibble(matrix(unlist(allres$F),ncol=2,byrow = T)); names(Fs) = c('Group','NA')
ps   = as_tibble(matrix(unlist(allres$pparam),ncol=2,byrow = T)); names(ps) = c('pGroup','NA')
widestats = allres %>% select(-F,-pparam) %>% as_tibble()
widestats = cbind(widestats,Fs,ps)

longstats = widestats  %>% pivot_longer(names_to = 'effect', cols =  c(Group)) %>% select(chan,time,effect,F=value)
tmp  = widestats %>% pivot_longer(names_to = 'effect', cols =  c(pGroup))
longstats$p=tmp$value

longstats = longstats %>%
  filter(time>= -200 & time<=600) %>% 
  group_by(effect,chan) %>% 
  mutate(pfdr = p.adjust(p, method = 'fdr')) %>% 
  arrange(chan)

# search any electrodes with effects PERMU
# tmp = longstats %>% 
#   group_by(chan,effect) %>%
#   summarise(sig = any(pfdr<0.05)) %>% pivot_wider(names_from=effect,values_from=sig)
# Group_only   = tmp %>% filter(Group) %>% select(chan)

# plot p-values per each channels:
longstats %>% filter(effect == 'Group') %>%
  ggplot(aes(x=time,y=log(pfdr))) + geom_line() +
  geom_hline(yintercept = c(log(0.05),log(0.01),log(0.001)),linetype = 'dashed',alpha=0.5)+
  geom_vline(xintercept = 0,alpha=.2) + facet_wrap(~chan) +  ggtitle("Interaction")
