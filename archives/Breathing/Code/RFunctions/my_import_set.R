my_import_set = function (file_name, df_out = FALSE) 
{
  temp_dat <- R.matlab::readMat(file_name)
  var_names <- dimnames(temp_dat$EEG)[[1]]
  n_chans <- temp_dat$EEG[[which(var_names == "nbchan")]]
  n_trials <- temp_dat$EEG[[which(var_names == "trials")]]
  times <- temp_dat$EEG[[which(var_names == "times")]]
  chan_info <- temp_dat$EEG[[which(var_names == "chanlocs")]]
  col_names <- dimnames(chan_info)[1]
  size_chans <- dim(chan_info)
  chan_info <- lapply(chan_info, function(x) ifelse(purrr::is_empty(x), 
                                                    NA, x))
  dim(chan_info) <- size_chans
  dimnames(chan_info) <- col_names
  chan_info <- parse_chaninfo(chan_info)
  
  fdt_file <- paste0(tools::file_path_sans_ext(file_name), ".fdt")
  fdt_file <- file(fdt_file, "rb")
  signals <- readBin(fdt_file, "double", n = n_chans * 
                       n_trials * length(times), size = 4, endian = "little")
  close(fdt_file)
  dim(signals) <- c(n_chans, length(times) * max(n_trials, 1))
  times <- rep(times, max(n_trials, 1))
  
  continuous <- FALSE
  
  signals <- data.frame(cbind(t(signals), times))
  srate <- c(temp_dat$EEG[[which(var_names == "srate")]])
  names(signals) <- c(unique(chan_info$electrode), "time")
  signals <- dplyr::group_by(signals, time)
  signals <- dplyr::mutate(signals, epoch = 1:dplyr::n())
  signals <- dplyr::ungroup(signals)
  

  
  event_info <- temp_dat$EEG[[which(var_names == "epoch")]]
  sdt = unlist(event_info %>% extract('sdt'))
  # type = unlist(event_info %>% extract('type'))
  
  event_table <- tibble::as_tibble(t(matrix(as.numeric(event_info), 
                                            nrow = dim(event_info)[1], ncol = dim(event_info)[3])))
  names(event_table) <- unlist(dimnames(event_info)[1])
  event_table$sdt=sdt
  # event_table$type=type
  
  
  # event_table$event_time <- (event_table$latency - 1)/srate
  # event_table <- event_table[c("latency", "event_time", "type", 
  #                              "epoch")]
  # event_table <- dplyr::rename(event_table, event_type = "type", 
  #                              event_onset = "latency")
  # event_table$time <- NA
  # if (df_out) {
    return(list(signals,event_table,chan_info))
  # }
  # else {
  #   signals$time <- signals$time/1000
  #   timings <- tibble::tibble(time = signals$time, epoch = signals$epoch, 
  #                             sample = 1:length(signals$time))
  #   event_table$time <- timings[which(timings$sample %in% 
  #                                       event_table$event_onset, arr.ind = TRUE), ]$time
  #   out_data <- eeg_data(signals[, 1:n_chans], srate = srate, 
  #                        timings = timings, chan_info = chan_info, events = event_table)
  #   if (!continuous) {
  #     class(out_data) <- c("eeg_epochs", "eeg_data")
  #   }
  #   out_data
  # }
}
