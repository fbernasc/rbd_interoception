# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

# import os
# import numpy as np
# import matplotlib.pyplot as plt
# import mne
# import yasa
import os
import mne
import yasa
import pandas as pd
import lightgbm as lgb


# look for files in sub folder
folder_path = "/media/fosco/T7/RBD/EEG/Preprocessed/"
extension = "_mne.edf"

# Get a list of all files in the folder
import glob
files = [f for f in glob.glob(folder_path + "**/*_mne.edf", recursive=True)]


# do sleep stating
for edf_file in files:
    
    # Read each EDF file using mne.io.read_raw_edf
    # raw = mne.io.read_raw_eeglab(os.path.join(root_folder, file_path))
    raw = mne.io.read_raw_edf(edf_file, preload=True)
    # raw.resample(200)
    
   
    raw.filter(0.3, 45)
    
    raw.set_eeg_reference(ref_channels=["M1","M2"])
    
    
    # Check if channel C3 or C4 is present in the raw data
    if 'C3' in raw.ch_names:
        sls = yasa.SleepStaging(raw, eeg_name='C3')
    elif 'C4' in raw.ch_names:
        sls = yasa.SleepStaging(raw, eeg_name='C4')
    else:
        raise ValueError("Neither C3 nor C4 is present in the raw data.")
        
    hypno_pred = sls.predict()
    # hypno_pred = yasa.hypno_str_to_int(hypno_pred)  # Convert "W" to 0, "N1" to 1, etc
    hypno = yasa.hypno_str_to_int(hypno_pred)
    hypno_up = yasa.hypno_upsample_to_data(hypno, sf_hypno=1/30, data=raw)
    
    output_file = os.path.splitext(edf_file)[0] + '_sleep_staging.csv'
    
    
    # data = raw.get_data(units="uV")
    # sf = raw.info['sfreq']
    # chan = raw.ch_names
    # yasa.plot_spectrogram(data[chan.index("C4")], sf, hypno_up);
      
    with open(output_file, 'w') as f:
        f.write('index,stage\n')
        for i, stage in enumerate(hypno_up):
            f.write(f'{i},{stage}\n')




edf_file = files[0]
# raw.drop_channels(['EEG O1', 'EEG O2','EKG'])
# chan = raw.ch_names
# print(chan)
# print(raw.info['sfreq'])
# raw.resample(100)

# data = raw.get_data(units="uV")

# !pip install lightgbm

# import yasa
# import lightgbm as lgb
# sls = yasa.SleepStaging(raw, eeg_name='C3')
# hypno_pred = sls.predict()  # Predict the sleep stages
# hypno_pred = yasa.hypno_str_to_int(hypno_pred)  # Convert "W" to 0, "N1" to 1, etc
# yasa.plot_hypnogram(hypno_pred);  # Plot
