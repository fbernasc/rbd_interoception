% function m01_preprocessing_vb_new(whichsubject)

clear all
close all
addpath(genpath('./matlab_code_Respiration/'));
addpath(genpath('./pre-processing/versionB/Respiration/'));
addpath(genpath('./matlab_code_Respiration/Functions/'));
addpath('./EEG/');
addpath('/home/fosco/toolboxes/eeglab2023.0')
addpath(genpath('./toolboxes/eeglab2023.0/plugins/cleanline-master/'))
eeglab nogui
addpath('/home/fosco/toolboxes/fieldtrip-master')
ft_defaults

reref = 1; % re-reference data?

whichsubject = [29:37];

% which subjects you want to process?
[who_idx,code_patient,patient,RBD,PSG_sess1,PSG_sess2,Gr,match,newID,age,gender,center,converted] = get_subjects_RBD(whichsubject);


for isub = 1:length(who_idx)

    % get cfgs
    cfg = get_cfg_RBD(code_patient{isub});
    EEG = [];

    % ---------------------------
    % Import parameters & path(s)
    % ---------------------------
    % Write a status message to the command line.
    fprintf('\n Now importing subject %s, (number %d of %d to process).\n\n',code_patient{isub},isub,length(who_idx));
    pause(1)

    if PSG_sess2(isub) == 1 % exlude the second session of patients that have 2

    else

        %-------------
        % Import data
        %-------------
        EEG = pop_loadset('filename',[code_patient{isub} '_import_w_physio.set'],'filepath',cfg.dir_eeg);

        % remove physio data:
        chanRespAbdomen  = squeeze(EEG.data(strcmp({EEG.chanlocs.labels},'Abdomen'),:,:));
        chanRespThorax   = squeeze(EEG.data(strcmp({EEG.chanlocs.labels},'Thorax'),:,:));
        chanResp_flow    = squeeze(EEG.data(strcmp({EEG.chanlocs.labels},'Flow_DR'),:,:));
        chansleep        = squeeze(EEG.data(strcmp({EEG.chanlocs.labels},'Sleep'),:,:));
        chanecg          = squeeze(EEG.data(strcmp({EEG.chanlocs.labels},'ECG'),:,:));

        % add chanlocs:
        EEG = pop_chanedit(EEG, 'lookup','/home/fosco/toolboxes/eeglab2022.0/plugins/dipfit/standard_BEM/elec/standard_1005.elc');
        EEG = eeg_checkset(EEG);

        EEG = pop_select(EEG,'nochannel',{'Abdomen' 'Thorax' 'Flow_DR' 'Sleep' 'ECG'});

        %-------------
        % filter data:
        %-------------
        EEG = pop_eegfiltnew(EEG,'locutoff',0.5,'plotfreqz',0);
        % EEG = pop_eegfiltnew(EEG,'hicutoff',40,'plotfreqz',0);

        eegchans  = {'C3','C4','F3','F4','O1','O2'};
        chansbin  = ismember({EEG.chanlocs.labels},eegchans);
        chans     = find(chansbin);
        EEG       = pop_cleanline(EEG, 'bandwidth',2,'chanlist',[chans] ,'computepower',1,...
                    'linefreqs',50,'newversion',0,'normSpectrum',0,'p',0.01,'pad',2,'plotfigures',0,...
                    'scanforlines',0,'sigtype','Channels','taperbandwidth',2,'tau',100,'verb',1,'winsize',4,'winstep',1);

       
        %------------
        % Re-ref data
        %------------
        if reref  == 1
            refchans  = find(ismember({EEG.chanlocs.labels},{'M1','M2'}));
            EEG       = pop_reref(EEG,refchans);
            fprintf('\n\n Rereferencing data \n\n');

            % EEG.data(strcmp({EEG.chanlocs.labels},'F3'),:) = EEG.data(strcmp({EEG.chanlocs.labels},'F3'),:)- EEG.data(strcmp({EEG.chanlocs.labels},'M2'),:);
            % EEG.data(strcmp({EEG.chanlocs.labels},'C3'),:) = EEG.data(strcmp({EEG.chanlocs.labels},'C3'),:)- EEG.data(strcmp({EEG.chanlocs.labels},'M2'),:);
            % EEG.data(strcmp({EEG.chanlocs.labels},'O1'),:) = EEG.data(strcmp({EEG.chanlocs.labels},'O1'),:)- EEG.data(strcmp({EEG.chanlocs.labels},'M2'),:);
            % 
            % EEG.data(strcmp({EEG.chanlocs.labels},'F4'),:) = EEG.data(strcmp({EEG.chanlocs.labels},'F4'),:) - EEG.data(strcmp({EEG.chanlocs.labels},'M1'),:);
            % EEG.data(strcmp({EEG.chanlocs.labels},'C4'),:) = EEG.data(strcmp({EEG.chanlocs.labels},'C4'),:) - EEG.data(strcmp({EEG.chanlocs.labels},'M1'),:);
            % EEG.data(strcmp({EEG.chanlocs.labels},'O2'),:) = EEG.data(strcmp({EEG.chanlocs.labels},'O2'),:) - EEG.data(strcmp({EEG.chanlocs.labels},'M1'),:);

        end


        % add physio back to data:
        EEG.chanlocs(EEG.nbchan+1).labels = 'ECG';
        EEG.data(EEG.nbchan+1,:,:) = chanecg;
        EEG = eeg_checkset(EEG);

        EEG.chanlocs(EEG.nbchan+1).labels = 'Abdomen';
        EEG.data(EEG.nbchan+1,:,:) = chanRespAbdomen;
        EEG = eeg_checkset(EEG);

        EEG.chanlocs(EEG.nbchan+1).labels = 'Thorax';
        EEG.data(EEG.nbchan+1,:,:) = chanRespThorax;
        EEG = eeg_checkset(EEG);

        EEG.chanlocs(EEG.nbchan+1).labels = 'Flow_DR';
        EEG.data(EEG.nbchan+1,:,:) = chanResp_flow;
        EEG = eeg_checkset(EEG);

        EEG.chanlocs(EEG.nbchan+1).labels = 'Sleep';
        EEG.data(EEG.nbchan+1,:,:) = chansleep;
        EEG = eeg_checkset(EEG);


        % remove eye-blink:
        cfg1            = [];
        cfg1.continuous = 'yes';

        % channel selection, cutoff and padding
        cfg1.artfctdef.zvalue.channel     = {'EOG'};
        cfg1.artfctdef.zvalue.cutoff      = 5;
        cfg1.artfctdef.zvalue.trlpadding  = 0;
        cfg1.artfctdef.zvalue.artpadding  = 0.1;
        cfg1.artfctdef.zvalue.fltpadding  = 0;

        % algorithmic parameters
        cfg1.artfctdef.zvalue.bpfilter   = 'yes';
        cfg1.artfctdef.zvalue.bpfilttype = 'but';
        cfg1.artfctdef.zvalue.bpfreq     = [2 15];
        cfg1.artfctdef.zvalue.bpfiltord  = 4;
        cfg1.artfctdef.zvalue.hilbert    = 'yes';

        % feedback
        cfg1.artfctdef.zvalue.interactive = 'no';
        data_continuous = eeglab2fieldtrip(EEG,'preprocessing');
        [cfg1, artifact_eog] = ft_artifact_zvalue(cfg1,data_continuous);


        % remove data where artifacts where observed:
        [EEGtest,com] = pop_select(EEG,'rmpoint',artifact_eog);
        EEG = eegh(com, EEG);

        pop_saveset(EEG,[code_patient{isub}  '_import_respiration.set'] , cfg.dir_eeg);

        %--------------------
        % Remove bad channels
        %--------------------
        % find cortical chans:
        eegchans  = [];
        chansbin  = [];
        % eegchans  = {'C3','C4','F3','F4','F7','F8','Fp1','Fp2','Fpz','Fz','O1','O2','P3','P4','Pz','T3','T4','T5','T6','Cz'};
        eegchans  = {'C3','C4','F3','F4','O1','O2'};
        chansbin  = ismember({EEG.chanlocs.labels},eegchans);
        chans     = find(chansbin);

        % store the nbr of electrodes in the structure:
        EEG.chaneeg = sum(chansbin);

        fprintf(['\n\n [ stage 2 ] epoch data at R-peak \n\n\n\n']);

        [EEG, ~, com] = pop_epoch(EEG,{'r-peak'},[cfg.epoch_ekg_tmin cfg.epoch_ekg_tmax], ...
            'newname', 'BDF file epochs', 'epochinfo', 'yes');
        EEG = eeg_checkset(EEG);
        EEG = eegh(com, EEG);

        EEGtmp = [];
        EEGtmp = pop_select(EEG,'channel',chans);
        EEGtmp = eeg_checkset( EEGtmp );

        % % save chanlocs for later interpol:
        % name = ['/home/fosco/Git_epfl/hep_rbd/Code/matlab_code/pre-processing/electrode_n' num2str(EEGtmp.nbchan) '.mat'];
        % chanlocs = EEGtmp.chanlocs;
        % save(name,'chanlocs')

        data = eeglab2fieldtrip(EEGtmp,'preprocessing');
        cfg1        = [];
        cfg1.method = 'summary';
        [dummy,badsegment] = ft_rejectvisual(cfg1, data);

        % remove bad electrode:
        differences1 = setdiff(data.label,dummy.label);
        if ~isempty(differences1)
            fprintf('\n\n Removing: %s \n\n',string(differences1));
            [EEG, com] = pop_select(EEG,'nochannel',differences1);
            EEG = eegh(com, EEG);
        end

        % % remove bad trials:
        % if ~isempty(badsegment)
        %     fprintf('\n\n Removing: %s \n\n',string(badsegment));
        %     [EEG, com] = pop_select(EEG,'rmtrial',badsegment);
        %     EEG = eegh(com, EEG);
        % end

        EEG = eeg_checkset( EEG );
        clear data dummy badsegment

        % ----------
        % Save data.
        % ----------
        [EEG, com] = pop_editset(EEG, 'setname', [code_patient{isub} '_import_w_physio.set']);
        pop_saveset(EEG,[code_patient{isub}  '_import_w_physio_1hz_MRef3.set'] , cfg.dir_eeg);
        EEG = eegh(com, EEG);
        clear EEG

    end
end
fprintf('Done.\n')
