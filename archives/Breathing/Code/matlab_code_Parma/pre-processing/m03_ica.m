clear all
close all
addpath(genpath('./matlab_code_Respiration/'));
addpath(genpath('./pre-processing/versionB/Respiration/'));
addpath(genpath('./matlab_code_Respiration/Functions/'));
addpath('./EEG/');
addpath('/home/fosco/toolboxes/eeglab2023.0')
eeglab nogui
addpath('/home/fosco/toolboxes/fieldtrip-master')
ft_defaults

whichsubject  = [1:38];
whichdata     = '_import_w_physio_1hz.set'; % _import_w_physio_reref
whichdataout  = '_ica_1hz.set';
whichdataout2 = '_ica_1hz.set';

% which subjects you want to process?
[who_idx,code_patient,patient,RBD,PSG_sess1,PSG_sess2,Gr,match,newID,age,gender,center,converted] = get_subjects_RBD(whichsubject);


parfor isub = 1:length(who_idx)

    % Load CFG file. I know, eval is evil, but this way we allow the user
    % to give the CFG function any arbitrary name, as defined in the EP
    % struct.
    % get cfgs
    cfg = get_cfg_RBD(code_patient{isub});
    EEG = [];

    if PSG_sess2(isub) == 1 % exlude the second session of patients that have 2

    else

        if exist([cfg.dir_eeg '/' cfg.subject_name whichdata],'file')

            fprintf('\nNow importing subject %s, (number %d of %d to process).\n\n',code_patient{isub},isub,length(who_idx));


            % Load data set.
            EEG = pop_loadset('filename', [cfg.subject_name whichdata],'filepath', cfg.dir_eeg, 'loadmode', 'all');
            EEG = pop_chanedit(EEG, 'lookup','/home/fosco/toolboxes/eeglab2022.0/plugins/dipfit/standard_BEM/elec/standard_1005.elc');


            % center data
            % if cfg.do_recenter
            %     fprintf('\n\n [ stage 1 ] centering the epochs \n\n');
            %     EEG = pop_rmbase(EEG,[EEG.xmin*1000 EEG.xmax*1000]);
            % end

            % select eeg chans:
            eegchans  = {'C3','C4','F3','F4','F7','F8','Fp1','Fp2','Fpz','Fz','O1','O2','P3','P4','Pz','T3','T4','T5','T6','Oz','Cz'}; % 'REF'
            chans   = [];
            chans   = find(ismember({EEG.chanlocs.labels},eegchans));

            % ECG, HEOG and VEOG are going to be a nuisance for now. Save 1hz
            % HP-filtered version for later
            EEG2 = EEG;

            whichecg = find(~cellfun(@isempty,regexp({EEG.chanlocs.labels},'EKG|ECG')));
            ecg  = squeeze(EEG2.data(whichecg,:,:));
            veog = squeeze(EEG2.data(strcmp({EEG2.chanlocs.labels},'EOG E1'),:,:));
            heog = squeeze(EEG2.data(strcmp({EEG2.chanlocs.labels},'EOG E2'),:,:));

            chanRespAbdomen  = squeeze(EEG2.data(strcmp({EEG2.chanlocs.labels},'Abdomen'),:,:));
            chanRespThorax   = squeeze(EEG2.data(strcmp({EEG2.chanlocs.labels},'Thorax'),:,:));
            chanResp_flow    = squeeze(EEG2.data(strcmp({EEG2.chanlocs.labels},'Flow_DR'),:,:));
            chansleep        = squeeze(EEG2.data(strcmp({EEG2.chanlocs.labels},'Sleep'),:,:));
            chanRef          = squeeze(EEG2.data(strcmp({EEG2.chanlocs.labels},'REF'),:,:));

            EEG.save_ecg   = ecg;
            EEG.save_heog  = heog;
            EEG.save_veog  = veog;
            EEG.save_sleep = chansleep;
            EEG.save_ref   = chanRef;
            EEG2.save_ecg  = ecg;
            EEG2.save_heog = heog;
            EEG2.save_veog = veog;
            EEG2.save_sleep = chansleep;
            EEG2.save_ref   = chanRef;

            EEG.save_chanRespAbdomen = chanRespAbdomen;
            EEG.save_chanRespThorax  = chanRespThorax;
            EEG.save_chanResp_flow   = chanResp_flow;
            EEG2.save_chanRespAbdomen = chanRespAbdomen;
            EEG2.save_chanRespThorax  = chanRespThorax;
            EEG2.save_chanResp_flow   = chanResp_flow;

            EEG  = pop_select(EEG, 'channel',{EEG.chanlocs(chans).labels});
            EEG2 = pop_select(EEG2,'channel',{EEG2.chanlocs(chans).labels});

            EEG = eeg_checkset(EEG);
            EEG2 = eeg_checkset(EEG2);

            % filter the data at 1hz - better for ica
            fprintf(['\n\n [ stage 3 ] Filter data at 1hz for ICA \n\n\n\n']);
            % EEG2 = pop_eegfiltnew(EEG2,'locutoff',1,'plotfreqz',0);



            %--------------
            % ICA with all
            %--------------
            if EEG2.nbchan > 1
                % % The following lines will first check that your data is of sufficient rank so as not to try to extract too many independent components. The next three lines are optional, but you are encouraged to use them.
                % fprintf(['\n\n [ stage 3 ] ICA on ' num2str(EEG.nbchan)  ' electrodes \n\n'])
                % pause(3)
                %
                datsvd = svd(reshape(EEG2.data,[EEG2.nbchan,EEG2.trials*EEG2.pnts]));
                % % figure;plot(datsvd);
                n = sum(datsvd > 100);
                n = n-1; % beacause ECG removed for ICA but taking into account for RANK calcution so far
                % [EEG2 com] = pop_runica(EEG2, 'icatype','binica', 'extended', 1, 'chanind',1:EEG2.nbchan, 'pca',n);

                % Run ICA.
                EEG2.data = double(EEG2.data);
                EEG.ICArank = EEG2.nbchan-2;

                % Set the rng to a fixed value so that everybody always gets the
                % same results. The exact value does not matter, 3 is a lucky
                % number.
                rng(3);

                [EEG2, com] = pop_runica(EEG2, 'icatype', 'binica', ...
                    'extended', 1, ...
                    'chanind',1:EEG2.nbchan, ...
                    'pca',EEG.ICArank);

                EEG.data = single(EEG.data);
            end

            %copy weight & sphere to original data
            EEG.icaweights  = EEG2.icaweights;
            EEG.icasphere   = EEG2.icasphere;
            EEG.icawinv     = EEG2.icawinv;
            EEG.icachansind = EEG2.icachansind;
            EEG.icaact      = EEG2.icaact;
            EEG = eeg_checkset(EEG);

            % place ECG back into the data:
            if ~isempty(EEG.save_ecg)
                EEG.chanlocs(EEG.nbchan+1).labels = 'ECG';
                EEG.data(EEG.nbchan+1,:,:) = squeeze(EEG.save_ecg);
                EEG = eeg_checkset(EEG);


                EEG2.chanlocs(EEG2.nbchan+1).labels = 'ECG';
                EEG2.data(EEG2.nbchan+1,:,:) = squeeze(EEG2.save_ecg);
                EEG2 = eeg_checkset(EEG2);
            end

            if ~isempty(EEG.save_heog)
                EEG.chanlocs(EEG.nbchan+1).labels = 'HEOG';
                EEG.data(EEG.nbchan+1,:,:)  = squeeze(EEG.save_heog);
                EEG = eeg_checkset(EEG);

                EEG2.chanlocs(EEG2.nbchan+1).labels = 'HEOG';
                EEG2.data(EEG2.nbchan+1,:,:) = squeeze(EEG2.save_heog);
                EEG2 = eeg_checkset(EEG2);
            end

            if ~isempty(EEG.save_veog)
                EEG.chanlocs(EEG.nbchan+1).labels = 'VEOG';
                EEG.data(EEG.nbchan+1,:,:) = squeeze(EEG.save_veog);
                EEG = eeg_checkset(EEG);

                EEG2.chanlocs(EEG2.nbchan+1).labels = 'VEOG';
                EEG2.data(EEG2.nbchan+1,:,:) = squeeze(EEG2.save_veog);
                EEG2 = eeg_checkset(EEG2);
            end

            if ~isempty(EEG.save_chanRespAbdomen)
                EEG.chanlocs(EEG.nbchan+1).labels = 'Abdomen';
                EEG.data(EEG.nbchan+1,:,:) = squeeze(EEG.save_chanRespAbdomen);
                EEG = eeg_checkset(EEG);

                EEG2.chanlocs(EEG2.nbchan+1).labels = 'Abdomen';
                EEG2.data(EEG2.nbchan+1,:,:) = squeeze(EEG2.save_chanRespAbdomen);
                EEG2 = eeg_checkset(EEG2);
            end

            if ~isempty(EEG.save_chanRespThorax)
                EEG.chanlocs(EEG.nbchan+1).labels = 'Thorax';
                EEG.data(EEG.nbchan+1,:,:) = squeeze(EEG.save_chanRespThorax);
                EEG = eeg_checkset(EEG);

                EEG2.chanlocs(EEG2.nbchan+1).labels = 'Thorax';
                EEG2.data(EEG2.nbchan+1,:,:) = squeeze(EEG2.save_chanRespThorax);
                EEG2 = eeg_checkset(EEG2);
            end

            if ~isempty(EEG.save_chanResp_flow)
                EEG.chanlocs(EEG.nbchan+1).labels = 'Flow_DR';
                EEG.data(EEG.nbchan+1,:,:) = squeeze(EEG.save_veog);
                EEG = eeg_checkset(EEG);

                EEG2.chanlocs(EEG2.nbchan+1).labels = 'Flow_DR';
                EEG2.data(EEG2.nbchan+1,:,:) = squeeze(EEG2.save_chanResp_flow);
                EEG2 = eeg_checkset(EEG2);
            end

            if ~isempty(EEG.save_sleep)
                EEG.chanlocs(EEG.nbchan+1).labels = 'Sleep';
                EEG.data(EEG.nbchan+1,:,:) = squeeze(EEG.save_sleep);
                EEG = eeg_checkset(EEG);

                EEG2.chanlocs(EEG2.nbchan+1).labels = 'Sleep';
                EEG2.data(EEG2.nbchan+1,:,:) = squeeze(EEG2.save_sleep);
                EEG2 = eeg_checkset(EEG2);
            end

            if ~isempty(EEG.save_ref)
                EEG.chanlocs(EEG.nbchan+1).labels = 'REF';
                EEG.data(EEG.nbchan+1,:,:) = squeeze(EEG.save_ref);
                EEG = eeg_checkset(EEG);

                EEG2.chanlocs(EEG2.nbchan+1).labels = 'REF';
                EEG2.data(EEG2.nbchan+1,:,:) = squeeze(EEG2.save_ref);
                EEG2 = eeg_checkset(EEG2);
            end


            % free-up some memory and remove pre-saved channels
            EEG.save_chanResp_flow    = [];
            EEG.save_chanRespThorax   = [];
            EEG2.save_chanRespAbdomen = [];
            EEG2.save_ref             = [];
            EEG2.save_veog            = [];
            EEG.save_heog             = [];
            EEG.save_ecg              = [];
            EEG.save_ref              = [];

            EEG  = eeg_checkset(EEG);
            EEG2 = eeg_checkset(EEG2);

            % Create output directory if necessary.
            if ~isdir(cfg.dir_eeg)
                system(sprintf('mkdir -p %s',cfg.dir_eeg));
            end

            EEG  = pop_saveset(EEG,[cfg.subject_name  whichdataout], cfg.dir_eeg);
            % EEG2 = pop_saveset(EEG2,[cfg.subject_name whichdataout2], cfg.dir_eeg);

        end

    end
end
