function [EEG] = findRespiration(EEG,plotfig)

% check if figure is same as for data in fieldtrip to ensure no
% changes in sampling etc when going from FT to EEGLAB
% Select interval from 4 seconds to 10 seconds
start_time = 500;  % Start time in seconds
end_time   = 650;  % End time in seconds

% Create logical index for the selected interval
positions1 = find(EEG.times == start_time);
positions2 = find(EEG.times == end_time);


chanRespAbdomen  = find(strcmp({EEG.chanlocs.labels},'Abdomen'));
chanRespThorax   = find(strcmp({EEG.chanlocs.labels},'Thorax'));
chanResp_flow    = find(strcmp({EEG.chanlocs.labels},'Flow_DR'));


% figure;
% subplot(2,2,1)
% plot(EEG.times(positions1:positions2),EEG.data(chanRespAbdomen,positions1:positions2)); hold on;
% title('Abdomen')
% subplot(2,2,2)
% plot(EEG.times(positions1:positions2),EEG.data(chanRespThorax,positions1:positions2)); hold on;
% title('Thorax')
% subplot(2,2,3)
% plot(EEG.times(positions1:positions2),EEG.data(chanResp_flow,positions1:positions2)); hold on;
% title('Flow DR')
%keyboard;

% get resipratio features:
fprintf('\n\n [stage 1.a] get respiration features from Belt \n\n')
dataType = 'humanBB';
bmObjAb  = [];
bmObjAb  = breathmetrics(EEG.data(chanRespAbdomen,:),EEG.srate,dataType);
bmObjAb.estimateAllFeatures();
% fig = bmObjAb.plotFeatures();

EEG.inhaleOnsetsAbdo = bmObjAb.inhaleOnsets;
EEG.exhaleOnsetsAbdo = bmObjAb.exhaleOnsets;
EEG.Average_Inter_Breath_Interval = (mean(diff([EEG.inhaleOnsetsAbdo])))/EEG.srate; % interval in seconds
EEG.IBIsd            = (std(diff([EEG.inhaleOnsetsAbdo])))/EEG.srate;
EEG.breathing_rate   = 60./[EEG.Average_Inter_Breath_Interval];

% get info also from thorax belt
bmObjTrx = [];
bmObjTrx = breathmetrics(EEG.data(chanRespThorax,:),EEG.srate,dataType);
bmObjTrx.estimateAllFeatures();
% fig = bmObjTrx.plotFeatures();

EEG.inhaleOnsetsTrx = bmObjTrx.inhaleOnsets;
EEG.exhaleOnsetsTrx = bmObjTrx.exhaleOnsets;

fprintf('\n\n [stage 1.b] get breathing phase interpolation from breathing belt \n\n')

chanresp = chanRespAbdomen; % chanRespThorax or chanResp_flow or chanRespAbdomen
indices = sort([EEG.inhaleOnsetsAbdo, EEG.exhaleOnsetsAbdo]);
interp_respiration = [];
for j = 1:2:length(indices)-2
    if indices(j) < indices(j+1) && indices(j+2) <= indices(end)
        interp_respiration(indices(j):indices(j+1)) = linspace(-pi, 0, indices(j+1)-indices(j)+1);
        interp_respiration(indices(j+1):indices(j+2)) = linspace(0, pi, indices(j+2)-indices(j+1)+1);
    end
end

if plotfig == 1
    figure; hold all
    subplot(2,2,1)
    plot(EEG.times(positions1:positions2),EEG.data(chanresp,positions1:positions2)); hold on;
    plot(EEG.times(positions1:positions2),interp_respiration(:,positions1:positions2)); hold on;
    title('Breathing belt')
    legend('Raw Respiration Belt', 'Respiration phase Belt');
end


% filter respiratory signal:
fprintf('\n\n [stage 2.a] get respiration features from nasal flow \n\n')

dataType  = 'humanAirflow';
bmObjNose = [];
bmObjNose = breathmetrics(EEG.data(chanResp_flow,:),EEG.srate,dataType);
bmObjNose.estimateAllFeatures();

EEG.inhaleOnsetsNose   = bmObjNose.inhaleOnsets;
EEG.exhaleOnsetsNose   = bmObjNose.exhaleOnsets;
EEG.bmObjNose          = bmObjNose;
% fig = bmObjNose.plotFeatures();

fprintf('\n\n [stage 2.b] get breathing phase interpolation from nasal flow \n\n')
indices = sort([EEG.inhaleOnsetsNose,EEG.exhaleOnsetsNose]);
interp_respiration = [];
for j = 1:2:length(indices)-2
    if indices(j) < indices(j+1) && indices(j+2) <= indices(end)
        interp_respiration(indices(j):indices(j+1)) = linspace(-pi, 0, indices(j+1)-indices(j)+1);
        interp_respiration(indices(j+1):indices(j+2)) = linspace(0, pi, indices(j+2)-indices(j+1)+1);
    end
end

% store data in EEG
EEG.respphaseNose = interp_respiration;

if plotfig == 1
    % figure
    subplot(2,2,2)
    plot(EEG.times(positions1:positions2),EEG.data(chanresp,positions1:positions2)); hold on;
    plot(EEG.times(positions1:positions2),interp_respiration(:,positions1:positions2)); hold on;
    title('Nasal flow');
    legend('Raw Nasal Respiration', 'Interpolated Respiration');
end


fprintf('\n\n [stage 3.a] get breathing phase interpolation respiration belt \n\n')
EEGresp  = [];
chanresp = chanRespAbdomen; % chanRespThorax or chanResp_flow or chanRespAbdomen
EEGresp  = pop_select(EEG, 'channel',chanresp);
% EEGresp.data = zscore(EEGresp.data);
% EEGresp.data = sgolayfilt(double(EEGresp.data),9,21);
EEGresp = pop_eegfiltnew(EEGresp, 'locutoff',0.2,'hicutoff',0.8,'plotfreqz',0);

% calculate respiratory phase
resp_phase = angle(hilbert(EEGresp.data)); %phase of the respiration signal
EEG.resp_phase = resp_phase;

% store data in EEG
EEG.respphaseHilbert = resp_phase;

if plotfig == 1
    subplot(2,2,3)
    % plot(EEG.times(positions1:positions2),EEG.data(chanresp,positions1:positions2)); hold on;
    plot(EEG.times(positions1:positions2),EEGresp.data(:,positions1:positions2)); hold on;
    plot(EEG.times(positions1:positions2),resp_phase(:,positions1:positions2)); hold on;
    title('Hilbert on Breathing Belt')
end


fprintf('\n\n [stage 4.a] get breathing phase interpolation respiration belt with sliding window \n\n')

% Parameters for sliding window
win_secs = 60;  % Adjust the window size in secs
windowSize = round(win_secs*EEG.srate); % convert to timepoints
numDataPoints = numel(EEGresp.data);
numWindows = floor(numDataPoints / windowSize);  % Number of non-overlapping windows

EEGresp  = [];
chanresp = chanRespAbdomen; % chanRespThorax or chanResp_flow or chanRespAbdomen
EEGresp  = pop_select(EEG, 'channel',chanresp);
% EEGresp.data = zscore(EEGresp.data);

peakIndices = [];
peakValues  = [];
troughsIndices = [];
troughsValues  = [];
localPeakIndices_adjust = [];
localPeakIndices_adjust = [];

% Find peaks within each sliding window
for i = 1:numWindows % for sliding win use: numDataPoints-windowSize+1
    window = [];
    % window = EEGresp.data(:,i:i+windowSize-1); % sliding win
    windowStart = (i-1)*windowSize + 1;
    windowEnd   = i*windowSize;
    timepoints  = ([windowStart:windowEnd]);
    window      = (EEGresp.data(:,windowStart:windowEnd));
    minProminences(i) = std(window) - (std(window)*0.2);

    % Find peaks and troughs using the updated minimum prominences
    [localPeaks,localPeakIndices]     = findpeaks(window,'MinPeakProminence', minProminences(i),'MinPeakDistance',0.6*EEG.srate);
    [localtroughs,locatroughsIndices] = findpeaks(-window,'MinPeakProminence', minProminences(i),'MinPeakDistance',0.6*EEG.srate);

    % re-encode the time to alligne to whole resp signal
    localPeakIndices_adjust = timepoints(localPeakIndices);
    peakIndices = [peakIndices localPeakIndices_adjust];
    peakValues  = [peakValues localPeaks];

    localtroughsIndices_adjust = timepoints(locatroughsIndices);
    troughsIndices = [troughsIndices localtroughsIndices_adjust];
    troughsValues  = [troughsValues localtroughs];

    % update raw respiration to zscore by window:
    EEGresp_up.data(:,windowStart:windowEnd) = window;
    EEG.peakIndices = peakIndices;
    EEG.peakValues  = peakValues;
    EEG.peakIndices = peakIndices;
    EEG.troughsIndices = troughsIndices;
    EEG.troughsValues  = troughsValues;
end

% Find peaks and troughs using the updated minimum prominences
indices = [];
indices = sort([peakIndices troughsIndices]);
interp_respiration = [];
for j = 1:2:length(indices)-2
    if indices(j) < indices(j+1) && indices(j+2) <= indices(end)
        interp_respiration(indices(j):indices(j+1))   = linspace(-pi, 0, indices(j+1)-indices(j)+1);
        interp_respiration(indices(j+1):indices(j+2)) = linspace(0, pi, indices(j+2)-indices(j+1)+1);
    end
end

% store data in EEG
EEG.respphaseFindpeak = interp_respiration;

if plotfig == 1
    subplot(2,2,4)
    % plot(EEG.times(positions1:positions2),EEG.data(chanresp,positions1:positions2)); hold on;
    plot(EEG.times(positions1:positions2),EEGresp.data(positions1:positions2)); hold on;
    % plot(EEGresp_up.data); hold on;
    % plot(peakIndices,peakValues,"o");hold on;
    % plot(troughsIndices2,troughsValues.*-1,"o");hold on;
    plot(EEG.times(positions1:positions2),interp_respiration(positions1:positions2)); hold on;
    title('Peakfind - andaptive win on normalized Breathing Belt')
end