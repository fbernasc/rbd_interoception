function [cfg] = get_cfg_RBD_Parma(code_patient)

%% Read info for this subject and get file names and dirctories.
cfg.dir_main      = '/mnt/nas/Users/Fosco/RBD/RBD_Parma';
cfg.subject_name  = code_patient;
cfg.dir_raw       = [cfg.dir_main '/EEG/Raw/'];
cfg.dir_eeg       = [cfg.dir_main '/EEG/Preprocessed/' cfg.subject_name filesep]; 
% cfg.dir_main      = 'smb://svfas5.epfl.ch/blanke-lab/Users/Fosco/Thibaut/';
% cfg.subject_name  = code_patient;
% cfg.dir_raw       = 'smb://svfas5.epfl.ch/blanke-lab/Users/Fosco/Thibaut/Raw/';
% cfg.dir_eeg       = ['smb://svfas5.epfl.ch/blanke-lab/Users/Fosco/Thibaut/Data/' cfg.subject_name filesep]; 

%% Data organization and content.
% Triggers that mark stimulus onset. These events will be used for
% epoching.
%  for epoching resting state
cfg.genfaketrigger = 1;
cfg.genfaketriggerepoch = 1;
cfg.triggernumber  = 'trigger';
cfg.epoch_tmin  = 0; %e.g. -2.000;
cfg.epoch_tmax  = 2; %e.g. 0.500;

% for loking at R-peak
cfg.genekgtrigger  = 1; % generate a trigger for the R-peak
cfg.genekgtriggerepoch = 1;
cfg.trig_target = [];   %e.g. [21:29 221:229]; 
cfg.epoch_ekg_tmin  = -0.2; %e.g. -2.000;
cfg.epoch_ekg_tmax  =  0.6; %e.g. 0.500;

% find hear-rate variability:
cfg.hrv = 1;

% Recenter data to zero:
cfg.do_recenter = 1; 

%% Parameters for data import and preprocessing.
% Indices of channels that contain data, including external electrodes, but not bipolar channels like VEOG, HEOG.
cfg.data_urchans = [5,6,10,11,12,14:17,20:23,28:29,33:37,55:58];%[1,3:15,17:50,52:63]; 
% cfg.chans_name   = {'EEG C3';'EEG C4';'EOG E1';'EOG E2';'EKG';'EEG F3';'EEG F4';'EEG F7';'EEG F8';'EEG Fp1';'EEG Fp2';'EEG Fpz';'EEG Fz';'EEG M1';'EEG M2';...
%                     'EEG O1';'EEG O2';'EEG Oz';'EEG P3';'EEG P4';'EEG T3';'EEG T4';'EEG T5';'EEG T6'};
cfg.chans_name   = {'C3';'C4';'EOG E1';'EOG E2';'EKG';'F3';'F4';'F7';'F8';'Fp1';'Fp2';'Fpz';'Fz';'M1';'M2';...
                    'O1';'O2';'Oz';'P3';'P4';'T3';'T4';'T5';'T6'};

% Indices of channels that contain data after rejecting the channels not
% selected in cfg.data_urchans. 
cfg.data_chans = 1:length(cfg.data_urchans);

% Indicate if you want to remove channels
cfg.rmchanels = 0;
cfg.rmchanum = [];

% what to select bad electrodes à la fieldtrip?
cfg.detectbadchanels = 1;

% Use these channels for computing bipolar HEOG and VEOG channel.
cfg.veog_chans = []; % if empty do nothing
cfg.heog_chans = [];

% Channel location file. If you use your own custom file, you have to
% provide the full path and filename.
%cfg.chanlocfile = '/home/fosco/toolboxes/eeglab2019_0/sample_locs/Standard-10-20-Cap19.ced';%standard-10-5-cap385.elp';

%% Reference
cfg.do_import_ref       = 0;
cfg.ImportReference     = 0;
cfg.do_preproc_reref    = 0;  
cfg.preproc_reference   = 257; % (31=Pz@Biosemi,32=Pz@CustomM43Easycap) AFTER import
cfg.do_reref_before_ica = 1;  
cfg.do_reref_after_ica  = 0;
cfg.preproc_reference   = [];
cfg.postproc_reference  = [];  % empty = average reference

%% Do you want to have a new sampling rate?
cfg.do_resampling     = 0;
cfg.new_sampling_rate = 512;

%% Interpolate missing channels after ICA:
cfg.interpolMissingChan = 1;
% cfg.chanlocsBE4interpol = '/home/sv/Matlabtoolboxes/Analysis_Data_Scripts/22q11/Code/chanlocsBE4interpol.mat';
% cfg.chanlocsBE4interpol_noRefChan = '/home/sv/Matlabtoolboxes/Analysis_Data_Scripts/22q11/Code/chanlocsBE4interpol_noRefChan.mat';

%% Filter parameters
% Do you want to high-pass filter the data?
cfg.do_hp_filter    = 1;
cfg.hp_filter_type  = 'hamming'; % or 'butterworth' - not recommended or hamming or kaiser
cfg.hp_filter_limit = 1; 
cfg.hp_filter_tbandwidth = 0.2;
cfg.hp_filter_pbripple = 0.01;

% Do you want to low-pass filter the data?
cfg.do_lp_filter    = 1;
cfg.lp_filter_type  = 'hamming'; % or 'blackman'
cfg.lp_filter_limit = 48; 
cfg.lp_filter_tbandwidth = 5;

% Do you want to bandpass both at same time:
cfg.bandpass = 0;
cfg.bp_filter_type = 'filtnew'; %% 'basicfilter%%%
                                % firwsord (band pass is done in once--not recomanded)
                                % basicfilter (which is a combination between pop_basicfilter & pop_newfilt)
% Do you want to use a notch filter? Note that in most cases Cleanline
% should be sufficient.
cfg.do_notch_filter    = 0;
cfg.notch_filter_lower = 49;
cfg.notch_filter_upper = 51;
cfg.notch_filter_order = []; % try this before with the gui, and change this! as it is related to the EEG.srate

cfg.do_ICA_hp_filter    = 0;
cfg.hp_ICA_filter_type  = 'hamming';
cfg.hp_ICA_filter_limit = 1;

% Do you want to use cleanline to remove 50Hz noise?
cfg.do_cleanline = 0; % avoid if do beamformer

% Do hp before ICA?
cfg.do_ICA_hp_filter = 0;
cfg.hp_ICA_filter_type = 'butterworth';
cfg.hp_ICA_filter_limit = 1;
 

%% Artifact detection:
% Visual inspection of the data
cfg.do_visual_inspection_preICA  = 0;
cfg.do_visual_inspection_postICA = 0;

% parameters for remove remove artifact channels & epochs
cfg.detectbadchanels = 1;
cfg.rejectbadchanels = 0;
cfg.rejectbadepochs  = 1;
cfg.rejectionmetric  = 'var'; % 'var' etc. check reject_chan function.
cfg.methodthreshold  = 'mean'; % mean or median
cfg.rejectionthreshold = 3; % value to multiply with iqr (e.g. threshold is median+2*iqr)

% Do you want to reject trials based on amplitude criterion? (automatic and
% manual)
cfg.do_apmli_rej_preICA = 1;
cfg.do_rej_thresh   = 0;
cfg.rej_thresh_pre  = 400;
cfg.rej_thresh_post = 150;
cfg.rej_thresh_tmin = cfg.epoch_tmin;
cfg.rej_thresh_tmax = cfg.epoch_tmax;

cfg.do_detrend = 0;
%% Parameters for ICA.
cfg.ica_type     = 'binica';
cfg.ica_extended = 1; % Run extended infomax ICA?
cfg.ica_rank     = 1;
cfg.ica_ncomps   = 0;
cfg.ica_chans    = cfg.data_chans; % Typicaly, ICA is computed on all channels, unless one channel is not really EEG.
cfg.ica_variance = 0;
cfg.amica        = 0;

%% Parameters for SASICA.
cfg.sasica_heogchan = num2str(cfg.data_chans+1);
cfg.sasica_veogchan = num2str(cfg.data_chans+2);
cfg.sasica_autocorr = 20;
cfg.sasica_focaltopo = 'auto';