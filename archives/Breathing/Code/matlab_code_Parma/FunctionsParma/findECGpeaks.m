function [rpeaks_no_outliers,qrspeaks_wout_outliers,ekg_IBIraw,ekg_IBImean,...
          ekg_IBIsd,ekg_HR,RR,rmssd,hrv_sdsd,hrv_sdnn,rrhrv,pLF,pHF,LFHFratio,VLF,LF,HF] = findECGpeaks(EEGin,ecgin,numWindows,windowSize,plotfig)


rpeaks   = [];
qrspeaks = [];
minProminences = [];

%
rpeaks_no_outliers= [];
qrspeaks_no_outliers= [];
ekg_IBIraw= [];
ekg_IBImean= [];
ekg_IBIsd= [];
ekg_HR= [];
RR= [];
rmssd= [];
hrv_sdsd= [];
hrv_EEsdnn= [];
rrhrv= [];
pLF= [];
pHF= [];
LFHFratio= [];
VLF= [];
LF= [];
HF= [];

wt = modwt(ecgin,5);
wtrec = zeros(size(wt));
wtrec(4:5,:) = wt(4:5,:);
y = imodwt(wtrec,'sym4');
y = abs(y).^2;

localPeakIndices_adjust = [];

for i = 1:numWindows % for sliding win use: numDataPoints-windowSize+1
    window = [];
    windowStart = (i-1)*windowSize + 1;
    windowEnd   = i*windowSize;
    timepoints  = ([windowStart:windowEnd]);
    window      = (y(:,windowStart:windowEnd));

    % remove outliers in the window:
    minProminences(i) = mean(window)+3*std(window);

    if minProminences(i) < 0.001 % sometime EKG signal is lost

    else

        % Find peaks and troughs using the updated minimum prominences
        [localPeaks_R,localPeakIndices_R] = findpeaks(window,'MinPeakHeight',minProminences(i),'MinPeakDistance',0.2*EEGin.srate);

        % re-encode the time to alligne to whole resp signal
        localPeakIndices_adjust_R = timepoints(localPeakIndices_R);
        rpeaks    = [rpeaks localPeakIndices_adjust_R];
        qrspeaks  = [qrspeaks localPeaks_R];
    end
end


if plotfig == 1
    figure
    subplot(1,3,1)
    plot(EEGin.times,ecgin)
    title('Raw ECG')
    subplot(1,3,2)
    plot(EEGin.times,y)
    title('R-Waves Localized by Wavelet Transform')
    hold on
    hwav = plot(rpeaks/EEGin.srate,qrspeaks,'ro');
    % hexp = plot(tm,y,'k*');
    % xlabel('Seconds')
end



% figure
% subplot(1,2,2)
% plot((1:length(ecgout))./EEG.srate,zscore(ecgout))
% hold on
% xlabel('Seconds')
% plot(rpeaks./EEG.srate,qrspeaks,'ro')
% title('EKG');

EEGin.ekgsrate   = EEGin.srate;
RRinterval_tpoin = diff(rpeaks);
RRinterval_sec   = diff(rpeaks)./EEGin.ekgsrate;
ekg_IBImean      = (mean(diff(rpeaks)))/EEGin.ekgsrate; % interval in seconds
ekg_HR           = 60/ekg_IBImean; %beats per minute

fprintf(['\n\n Heart-rate before outliers removal:' num2str(ekg_HR) '\n\n']);
EEGin.ekg_IBImean = [];
EEGin.ekg_HR = [];

% remove outliers intervals:
% Define the outlier threshold
% threshold = 1.5 * iqr(diff(rpeaks));

% Find the indices of data points that are outliers
% outliers_below = diff(rpeaks) < (median(diff(rpeaks)) - threshold);
% outliers_above = diff(rpeaks) > (median(diff(rpeaks)) + threshold);
outliers_below = (diff(rpeaks)./EEGin.ekgsrate) < 0.4;  %60/0.4 = 150bpm
outliers_above = (diff(rpeaks)./EEGin.ekgsrate) > 1.5;  %60/1.6 = 40bpm
outliers = outliers_below | outliers_above;

% Remove outliers from the original dataset
rpeaks_no_outliers   = rpeaks(~outliers);
qrspeaks_no_outliers = qrspeaks(~outliers);

% store values in EEG:
qrspeaks_wout_outliers  = qrspeaks_no_outliers;
rpeaks_wout_outliers    = rpeaks_no_outliers;

% calculate IBI:
ekg_IBImean = [];
ekg_IBIsd   = [];
EEGin.ekgsrate    = EEGin.srate;
ekg_IBIraw  = diff(rpeaks_no_outliers);
ekg_IBImean = (mean(diff(rpeaks_no_outliers)))/EEGin.ekgsrate; % interval in seconds
ekg_IBIsd   = (std(diff(rpeaks_no_outliers)))/EEGin.ekgsrate;
ekg_HR      = 60/ekg_IBImean; %beats per minute
% EEG.ekg_HR      = (length(rpeaks)/(length(EEG.times)/EEG.ekgsrate))*60;
% if EEG.srate defined in the peakfind
% EEG.ekg_IBImean = (mean(diff(rpeaks_no_outliers))); % interval in seconds
% EEG.ekg_IBIsd   = (std(diff(rpeaks_no_outliers)));
% EEG.ekg_HR      = 60/EEG.ekg_IBImean;


fprintf(['\n\n Heart-rate after outliers removal:' num2str(ekg_HR) '\n\n']);

if plotfig == 1
    subplot(1,3,3)
    plot(EEGin.times,y)
    hold on
    title('R-Waves Localized by Wavelet Transform - No outliers')
    hold on
    hwav = plot(rpeaks_wout_outliers/EEGin.srate,qrspeaks_wout_outliers,'ro');
    % hexp = plot(tm,y,'k*');
    % xlabel('Seconds')
end


% HRV
RR = ekg_IBIraw/EEGin.srate;
rmssd    = HRV.RMSSD(RR,60);
hrv_sdsd = SDSD(RR,60);
hrv_sdnn = SDNN(RR,60);
rrhrv    = HRV.rrHRV(RR,60);
[pLF,pHF,LFHFratio,VLF,LF,HF] = fft_val(RR,60,EEGin.srate);

pause(2)