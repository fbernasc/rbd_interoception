function [pLF,pHF,LFHFratio,VLF,LF,HF] = fft_val(RR,num,Fs,type,overlap) 
%fft_val Spectral analysis.
%   [pLF,pHF,LFHFratio,VLF,LF,HF] = fft_val(RR,num,Fs,type) uses FFT to
%   compute the spectral density function of the interpolated RR tachogram.
%   RR is a vector containing RR intervals in seconds.
%   num specifies the number of successive values for which the spectral
%   density function will be estimated. The density of very low, low and
%   high frequency parts will be computed.   
%   Fs specifies the sampling frequency.
%   type is the interpolation type. Look up interp1 function of Matlab for
%   accepted types (default: 'spline').
%   For faster computation on local measures you can specify an overlap.
%   This is a value between 0 and 1. (default: 1)
%
%   Example: If RR = repmat([1 .98 .9],1,20),
%      then [~,~,LFHFratio] = HRV.fft_val(RR,60,1000) results in
%      LFHFratio = [NaN;NaN;NaN;0;0; ... ;0.0933;0.0828;0.0574].
%
%   See also INTERP1, FFT.
    
    RR = RR(:);
    if nargin<3 || isempty(num) || isempty(Fs)
        error('HRV.fft_val: wrong number or types of arguments');
    end
    if nargin<4 || isempty(type)
        type = 'spline';
    end
    if nargin<5 || isempty(overlap)
        overlap = 1;
    end
    
    if num==0
        [pLF,pHF,LFHFratio,VLF,LF,HF,~,~,~] = HRV.fft_val_fun(RR,Fs,type);    
    else
        pLF = NaN(size(RR));
        pHF = NaN(size(RR));
        VLF = NaN(size(RR));
        LF  = NaN(size(RR));
        HF  = NaN(size(RR));
        LFHFratio = NaN(size(RR));
        
        if overlap==1
            steps = 1; 
        else
            steps = ceil(num*(1-overlap));
        end
        for j=steps:steps:length(RR)
            [pLF(j),pHF(j),LFHFratio(j),VLF(j),LF(j),HF(j),~,~,~] = ...
                HRV.fft_val_fun(RR(max([1 j-num+1]):j),Fs,type);
        end
    end
end