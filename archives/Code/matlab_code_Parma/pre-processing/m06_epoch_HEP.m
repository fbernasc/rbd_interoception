clear all
close all

addpath(genpath('./matlab_code_Respiration/'));
addpath(genpath('./pre-processing/versionB/Respiration/'));
addpath(genpath('./matlab_code_Respiration/Functions/'));
addpath('./EEG/');
addpath('/home/fosco/toolboxes/eeglab2022.0')
eeglab nogui
addpath('/home/fosco/toolboxes/fieldtrip-master')
ft_defaults

whichsubject = [1:38];
whichdata    = '_ica_clean.set'; % _ica_1hz_clean
whichdataout = '_HEP.set';

RBDcode = 0;
HCcode  = 0;

% which subjects you want to process?
[who_idx,code_patient,patient,RBD,PSG_sess1,PSG_sess2,Gr,match,newID,age,gender,center,converted] = get_subjects_RBD(whichsubject);

filename  = ['/media/fosco/T7/RBD/EEG/RBD_nbr_trials_awake_200.csv'];
trialfileData  = fopen(filename,'at');
fprintf(trialfileData, 'suj,trials,sleep,patient \n');

filename1 = ['/media/fosco/T7/RBD/EEG/RBD_nbr_trials_200.csv'];
trialfileData1  = fopen(filename1,'at');
fprintf(trialfileData1,'suj,trials,sleep,patient \n');

for isub = 1:length(who_idx)

    % -------------
    % Prepare data.
    % -------------

    cfg = get_cfg_RBD(code_patient{isub});

    if PSG_sess2(isub) == 1 % exlude the second session of patients that have 2

    else


        % Write a status message to the command line.
        EEG = [];
        EEG = pop_loadset('filename',[cfg.subject_name whichdata],'filepath',cfg.dir_eeg,'loadmode','all');

        % % remove the first and last minutes which are often noisy:
        % fprintf('\n\n [stage 0 ] I cut the first and last minutes \n\n')
        % start = 5*60; % 5min in seconds
        % stop  = EEG.xmax-start;
        % EEG   = pop_select(EEG,'time',[start stop]);
        % EEG   = eeg_checkset(EEG);

        % identify reversals in sleep stages:
        sleepchan = squeeze(EEG.data(strcmp({EEG.chanlocs.labels},'Sleep'),:,:));
        changedIndexes = diff(sleepchan)~=0;
        resultcycles = find(changedIndexes==1);
        awakefirst   = zeros(1,length(sleepchan));
        awakefirst(1:resultcycles(1)) = 1;

        for i=1:length(EEG.event)
            EEG.event(i).awake = awakefirst(EEG.event(i).latency);
        end

        EEG = eeg_checkset(EEG);
        EEG = eeg_checkset(EEG, 'eventconsistency');

        fprintf(['\n\n [ Recodring duration ] ' num2str(EEG.times(end)/60)  ' minutes\n\n']);


        fprintf(['\n\n [ stage 2 ] epoch data at R-peak \n\n\n\n']);
        [EEG, ~, com] = pop_epoch(EEG,{'r-peak'},[cfg.epoch_ekg_tmin cfg.epoch_ekg_tmax], ...
            'newname', 'BDF file epochs', 'epochinfo', 'yes');
        EEG = eeg_checkset(EEG);
        EEG = eegh(com, EEG);


        % keep only chans of interest:
        eegchans  = {'C3','C4','F3','F4','F7','F8','Fp1','Fp2','Fpz','Fz','O1','O2','P3','P4','Pz','T3','T4','T5','T6','Oz','Cz','ECG','REF'};
        chans     = [];
        chans     = find(ismember({EEG.chanlocs.labels},eegchans));
        EEG       = pop_select(EEG, 'channel',{EEG.chanlocs(chans).labels});


        % pop_export(EEG,[cfg.dir_eeg cfg.subject_name '_HEP.csv'],'time','on','elec','on','transpose','off')

        % remove trials based on amplitued on specific EEG channels (wihouth ECG, etc that we still want in the dataset):
        eegchanskeep = {'C3','C4','F3','F4','F7','F8','Fp1','Fp2','Fpz','Fz','O1','O2','P3','P4','Pz','T3','T4','T5','T6','Cz'}; % Oz
        eegchanskeepnum = find(ismember({EEG.chanlocs.labels},eegchanskeep));

        [EEG,com] = pop_eegthresh(EEG,1,eegchanskeepnum,-200,200,EEG.xmin, EEG.xmax,[],1);

        pause(2)


        % ----------
        % Save data.
        % ------------------------------------------------
        % select only the first part when they are awake:
        %-------------------------------------------------
        EEGtmp = [];
        EEGtmp = pop_selectevent(EEG,'type','r-peak','awake',1);

        fprintf(trialfileData,'%s,%d,%s,%d\n',...
            cfg.subject_name,...
            EEGtmp.trials,...
            'awake',...
            patient(isub));

        filename_awake =[cfg.dir_eeg '/' cfg.subject_name '_HEPs_RBD_200_awake.csv'];
        resultfileData = fopen(filename_awake,'at');
        fprintf(resultfileData, 'amplitude patient age converted chan time sleep HR IBI rmssd hrv_sdsd hrv_sdnn rrhrv pLF pHF LFHFratio\n');

        % for itrial = 1:EEG.trials
        for ichan = 1:EEGtmp.nbchan
            for itime = 1:length(EEG.times)
                fprintf(resultfileData,'%f %d %d %s %s %d %s %f %f %f %f %f %f %f %f %f\n',...
                    squeeze(mean(EEGtmp.data(ichan,itime,:),3)),...
                    patient(isub), ...
                    age(isub),...
                    converted{isub},...
                    EEGtmp.chanlocs(ichan).labels,...
                    EEGtmp.times(itime),...
                    'awake',...
                    EEGtmp.ekg_HR,...
                    EEGtmp.ekg_IBImean,...
                    mean(EEGtmp.rmssd,'omitnan'),...
                    mean(EEGtmp.hrv_sdsd,'omitnan'),...
                    mean(EEGtmp.hrv_sdnn,'omitnan'),...
                    mean(EEGtmp.rrhrv,'omitnan'),...
                    mean(EEGtmp.pLF,'omitnan'),...
                    mean(EEGtmp.pHF,'omitnan'),...
                    mean(EEGtmp.LFHFratio,'omitnan'));
            end
        end
        % end
        fclose(resultfileData);

        %----------------------------------
        % select all the sleep stating:
        %-----------------------------------

        % EEG = pop_editset(EEG,'setname',[cfg.subject_name  whichdataout]);
        % EEG = pop_saveset( EEG, [cfg.subject_name  whichdataout] , cfg.dir_eeg);

        % identify who many sleep stages are present:
        lsleep = unique([EEG.event.sleep]);
        for s = 1:length(lsleep)
            EEGtmp = [];
            EEGtmp = pop_selectevent(EEG,'type','r-peak','sleep',lsleep(s));
            % EEGtmp.data = mean(EEGtmp.data(:,:,:),3);
            % EEGtmp = eeg_checkset(EEGtmp);
            % pop_export(EEGtmp,[cfg.dir_eeg cfg.subject_name '_HEP' '_sleep_' num2str(lsleep(s)) '.csv'],'time','on','elec','on','transpose','on')
            % EEGtmp = pop_saveset( EEGtmp, [cfg.subject_name  whichdataout(1:end-4)  '_sleep_' num2str(lsleep(s))] , cfg.dir_eeg);

            fprintf(trialfileData1,'%s,%d,%d,%d\n',...
                cfg.subject_name,...
                EEGtmp.trials,...
                lsleep(s),...
                patient(isub));


            % convert to .csv
            filename=[cfg.dir_eeg '/' cfg.subject_name '_HEPs_RBD_200_sleep_' num2str(lsleep(s)) '.csv'];
            resultfileData1 = fopen(filename,'at');

            % header:
            fprintf(resultfileData1, 'amplitude patient age converted chan time sleep HR IBI rmssd hrv_sdsd hrv_sdnn rrhrv pLF pHF LFHFratio\n');

            % for itrial = 1:EEG.trials
            for ichan = 1:EEGtmp.nbchan
                for itime = 1:length(EEG.times)
                    fprintf(resultfileData1,'%f %d %d %s %s %d %d %f %f %f %f %f %f %f %f %f\n',...
                        squeeze(mean(EEGtmp.data(ichan,itime,:),3)),...
                        patient(isub), ...
                        age(isub),...
                        converted{isub},...
                        EEGtmp.chanlocs(ichan).labels,...
                        EEGtmp.times(itime),...
                        lsleep(s),...
                        EEGtmp.ekg_HR,...
                        EEGtmp.ekg_IBImean,...
                        mean(EEGtmp.rmssd,'omitnan'),...
                        mean(EEGtmp.hrv_sdsd,'omitnan'),...
                        mean(EEGtmp.hrv_sdnn,'omitnan'),...
                        mean(EEGtmp.rrhrv,'omitnan'),...
                        mean(EEGtmp.pLF,'omitnan'),...
                        mean(EEGtmp.pHF,'omitnan'),...
                        mean(EEGtmp.LFHFratio,'omitnan'));
                end
            end
            % end
            fclose(resultfileData1);

        end
    end
end

fclose(trialfileData);
fclose(trialfileData1);
fprintf('\n DONE...\n')
