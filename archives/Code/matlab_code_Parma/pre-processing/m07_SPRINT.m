clear all
close all

addpath(genpath('./matlab_code_Respiration/'));
addpath(genpath('./pre-processing/versionB/Respiration/'));
addpath(genpath('./matlab_code_Respiration/Functions/'));
addpath('./EEG/');
addpath('/home/fosco/toolboxes/eeglab2022.0')
eeglab nogui
addpath('/home/fosco/toolboxes/fieldtrip-master')
ft_defaults

% addpath('/home/fosco/toolboxes/brainstorm3')
% brainstorm

whichsubject = [1:23];
whichdata    = '_import_respiration.set'; % select between _import_ekg and _import_continous
whichdataout = '_SPRINT.set';

% which subjects you want to process?
[who_idx,code_patient,patient,RBD,PSG_sess1,PSG_sess2,Gr,match,newID,age,gender,center,converted] = get_subjects_RBD(whichsubject);


for isub = 1:length(who_idx)

    % -------------
    % Prepare data.
    % -------------

    cfg = get_cfg_RBD(code_patient{isub});
    EEG = [];

    if PSG_sess2(isub) == 1 % exlude the second session of patients that have 2

    else


        % Write a status message to the command line.
        EEG = pop_loadset('filename',[cfg.subject_name whichdata],'filepath',cfg.dir_eeg,'loadmode','all');

        % % remove the first and last minutes which are often noisy:
        start = 5*60; % 5min in seconds
        stop  = EEG.xmax-start;
        EEG   = pop_select(EEG,'time',[start stop]);
        EEG   = eeg_checkset(EEG);

        fprintf(['\n\n [ stage 6 ] perform SPRiNT \n\n']);

        % select EEG chans
        starttimer = tic;
        eegchans  = {'C3','C4','F3','F4','F7','F8','Fp1','Fp2','Fpz','Fz','O1','O2','P3','P4','Pz','T3','T4','T5','T6'}; % ,'C4','F3','F4','F7','F8','Fp1','Fp2','Fpz','Fz','O1','O2','P3','P4','Pz','T3','T4','T5','T6' 
        chans     = [];
        chans     = find(ismember({EEG.chanlocs.labels},eegchans));

        EEG_tmp   = [];
        EEG_tmp   = pop_select(EEG, 'channel',chans);

        % SPRiNT (without Brainstorm)
        % Inputs
        F = EEG_tmp.data(:,:);               % Input time series

        % STFT opts
        opt.sfreq = EEG.srate;              % Input sampling rate
        opt.WinLength = 1;                  % STFT window length
        opt.WinOverlap = 75;                % Overlap between sliding windows (in %)
        opt.WinAverage = 0;                 % Number of sliding windows averaged by time point default 5

        % specparam opts
        opt.freq_range          = [1 40];
        opt.peak_width_limits   = [0.5 12];
        opt.max_peaks           = 4; % def 3
        opt.min_peak_height     = 0; % 6 / 10; % convert from dB to B
        opt.aperiodic_mode      = 'fixed'; % alternative: knee
        opt.peak_threshold      = 2.0;   % 2 std dev: parameter for interface simplification
        % Matlab-only options
        opt.peak_type           = 'gaussian'; % alternative: cauchy
        opt.proximity_threshold = 2;
        opt.guess_weight        = 'none';
        opt.thresh_after        = true;   % Threshold after fitting, always selected for Matlab
        % (mirrors the Python FOOOF closest by removing peaks
        % that do not satisfy a user's predetermined conditions)
        % only used in the absence of the

        if license('test','optimization_toolbox') % check for optimization toolbox
            opt.hOT = 1;
            disp('Using constrained optimization, Guess Weight ignored.')
        else
            opt.hOT = 0;
            disp('Using unconstrained optimization, with Guess Weights.')
        end
        
        opt.rmoutliers  = 'yes';
        opt.maxfreq     = 2.5;
        opt.maxtime     = 6;
        opt.minnear     = 3;

        Freqs = 0:1/opt.WinLength:opt.sfreq/2;
        channel = struct('data',[],'peaks',[],'aperiodics',[],'stats',[]);

        % Compute short-time Fourier transform
        [TF, ts] = SPRiNT_stft(F,opt);
        outputStruct = struct('opts',opt,'freqs',Freqs,'channel',channel);

        % Parameterize STFTs
        s_data = [];
        s_data = SPRiNT_specparam_matlab(TF,outputStruct.freqs,outputStruct.opts,ts);

        EEG.sprint = s_data;
        tEnd = toc(starttimer);

        % % use brainstorm from SPRiNT to make it faster:
        % % Input files
        % sFiles = {...
        %     'sub-3bd44186-cac5-4408-9794-c7bd53651e54_eeg/sub-3bd44186-cac5-4408-9794-c7bd53651e54_eeg_ica_clean/data_block001.mat'};
        %
        % % Start a new report
        % bst_report('Start', sFiles);
        %
        % % Process: SPRiNT: Spectral Parameterization Resolved in Time
        % sFiles = bst_process('CallProcess', 'process_sprint', sFiles, [], ...
        %     'sensortypes',   'C4,C3', ...
        %     'timewindow',    [0,300], ...
        %     'stft',          [], ...
        %     'win_length',    1, ...
        %     'win_overlap',   75, ...
        %     'loc_average',   0, ...
        %     'fooof',         [], ...
        %     'freqrange',     [1, 40], ...
        %     'peaktype',      'gaussian', ...  % Gaussian
        %     'peakwidth',     [1, 12], ...
        %     'maxpeaks',      6, ...
        %     'minpeakheight', 0, ...
        %     'proxthresh',    2, ...
        %     'apermode',      'fixed', ...  % Fixed
        %     'guessweight',   'none', ...  % None
        %     'postproc',      [], ...
        %     'rmoutliers',    'yes', ...  % Yes
        %     'maxfreq',       2.5, ...
        %     'maxtime',       6, ...
        %     'minnear',       3);
        %
        % % Save and display report
        % ReportFile = bst_report('Save', sFiles);
        % bst_report('Open', ReportFile);
        % bst_report('Export', ReportFile, ExportDir);
        % bst_report('Email', ReportFile, username, to, subject, isFullReport);

        % Delete temporary files
        % gui_brainstorm('EmptyTempFolder');


        % ----------
        % Save data.
        % ----------
        EEG = pop_editset(EEG,'setname',[cfg.subject_name  whichdataout]);
        EEG = pop_saveset( EEG, [cfg.subject_name  whichdataout] , cfg.dir_eeg);

        clear EEG EEG_tmp

    end
end
