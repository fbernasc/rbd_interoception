clear all
close all

addpath(genpath('./matlab_code_Respiration/'));
addpath(genpath('./pre-processing/versionB/Respiration/'));
addpath(genpath('./matlab_code_Respiration/Functions/'));
addpath('./EEG/');
addpath('/home/fosco/toolboxes/eeglab2022.0')
eeglab nogui
addpath('/home/fosco/toolboxes/fieldtrip-master')
ft_defaults


whichsubject = [1:37];
whichdata    = '_SPRINT_4.set'; % select between _import_ekg and _import_continous
whichdataout = '_SPRINT_breathing.set';

% which subjects you want to process?
[who_idx,code_patient,patient,RBD,PSG_sess1,PSG_sess2,Gr,match,newID,age,gender,center,converted] = get_subjects_RBD(whichsubject);


for isub = 1:length(who_idx)

    % -------------
    % Prepare data.
    % -------------

    cfg = get_cfg_RBD(code_patient{isub});
    EEG = [];

    if PSG_sess2(isub) == 1 % exlude the second session of patients that have 2

    else

        % Load EEG data:
        EEG = pop_loadset('filename',[cfg.subject_name whichdata],'filepath',cfg.dir_eeg,'loadmode','all');

        % link aperiodic and respiratory phase:
        % Define parameters used for SPRINT
        window_width  = 1;   % 1 second window
        overlap       = 0.5; % 75% overlap
        sampling_rate = EEG.srate;  % Sampling rate of your neural data

        % Load respiratory phase data
        respiratory_phase = EEG.respphaseHilbert;  % Assuming EEG.respphaseHilbert is a vector
        respiratory_phases = [];

        % Calculate number of samples for the moving window
        window_length  = round(window_width * sampling_rate);
        overlap_length = round(window_length * overlap);

        % Iterate through time points with the moving window
        for t = 1:overlap_length:(length(EEG.respphaseHilbert) - window_length)
            % Extract the current window
            window_data = EEG.respphaseHilbert(t:t+window_length-1);

            % Extract respiratory phase at the center of the window
            center_time = t + (window_length / 2);
            
            % Find the nearest respiratory phase value
            [~, closest_phase_index] = min(abs(respiratory_phase - center_time));

            % Use the closest_phase_index to get the respiratory phase
            respiratory_phase_at_center = respiratory_phase(closest_phase_index);

            % Store respiratory phase
            respiratory_phases = [respiratory_phases, respiratory_phase_at_center];
        end


        n_bins = 60;
        delta_omega = pi / 30;
        bin_edges = linspace(-pi, pi, n_bins + 1);

        % Initialize arrays to store aperiodic and periodic signals in each phase bin
        aperiodic_signals = [];
        periodic_signals  = [];

        %loop over channels:
        for ichan=1:size(EEG.sprint.SPRiNT.channel,2)
            for i = 1:n_bins
                bin_start = bin_edges(i);
                bin_end = bin_edges(i + 1);

                % Select data points within the current phase bin
                bin_indices = find(respiratory_phases >= bin_start & respiratory_phases < bin_end);

                % Extract the corresponding SPRiNT outputs for the current phase bin
                tmp_periodic   = [];
                tmp_aperiodic1 = [];
                tmp_aperiodic2 = [];
                tmp_periodic   = [EEG.sprint.SPRiNT.channel(ichan).peaks.center_frequency];
                tmp_aperiodic1 = [EEG.sprint.SPRiNT.channel(ichan).aperiodics.offset];
                tmp_aperiodic2 = [EEG.sprint.SPRiNT.channel(ichan).aperiodics.exponent];

                % Compute the bin-wise averages of aperiodic and periodic signals
                aperiodic_signals_offset(ichan,i,:)   = nanmean(tmp_aperiodic1(bin_indices));
                aperiodic_signals_exponent(ichan,i,:) = nanmean(tmp_aperiodic1(bin_indices));
                % periodic_signals(ichan,i,:)           = nanmean(tmp_periodic(bin_indices));
            end
        end

        figure;
        polarplot(linspace(-pi, pi, n_bins), aperiodic_signals_exponent(:,:,:), '-o');
        % ----------
        % Save data.
        % ----------
        EEG = pop_editset(EEG,'setname',[cfg.subject_name  whichdataout]);
        EEG = pop_saveset( EEG, [cfg.subject_name  whichdataout] , cfg.dir_eeg);

        clear EEG EEG_tmp

    end
end
