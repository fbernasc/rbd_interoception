% function m01_preprocessing_Respiration(whichsubject)
clear all
close all
addpath(genpath('./matlab_code_Parma/'));
addpath(genpath('./matlab_code_Parma/FunctionsParma'));
% addpath(genpath('/mnt/nas/Users/Fosco/RBD/RBD_Parma/'));
addpath('./EEG/');
addpath('/home/fosco/toolboxes/eeglab2022.0')
eeglab nogui
addpath('/home/fosco/toolboxes/fieldtrip-master')
ft_defaults

whichsubject = [26:38];

% which subjects you want to process?
[who_idx,code_patient,gender,age,patient,RBD] = get_subjects_RBD_Parma(whichsubject);


for isub = 1:length(who_idx)

    % get cfgs
    cfg = get_cfg_RBD_Parma(code_patient{isub});
    EEG = [];

    % ---------------------------
    % Import parameters & path(s)
    % ---------------------------
    % Write a status message to the command line.
    fprintf('\n Now importing subject %s, (number %d of %d to process).\n\n',code_patient{isub},isub,length(who_idx));
    pause(1)

    %-------------
    % Import data
    %-------------

    eegData = [cfg.dir_raw code_patient{isub} '/' code_patient{isub} '.edf'];

    if ~exist(eegData,'file')
        fprintf('Does not exist!\n',eegData)
    else

        % Create output directory if necessary.
        if ~isdir(cfg.dir_eeg)
            mkdir(cfg.dir_eeg);
        end

        % get the reference:
        eegtmp = pop_biosig(eegData);
        ref    = eegtmp.ref;
        clear eegtmp

        data = [];
        [data, event] = edf2fieldtrip(eegData);
        % [hdr] = ft_read_header(eegData);


        % re-encoge chans name:
        chanekg = find(~cellfun(@isempty,regexp([data.label],'ECG1-ECG2|ekg|ECG|EKG')));
        data.label{chanekg} = 'ECG';

        breathing = find(~cellfun(@isempty,regexp([data.label],'ADDOME')));
        if breathing
            data.label{breathing} = 'Abdomen';
        end

        breathing1 = find(~cellfun(@isempty,regexp([data.label],'TORACE|toracico')));
        if breathing1
            data.label{breathing1} = 'Thorax';
        end

        eog = find(~cellfun(@isempty,regexp([data.label],'ROC-LOC|EOG dx')));
        if eog
            data.label{eog} = 'EOG';
        end

        tib1 = find(~cellfun(@isempty,regexp([data.label],'SX1-SX2')));
        if tib1
            data.label{tib1} = 'Tib-L';
        end

        tib2 = find(~cellfun(@isempty,regexp([data.label],'DX1-DX2|Dx1-DX2')));
        if tib2
            data.label{tib2} = 'Tib-R';
        end

        chin = find(~cellfun(@isempty,regexp([data.label],'EMG1-EMG2')));
        if chin
            data.label{chin} = 'ChinL';
        end

        % remove A1 or A2 ref:
        cellArray  = cellfun(@(x) strrep(x,{'-A1', '-A2'}, ''),[data.label],'UniformOutput',false);
        newlabel   = cellfun(@(x) x{1,1},cellArray,'UniformOutput',false);
        data.label = newlabel;

        % data has one extrachan
        selchan = [];
        selchan = ft_channelselection({'all' '-DHR' '-EDF Annotations' '-Arm-L' '-Arm-R' ...
            '-ChinA' '-ChinL' '-ChinR' '-Elevation_DR' '-Flattening' '-Gravity X' '-Gravity Y' '-Mask Snore'...
            '-Snore_DR' '-Snoring Sensor' '-Thermistor' '-Tib-L' '-Tib-R' '-PLETH' '-STAT' '-HR' '-SAO2' '-Position' '-MIC' '-SpO2' ...
            '-TERMISTORE' '-deltoide' '-TAG' '-THE' '-milo'}, data.label);
        cfg1 = [];
        cfg1.channel = selchan;
        data = ft_selectdata(cfg1,data);

        % add chan info
        fprintf('\n\n [stage 0] add channels info \n\n')
        EEG = fieldtrip2eeglab(data);
        EEG.chanlocs = struct(EEG.chanlocs);

        for i = 1:size(data.label,1)
            EEG.chanlocs(i).labels = data.label{i};
            EEG.chanlocs(i).ref = 'EEG REF';
            EEG.chanlocs(i).X = [];
            EEG.chanlocs(i).Y = [];
            EEG.chanlocs(i).Z = [];
            EEG.ref           = 'EEG REF';

            if contains(EEG.chanlocs(i).labels,'EEG') == 1
                EEG.type = 'EEG';
            else
                EEG.type = 'external';
            end% Oz)
        end

        clear data

        tmp      = [];
        tmp      = EEG.data;
        EEG.data = [];
        EEG.data = tmp;
        EEG      = eeg_checkset(EEG);

        clear tmp

        % downsample the data:
        if EEG.srate > 200
            EEG = pop_resample(EEG,200);
        end

        % add chanlocs:
        EEG = pop_chanedit(EEG, 'lookup','/home/fosco/toolboxes/eeglab2022.0/plugins/dipfit/standard_BEM/elec/standard_1005.elc');
        EEG = eeg_checkset(EEG);

        for i = 1:EEG.nbchan
            EEG.chanlocs(i).ref = 'EEG REF';
            EEG.ref             = 'EEG REF';
        end
        pop_saveset(EEG,[code_patient{isub}  '_import.set'] , cfg.dir_eeg);


        % save also for mne sleep staging:
        EEG  = pop_select(EEG, 'nochannel',{'Abdomen', 'Activity_DR', 'Arm-L', 'Arm-R','ChinA', 'ChinL', 'ChinR',...
            'EOG', 'EOG E2', 'Elevation_DR', 'Flattening', 'Flow_DR', 'Gravity X', 'Gravity Y', 'Heart Rate_DR',...
            'Light_BU','Nasal Pressure', 'pCO2', 'Phase_DR',...
            'Plethysmogram', 'Position_DR', 'Pulse', 'RD-Quality',...
            'EEG REF', 'RMI_DR', 'RR_DR', 'Snore_DR', 'Snoring Sensor',...
            'SpO2 BB', 'SpO2-Quality_DR', 'SpO2', 'Thermistor', 'Thorax',...
            'Tib-L', 'Tib-R', 'Tidal Volume_DR', 'XFlow_DR',...
            'XSum_DR','EKG','Mask Pressure','Pneumoflow','REF','PC02 Tosca','MIC'});

        fprintf(['\n\n' code_patient{isub} '\n\n'])
        % Writing the data
        pop_writeeeg(EEG, [cfg.dir_eeg code_patient{isub}  '_mne.edf'] , 'TYPE','EDF');
        clear EEG
    end
end
% end

