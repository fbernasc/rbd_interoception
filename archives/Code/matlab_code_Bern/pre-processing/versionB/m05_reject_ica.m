clear all
close all

visual_ica = 1;

addpath(genpath('./matlab_code_Respiration/'));
addpath(genpath('./pre-processing/versionB/Respiration/'));
addpath(genpath('./matlab_code_Respiration/Functions/'));
addpath('./EEG/');
addpath('/home/fosco/toolboxes/eeglab2023.0')
eeglab nogui
addpath('/home/fosco/toolboxes/fieldtrip-master')
ft_defaults

whichsubject = [1:38];
whichdata    = '_ica_1hz.set'; % select between _import_ekg and _import_continous
whichdataout = '_ica_1hz_clean.set';

% which subjects you want to process?
[who_idx,code_patient,patient,RBD,PSG_sess1,PSG_sess2,Gr,match,newID,age,gender,center,converted] = get_subjects_RBD(whichsubject);


for isub = 1:length(who_idx)

    % -------------
    % Prepare data.
    % -------------

    cfg = get_cfg_RBD(code_patient{isub});
    EEG = [];

    if PSG_sess2(isub) == 1 % exlude the second session of patients that have 2

    else


        % Write a status message to the command line.
        EEG = pop_loadset('filename',[cfg.subject_name whichdata],'filepath',cfg.dir_eeg,'loadmode','all');

        % link electrodes position
        % EEG = pop_chanedit(EEG, 'lookup','/home/fosco/toolboxes/eeglab2022.0/plugins/dipfit/standard_BEM/elec/standard_1005.elc');

        % if EEG.nbchan > 3 %two chans are for eog and ecg
            if visual_ica
                [EEG,com] = SASICA(EEG);
                
                keyboard
                EEG = evalin('base','EEG'); %SASICA stores the results in base workspace via assignin. So we have to use this workaround...
                EEG = eegh(com,EEG);

                fprintf(['\n I will remove ' num2str(sum(EEG.reject.gcompreject)) ' components \n']);
                ncomps = sum(EEG.reject.gcompreject);
                [EEG,com] = pop_subcomp(EEG,find(EEG.reject.gcompreject),1);

            else
                [EEG,com]= eeg_SASICA(EEG,'MARA_enable',0,...
                    'FASTER_enable',1,'FASTER_blinkchanname','VEOG',...
                    'ADJUST_enable',0,...
                    'chancorr_enable',0,'chancorr_channames','ECG','chancorr_corthresh',0.6,... % 'auto4'
                    'EOGcorr_enable',1,'EOGcorr_Heogchannames','HEOG','EOGcorr_corthreshH',0.6,... % 'auto4'
                    'EOGcorr_Veogchannames','VEOG','EOGcorr_corthreshV',0.6,...  % 'auto4'
                    'resvar_enable',0,'resvar_thresh',15,...
                    'SNR_enable',0,'SNR_snrcut',1,'SNR_snrBL',[-Inf 0] ,'SNR_snrPOI',[0 Inf],...
                    'trialfoc_enable',0,'trialfoc_focaltrialout','auto',...
                    'focalcomp_enable',0,'focalcomp_focalICAout','auto',...
                    'autocorr_enable',0,'autocorr_autocorrint',20,...
                    'autocorr_dropautocorr','auto','opts_noplot',1,...
                    'opts_nocompute',0,'opts_FontSize',14);

                ncomps = sum(EEG.reject.gcompreject);

                %     keyboard;
                %     EEG = evalin('base','EEG');
                %     EEG = eegh(com,EEG);

                fprintf(['\n I will remove ' num2str(ncomps) ' components \n\n']);
                pause(2)

                V_Components_to_remove = find(EEG.reject.gcompreject == 1);
                [EEG, com] = pop_subcomp(EEG, V_Components_to_remove);

            end
        % end

        if isempty(com)
            return
        end

        % ----------
        % Save data.
        % ----------
        EEG = pop_editset(EEG,'setname',[cfg.subject_name  whichdataout]);
        EEG = pop_saveset( EEG, [cfg.subject_name  whichdataout] , cfg.dir_eeg);
    end
end
% fclose(resultfileData);
fprintf('Done.\n')
