function chaneog = findeogchan(EEGinput,chaninput)
chaneog = find(~cellfun(@isempty,regexp({EEGinput},chaninput)));
end
