function [who_idx,code_patient,parkinson,dementia] = get_subjects_RBD_online(whichsubject)
% helper function to decode which subjects are to be processed.

if isempty(whichsubject)
    T = readtable('/media/fosco/T7/mros/datasets/mros-visit1-dataset-0.6.0.csv');
    who_idx = 1:size(T(:,1),1);
    code_patient = table2array(T(:,1));
    parkinson    = table2array(T(:,96));
    dementia     = table2array(T(:,464));

    
elseif isnumeric(whichsubject) % If who is just a numeric index.   
    T = readtable('/media/fosco/T7/mros/datasets/mros-visit1-dataset-0.6.0.csv');
    who_idx = 1:size(T(:,1),1);
    code_patient = table2array(T(whichsubject,1));
    parkinson    = table2array(T(whichsubject,96));
    dementia     = table2array(T(whichsubject,464));

end