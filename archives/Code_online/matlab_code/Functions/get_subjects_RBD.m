function [who_idx,code_patient,patient,RBD,PSG_sess1,PSG_sess2,Gr,match,newID,age,gender,center,converted] = get_subjects_RBD(whichsubject)
% helper function to decode which subjects are to be processed.

if isempty(whichsubject)
    %     [~,num,Raw] = xlsread('/media/fbernasc/Elements/Parkinson/Info/Patients_info_amica_noHC.xlsx','Sheet1');
    T = readtable('/home/fosco/Git_epfl/hep_rbd/RBD_patients_info.csv');
    who_idx = 1:size(T(:,1),1);
    code_patient = table2array(T(:,2));
    patient  = table2array(T(:,3));
    RBD      = table2array(T(:,4));
    PSG_sess1 = table2array(T(:,5));
    PSG_sess2 = table2array(T(:,6));
    Gr = table2array(T(:,7));
    match = table2array(T(:,8));
    newID = table2array(T(:,9));
    age    = table2array(T(:,10));
    gender = table2array(T(:,11));
    center =  table2array(T(:,12));
    converted =  table2array(T(:,13));
    
elseif isnumeric(whichsubject) % If who is just a numeric index.
    %     [~,num,Raw] = readtable('/home/fosco/Git/RBD/EEG/Data/RBD_patients_info.csv');
    %     Raw(1,:)=[]; % remove header
    %     who_idx = Raw(whichsubject,2);
    %     code_patient = Raw(whichsubject,1);
    %     patient = Raw(whichsubject,5);
    %     ID      = Raw(whichsubject,2);
    %     MH      = Raw(whichsubject,6);
    
    T = readtable('/home/fosco/Git_epfl/hep_rbd/RBD_patients_info.csv');
    who_idx = 1:size(T(whichsubject,1),1);
    code_patient = table2array(T(whichsubject,2));
    patient  = table2array(T(whichsubject,3));
    RBD      = table2array(T(whichsubject,4));
    PSG_sess1 = table2array(T(whichsubject,5));
    PSG_sess2 = table2array(T(whichsubject,6));
    Gr = table2array(T(whichsubject,7));
    match = table2array(T(whichsubject,8));
    newID = table2array(T(whichsubject,9));
    age    = table2array(T(whichsubject,10));
    gender = table2array(T(whichsubject,11));
    center =  table2array(T(whichsubject,12));
    converted =  table2array(T(whichsubject,13));

end