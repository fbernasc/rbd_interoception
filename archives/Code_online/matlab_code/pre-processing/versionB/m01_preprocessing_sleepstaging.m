function m01_preprocessing_sleepstaging(whichsubject)

addpath('.././pre-processing');
addpath('.././Functions');
addpath('.././EEG/');

% which subjects you want to process?
[who_idx,code_patient,patient,RBD,PSG_sess1,PSG_sess2,Gr,match,newID,age,gender,center,converted] = get_subjects_RBD(whichsubject);
addpath('/home/fosco/toolboxes/Functions/erplab_4.0.3.1')

for isub = 1:length(who_idx)
    
    % get cfgs
    cfg = get_cfg_RBD(code_patient{isub});
    EEG = [];
    
    % ---------------------------
    % Import parameters & path(s)
    % ---------------------------
    % Write a status message to the command line.
    fprintf('\n Now importing subject %s, (number %d of %d to process).\n\n',code_patient{isub},isub,length(who_idx));
    pause(1)
    
    % Create output directory if necessary.
    if ~isdir(cfg.dir_eeg)
        mkdir(cfg.dir_eeg);
    end
    
    %-------------
    % Import data
    %-------------
    eegData = [cfg.dir_raw code_patient{isub} '.edf'];
    
    if ~exist(eegData,'file')
        fprintf('Does not exist!\n',eegData)
    else
        
        if cfg.do_import_ref
            fprintf('Importing %s\n',eegData)
            EEG = pop_biosig(eegData,'ref',cfg.ImportReference);
        else
            EEG = pop_biosig(eegData);
        end
        
        
        if strcmp(center{isub},'Bern')
            if EEG.srate>200
                EEG.srate = 200;
            end
        elseif strcmp(center{isub},'Parma')
            
        end
        
        % Writing the data
        pop_writeeeg(EEG,[cfg.dir_eeg cfg.subject_name  '_import_4sleep.edf'], 'TYPE','EDF');
        
        %         % Now importing again the saved file
        %         EEG = pop_biosig([datapath datafile2save]);
        %         [EEG, com] = pop_editset(EEG, 'setname', [cfg.subject_name ' import_4sleep']);
        %         EEG = eegh(com, EEG);
        %         pop_saveset(EEG,[cfg.subject_name  '_import_4sleep.set'] , cfg.dir_eeg);
        clear EEG
        
        
    end
end
fprintf('Done.\n')
