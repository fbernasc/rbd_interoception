function m01_preprocessing_online(whichsubject)

% which subject you want to preprocess:
% whichsubject = []; % empty = all

addpath('./pre-processing');
addpath('./Functions');
addpath('./EEG/');
addpath('/home/fosco/toolboxes/eeglab2022.0')
eeglab nogui
addpath('/home/fosco/toolboxes/fieldtrip-master')
ft_defaults

% which subjects you want to process?
[who_idx,code_patient,parkinson,dementia] = get_subjects_RBD_online(whichsubject);
addpath('/home/fosco/toolboxes/Functions/erplab_4.0.3.1')

% Start parallel pool
numWorkers = 1;
delete(gcp)
if isempty(gcp('nocreate'))
    parpool(numWorkers);
end

parfor isub = 1:length(who_idx)

    % get cfgs
    cfg = get_cfg_RBD_online(code_patient{isub});

    % ---------------------------
    % Import parameters & path(s)
    % ---------------------------
    % Write a status message to the command line.
    fprintf('\n Now importing subject %s, (number %d of %d to process).\n\n',code_patient{isub},isub,length(who_idx));
    pause(1)

    % Create output directory if necessary.
    if ~isfolder(cfg.dir_eeg)
        mkdir(cfg.dir_eeg);
    end

    %-------------
    % Import data
    %-------------
    eegData = [cfg.dir_raw 'mros-visit1-' code_patient{isub} '.edf'];

    if ~exist(eegData,'file')
        fprintf('Does not exist! \n',eegData)
    else

        if cfg.do_import_ref
            fprintf('Importing %s \n',eegData)
            %             EEG = pop_biosig(eegData,'ref',cfg.ImportReference);
            [data, event] = edf2fieldtrip(eegData);
            EEG = fieldtrip2eeglab(data);
            EEG = eeg_checkset(EEG);
            %             clear data event

        else
            % EEG = pop_biosig(eegData);
            data = [];
            [data, event] = edf2fieldtrip(eegData);
            %             [hdr] = ft_read_header(eegData);
            EEG = fieldtrip2eeglab(data);

            % data has one extrachan
            selchan = ft_channelselection({'all' '-DHR'}, data.label);
            cfg1 = [];
            cfg1.channel = selchan;
            data = ft_selectdata(cfg1,data);

            % add chan info
            fprintf('\n\n [stage 0] add channels info \n\n')
            %             chaninfo = load("/home/fosco/Git_epfl/hep_rbd/Chaninfo.mat");
            EEG.chanlocs = struct(EEG.chanlocs);

            for i = 1:size(data.label,1)-1
                EEG.chanlocs(i).labels = data.label{i};
                EEG.chanlocs(i).ref = [];
                EEG.chanlocs(i).X = [];
                EEG.chanlocs(i).Y = [];
                EEG.chanlocs(i).Z = [];
            end

            tmp = EEG.data(1:end-1,:);
            EEG.data = [];
            EEG.data = tmp;
            EEG = eeg_checkset(EEG);
        end


        if EEG.srate>200
            EEG = pop_resample(EEG,256);
            EEG = eeg_checkset(EEG);
        end


        %----------------------------------------
        % Shorten the data (e.g. from 5min to 15min)
        %----------------------------------------
        fprintf('\n\n [ stage 1 ] reduce data lenght \n\n');

        t1 = 10;   % from minute; the first part of the recording is empty or noisy
        t2 = 15;   % till minute
        EEG.datashort = [];
        EEG.datashort = EEG.data(:,((t1*60)*EEG.srate):((t2*60)*EEG.srate));
        EEG.data = [];
        EEG.data = EEG.datashort;
        EEG.timeselectonset = t1;
        EEG.timeselectoffst = t2;
        EEG = eeg_checkset(EEG);

        % change EOG name:
        tmpchan = {EEG.chanlocs.labels};
        chaneog = find(~cellfun(@isempty,regexp(tmpchan,'ROC|LOC')));
        chaneog

        EEG.chanlocs(chaneog(1)).labels = 'EOG E1';
        EEG.chanlocs(chaneog(2)).labels = 'EOG E2';

        %-------------------
        % Find R-peak etc.
        %-------------------
        % creat EKG channel
        chanekg = find(~cellfun(@isempty,regexp(tmpchan,'ECG L|ECG R')));
        EEG.data(EEG.nbchan+1,:) = EEG.data(chanekg(1),:) - EEG.data(chanekg(2),:);
        EEG.chanlocs(EEG.nbchan+1).labels = 'ECG';
        EEG = eeg_checkset(EEG);

        if cfg.genekgtrigger
            % Process ECG
            tmpchan = {EEG.chanlocs.labels};
            ecgchan = strcmp(tmpchan,'ECG')
            fprintf('\n\n [ stage 2 ] finding r-peaks in ECG \n\n');

            % filter the EKG:
            ord=2;
            Wn     = [3 30]/fix(EEG.srate/2);
            [B,A]  = butter (ord,Wn);
            EEG.ecgout = filtfilt(B,A,double(EEG.data(ecgchan,:)));
            EEG.ecgout = (EEG.ecgout-min(EEG.ecgout))/(max(EEG.ecgout)-min(EEG.ecgout));
            [EEG.qrspeaks,EEG.rpeaks] = findpeaks(zscore(EEG.ecgout),...
                'MinPeakProminence',3,'MinPeakDistance',0.2*EEG.srate);
            fprintf('\n\n [ stage 2 ] found peaks \n\n');

            % calculate IBI:
            EEG.ekg_IBImean = [];
            EEG.ekg_IBIsd   = [];
            EEG.ekgsrate    = EEG.srate;
            EEG.ekg_IBIraw  = diff(EEG.rpeaks);
            EEG.ekg_IBImean = (mean(diff(EEG.rpeaks)))/EEG.ekgsrate; % interval in seconds
            EEG.ekg_IBIsd   = (std(diff(EEG.rpeaks)))/EEG.ekgsrate;
            %                 EEG.ekg_HR      = (length(rpeaks)/(length(EEG.times)/EEG.ekgsrate))*60;
            EEG.ekg_HR      = 60/EEG.ekg_IBImean; %beats per minute

            fprintf('\n\n [ stage 2a ] found peaks \n\n');

            len = length(EEG.event);
            for i=1:length(EEG.rpeaks)
                e = len + i;
                EEG.event(e).latency = EEG.rpeaks(i);
                EEG.event(e).duration = 0;
                EEG.event(e).type = 'r-peak';
            end
            EEG = eeg_checkset(EEG, 'eventconsistency');
            fprintf('\n\n [ stage 2b ] added r-peaks as events \n\n');

            % calculate HRV
            EEG.rr_test_st2 = {};
            EEG.rr_test_st2.code = eegData % code_patient{isub};
            EEG.rr_test_st2.rr_interval = EEG.ekg_IBIraw./EEG.ekgsrate;
            EEG.rr_test_st2.rr_peaks    = EEG.rpeaks./EEG.ekgsrate;
            [EEG.hrv_feats_tb, EEG.hrv_feats_epochs_tb] = hrv_features(EEG.rr_test_st2);
            fprintf('\n\n [ stage 2b ] found HRV \n\n');

            EEG.mean_NN   = EEG.hrv_feats_tb(:,2);   % mean NN (normalised RR interval)
            EEG.SD_NN     = EEG.hrv_feats_tb(:,3);   % standard deviation of NN
            EEG.VLF_power = EEG.hrv_feats_tb(:,4);   % power in the very low frequency band (0.01 to 0.04 Hz)
            EEG.LF_power  = EEG.hrv_feats_tb(:,5);   % power in the low frequency band (0.04 to 0.2 Hz)
            EEG.HF_power  = EEG.hrv_feats_tb(:,6);   % power in the high frequency band (0.2 to 2 Hz)
            EEG.LF_HF_ratio = EEG.hrv_feats_tb(:,7); % LF_power:HF_power ratio

            %             fprintf('\n\n [ stage 2c ] found HRV part 2\n\n');
            %                         figure
            %                         plot((1:length(ecgout))/1000,zscore(ecgout))
            %                         hold on
            %                         xlabel('Seconds')
            %                         plot(rpeaks/1000,qrspeaks,'ro')
            %                         keyboard;


        end

        %-------------
        % filter data:
        %-------------
        fprintf('\n\n [ stage 3 ] filter data \n\n');
        EEG = pop_eegfiltnew(EEG,'locutoff', 1,'plotfreqz',0);
        EEG = pop_eegfiltnew(EEG,'hicutoff',45,'plotfreqz',0);

        %--------------------
        % Remove bad channels
        %--------------------
        %         % find cortical chans:
        %         eegchans  = [];
        %         chansbin  = [];
        %         eegchans  = {'C3','C4','F3','F4','F7','F8','Fp1','Fp2','Fpz','Fz','O1','O2','P3','P4','Pz','T3','T4','T5','T6'}; % Oz
        %         chansbin  = ismember({EEG.chanlocs.labels},eegchans);
        %         chans     = find(chansbin);
        %
        %         % store the nbr of electrodes in the structure:
        %         EEG.chaneeg = sum(chansbin);
        %
        %         EEGtmp = [];
        %         EEGtmp = pop_select(EEG,'channel',[chans]);
        %
        %         if strcmp(center{isub},'Bern')
        %             EEGtmp = pop_chanedit(EEGtmp, 'lookup','/home/fosco/toolboxes/eeglab2022.0/plugins/dipfit/standard_BEM/elec/standard_1005.elc');
        %         end
        %
        %         % save chanlocs for later interpol:
        %         name = ['/home/fosco/Git_epfl/hep_rbd/Code/matlab_code/pre-processing/electrode_n' num2str(EEGtmp.nbchan) '.mat'];
        %         chanlocs = EEGtmp.chanlocs;
        %         save(name,'chanlocs')
        %
        %         % https://eeglab.org/tutorials/06_RejectArtifacts/cleanrawdata.html
        %         % https://sccn.ucsd.edu/githubwiki/files/cleanRawData_30thEEGLABWorkshop.pdf
        %         EEGtmp = pop_clean_rawdata(EEGtmp,...
        %             'FlatlineCriterion',5,...
        %             'ChannelCriterion',0.6,...
        %             'LineNoiseCriterion',4,...
        %             'Highpass','off',...
        %             'BurstCriterion','off',...
        %             'WindowCriterion','off',...
        %             'BurstRejection','off',...
        %             'Distance','Euclidian');
        %
        %         %
        %         pause(3)
        %
        %         which_diff = [];
        %         which_rm   = [];
        %         rm_chanID  = [];
        %
        %         which_diff = find(~ismember(eegchans,{EEGtmp.chanlocs.labels})); % since not same electr
        %         which_rm   = eegchans{[which_diff]};
        %         rm_chanID  = find(contains({EEG.chanlocs.labels},which_rm));
        %         EEG = pop_select(EEG,'nochannel',[rm_chanID]);
        %         EEG = eeg_checkset( EEG );

        % clear data using ASR
        channelIndices = find(ismember(tmpchan,{'C3','C4','ECG','EOG E1','EOG E2','A1','A2','Airflow'}));
        EEG = pop_select(EEG,'channel',channelIndices);
        fprintf('\n\n [ stage 4 ] remove bad time-periods B \n\n');

        EEG = pop_clean_rawdata(EEG, 'FlatlineCriterion','off',...
            'ChannelCriterion','off',...
            'LineNoiseCriterion','off',...
            'Highpass','off',...
            'BurstCriterion',30,...    % lower values are more agressive criteria
            'WindowCriterion',0.25,... % 0.05 (very aggressive) to 0.3 (very lax)
            'BurstRejection','on',...
            'Distance','Euclidian',...
            'WindowCriterionTolerances',[-Inf 10]);

        EEG = eeg_checkset( EEG );

        %-----------
        % epoch data
        %-----------
        %         if cfg.genfaketriggerepoch
        %             EEGcont = [];
        %             EEGcont = EEG;
        %             if cfg.genfaketrigger
        %                 fprintf(['\n\n [ stage 3 ] segmenting continous data \n\n']);
        %                 t = cfg.epoch_tmax;
        %                 n = 0;
        %                 for l = 1:length(EEGcont.times)
        %                     if l == 1 || (((t*EEGcont.srate)* round(double(l)/(t*EEGcont.srate)) == l )) == 1
        %                         n = n+1;
        %                         EEGcont.event(n).type    = 'trigger';
        %                         EEGcont.event(n).latency = l;
        %                         EEGcont.event(n).urevent = n;
        %
        %                     elseif l == (((EEGcont.event(end).latency - EEGcont.event(1).latency)/EEGcont.srate)*60*EEGcont.srate)
        %                         break
        %
        %                     end
        %                 end
        %             end
        %
        %             [EEGcont, ~, com] = pop_epoch(EEGcont,{cfg.triggernumber},[cfg.epoch_tmin cfg.epoch_tmax], ...
        %                 'newname', 'BDF file epochs', 'epochinfo', 'yes');
        %             EEGcont = eegh(com, EEGcont);
        %
        %             % --------------------------------------------------------------
        %             % Save data.
        %             % --------------------------------------------------------------
        %             [EEGcont, com] = pop_editset(EEGcont, 'setname', [cfg.subject_name ' import_ekg_versionb_early']);
        %             EEGcont = eegh(com, EEGcont);
        %             pop_saveset(EEGcont,[cfg.subject_name  '_import_continous_versionb_early.set'] , cfg.dir_eeg);
        %             clear EEGcont
        %         end

        if cfg.genekgtriggerepoch

            fprintf('\n\n [ stage 5 ] epoching R-peaks \n\n');
            [EEG, ~, com] = pop_epoch(EEG,{'r-peak'},[cfg.epoch_ekg_tmin cfg.epoch_ekg_tmax], ...
                'newname', 'BDF file epochs', 'epochinfo', 'yes');
            EEG = eegh(com,EEG);

            % ----------
            % Save data.
            % ----------
            fprintf('\n\n [ stage 6 ] saving data data \n\n');
            [EEG, com] = pop_editset(EEG, 'setname', [cfg.subject_name 'mros-visit1-import']);
            EEG = eegh(com, EEG);
            pop_saveset(EEG,[cfg.subject_name  'mros-visit1-import.set'] , cfg.dir_eeg)

        end
    end
end
fprintf('Done.\n')
