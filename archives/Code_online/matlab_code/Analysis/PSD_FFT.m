% fosco.bernasconi@gmail.com

clear all; close all;
savecsv = 1; % generate .csv for analyses in R

whichsubject = [];
isub1 = 0;
isub2 = 0;


[who_idx,patient,ID,MH,MCI,MCIR1,MCIR3,Agonist,LEDD,Passage,PH,Tactile,VH,Paredolia,Audio,Olfactory,Hallucinations,reject,pdcrs,moca,...
    age,udprs3,depress,apati,sex,duration,insomnia,pdcrsfront,pdcrspost,rbd,rbd_id,attention_pdcrs,...
    vis_att,vis_wm,verbal_att,verbal_wm,phonemic_fluency,semantic_fluency,suatined_att,set_shifting,...
    post_visuospatial_abilities1,post_visuospatial_abilities2,post_visuospatial_abilities3,post_visuospatial_abilities4,post_visuospatial_abilities5,...
    naming,lang_compr,free_verb_memo,cue_verb_memo,free_verb_memo2,cue_verb_memo2,cue_verb_memo3,vis_memo,code_patient] = get_subjects_PD(whichsubject);

if savecsv == 1
    filename=['/home/fosco/Git/parkinson/rs_EEG_Barcelona/clean/Data/PSD_FFT_avgtrials.csv'];
    if exist(filename,'file')==2,delete(filename),end
    resultfileData = fopen(filename,'at');
end


for isub = 1:length(who_idx)
    
    % get cfgs
    cfg = get_cfg_Parkinson(ID{isub});
    
    % ----------------
    % Prepare data.
    % ----------------
    fprintf('\nNow importing subject %s, (number %d of %d to process).\n\n',ID{isub},isub,length(who_idx));
    
    % Load data set.
    EEG = [];
    EEG = pop_loadset('filename',[cfg.subject_name '_VisCleanAfterIca.set'],...
        'filepath',cfg.dir_eeg,'loadmode','all'); % _VisCleanAfterIca_amica.set
    
    % remove eog:
    [EEG,~] = pop_select(EEG,'nochannel',{'HEOG1' 'HEOG2' 'VEOG1' 'VEOG2' 'HEOG' 'VEOG'});
    
    % convert to ft:
    dataft = [];
    dataft = eeglab2fieldtrip(EEG,'preprocessing');
%     dataft.label=dataft.label';
    
    % Get global Power Spectrum:
    cfg            = [];
    cfg.method     = 'mtmfft';
    cfg.taper      = 'hanning';
%     cfg.tapsmofrq  = 1; % smoothing
    cfg.foi        = 1:0.5:45;
    cfg.pad        = 'nextpow2';
    
    if savecsv == 1
        power          = [];
        cfg.keeptrials = 'no';
        power = ft_freqanalysis(cfg,dataft);
    else
        cfg.keeptrials = 'no';
        if MH{isub} == 0
            isub1 = isub1 +1;
            power_nMH{isub1} = ft_freqanalysis(cfg,dataft);
        else
            isub2 = isub2 +1;
            power_MH{isub2} = ft_freqanalysis(cfg,dataft);
        end
    end

    
%     if strcmp(cfg.keeptrials,'yes') == 1
%         for ichan = 1:size(power.powspctrm,2)
%             for ifreq = 1:size(power.powspctrm,3)
%                 for itrial = 1:size(power.powspctrm,1)
%                     fprintf(resultfileData,'%f %d %f %d %s %d %d %d %d %d %d %d %d %d %s %s %f %f %d %d %d %d %d %d %d %d %s \n',...
%                         power.powspctrm(itrial,ichan,ifreq),...
%                         isub,power.freq(ifreq),MH{isub},patient{isub},LEDD{isub},PH{isub},Passage{isub},Tactile{isub},Visual1{isub},Visual2{isub},...
%                         Hallucinations{isub},Audio{isub},Olfactory{isub},ID{isub},power.label{ichan},age{isub},duration{isub},udprs3{isub},pdcrs{isub},...
%                         rbd{isub},itrial,pdcrsfront{isub},pdcrspost{isub},rbd_id{isub},attention_pdcrs{isub},code_patient{isub});
%                     
%                 end
%             end
%         end
%     else
if savecsv == 1
    if strcmp(cfg.keeptrials,'yes') == 1
        for ichan = 1:size(power.powspctrm,2)
            for ifreq = 1:size(power.powspctrm,3)
                for itrial = 1:size(power.powspctrm,1)
                    fprintf(resultfileData,'%f %d %f %d %s %d %d %d %d %d %d %s %s %f %f %d %d %d %d %d %s %d %d %d %d \n',...
                        power.powspctrm(itrial,ichan,ifreq),...
                        isub,power.freq(ifreq),MH{isub},patient{isub},LEDD{isub},PH{isub},Passage{isub},VH{isub},Paredolia{isub},...
                        Hallucinations{isub},ID{isub},EEG.chanlocs(ichan).labels,age{isub},duration{isub},udprs3{isub},pdcrs{isub},...
                        rbd{isub},pdcrsfront{isub},pdcrspost{isub},code_patient{isub},itrial,Tactile{isub},Audio{isub},Olfactory{isub});
                end
            end
        end
    else
        for ichan = 1:size(power.powspctrm,1)
            for ifreq = 1:size(power.powspctrm,2)
                fprintf(resultfileData,'%f %d %f %d %s %d %d %d %d %d %d %s %s %f %f %d %d %d %d %d %s %d %d %d \n',...
                    power.powspctrm(ichan,ifreq),...
                    isub,power.freq(ifreq),MH{isub},patient{isub},LEDD{isub},PH{isub},Passage{isub},VH{isub},Paredolia{isub},...
                    Hallucinations{isub},ID{isub},EEG.chanlocs(ichan).labels,age{isub},duration{isub},udprs3{isub},pdcrs{isub},...
                    rbd{isub},pdcrsfront{isub},pdcrspost{isub},code_patient{isub},Tactile{isub},Audio{isub},Olfactory{isub});
            end
        end
    end
end
end

if savecsv == 1
    fclose(resultfileData);
end
fprintf('\n you got the power...with great power comes great responsibility \n')

%% stat in fieldtrip
grandavg_nMH = {};
grandavg_MH  = {};

cfg = [];
cfg.keepindividual = 'yes';
cfg.foilim      = 'all';
cfg.parameter   = 'powspctrm';
cfg.channel     = 'all';
cfg.avgoverchan = 'no';
cfg.avgoverfreq = 'no';
grandavg_nMH   = ft_freqgrandaverage(cfg,power_nMH{:}); 
grandavg_MH    = ft_freqgrandaverage(cfg,power_MH{:});

for e = 1:length(power_nMH{1}.elec.label)
cfg             = [];
cfg.channel     = power_nMH{1}.elec.label{e};
cfg.method      = 'montecarlo';
cfg.statistic   = 'indepsamplesT';
cfg.alpha       =  0.05;
cfg.numrandomization = 5000;
cfg.layout      = 'elec1005.lay';
cfg.frequency   = 'all';
cfg.parameter   = 'powspctrm';
cfg.avgoverchan = 'no';
cfg.avgovertime = 'no';
cfg.avgoverfreq = 'no';
cfg.correcttail = 'alpha';

% parameters for clustering:
cfg.correctm         = 'cluster'; % cluster fdr holm max
cfg.clusteralpha     = 0.05;
cfg.clusterstatistic = 'maxsum'; % 'maxsum', 'maxsize', 'wcm' 
cfg.minnbchan        = [];
cfg.tail             = 0;
cfg.clustertail      = 0;
cfg_neighb           = [];
cfg.feedback         = 'yes';
cfg_neighb.method    = 'template';
cfg_neighb.template  = 'elec1005_neighb.mat';
neighbours           = ft_prepare_neighbours(cfg_neighb,dataft);
cfg.neighbours       = neighbours;

% define a matrix according to trials numbers:
Nsub = size(grandavg_nMH.powspctrm,1)+size(grandavg_MH.powspctrm,1);
cfg.design(1,1:Nsub) = [ones(1,size(grandavg_nMH.powspctrm,1))...
    2*ones(1,size(grandavg_MH.powspctrm,1))];
cfg.ivar = 1;

% Do stats:
stat   = [];
[stat] = ft_freqstatistics(cfg,grandavg_nMH,grandavg_MH);

grandavg_nMH.mask(e,:) = stat.mask;
grandavg_MH.mask(e,:)  = stat.mask;

end

cfg = [];
cfg.showlabels    = 'yes'; 
cfg.layout        = 'elec1005.lay';
cfg.maskparameter = 'mask';
cfg.maskstyle     = 'box';
cfg.parameter     = 'powspctrm';
ft_multiplotER(cfg,grandavg_nMH,grandavg_MH);
