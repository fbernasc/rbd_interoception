function PSD2R(whichsubject)

[who_idx,patient,ID,MH,MCI,MCIR1,MCIR3,Agonist,LEDD,Passage,PH,Tactile,Visual1,Visual2,Audio,Olfactory,Hallucinations,reject,pdcrs,moca,...
    age,udprs3,depress,apati,sex,duration,insomnia,pdcrsfront,pdcrspost,rbd,rbd_id,attention_pdcrs,...
    vis_att,vis_wm,verbal_att,verbal_wm,phonemic_fluency,semantic_fluency,suatined_att,set_shifting,...
    post_visuospatial_abilities1,post_visuospatial_abilities2,post_visuospatial_abilities3,post_visuospatial_abilities4,post_visuospatial_abilities5,...
    naming,lang_compr,free_verb_memo,cue_verb_memo,free_verb_memo2,cue_verb_memo2,cue_verb_memo3,vis_memo,code_patient] = get_subjects_PD(whichsubject);

freqbands = {[1 3];[4 7];[8 12];[13 30];[31 45]};
fblabels  = {'Delta [1 3]';'Theta [4 7]';'Alpha [8-12Hz]';'Beta [13 30Hz]';'Gamma [31-45Hz]'};

% filename=['/media/sv/Elements/Parkinson/Results/PD_PowerSpectrum_dpss.csv'];
filename=['/home/fosco/Git/parkinson/rs_EEG_Barcelona/Data/PD_PowerSpectrum_dpss.csv'];
if exist(filename,'file')==2,delete(filename),end
resultfileData = fopen(filename,'at');

for isub = 1:length(who_idx)
    
    EEG  = [];   
    data = [];
    
    % get cfgs
    cfg = get_cfg_Parkinson(ID{isub});     
    % --------------------------------------------------------------
    % Prepare data.
    % --------------------------------------------------------------
    fprintf('\nNow importing subject %s, (number %d of %d to process).\n\n',ID{isub},isub,length(who_idx));   
    
    % Load data set.
    EEG = pop_loadset('filename',[cfg.subject_name '_VisCleanAfterIca_amica.set'],...
        'filepath',cfg.dir_eeg,'loadmode','all');

    % remove eog:
    [EEG,com] = pop_select(EEG,'nochannel',{'HEOG1' 'HEOG2' 'VEOG1' 'VEOG2' 'HEOG' 'VEOG'});
    
    % convert to ft:
    dataft = eeglab2fieldtrip(EEG,'preprocessing');
    dataft.label=dataft.label';
    data = dataft;            
    
    % Compute Power:        
    cfg1 = [];
    cfg1.method  = 'mtmfft';
    cfg1.output  = 'pow';
    cfg1.channel = 'all';
    cfg1.taper   = 'dpss';
    cfg1.layout  = 'elec1005.lay';
    cfg1.trials  = 'all';
    cfg1.keeptrials = 'yes';
    cfg1.pad     = 'nextpow2';

    for freq = 1:length(freqbands)
        
        % this avg the frequency band of interest:
        cfg1.foi       = ((freqbands{freq}(2)-freqbands{freq}(1))/2)+freqbands{freq}(1);
        cfg1.tapsmofrq = ((freqbands{freq}(2)-freqbands{freq}(1))/2);
        Power = ft_freqanalysis(cfg1,data);
       
%         switch patient{isub}
%             case 'Control'
%                 condi = 'HC';
%             case 'Patient'
%                 if MH{isub} == 0 
%                     condi = 'PD_MH-';
%                 elseif MH{isub} == 1
%                     condi = 'PD_MH+';
%                 end
%         end
        
        if freq == 1
            freqlab = 'Delta';
        elseif freq == 2
            freqlab = 'Theta';
        elseif freq == 3
            freqlab = 'Alpha';
        elseif freq == 4
            freqlab = 'Beta';
        elseif freq == 5
            freqlab = 'Gamma';
        end

        for itrial = 1:size(Power.powspctrm,1)
            for ichan = 1:size(Power.powspctrm,2)
                fprintf(resultfileData,'%f %d %s %d %s %d %d %d %d %d %d %d %d %d %d %d %d %s %d %d %d %s %d %d \n',Power.powspctrm(itrial,ichan),...
                    isub,freqlab,MH{isub},patient{isub},MCI{isub},...
                    MCIR1{isub},MCIR3{isub},LEDD{isub},PH{isub},Passage{isub},Tactile{isub},Visual1{isub},Visual2{isub},...
                    Hallucinations{isub},Audio{isub},Olfactory{isub},ID{isub},itrial,ichan,reject{isub},data.label{ichan},moca{isub},code_patient{isub});
            end
        end
        
        clear condi Power
    end
    clear data
end
fclose(resultfileData);
fprintf('\n DONE...\n')
end


