% Here we compute scalp Connectivity, and other network analysis:
% fosco.bernasconi@gmail.com

clear all; close all;
whichsubject = [];

[who_idx,patient,ID,MH,MCI,MCIR1,MCIR3,Agonist,LEDD,Passage,PH,Tactile,Visual1,Visual2,Audio,Olfactory,Hallucinations,reject,pdcrs,moca,...
    age,udprs3,depress,apati,sex,duration,insomnia,pdcrsfront,pdcrspost,rbd,rbd_id] = get_subjects_PD(whichsubject);

filename=['/home/fbernasc/Git/parkinson/rs_EEG_Barcelona/Data/PD_Connectivity_MH_edge_betweenness.csv'];
if exist(filename,'file')==2,delete(filename),end
resultfileData = fopen(filename,'at');

freqbands = {[4 7];[8 12];[13 30];[31 45]};
fblabels  = {'Theta [4 7]';'Alpha [8-12Hz]';'Beta [13 30Hz]';'Gamma [31-45Hz]'};
cont   = 0;
PD     = 0;
cPDPH  = 0;
suj    = 0;
cPDnPH = 0;

pow     = 'powspctrm';
msr     = 'edge_betweenness';   % clustering_coef / degrees / transitivity / density / assortativity / betweenness /
method  = 'wpli_debiased';
complex = 'abs';
doLaplacian = 0;
ttest   = 1; % just compare the PDs

for isub = 1:length(who_idx)
    
    EEG = [];
    
    % get cfgs
    cfg = get_cfg_Parkinson(ID{isub});
    % --------------------------------------------------------------
    % Prepare data.
    % --------------------------------------------------------------
    fprintf('\nNow importing subject %s, (number %d of %d to process).\n\n',ID{isub},isub,length(who_idx));
    
    % Load data set.
    EEG = pop_loadset('filename',[cfg.subject_name '_VisCleanAfterIca.set'],...
        'filepath',cfg.dir_eeg,'loadmode','all');
    
    % remove eog:
    [EEG,~] = pop_select(EEG,'nochannel',{'HEOG1' 'HEOG2' 'VEOG1' 'VEOG2' 'HEOG' 'VEOG'});
    
    if doLaplacian == 1
        [surf_lap] = laplacian_perrinX(EEG.data,[EEG.chanlocs.X],[EEG.chanlocs.Y],[EEG.chanlocs.Z]);
        EEG.data = surf_lap;
    end
    
    % convert to ft:
    dataft = [];
    dataft = eeglab2fieldtrip(EEG,'preprocessing');
    dataft.label=dataft.label';
    
    if MH{isub} == 1 && strcmp(patient{isub},'PD') == 1
        cPDPH = cPDPH+1;        
    elseif MH{isub} == 0 && strcmp(patient{isub},'PD') == 1
        cPDnPH = cPDnPH +1;        
    elseif MH{isub} == 0  && strcmp(patient{isub},'HC') == 1
        cont = cont+1;
    end
    
    
    for freq = 1:length(freqbands)
        
        % Compute Power per frequency band (mean of the band):
        cfg1 = [];
        cfg1.method  = 'mtmfft';
        cfg1.output  = 'fourier';
        cfg1.channel = 'all';
        cfg1.taper   = 'dpss'; % dpss or hanning https://www.natmeg.se/MEEG_course2018/freqanalysis.html
        cfg1.layout  = 'elec1005.lay';
        cfg1.trials  = 'all';
        cfg1.pad     = 'nextpow2';
        cfg1.keeptrials = 'yes';
        
        Power = [];
        fd    = [];
        
        cfg1.foi       = ((freqbands{freq}(2)-freqbands{freq}(1))/2)+freqbands{freq}(1);
        cfg1.tapsmofrq = ((freqbands{freq}(2)-freqbands{freq}(1))/2);
        Power          = ft_freqanalysis(cfg1,dataft);
        
        % compute connectivity:
        cfg = [];
        cfg.method  = method;
        cfg.complex = complex;
        fd          = ft_connectivityanalysis(cfg,Power);
        fd.method   = method;
        fd.complex  = complex;
        
        % encode the absolute value if we deal with wpli / wpli_debiased
        if strcmp(fd.method,'wpli')==1 || strcmp(fd.method,'wpli_debiased')==1
            if isfield(fd,'wplispctrm'), fd.wplispctrm=abs(fd.wplispctrm);end
            if isfield(fd,'wplispctrm_debiased'), fd.wplispctrm_debiased=abs(fd.wplispctrm_debiased);end
        end
        
        % reencode the connectivity measure with a generic name 'conspctrm'
        fd.conspctrm=eval(['fd.' fd.method 'spctrm']);
        fd=rmfield(fd,[fd.method 'spctrm']);
        
        % get threshold based on connectivity values:
        network_full  = [];
        cfg           = [];
        cfg.method    = msr;
        cfg.parameter = 'conspctrm';
%         connectivitymat =  fd.conspctrm; % get upper part of matrix:
%         connectivitymat = nonzeros(triu(connectivitymat));% clear 1's from the diagonal:
%         connectivitymat(connectivitymat==1)=[];
%         connectivitymat(isnan(connectivitymat))=[];
%         cfg.threshold = 1*std(connectivitymat)+mean(connectivitymat);
        cfg.threshold = .1;
        network_full  = ft_networkanalysis(cfg,fd);
        
        if strcmp(msr,'clustering_coef')
            network_full.degrees = network_full.clustering_coef; % I do this to avoid changing the code below for each measure (e.g., clustering_coef).
        elseif strcmp(msr,'transitivity')
            network_full.degrees = network_full.transitivity; % I do this to avoid changing the code below for each measure (e.g., clustering_coef).
        elseif strcmp(msr,'density')
            network_full.degrees = network_full.density;
        elseif strcmp(msr,'assortativity')
            network_full.degrees = network_full.assortativity;
        elseif strcmp(msr,'betweenness')
            network_full.degrees = network_full.betweenness;
        elseif strcmp(msr,'edge_betweenness')
            network_full.degrees = network_full.edge_betweenness;
        end
        
        if patient{isub} == 'HC'
%             cont = cont+1;
            ConnectivityHC{cont,freq} = fd;
            %                     ConnectivityHCseed{cont,freq}  = fdSelect;
            ConnectivityindexHC{cont,freq} = network_full;
            ConnectivityHC{cont,freq}.dimord = 'chan_freq';
%             ConnectivityHCseed{cont,freq}.moca = moca{isub};
            suj = suj+1;
            typepat = 'HC';
        elseif patient{isub} == 'PD'
            suj = suj+1;
            if MH{isub} == 1
%                 cPDPH = cPDPH+1;
                ConnectivityPDMH{cPDPH,freq} = fd;
                ConnectivityindexPDMH{cPDPH,freq} = network_full;
                ConnectivityPDMH{cPDPH,freq}.dimord = 'chan_freq';
%                 ConnectivityPD_MHseed{cPDPH,freq}.moca = moca{isub};
                typepat = 'PDMH';
            elseif MH{isub} == 0
%                 cPDnPH = cPDnPH +1;
                ConnectivityPDnMH{cPDnPH,freq} = fd;
                ConnectivityindexPDnMH{cPDnPH,freq}  = network_full;
                ConnectivityPDnMH{cPDnPH,freq}.dimord = 'chan_freq';
%                 ConnectivityPDnoMHseed{cPDnPH,freq}.moca = moca{isub};
                typepat = 'PDnMH';
            end
        end
        
        if strcmp(msr,'transitivity') || strcmp(msr,'density') || strcmp(msr,'assortativity')
            for ichan = 1
                fprintf(resultfileData,'%d %d %s %s %s %d %d %f %d %d %d %d %d %d %f\n',network_full.degrees(ichan),...
                    isub,patient{isub},ID{isub},dataft.label{ichan},moca{isub},MH{isub},age{isub},...
                    pdcrsfront{isub},pdcrspost{isub},rbd{isub},rbd_id{isub},freq,pdcrs{isub},duration{isub});
            end
            
        else
            for ichan = 1:size(network_full.label,1)
                fprintf(resultfileData,'%d %d %s %s %s %d %d %f %d %d %d %d %d %d %f\n',network_full.degrees(ichan),...
                    isub,patient{isub},ID{isub},dataft.label{ichan},moca{isub},MH{isub},age{isub},...
                    pdcrsfront{isub},pdcrspost{isub},rbd{isub},rbd_id{isub},freq,pdcrs{isub},duration{isub});
            end
        end
    end
end

fclose(resultfileData);


%%

figpos = 1;
f=figure(1);
switch msr
    case 'connectivity'
%         in1 = ConnectivityHCseed;
        in2 = ConnectivityPDMH;
        in3 = ConnectivityPDnMH;
        param = 'conspctrm';
    case 'degrees'
%         in1 = ConnectivityindexHC;
        in2 = ConnectivityindexPDMH;
        in3 = ConnectivityindexPDnMH;
        param = 'degrees';
    case 'clustering_coef'
        in1 = ConnectivityindexHC;
%         in2 = ConnectivityindexPD_MH;
        in3 = ConnectivityindexPDnMH;
        param = 'clustering_coef';
    case 'transitivity'
%         in1 = ConnectivityindexHC;
        in2 = ConnectivityindexPDMH;
        in3 = ConnectivityindexPDnMH;
        param = 'transitivity';
end

for ifreq = 1:length(freqbands)
    % average maps for each group:
    cfg = [];
    cfg.keepindividual = 'yes';
    cfg.foilim      = 'all';
    cfg.parameter   = param;
    cfg.channel     = 'all';
    cfg.avgoverchan = 'no';
    cfg.avgoverfreq = 'no';
    
%     grandavg{:,1} = ft_freqgrandaverage(cfg,in1{:,ifreq}); % HC
    grandavg{:,1} = ft_freqgrandaverage(cfg,in2{:,ifreq}); % PD_PH
    grandavg{:,2} = ft_freqgrandaverage(cfg,in3{:,ifreq}); % PD
    
    if ttest == 1
        
        cfg             = [];
        cfg.channel     = 'all';
        cfg.method      = 'montecarlo';
        cfg.statistic   = 'indepsamplesT';
        cfg.alpha       =  0.05;
        cfg.numrandomization = 2000;
        cfg.layout      = 'elec1005.lay';
        cfg.parameter   = 'individual';
        cfg.avgoverchan = 'no';
        cfg.avgovertime = 'no';
        cfg.correcttail = 'alpha';

        % parameters for clustering:
        cfg.correctm         = 'cluster'; 
        cfg.clusteralpha     = 0.05;
        cfg.clusterstatistic = 'maxsum'; 
        cfg.minnbchan        = 2;
        cfg.tail             = 0;
        cfg.clustertail      = 0;
        cfg_neighb           = [];
        cfg_neighb.method    = 'template';
        neighbours           = ft_prepare_neighbours(cfg_neighb,dataft);
        cfg.neighbours       = neighbours;
        
        switch msr
            case 'connectivity'
                Nsub = size(grandavg{1,1}.conspctrm,1)+size(grandavg{1,2}.conspctrm,1);
                cfg.design(1,1:Nsub) = [1*ones(1,size(grandavg{1,1}.conspctrm,1)) 2*ones(1,size(grandavg{1,2}.conspctrm,1))];
            case 'degrees'
                Nsub = size(grandavg{1,1}.degrees,1)+size(grandavg{1,2}.degrees,1);
                cfg.design(1,1:Nsub) = [1*ones(1,size(grandavg{1,1}.degrees,1)) 2*ones(1,size(grandavg{1,2}.degrees,1))];
            case 'clustering_coef'
                Nsub = size(grandavg{1,1}.clustering_coef,1)+size(grandavg{1,3}.clustering_coef,1);
                cfg.design(1,1:Nsub) = [1*ones(1,size(grandavg{1,2}.clustering_coef,1)) 2*ones(1,size(grandavg{1,2}.clustering_coef,1))];
            case 'transitivity'
                Nsub = size(grandavg{1,1}.transitivity,1)+size(grandavg{1,2}.transitivity,1);
                cfg.design(1,1:Nsub) = [1*ones(1,size(grandavg{1,1}.transitivity,1)) 2*ones(1,size(grandavg{1,2}.transitivity,1))];
        end
        cfg.ivar = 1;
        
        % Do stats:
        [stat] = ft_freqstatistics(cfg,grandavg{:,1},grandavg{:,2});
    else
        
        cfg             = [];
        cfg.channel     = 'all';
        cfg.method      = 'montecarlo';
        cfg.statistic   = 'indepsamplesF';
        cfg.alpha       =  0.05;
        cfg.numrandomization = 5000;
        cfg.layout      = 'elec1005.lay';
        cfg.frequency   = 'all';
        cfg.parameter   = param;
        cfg.avgoverchan = 'no';
        cfg.avgovertime = 'no';
        cfg.avgoverfreq = 'no';
        %         cfg.correcttail = 'alpha';
        
        % parameters for clustering:
        cfg.correctm = 'holm'; % cluster fdr holm max
        
        switch msr
            case 'connectivity'
                Nsub = size(grandavg{1,1}.conspctrm,1) +size(grandavg{1,2}.conspctrm,1)+size(grandavg{1,3}.conspctrm,1);
                cfg.design(1,1:Nsub) = [ones(1,size(grandavg{1,1}.conspctrm,1))...
                    2*ones(1,size(grandavg{1,2}.conspctrm,1)) 3*ones(1,size(grandavg{1,3}.conspctrm,1))];
            case 'degrees'
                Nsub = size(grandavg{1,1}.degrees,1) +size(grandavg{1,2}.degrees,1)+size(grandavg{1,3}.degrees,1);
                cfg.design(1,1:Nsub) = [ones(1,size(grandavg{1,1}.degrees,1))...
                    2*ones(1,size(grandavg{1,2}.degrees,1)) 3*ones(1,size(grandavg{1,3}.degrees,1))];
            case 'clustering_coef'
                Nsub = size(grandavg{1,1}.clustering_coef,1) +size(grandavg{1,2}.clustering_coef,1)+size(grandavg{1,3}.clustering_coef,1);
                cfg.design(1,1:Nsub) = [ones(1,size(grandavg{1,1}.clustering_coef,1))...
                    2*ones(1,size(grandavg{1,2}.clustering_coef,1)) 3*ones(1,size(grandavg{1,3}.clustering_coef,1))];
            case 'transitivity'
                Nsub = size(grandavg{1,1}.transitivity,1) +size(grandavg{1,2}.transitivity,1)+size(grandavg{1,3}.transitivity,1);
                cfg.design(1,1:Nsub) = [ones(1,size(grandavg{1,1}.transitivity,1))...
                    2*ones(1,size(grandavg{1,2}.transitivity,1)) 3*ones(1,size(grandavg{1,3}.transitivity,1))];
        end
        cfg.ivar = 1;
        
        % Do stats:
        [stat] = ft_freqstatistics(cfg,grandavg{:,1},grandavg{:,2},grandavg{:,3});
        
        
    end
    
    
    tvalues{ifreq,:} = stat.stat;
    pvalues{ifreq,:} = stat.prob;
    mask{ifreq,:}    = stat.mask;
    
    
    % redo avg for ploting
    cfg = [];
    cfg.keepindividual = 'no';
    cfg.foilim      = 'all';
    cfg.parameter   = param;
    cfg.channel     = 'all';
    cfg.avgoverchan = 'no';
    cfg.avgoverfreq = 'no';
    
    grandavg1{:,1} = ft_freqgrandaverage(cfg,in1{:,ifreq});
    grandavg1{:,2} = ft_freqgrandaverage(cfg,in2{:,ifreq});
    grandavg1{:,3} = ft_freqgrandaverage(cfg,in3{:,ifreq});
    
    cfg = [];
    cfg.parameter =  param;
    cfg.layout    = 'elec1005.lay';
    cfg.comment   = 'no';
    cfg.colorbar  = 'no';
    cfg.zlim      = [5 10];
    
    if ttest == 1
        set(f,'Color','w');
        subplot(4,3,figpos)
        figpos = figpos +1;
        title(['PDPH + ' fblabels(ifreq) ' Hz']);
        ft_topoplotER(cfg,grandavg1{:,2});
        subplot(4,3,figpos)
        figpos = figpos +1;
        title(['PDPH - ' fblabels(ifreq) ' Hz']);
        ft_topoplotER(cfg,grandavg1{:,3});
        
        
        if isempty(stat.mask) == 0
            cfg.highlightchannel = [find(stat.mask)];
        elseif isempty(stat.mask) == 1
            cfg.highlightchannel = [];
        end
        
        cfg.parameter = 'stat';
        cfg.marker    = 'on';
        cfg.highlight = 'on';
        cfg.highlightsymbol = '*';
        cfg.highlightsize   = 6;
        cfg.zlim     = 'maxmin';
        cfg.comment  = 'no';
        cfg.colorbar = 'no';
        cfg.highlightcolor = [1 1 1];
        subplot(4,3,figpos)
        title(['t-values ' fblabels(ifreq) ' Hz']);
        figpos = figpos +1;
        ft_topoplotER(cfg,stat);
        ft_hastoolbox('brewermap', 1);         % ensure this toolbox is on the path
        colormap(flipud(brewermap(128,'RdBu')))
        
    else
        
        set(f,'Color','w');
        subplot(2,3,1)
        figpos = figpos +1;
        title(['PD+']);
        ft_topoplotER(cfg,grandavg1{:,2});
        subplot(2,3,2)
        figpos = figpos +1;
        title(['PD']);
        ft_topoplotER(cfg,grandavg1{:,3});
        subplot(2,3,3)
        figpos = figpos +1;
        title(['HC']); hold on;
        ft_topoplotER(cfg,grandavg1{:,1});
        if isempty(stat.mask) == 0
            cfg.highlightchannel = [find(stat.mask)];
        elseif isempty(stat.mask) == 1
            cfg.highlightchannel = [];
        end
        
        cfg.parameter = 'stat';
        cfg.marker    = 'on';
        cfg.highlight = 'on';
        cfg.highlightsymbol = '*';
        cfg.highlightsize   = 6;
        cfg.zlim     = 'maxmin';
        cfg.comment  = 'no';
        cfg.colorbar = 'no';
        cfg.highlightcolor = [1 1 1];
        subplot(2,3,5)
        title(['F -values']);
        figpos = figpos +1;
        ft_topoplotER(cfg,stat);
        ft_hastoolbox('brewermap', 1);         % ensure this toolbox is on the path
        colormap(flipud(brewermap(128,'RdBu')))
        
    end
    
    clear grandavg stat
end

%% Extract avg of significant cluster for theta:
sig = find(mask{1,:});

% Avg subjects:
for i = 1:cont
    m11(i,:) = mean(ConnectivityindexHC{i,1}.degrees(sig));
end

for i2 = 1:PDMCI
    m12(i2,:) = mean(ConnectivityindexPD_MH{i2,1}.degrees(sig));
end

for i3 = 1:PDnoMCI
    m13(i3,:) = mean(ConnectivityindexPDnoMH{i3,1}.degrees(sig));
end

figure(3);
ax1=subplot(1,3,1);
boxplot(m11)
title('HC')
ax2=subplot(1,3,2);
boxplot(m12)
title('PD+')
ax3=subplot(1,3,3);
boxplot(m13)
title('PD')
linkaxes([ax1,ax2,ax3],'xy')

% post-hoc t-test:
[h,p,ci,stats] = ttest2(m11,m12); % HC vs. PH
[h,p,ci,stats] = ttest2(m11,m13); % HC vs PD
[h,p,ci,stats] = ttest2(m12,m13); % PH vs PD





