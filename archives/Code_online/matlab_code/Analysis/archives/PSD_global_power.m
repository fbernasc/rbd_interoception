% fosco.bernasconi@gmail.com

clear all; close all;

whichsubject = [];

[who_idx,patient,ID,MH,MCI,MCIR1,MCIR3,Agonist,LEDD,Passage,PH,Tactile,Visual1,Visual2,Audio,Olfactory,Hallucinations,reject,pdcrs,moca,...
    age,udprs3,depress,apati,sex,duration,insomnia,pdcrsfront,pdcrspost,rbd,rbd_id,attention_pdcrs,...
    vis_att,vis_wm,verbal_att,verbal_wm,phonemic_fluency,semantic_fluency,suatined_att,set_shifting,...
    post_visuospatial_abilities1,post_visuospatial_abilities2,post_visuospatial_abilities3,post_visuospatial_abilities4,post_visuospatial_abilities5,...
    naming,lang_compr,free_verb_memo,cue_verb_memo,free_verb_memo2,cue_verb_memo2,cue_verb_memo3,vis_memo,code_patient] = get_subjects_PD(whichsubject);

% filename=['/home/fbernasc/Git/parkinson/rs_EEG_Barcelona/Data/PD_PowerSpectrum_hann_MH_new_final_1HzB.csv'];
% if exist(filename,'file')==2,delete(filename),end
% resultfileData = fopen(filename,'at');

%
hc    = 0;
pdmh  = 0;
pdnmh = 0;
excl  = 0;

for isub = 1:length(who_idx)
    
    % get cfgs
    cfg = get_cfg_Parkinson(ID{isub});
       
    if duration{isub}<= 10         
        if duration{isub}>=1.5
        
        % ----------------
        % Prepare data.
        % ----------------
        fprintf('\nNow importing subject %s, (number %d of %d to process).\n\n',ID{isub},isub,length(who_idx));
        
        % Load data set.
        EEG = [];
        EEG = pop_loadset('filename',[cfg.subject_name '_VisCleanAfterIcaB.set'],...
            'filepath',cfg.dir_eeg,'loadmode','all'); % _VisCleanAfterIca_amica.set
        
        % remove eog:
        [EEG,~] = pop_select(EEG,'nochannel',{'HEOG1' 'HEOG2' 'VEOG1' 'VEOG2' 'HEOG' 'VEOG'});
        
        % convert to ft:
        dataft = [];
        dataft = eeglab2fieldtrip(EEG,'preprocessing');
        dataft.label=dataft.label';
        
        % Get global Power Spectrum:
        cfg                         = [];
        cfg.method                  = 'mtmfft';
        cfg.taper                   = 'hanning';
        cfg.tapsmofrq               = 1; % smoothing
        cfg.foi                     = 1:1:45;
        cfg.keeptrials              = 'no';
        
        
        power = [];
        switch patient{isub}
            case 'HC'
                hc = hc+1;
                power = ft_freqanalysis(cfg,dataft);
%                 power.powspctrm = zscore(power.powspctrm,0,2);
                psd_HC{hc,:} = power;
                %                 [X,freqs] = my_fft_complex(EEG.data,2,EEG.srate,length(EEG.times)); %
                %                 psd_HC{psd1,:}.powspctrm = 10*log10(squeeze(mean((abs(X(:,1:89,:)).^2),3)));
            case 'PD'
                if MH{isub} == 1
                    pdmh = pdmh+1;
                    power = ft_freqanalysis(cfg,dataft);
%                     power.powspctrm = zscore(power.powspctrm,0,2);
                    psd_PDMH{pdmh,:}      = power;
                    age_PDMH(pdmh,:)      = age{isub};
                    duration_PDMH(pdmh,:) = duration{isub};
                    udprs3_PDMH(pdmh,:)   = udprs3{isub};
                    LEDD_PDMH(pdmh,:)     = LEDD{isub};
                    MH_PDMH(pdmh,:)       = MH{isub};
                    rbd_PDMH(pdmh,:)      = rbd{isub};
                    %                     [X,freqs] = my_fft_complex(EEG.data,2,EEG.srate,length(EEG.times)); %
                    %                     psd_PDPH{psd2,:}.powspctrm = 10*log10(squeeze(mean((abs(X(:,1:89,:)).^2),3)));
                elseif MH{isub} == 0
                    pdnmh = pdnmh+1;
                    power = ft_freqanalysis(cfg,dataft);
%                     power.powspctrm = zscore(power.powspctrm,0,2);
                    psd_PDnMH{pdnmh,:}      = power;
                    age_PDnMH(pdnmh,:)      = age{isub};
                    duration_PDnMH(pdnmh,:) = duration{isub};
                    udprs3_PDnMH(pdnmh,:)   = udprs3{isub};
                    LEDD_PDnMH(pdnmh,:)     = LEDD{isub};
                    MH_PDnMH(pdnmh,:)       = MH{isub};
                    rbd_PDnMH(pdnmh,:)      = rbd{isub};
                    %                     [X,freqs] = my_fft_complex(EEG.data,2,EEG.srate,length(EEG.times)); %
                    %                     psd_PD{psd3,:}.powspctrm = 10*log10(squeeze((mean((abs(X(:,1:89,:)).^2),3))));
                end
        end
        
        %     for ichan = 1:size(power.powspctrm,2)
        %         for ifreq = 1:size(power.powspctrm,3)
        %             for itrial = 1:size(power.powspctrm,1)
        %                 fprintf(resultfileData,'%f %d %d %d %s %d %d %d %d %d %d %d %d %d %s %s %f %f %d %d %d %d %d %d %d %d \n',...
        %                     power.powspctrm(itrial,ichan,ifreq),...
        %                     isub,power.freq(ifreq),MH{isub},patient{isub},LEDD{isub},PH{isub},Passage{isub},Tactile{isub},Visual1{isub},Visual2{isub},...
        %                     Hallucinations{isub},Audio{isub},Olfactory{isub},ID{isub},power.label{ichan},age{isub},duration{isub},udprs3{isub},pdcrs{isub},...
        %                     rbd{isub},itrial,pdcrsfront{isub},pdcrspost{isub},rbd_id{isub},attention_pdcrs{isub});
        %
        %             end
        %         end
        %     end
        
        
        %     end
        end
        else
    end
end

% fclose(resultfileData);
fprintf('\n you got the power... \n')

%%
filename=['/home/fbernasc/Git/parkinson/rs_EEG_Barcelona/Data/PD_Neuropsy_MH.csv'];
if exist(filename,'file')==2,delete(filename),end
resultfileData2 = fopen(filename,'at');

for isub = 1:length(who_idx)
    % get cfgs
    cfg = get_cfg_Parkinson(ID{isub});
    % --------------------------------------------------------------
    % Prepare data.
    % --------------------------------------------------------------
    fprintf('\nNow importing subject %s, (number %d of %d to process).\n\n',ID{isub},isub,length(who_idx));
    
    
    fprintf(resultfileData2,' %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %s %d  \n',...
        vis_att{isub},vis_wm{isub},verbal_att{isub},verbal_wm{isub},phonemic_fluency{isub},semantic_fluency{isub},suatined_att{isub},set_shifting{isub},...
        post_visuospatial_abilities1{isub},post_visuospatial_abilities2{isub},post_visuospatial_abilities3{isub},...
        post_visuospatial_abilities4{isub},post_visuospatial_abilities5{isub},...
        naming{isub},lang_compr{isub},free_verb_memo{isub},cue_verb_memo{isub},free_verb_memo2{isub},cue_verb_memo2{isub},cue_verb_memo3{isub},vis_memo{isub},...
        age{isub},MH{isub},ID{isub},duration{isub});
    
end

fclose(resultfileData2);

%%
% do grand avg
cfg = [];
cfg.keepindividual = 'no';
cfg.foilim      = 'all';
cfg.parameter   = 'powspctrm';
cfg.channel     = 'all';
cfg.avgoverchan = 'no';
cfg.avgoverfreq = 'no';

% grandavgHC    = ft_freqgrandaverage(cfg,psd_HC{:});
grandavgPDMH    = ft_freqgrandaverage(cfg,psd_PDMH{:}); % not MCI
grandavgPDnMH   = ft_freqgrandaverage(cfg,psd_PDnMH{:});    % yes MCI

cfg = [];
cfg.parameter       = 'powspctrm';
cfg.layout          = 'elec1005.lay';
cfg.showlabels      = 'yes';
cfg.xlim            = [1 45];

cfg = [];
cfg.keepindividual = 'yes';
cfg.foilim      = 'all';
cfg.parameter   = 'powspctrm';
cfg.channel     = 'all';
cfg.avgoverchan = 'no';
cfg.avgoverfreq = 'no';

grandavgPDMH2   = ft_freqgrandaverage(cfg,psd_PDMH{:});
grandavgPDnMH2  = ft_freqgrandaverage(cfg,psd_PDnMH{:});

% do stat
cfg             = [];
cfg.channel     = 'all';
cfg.method      = 'montecarlo';
cfg.statistic   = 'ft_statfun_indepsamplesT';
cfg.alpha       =  0.05;
cfg.numrandomization = 5000;
cfg.layout      = 'elec1005.lay';
cfg.frequency   = 'all';
cfg.parameter   = 'powspctrm';
cfg.avgoverchan = 'no';
cfg.avgovertime = 'no';
cfg.avgoverfreq = 'no';
cfg.correcttail = 'alpha';
cfg.foi = [4 12];

% parameters for clustering:
cfg.correctm    = 'cluster'; % cluster fdr holm max
cfg.clusteralpha     = 0.05;
cfg.clusterstatistic = 'maxsum'; % 'maxsum', 'maxsize', 'wcm'
cfg.minnbchan        = 1;
cfg.tail             = 0;
cfg.clustertail      = 0;
cfg_neighb           = [];
cfg.feedback         = 'yes';
cfg_neighb.method    = 'template';
cfg_neighb.template  = 'elec1005_neighb.mat';
neighbours           = ft_prepare_neighbours(cfg_neighb,dataft);
cfg.neighbours       = neighbours;

% define a matrix according to trials numbers:
Nsub = size(grandavgPDMH2.powspctrm,1) + size(grandavgPDnMH2.powspctrm,1);

cfg.design(1,1:Nsub) = [ones(1,size(grandavgPDMH2.powspctrm,1)) 2*ones(1,size(grandavgPDnMH2.powspctrm,1))];
cfg.ivar = 1;

% Do stats:
[stat] = ft_freqstatistics(cfg,grandavgPDMH2,grandavgPDnMH2);
%
% grandavgHC.mask = stat.mask;
% grandavgPDPH.mask = stat.mask;
% grandavgPD.mask = stat.mask;
%
% cfg = [];
% cfg.showlabels    = 'yes';
% cfg.layout        = 'elec1005.lay';
% cfg.maskparameter = 'mask';
% cfg.maskstyle     = 'box';
% cfg.parameter     = 'powspctrm';
% ft_multiplotER(cfg,grandavgHC,grandavgPDPH,grandavgPD);
