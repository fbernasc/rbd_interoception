% clear all; close all; clc;

clear all
whichsubject = [];
whichdata    = '_cleanica_wtecg_1hz_early.set'; % select between _import_ekg and _import_continous

[who_idx,code_patient,patient,RBD,PSG_sess1,PSG_sess2,Gr,match,newID] = get_subjects_RBD(whichsubject);

cnt = 0;

for isub = 1:length(who_idx)
    
    % -------------
    % Prepare data.
    % -------------
    
    cfg = get_cfg_RBD(code_patient{isub});
    EEG = [];
    
    cnt = cnt+1;
    % -------------
    % Prepare data.
    % -------------
    cfg = get_cfg_RBD(code_patient{isub});
    EEG = [];
    
    
    % Write a status message to the command line.
    EEG = pop_loadset('filename',[cfg.subject_name whichdata ],'filepath',cfg.dir_eeg,'loadmode','all');
    
    % remove EOG:
    veog = find(contains({EEG.chanlocs.labels},'HEOG'));
    heog = find(contains({EEG.chanlocs.labels},'VEOG'));
%     ecg2 = find(contains({EEG.chanlocs.labels},'ECG2+'));
    EEG  = pop_select(EEG,'nochannel',[veog heog]);
    
    % select only chans of interest:
    chansel = find(contains({EEG.chanlocs.labels},{'Fp1','Fp2','C3','C4','ECG'}));
    EEG  = pop_select(EEG,'channel',[chansel]);
    
    dataft =[];
    data1  =[];
    dataft = eeglab2fieldtrip(EEG,'preprocessing');
    dataft.label=dataft.label';
    data1 = dataft;
    
    cfg = [];
    cfg.removemean = 'no';
    cfg.keeptrials = 'no';
    dataout{cnt} = ft_timelockanalysis(cfg,data1);
    dataout{cnt}.group = Gr(isub);
    
    % normalize data:
    for c = 1:size(dataout{cnt}.avg,1)
        dataout{cnt}.avgzscore(c,:) = zscore(dataout{cnt}.avg(c,:));
    end
    %
    %         cfg = [];
    %         cfg.baseline       = [-0.200 0];
    %         dataout{cnt,group} = ft_timelockbaseline(cfg,dataout{cnt,group});
    
    
end


%% prepare data:
dataAvg = [];
bla = 0;
bli = 0;
dataAvgIRB1 = {};
dataAvgIRB2 = {};
dataAvgnIRB = {};
cirb1 = 1;
cirb2 = 1;
cnirb = 1;

for isuj = 1:cnt
    if dataout{isuj}.group == 1
        dataAvgIRB1{cirb1} = dataout{isuj};
        cirb1 = cirb1 +1;
    elseif dataout{isuj}.group == 2
        dataAvgIRB2{cirb2} = dataout{isuj};
        cirb2 = cirb2 +1;
    elseif dataout{isuj}.group == 3
        dataAvgnIRB{cnirb} = dataout{isuj};
        cnirb = cnirb +1;
    end
end

% conf_colorSZ  = [83,169,212]./255;
% conf_colorSE  = [186,39,50]./255;
% 
% figure('Color','w')
% for ielect = 1:length({EEG.chanlocs.labels})
%     subplot(6,6,ielect)
%     title(['Electrode (' EEG.chanlocs(ielect).labels ')']);hold on;
%     cishade(squeeze(dataAvgIRB1(ielect,:)),[],conf_colorSZ,dataout{1}.time,'LineWidth',2); hold on;
%     cishade(squeeze(dataAvgnIRB(ielect,:)),[],conf_colorSE,dataout{1}.time,'LineWidth',2); hold on;
%     %     ylim([inf,sup])
%     %     gridxy(0,0,'Color',[0.4 0.04 0.4],'linestyle', '--')
% end

%% do stats:
clear grandavgstat dataoutbsl grandavgstat1 stat

%
% for the plot:
toi = [-0.2 0.6];
cfg = [];
cfg.keepindividual = 'no';
cfg.parameter      = 'avgzscore';
cfg.channel        = 'all';
cfg.avgoverchan    = 'no';
cfg.latency        = toi;
grandavgstat1{:,1} = ft_timelockgrandaverage(cfg,dataAvgIRB1{1,:});
grandavgstat1{:,2} = ft_timelockgrandaverage(cfg,dataAvgnIRB{1,:});


% prepare layout:
% cfg = [];
% cfg.rotate = 90;
% layout = ft_prepare_layout(cfg,dataout{1,1});
% ft_plot_layout(layout)

% for the stats:
cfg = [];
cfg.keepindividual = 'yes';
cfg.parameter      = 'avgzscore';
cfg.channel        = 'all';
cfg.avgoverchan    = 'no';
cfg.latency        = toi;
grandavgstat{:,1}  = ft_timelockgrandaverage(cfg,dataAvgIRB1{1,:});
grandavgstat{:,2}  = ft_timelockgrandaverage(cfg,dataAvgnIRB{1,:});

% t-test
for chan = 1:size(grandavgstat{:,1}.label,1)
    cfg             = [];
    cfg.channel     = grandavgstat{:,1}.label{chan};
    cfg.method      = 'montecarlo';
    cfg.statistic   = 'indepsamplesT';
    cfg.alpha       =  0.05;
    cfg.numrandomization = 2000;
    cfg.layout      =  '20e_layout1';
    cfg.parameter   = 'individual';
    cfg.avgoverchan = 'no';
    cfg.avgovertime = 'no';
    cfg.correcttail = 'alpha';
    
    
    cfg.correctm         = 'cluster';
    cfg.clusteralpha     = 0.05;
    cfg.clusterstatistic = 'maxsum';
    cfg.minnbchan        = 0;
    cfg.tail             = 0;
    cfg.clustertail      = 0;
    cfg_neighb           = [];
    cfg.neighbours       = [];
    
    % define a matrix according to trials numbers:
    Nsub = length([ones(1,size(dataAvgIRB1,2)),2*ones(1,size(dataAvgnIRB,2))]);
    cfg.design(1,1:Nsub) = [ones(1,size(dataAvgIRB1,2)),2*ones(1,size(dataAvgnIRB,2))];
    cfg.ivar = 1;
    stat{chan} = ft_timelockstatistics(cfg,grandavgstat{:,1},grandavgstat{:,2});
    grandavgstat1{:,1}.mask(chan,:) = stat{chan}.mask;
    grandavgstat1{:,1}.stat(chan,:) = stat{chan}.stat;
    grandavgstat1{:,2}.mask(chan,:) = stat{chan}.mask;
    
end

figure('Color','w')
cfg = [];
cfg.showlabels    = 'yes';
cfg.layout        = '20e_layout1';
cfg.maskparameter = 'mask';
cfg.maskstyle     = 'box';
cfg.channel       = 'all';
ft_multiplotER(cfg,grandavgstat1{:,1},grandavgstat1{:,2});

%%
% find significant time:
whichel = find([stat{6}.mask]);
t1 = stat{6}.time(whichel(1));
t2 = stat{6}.time(whichel(end));

cfg = [];
cfg.xlim = [t1 t2];
cfg.layout = '19e_layout1';
cfg.parameter = 'stat';
cfg.marker    = 'on';
cfg.comment  = 'no';
cfg.maskparameter = 'mask';
figure('Color','w');
ft_topoplotER(cfg,grandavgstat1{:,1}); colorbar


conf.color.SE   = [83,169,212]./255;
conf.color.SZ  = [186,39,50]./255;

inf  = -1.5;
sup  =  1.5;
tmin = -1;
tmax =  1;
e    = {'Fz'};
elec = find(ismember({EEG.chanlocs.labels},e));

signif = NaN(1,length(dataout{1,1}.time));
[c1]  = whichel(1);
[c2]  = whichel(end);
signif(c1:c2) = -1;

figure('Color','w')
subplot(1,2,1)
title(['HEP on ' EEG.chanlocs(elec).labels '']);hold on;
cishade(squeeze(dataAvgSE(:,elec,:)),[],conf.color.SE,dataout{1,1}.time,'LineWidth',2); hold on;
cishade(squeeze(dataAvgSZ(:,elec,:)),[],conf.color.SZ,dataout{1,1}.time,'LineWidth',2); hold on;
plot(dataout{1,1}.time,signif,'LineWidth',4);
ylim([inf,sup])
gridxy(0,0,'Color',[0.4 0.04 0.4],'linestyle', '--')
subplot(1,2,2)
title(['t-values']);hold on;
cfg = [];
cfg.xlim = [t1 t2];
cfg.layout = '19e_layout1';
cfg.parameter = 'stat';
cfg.marker    = 'on';
cfg.comment  = 'no';
cfg.maskparameter = 'mask';
cfg.colorbar = 'yes';
ft_topoplotER(cfg,grandavgstat1{:,1});
%     % so far it was the same as above, now change the colormap
%     ft_hastoolbox('brewermap', 1);         % ensure this toolbox is on the path
%     colormap(flipud(brewermap(128,'RdBu'))) % change the colormap

