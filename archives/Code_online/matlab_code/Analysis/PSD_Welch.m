clear all

% Load Data
whichsubject = [];
psds = [];
psds_avg = [];

cnt  = 0;

[who_idx,patient,ID,MH,MCI,MCIR1,MCIR3,Agonist,LEDD,Passage,PH,Tactile,VH,Paredolia,Audio,Olfactory,Hallucinations,reject,pdcrs,moca,...
    age,udprs3,depress,apati,sex,duration,insomnia,pdcrsfront,pdcrspost,rbd,rbd_id,attention_pdcrs,...
    vis_att,vis_wm,verbal_att,verbal_wm,phonemic_fluency,semantic_fluency,suatined_att,set_shifting,...
    post_visuospatial_abilities1,post_visuospatial_abilities2,post_visuospatial_abilities3,post_visuospatial_abilities4,post_visuospatial_abilities5,...
    naming,lang_compr,free_verb_memo,cue_verb_memo,free_verb_memo2,cue_verb_memo2,cue_verb_memo3,vis_memo,code_patient] = get_subjects_PD(whichsubject);


filename=['/home/fosco/Git/parkinson/rs_EEG_Barcelona/clean/Data/PSD_Welch_singletrials.csv'];
if exist(filename,'file')==2,delete(filename),end
resultfileData = fopen(filename,'at');



filename=['/home/fosco/Git/parkinson/rs_EEG_Barcelona/clean/Data/PSD_Welch_avgtrials.csv'];
if exist(filename,'file')==2,delete(filename),end
resultfileData2 = fopen(filename,'at');


for isub = 1:length(who_idx)
    
    % get cfgs
    cfg = get_cfg_Parkinson(ID{isub});
    
    % ----------------
    % Prepare data.
    % ----------------
    fprintf('\nNow importing subject %s, (number %d of %d to process).\n\n',ID{isub},isub,length(who_idx));
    
%     if duration{isub} <= 10 && duration{isub} >= 1.5
        cnt = cnt+1;
        % Load data set.
        EEG = [];
        EEG = pop_loadset('filename',[cfg.subject_name '_VisCleanAfterIca.set'],...
            'filepath',cfg.dir_eeg,'loadmode','all'); % _VisCleanAfterIca_amica.set
        
        % remove eog:
        [EEG,~] = pop_select(EEG,'nochannel',{'HEOG1' 'HEOG2' 'VEOG1' 'VEOG2' 'HEOG' 'VEOG'});
        
        %% Calculate Power Spectra
        psds_tmp     = [];
        freqs        = [];
        
        % Calculate power spectra with Welch's method https://groups.google.com/g/analyzingneuraltimeseriesdata/c/D7u1bcNvPNc
        %         EEG_test1093 = EEG.data(:,:,1)'; % transpose
        %         restEEG = EEG_test1093; % identify the experimental data you are working with
        %         fs=500; %sampling rate
        %         window=2500; %at a sampling rate of 500hz, 2500samples is 5 sec
        %         freqrange=1:40; %identify the frequency range you want to extract PSD in
        %
        %         % calculate PSDs useing pwelch.m
        %         [restPSD,freqs]=pwelch(restEEG,window,[],freqrange,fs); % the [] is to signal the default of 50% overlapping window
        
        psds_tmp = [];
        rsEEG = [];
        for t = 1:size(EEG.data,3)                        
            rsEEG = EEG.data(:,:,t)';
            [psds_tmp(t,:,:),freqs] = pwelch(rsEEG,2*EEG.srate,0,[],EEG.srate); % 2sec win (2*EEG.srate) & zero overlap;
        end
        
        
        for ichan = 1:size(EEG.data,1)
            for ifreq = 1:length(freqs)
                for itrial = 1:size(EEG.data,3)
                    fprintf(resultfileData,'%f %d %f %d %s %d %d %d %d %d %d %s %s %f %f %d %d %d %d %d %s %d %d %d %d \n',...
                    psds_tmp(itrial,ifreq,ichan),...
                        isub,freqs(ifreq),MH{isub},patient{isub},LEDD{isub},PH{isub},Passage{isub},VH{isub},Paredolia{isub},...
                        Hallucinations{isub},ID{isub},EEG.chanlocs(ichan).labels,age{isub},duration{isub},udprs3{isub},pdcrs{isub},...
                        rbd{isub},pdcrsfront{isub},pdcrspost{isub},code_patient{isub},itrial,Tactile{isub},Audio{isub},Olfactory{isub});
                end
            end
        end
        
        
        % avg over epochs:
        psds_tmp2 = [];
        psds_tmp2 = squeeze(mean(psds_tmp,1));
        for ichan = 1:size(EEG.data,1)
            for ifreq = 1:length(freqs)
                fprintf(resultfileData2,'%f %d %f %d %s %d %d %d %d %d %d %s %s %f %f %d %d %d %d %d %s %d %d %d \n',...
                    psds_tmp2(ifreq,ichan),...
                    isub,freqs(ifreq),MH{isub},patient{isub},LEDD{isub},PH{isub},Passage{isub},VH{isub},Paredolia{isub},...
                    Hallucinations{isub},ID{isub},EEG.chanlocs(ichan).labels,age{isub},duration{isub},udprs3{isub},pdcrs{isub},...
                    rbd{isub},pdcrsfront{isub},pdcrspost{isub},code_patient{isub},Tactile{isub},Audio{isub},Olfactory{isub});
            end
        end
        
        
%     end
end

fclose(resultfileData);
fclose(resultfileData2);
