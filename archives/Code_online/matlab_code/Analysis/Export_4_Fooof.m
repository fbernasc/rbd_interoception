%% Matlab PreProcessing - Multiple PSDs
% 
% This script is an example, with multiple power spectra,
% of integrating Python FOOOF into a Matlab workflow.
%
% It is part of a trio of files that must be run in order:
% - `MultiPSD_A_*
% - `MultiPSD_B_*
% - `MultiPSD_C_*
%
clear all


% Load Data
whichsubject = [];
psds         = [];
psds_avg     = [];
subjects     = [];
sub_c        = [];
subjects_avg = [];
chans      = [];
chans_c    = [];
chans_avg  = [];
halls      = [];
halls_c    = [];
MH_avg     = [];
fronts     = [];
fronts_c   = [];
fronts_avg = [];
psds_c     = [];
cluster    = {'frontal','central','temporal','parietal','occipital'};
cnt        = 0; 


[who_idx,patient,ID,MH,MCI,MCIR1,MCIR3,Agonist,LEDD,Passage,PH,Tactile,Visual1,Visual2,Audio,Olfactory,Hallucinations,reject,pdcrs,moca,...
    age,udprs3,depress,apati,sex,duration,insomnia,pdcrsfront,pdcrspost,rbd,rbd_id,attention_pdcrs,...
    vis_att,vis_wm,verbal_att,verbal_wm,phonemic_fluency,semantic_fluency,suatined_att,set_shifting,...
    post_visuospatial_abilities1,post_visuospatial_abilities2,post_visuospatial_abilities3,post_visuospatial_abilities4,post_visuospatial_abilities5,...
    naming,lang_compr,free_verb_memo,cue_verb_memo,free_verb_memo2,cue_verb_memo2,cue_verb_memo3,vis_memo,code_patient] = get_subjects_PD(whichsubject);

for isub = 1:length(who_idx)
    
    % get cfgs
    cfg = get_cfg_Parkinson(ID{isub});
    


    if duration{isub} <= 10 && duration{isub} >= 1.5   
        % ----------------
        % Prepare data.
        % ----------------
        fprintf('\nNow importing subject %s, (number %d of %d to process).\n\n',ID{isub},isub,length(who_idx));
        pause(0.5)
        cnt = cnt+1;
        % Load data set.
        EEG = [];
        EEG = pop_loadset('filename',[cfg.subject_name '_VisCleanAfterIca.set'],...
            'filepath',cfg.dir_eeg,'loadmode','all'); % _VisCleanAfterIca_amica.set

        % remove eog:
        [EEG,~] = pop_select(EEG,'nochannel',{'HEOG1' 'HEOG2' 'VEOG1' 'VEOG2' 'HEOG' 'VEOG'});

        %% Calculate Power Spectra
        psds_tmp     = [];
        freqs        = [];
        
        % Calculate power spectra with Welch's method https://groups.google.com/g/analyzingneuraltimeseriesdata/c/D7u1bcNvPNc
%         EEG_test1093 = EEG.data(:,:,1)'; % transpose 
%         restEEG = EEG_test1093; % identify the experimental data you are working with
%         fs=500; %sampling rate
%         window=2500; %at a sampling rate of 500hz, 2500samples is 5 sec
%         freqrange=1:40; %identify the frequency range you want to extract PSD in
% 
%         % calculate PSDs useing pwelch.m
%         [restPSD,freqs]=pwelch(restEEG,window,[],freqrange,fs); % the [] is to signal the default of 50% overlapping window
        
        psds_tmp = [];
        rsEEG    = [];
        
        % calculate power:
        for t = 1:size(EEG.data,3)         
            rsEEG = EEG.data(:,:,t)';
            [psds_tmp(t,:,:),freqs] = pwelch(rsEEG,2*EEG.srate,0,[],EEG.srate); % 2sec win (2*EEG.srate) & zero overlap; 
%             subject{t}  = ID{isub};
%             
        end
        
        % mean over trials & combine to previous subject psd:
        psds = [psds squeeze(mean(psds_tmp,1))];
        
        % creat ID vector (to use after):
        sub      = zeros(size(EEG.data,1),1);  
        sub(:)   = str2num(ID{isub}(3:end)); 
        subjects = [subjects;sub];
        
        % creat chan vector (to use after):
        chan     = zeros(size(EEG.data,1),1);  
        chan(:)   = 1:19; 
        chans = [chans;chan];
        
        % creat MH vector (to use after):
        hall      = zeros(size(EEG.data,1),1);  
        hall(:)   = MH{isub}; 
        halls = [halls;hall];
        
        % creat PD-CRS vector (to use after):
        front      = zeros(size(EEG.data,1),1);  
        front(:)  = pdcrsfront{isub}; 
        fronts    = [fronts;front];
        
        %%%%%%%%%%%%%%%%%%%%%
        % mean over electodes clusters:
        for c = 1:length(cluster)
            
            elect = [];
            if strcmp(cluster{c},'frontal') == 1
                elect = [1:7];
            elseif strcmp(cluster{c},'central') == 1
                elect = [9:11];
            elseif strcmp(cluster{c},'temporal') == 1
                elect = [8,12];
            elseif strcmp(cluster{c},'parietal') == 1
                elect = [13:17];
            elseif strcmp(cluster{c},'occipital') == 1
                elect = [18:19];
            end
            
            tmp_c    = [];
            tmp_c    = squeeze(mean(mean(psds_tmp(:,:,elect),1),3));
            psds_c   = [psds_c; squeeze(mean(mean(tmp_c,1),3))];

            % creat ID vector (to use after):
            sub_c  = [sub_c; str2num(ID{isub}(3:end))]; 
        
            % creat chan vector (to use after):
            chans_c = [chans_c;c];
            
            % creat MH vector (to use after):
            halls_c    = [halls_c;MH{isub}];
            
            % creat PD-CRS vector (to use after):
            fronts_c   = [fronts_c;pdcrsfront{isub}];
        end
        

        %%%%%%%%%%%%%%%%%%%%%
        % mean over electodes:
        psds_avg = [psds_avg; squeeze(mean(mean(psds_tmp,1),3))];
        
        % creat ID vector (to use after): 
        sub1 = str2num(ID{isub}(3:end)); 
        subjects_avg = [subjects_avg; sub1];
        
        % creat chan vector (to use after):
        chans_avg = [chans_avg;1];
        
        % creat chan vector (to use after):
        MH_avg  = [MH_avg; MH{isub}];
        
        % creat PD-CRS vector (to use after):
        fronts_avg  = [fronts_avg;pdcrsfront{isub}];
    end
end


% write csv for all chans:
all = [];
all = zeros(1+size(psds,2),4+length(freqs));
all(1,5:end) = round(freqs*2)/2; 
all(2:end,5:end) = psds';
all(2:end,1) = subjects';
all(2:end,2) = chans';
all(2:end,3) = halls';
all(2:end,4) = fronts';
all = num2cell(all);
all{1,1} = 'ID';
all{1,2} = 'Chan';
all{1,3} = 'MH';
all{1,4} = 'pdcrsf';
writecell(all,'/home/fosco/Git/parkinson/rs_EEG_Barcelona/clean/Data/rsEEG/fooof/power_spectra_PD_all_chan_suj.csv')

% write csv for clusters of chans:
all = [];
all = zeros(1+size(psds_c',2),4+length(freqs));
all(1,5:end) = round(freqs*2)/2; 
all(2:end,5:end) = psds_c;
all(2:end,1) = sub_c';
all(2:end,2) = chans_c';
all(2:end,3) = halls_c';
all(2:end,4) = fronts_c';
all = num2cell(all);
all{1,1} = 'ID';
all{1,2} = 'Chan';
all{1,3} = 'MH';
all{1,4} = 'pdcrsf';
writecell(all,'/home/fosco/Git/parkinson/rs_EEG_Barcelona/clean/Data/rsEEG/fooof/power_spectra_PD_clusters_suj.csv')


% write csv for avg of all chans:
avg = [];
avg = zeros(1+size(psds_avg,1),4+length(freqs));
avg(1,5:end) = round(freqs*2)/2; 
avg(2:end,5:end) = psds_avg;
avg(2:end,1) = subjects_avg';
avg(2:end,2) = chans_avg';
avg(2:end,3) = MH_avg';
avg = num2cell(avg);
avg{1,1} = 'ID';
avg{1,2} = 'Chan';
avg{1,3} = 'MH';
avg{1,4} = 'pdcrsf';
writecell(avg,'/home/fosco/Git/parkinson/rs_EEG_Barcelona/clean/Data/rsEEG/fooof/power_spectra_PD_avg_chan_suj.csv')

% % Save the power spectra out to mat files
% save('/home/fosco/Git/parkinson/rs_EEG_Barcelona/clean/Data/rsEEG/fooof/power_spectra_PD_all_chan_suj', 'freqs', 'psds');
% csvwrite('/home/fosco/Git/parkinson/rs_EEG_Barcelona/clean/Data/rsEEG/fooof/power_spectra_PD_all_chan_suj.csv', psd);
% csvwrite('/home/fosco/Git/parkinson/rs_EEG_Barcelona/clean/Data/rsEEG/fooof/power_spectra_PD_avg_chan_suj.csv', psd_avg);
% csvwrite_with_headers('/home/fosco/Git/parkinson/rs_EEG_Barcelona/clean/Data/rsEEG/fooof/freqs.csv',[round(freqs,1)],{'Hz'})
