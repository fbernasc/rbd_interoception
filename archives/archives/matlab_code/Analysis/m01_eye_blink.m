filename=['/home/fbernasc/Git/parkinson/rs_EEG_Barcelona/Data/PD_barcelona_eyeblink.csv'];
if exist(filename,'file')==2,delete(filename),end
resultfileData = fopen(filename,'at');

% which subjects you want to process?
whichsubject = [];

[who_idx,patient,ID,MH,MCI,MCIR1,MCIR3,Agonist,LEDD,Passage,PH,Tactile,Visual1,Visual2,Audio,Olfactory,Hallucinations,reject,pdcrs,moca,...
    age,udprs3,depress,apati,sex,duration,insomnia,pdcrsfront,pdcrspost,rbd,rbd_id] = get_subjects_PD(whichsubject);

addpath('/home/sv/Matlabtoolboxes/Functions/erplab_4.0.3.1')

for isub = 1:length(who_idx)

% get cfgs
cfg = get_cfg_Parkinson(ID{isub}); 
EEG = [];

% ---------------------------
% Import parameters & path(s)
% --------------------------- 
% Write a status message to the command line.
fprintf('\nNow importing subject %s, (number %d of %d to process).\n\n',ID{isub},isub,length(who_idx));    

% Create output directory if necessary.
if ~isdir(cfg.dir_eeg)
    mkdir(cfg.dir_eeg);
end

%-------------
% Import data
%-------------                  
eegData = [cfg.dir_raw ID{isub} 'S1RES1.eeg']; 

if ~exist(eegData,'file')
    error('%s Does not exist!\n',eegData)
else
    if cfg.do_import_ref
        fprintf('Importing %s\n',eegData)
        EEG = pop_biosig(eegData,'ref',cfg.ImportReference);
    else
        EEG = pop_biosig(eegData);
    end
end

% [EEG] = pop_chanedit(EEG,'lookup','/home/fbernasc/toolboxes/eeglab2019_0/plugins/dipfit2.3/standard_BESA/standard-10-5-cap385.elp',...
% 'load',{'//home/fbernasc/toolboxes/eeglab2019_0/sample_locs/Standard-10-20-Cap19.ced' 'filetype' 'autodetect'});

if ~isempty(cfg.heog_chans)
    fprintf('Computing HEOG from channels %s and %s\n', ...
        EEG.chanlocs(cfg.heog_chans(1)).labels, ...
        EEG.chanlocs(cfg.heog_chans(2)).labels)

    iHEOG = EEG.nbchan + 1;
    EEG.nbchan = iHEOG;
    EEG.chanlocs(iHEOG) = EEG.chanlocs(end);
    EEG.chanlocs(iHEOG).labels = 'HEOG';
    EEG.data(iHEOG,:) = EEG.data(cfg.heog_chans(1),:,:) - EEG.data(cfg.heog_chans(2),:,:);
end

if ~isempty(cfg.veog_chans)
    fprintf('Computing VEOG from channels %s and %s\n', ...
        EEG.chanlocs(cfg.veog_chans(1)).labels, ...
        EEG.chanlocs(cfg.veog_chans(2)).labels)

    iVEOG = EEG.nbchan + 1;
    EEG.nbchan = iVEOG;
    EEG.chanlocs(iVEOG) = EEG.chanlocs(end);
    EEG.chanlocs(iVEOG).labels = 'VEOG';
    EEG.data(iVEOG,:) = EEG.data(cfg.veog_chans(1),:,:) - EEG.data(cfg.veog_chans(2),:,:);
end

[EEG,com] = pop_eegfiltnew(EEG,1,45,[],0,[],0);
EEG       = eegh(com,EEG);

dat = eeglab2fieldtrip(EEG,'preprocessing');

cfg1 = [];
cfg1.continuous = 'yes';
cfg1.artfctdef.eog.bpfilter   = 'yes';
cfg1.artfctdef.eog.bpfilttype = 'but';
cfg1.artfctdef.eog.bpfreq     = [1 15];
cfg1.artfctdef.eog.bpfiltord  = 4;
cfg1.artfctdef.eog.hilbert    = 'yes';
cfg1.artfctdef.eog.channel    = [1 22 23 25]; % veog
[cfg1, artifact] = ft_artifact_eog(cfg1,dat);

fprintf(resultfileData,'%s %d %d %f %f %f %f %s %f %d %f %d %d %d \n',ID{isub},isub,size(artifact,1),EEG.xmax,((size(artifact,1))*60)./EEG.xmax,...
    moca{isub},PH{isub},patient{isub},Hallucinations{isub},MH{isub},duration{isub},pdcrsfront{isub},pdcrspost{isub},pdcrs{isub});

clear EEG artifact

end

fclose(resultfileData);
fprintf('Done.\n')
