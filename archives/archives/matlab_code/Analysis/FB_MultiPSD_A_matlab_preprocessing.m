%% Matlab PreProcessing - Multiple PSDs
% 
% This script is an example, with multiple power spectra,
% of integrating Python FOOOF into a Matlab workflow.
%
% It is part of a trio of files that must be run in order:
% - `MultiPSD_A_*
% - `MultiPSD_B_*
% - `MultiPSD_C_*
%
clear all
%% Load Data
whichsubject = [];
psds = [];
psds_avg = [];

cnt  = 0; 
[who_idx,patient,ID,MH,MCI,MCIR1,MCIR3,Agonist,LEDD,Passage,PH,Tactile,Visual1,Visual2,Audio,Olfactory,Hallucinations,reject,pdcrs,moca,...
    age,udprs3,depress,apati,sex,duration,insomnia,pdcrsfront,pdcrspost,rbd,rbd_id,attention_pdcrs,...
    vis_att,vis_wm,verbal_att,verbal_wm,phonemic_fluency,semantic_fluency,suatined_att,set_shifting,...
    post_visuospatial_abilities1,post_visuospatial_abilities2,post_visuospatial_abilities3,post_visuospatial_abilities4,post_visuospatial_abilities5,...
    naming,lang_compr,free_verb_memo,cue_verb_memo,free_verb_memo2,cue_verb_memo2,cue_verb_memo3,vis_memo,code_patient] = get_subjects_PD(whichsubject);

for isub = 1:length(who_idx)
    
    % get cfgs
    cfg = get_cfg_Parkinson(ID{isub});
    
    % ----------------
    % Prepare data.
    % ----------------
    fprintf('\nNow importing subject %s, (number %d of %d to process).\n\n',ID{isub},isub,length(who_idx));

    if duration{isub} <= 10 && duration{isub} >= 1.5
        cnt = cnt+1;
        % Load data set.
        EEG = [];
        EEG = pop_loadset('filename',[cfg.subject_name '_VisCleanAfterIca.set'],...
            'filepath',cfg.dir_eeg,'loadmode','all'); % _VisCleanAfterIca_amica.set

        % remove eog:
        [EEG,~] = pop_select(EEG,'nochannel',{'HEOG1' 'HEOG2' 'VEOG1' 'VEOG2' 'HEOG' 'VEOG'});

        %% Calculate Power Spectra
        psds_tmp     = [];
        freqs        = [];
        
        % Calculate power spectra with Welch's method https://groups.google.com/g/analyzingneuraltimeseriesdata/c/D7u1bcNvPNc
%         EEG_test1093 = EEG.data(:,:,1)'; % transpose 
%         restEEG = EEG_test1093; % identify the experimental data you are working with
%         fs=500; %sampling rate
%         window=2500; %at a sampling rate of 500hz, 2500samples is 5 sec
%         freqrange=1:40; %identify the frequency range you want to extract PSD in
% 
%         % calculate PSDs useing pwelch.m
%         [restPSD,freqs]=pwelch(restEEG,window,[],freqrange,fs); % the [] is to signal the default of 50% overlapping window
        
        psds_tmp = [];
        rsEEG    = [];
        for t = 1:size(EEG.data,3)         
            rsEEG = EEG.data(:,:,t)';
            [psds_tmp(t,:,:),freqs]=pwelch(rsEEG,2*EEG.srate,0,[],EEG.srate); % 2sec win (2*EEG.srate) & zero overlap; 
        end
        
        % combine
        psds = [psds squeeze(mean(psds_tmp,1))];
        
        % mean over electodes:
        psds_avg = [psds_avg; squeeze(mean(mean(psds_tmp,1),3))];
        
    end
end

csvwrite('/home/fosco/Git/parkinson/rs_EEG_Barcelona/clean/matlab_code/power_spectra_PD_all_chan_suj_Test.csv',psd)
T = array2table(psds');
T.Properties.VariableNames(1:2) = {'ID','Chan'};
T.Properties.VariableNames(3:end) = {num2str(freqs)};
writetable(T,'file1.csv')


% add header:
psd = zeros(1+size(psds,2),length(freqs));
psd(1,:) = freqs;
psd(2:end,:) = psds';

% avg:
psd_avg = zeros(1+size(psds_avg,1),length(freqs));
psd_avg(1,:) = freqs;
psd_avg(2:end,:) = psds_avg;

% Save the power spectra out to mat files
save('/home/fosco/Git/parkinson/rs_EEG_Barcelona/clean/Data/rsEEG/fooof/power_spectra_PD_all_chan_suj', 'freqs', 'psds');
csvwrite('/home/fosco/Git/parkinson/rs_EEG_Barcelona/clean/Data/rsEEG/fooof/power_spectra_PD_all_chan_suj.csv', psd);
csvwrite('/home/fosco/Git/parkinson/rs_EEG_Barcelona/clean/Data/rsEEG/fooof/power_spectra_PD_avg_chan_suj.csv', psd_avg);
csvwrite_with_headers('/home/fosco/Git/parkinson/rs_EEG_Barcelona/clean/Data/rsEEG/fooof/freqs.csv',[round(freqs,1)],{'Hz'})
