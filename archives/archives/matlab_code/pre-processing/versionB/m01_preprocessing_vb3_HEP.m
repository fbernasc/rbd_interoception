function m01_preprocessing_vb3_HEP(whichsubject)

addpath('./pre-processing');
addpath('./Functions');
addpath('./EEG/');

% which subjects you want to process?
[who_idx,code_patient,patient,RBD,PSG_sess1,PSG_sess2,Gr,match,newID,age,gender,center] = get_subjects_RBD(whichsubject);
addpath('/home/fosco/toolboxes/Functions/erplab_4.0.3.1')

for isub = 1:length(who_idx)
    
    % get cfgs
    cfg = get_cfg_RBD(code_patient{isub});
    EEG = [];
    
    % ---------------------------
    % Import parameters & path(s)
    % ---------------------------
    % Write a status message to the command line.
    fprintf('\n Now importing subject %s, (number %d of %d to process).\n\n',code_patient{isub},isub,length(who_idx));
    pause(1)
    
    % Create output directory if necessary.
    if ~isdir(cfg.dir_eeg)
        mkdir(cfg.dir_eeg);
    end
    
    %-------------
    % Import data
    %-------------
    eegData = [cfg.dir_raw code_patient{isub} '.edf'];
    
    if ~exist(eegData,'file')
        fprintf('Does not exist!\n',eegData)
    else
        if cfg.do_import_ref
            fprintf('Importing %s\n',eegData)
            EEG = pop_biosig(eegData,'ref',cfg.ImportReference);
        else
            EEG = pop_biosig(eegData);
        end
        
        if strcmp(center{isub},'Bern')
            chanekg = find(~cellfun(@isempty,regexp({EEG.chanlocs.labels},'ECG1-ECG2|ekg|ECG|EKG')));
            EEG.chanlocs(chanekg).labels = 'ECG';
            if EEG.srate>200
                EEG.srate = 200;
                EEG = eeg_checkset(EEG);
            end
        elseif strcmp(center{isub},'Parma')
            EEG = pop_resample(EEG,200);
            EEG = eeg_checkset(EEG);
            EEG.chanlocs_orig = EEG.chanlocs;
            
            chanekg = find(~cellfun(@isempty,regexp({EEG.chanlocs.labels},'ECG1-ECG2|ekg|ECG|EKG')));
            EEG.chanlocs(chanekg).labels = 'ECG';
            
            % sometimes the ECG electrodes are inverted:
            if any(strcmpi(code_patient{isub},{'n14','n3','n4','n6','n8','rbd18','rbd5'}))
                
            else
                EEG.data(chanekg,:) = -EEG.data(chanekg,:);
            end
            
            chaneog = find(~cellfun(@isempty,regexp({EEG.chanlocs.labels},'ROC-LOC')));
            
            if ~isempty(chaneog)
                EEG.chanlocs(chaneog).labels = 'EOG E1';
            else
                chaneogR = find(~cellfun(@isempty,regexp({EEG.chanlocs.labels},'EOG dx|LOC-A1|LOC|EOG-L')));
                chaneogL = find(~cellfun(@isempty,regexp({EEG.chanlocs.labels},'EOG sin|ROC-A2|ROC|EOG-R')));
                EEG.chanlocs(chaneogR).labels = 'EOG E1';
                EEG.data(chaneogR,:) = EEG.data(chaneogR,:) - EEG.data(chaneogL,:);
                
            end
            
            chanC4  = find(~cellfun(@isempty,regexp({EEG.chanlocs.labels},'C4-A1|C4A1')));
            chanC4b = find(~cellfun(@isempty,regexp({EEG.chanlocs.labels},'F4-C4')));
            
            chanC3  = find(~cellfun(@isempty,regexp({EEG.chanlocs.labels},'C3-A2|C3A2')));
            chanC3b = find(~cellfun(@isempty,regexp({EEG.chanlocs.labels},'F3-C3')));
            
            chanFp1  = find(~cellfun(@isempty,regexp({EEG.chanlocs.labels},'Fp1-F3')));
            chanFp2  = find(~cellfun(@isempty,regexp({EEG.chanlocs.labels},'Fp2-F4')));
            chanFC3 = find(~cellfun(@isempty,regexp({EEG.chanlocs.labels},'F3-C3')));
            chanFC4  = find(~cellfun(@isempty,regexp({EEG.chanlocs.labels},'F4-C4')));
            chanCP3  = find(~cellfun(@isempty,regexp({EEG.chanlocs.labels},'C3-P3')));
            chanCP4  = find(~cellfun(@isempty,regexp({EEG.chanlocs.labels},'C4-P4')));
            chanPO1  = find(~cellfun(@isempty,regexp({EEG.chanlocs.labels},'P4-O1')));
            chanPO2  = find(~cellfun(@isempty,regexp({EEG.chanlocs.labels},'P4-O2')));
            
            if ~isempty(chanC4)
                EEG.chanlocs(chanC4).labels = 'C4';
            elseif isempty(chanC4) && ~isempty(chanC4b)
                EEG.chanlocs(chanC4b).labels = 'FC4';
            end
            
            if ~isempty(chanC3)
                EEG.chanlocs(chanC3).labels = 'C3';
            elseif isempty(chanC3) && ~isempty(chanC3b)
                fprintf('\n\n [ stage 0 ] finding C3 \n\n');
                EEG.chanlocs(chanC3b).labels = 'FC3';
            end
            
            if ~isempty(chanFp1)
                EEG.chanlocs(chanFp1).labels = 'Fp1';
            end
            
            if ~isempty(chanFp2)
                EEG.chanlocs(chanFp2).labels = 'Fp2';
            end
            
            if ~isempty(chanFC3)
                EEG.chanlocs(chanFC3).labels = 'FC3';
            end
            
            if ~isempty(chanFC4)
                EEG.chanlocs(chanFC4).labels = 'FC4';
            end
            
            if ~isempty(chanCP3)
                EEG.chanlocs(chanCP3).labels = 'CP3';
            end
            
            if ~isempty(chanCP4)
                EEG.chanlocs(chanCP4).labels = 'CP4';
            end
            
            if ~isempty(chanPO1)
                EEG.chanlocs(chanPO1).labels = 'PO1';
            end
            
            if ~isempty(chanPO2)
                EEG.chanlocs(chanPO2).labels = 'PO2';
            end
        end
        
        
        %----------------------------------------
        % Shorten the data (e.g. from 5min to 15min)
        %----------------------------------------
        t1 = 10;   % from minute; the first part of the recording is empty or noisy
        t2 = 15;   % till minute
        EEG.datashort = [];
        EEG.datashort = EEG.data(:,((t1*60)*EEG.srate):((t2*60)*EEG.srate));
        EEG.data = [];
        EEG.data = EEG.datashort;
        EEG = eeg_checkset(EEG);
        EEG.timeselectonset = t1;
        EEG.timeselectoffst = t2;
        
        %-----
        % Find R-peak etc.
        %------------
        
        if cfg.genekgtrigger
            %% Process ECG
            ecgchan = [];
            ecgin   = [];
            ecgchan = strcmp({EEG.chanlocs.labels},'ECG');
            fprintf('\n\n [ stage 1 ] finding r-peaks in ECG \n\n');
            ecgin = EEG.data(ecgchan,:);
            
            % filter the EKG:
            % Filter order
            ord=2;
            ecgin = double(ecgin);
            
            % Filter
            Wn     = [3 30]/fix(EEG.srate/2);
            [B,A]  = butter (ord,Wn);
            ecgout = filtfilt(B,A,ecgin);
            ecgout = (ecgout-min(ecgout))/(max(ecgout)-min(ecgout));
            [qrspeaks,rpeaks] = findpeaks(zscore(ecgout),...
                'MinPeakProminence',3,'MinPeakDistance',0.2*EEG.srate);
            
            % calculate IBI:
            EEG.ekg_IBImean = [];
            EEG.ekg_IBIsd   = [];
            EEG.ekgsrate    = EEG.srate;
            EEG.ekg_IBIraw  = diff(rpeaks);
            EEG.ekg_IBImean = (mean(diff(rpeaks)))/EEG.ekgsrate; % interval in seconds
            EEG.ekg_IBIsd   = (std(diff(rpeaks)))/EEG.ekgsrate;
            %                 EEG.ekg_HR      = (length(rpeaks)/(length(EEG.times)/EEG.ekgsrate))*60;
            EEG.ekg_HR      = 60/EEG.ekg_IBImean; %beats per minute
            
            % calculate HRV
            rr_test_st2 = {};
            rr_test_st2.code = code_patient{isub};
            rr_test_st2.rr_interval = EEG.ekg_IBIraw./EEG.ekgsrate;
            rr_test_st2.rr_peaks    = rpeaks./EEG.ekgsrate;
            [hrv_feats_tb, hrv_feats_epochs_tb] = hrv_features(rr_test_st2);
            
            EEG.mean_NN   = hrv_feats_tb(:,2); % mean NN (normalised RR interval)
            EEG.SD_NN     = hrv_feats_tb(:,3); % standard deviation of NN
            EEG.VLF_power = hrv_feats_tb(:,4); % power in the very low frequency band (0.01 to 0.04 Hz)
            EEG.LF_power  = hrv_feats_tb(:,5); % power in the low frequency band (0.04 to 0.2 Hz)
            EEG.HF_power  = hrv_feats_tb(:,6); % power in the high frequency band (0.2 to 2 Hz)
            EEG.LF_HF_ratio = hrv_feats_tb(:,7); % LF_power:HF_power ratio
            
            %             figure
            %             plot((1:length(ecgout))/1000,zscore(ecgout))
            %             hold on
            %             xlabel('Seconds')
            %             plot(rpeaks/1000,qrspeaks,'ro')
            %             keyboard;
            %
            len = length(EEG.event);
            for i=1:length(rpeaks)
                e = len + i;
                EEG.event(e).latency = rpeaks(i);
                EEG.event(e).duration = 0;
                EEG.event(e).type = 'r-peak';
            end
            EEG = eeg_checkset(EEG, 'eventconsistency');
            
        end
        
        %-------------
        % filter data:
        %-------------
        EEG = pop_eegfiltnew(EEG,'locutoff',1,'plotfreqz',0);
        EEG = pop_eegfiltnew(EEG,'hicutoff',45,'plotfreqz',0);
        
        %------------
        % epoch data
        %------------
        if cfg.genekgtriggerepoch
            
            fprintf(['\n\n [ stage 4 ] epoching R-peaks \n\n']);
            
            [EEG, ~, com] = pop_epoch(EEG,{'r-peak'},[cfg.epoch_ekg_tmin cfg.epoch_ekg_tmax], ...
                'newname', 'BDF file epochs', 'epochinfo', 'yes');
            EEG = eegh(com, EEG);
            
            fprintf(['\n\n [ stage 5 ] I have ' num2str(EEG.nbchan)  ' electrodes \n\n']);
            pause(3)
            
        end
        
        %-----------------
        % remove artifacts
        %-----------------
        % find cortical chans:
        eegchans  = [];
        chansbin  = [];
        if strcmp(center{isub},'Bern')
            eegchans  = {'C3','C4','F3','F4','F7','F8','Fp1','Fp2','Fpz','Fz','O1','O2','P3','P4','Pz','T3','T4','T5','T6'}; % Oz
        elseif strcmp(center{isub},'Parma')
            eegchans  = {'C3','C4','Fp1','Fp2','FC3','FC4','CP3','CP4','PO1','PO2'};
        end
        chansbin  = ismember({EEG.chanlocs.labels},eegchans);
        chans     = find(chansbin);
        
        % store the nbr of electrodes in the structure:
        EEG.chaneeg = sum(chansbin);
        
        EEG_tmpdat = [];
        EEG_tmpdat = pop_select(EEG,'channel',[chans]);
        
        if strcmp(center{isub},'Bern')
            EEG_tmpdat = pop_chanedit(EEG_tmpdat, 'lookup','/home/fosco/toolboxes/eeglab2022.0/plugins/dipfit/standard_BEM/elec/standard_1005.elc');
        end
        
        
        % convert to fieldtrip to use their toolbox for visual insepction:
        % remove bad chanels:
        cfg.detectbadchanels = 1
        if cfg.detectbadchanels
            Index_fp1 = find(~cellfun(@isempty,regexp({EEG_tmpdat.chanlocs.labels},'Fp1')));
            Index_fp2 = find(~cellfun(@isempty,regexp({EEG_tmpdat.chanlocs.labels},'Fp2')));
            Index_fpz = find(~cellfun(@isempty,regexp({EEG_tmpdat.chanlocs.labels},'Fpz')));
            [badsensorsIndex,badepochIndex] = reject_chan_rbd(cfg,EEG_tmpdat,Index_fp1,Index_fp2,Index_fpz);
            [EEG,com]  = pop_select(EEG,'nochannel',{EEG.chanlocs(badsensorsIndex).labels});
            EEG = eegh(com,EEG);
            EEG = eeg_checkset(EEG);
            
            EEG_tmpdat = [];
            EEG_tmpdat = pop_select(EEG,'channel',[chans]);
        end
        
        %         keyboard
        
        cfg.detectbadepochs = 1
        if cfg.detectbadepochs
            Index_fp1 = find(~cellfun(@isempty,regexp({EEG_tmpdat.chanlocs.labels},'Fp1')));
            Index_fp2 = find(~cellfun(@isempty,regexp({EEG_tmpdat.chanlocs.labels},'Fp2')));
            Index_fpz = find(~cellfun(@isempty,regexp({EEG_tmpdat.chanlocs.labels},'Fpz')));
            [badsensorsIndex,badepochIndex] = reject_chan_rbd(cfg,EEG_tmpdat,Index_fp1,Index_fp2,Index_fpz);
            [EEG,com]  = pop_select(EEG,'notrial',badepochIndex');
            EEG = eegh(com,EEG);
            EEG = eeg_checkset(EEG);
        end
        
        %         keyboard
        
        % save chanlocs for later interpol:
        name = ['/home/fosco/Git_epfl/hep_rbd/Code/matlab_code/pre-processing/electrode_n' num2str(EEG_tmpdat.nbchan) '.mat'];
        chanlocs = EEG_tmpdat.chanlocs;
        save(name,'chanlocs')
        
        % --------------------------------------------------------------
        % Save data.
        % --------------------------------------------------------------
        [EEG, com] = pop_editset(EEG, 'setname', [cfg.subject_name ' import_ekg_versionb_early']);
        EEG = eegh(com, EEG);
        pop_saveset(EEG,[cfg.subject_name  '_import_ekg_versionb_early.set'] , cfg.dir_eeg);
        clear EEG
        
        
    end
end
fprintf('Done.\n')
