function m01_preprocessing_vb3(whichsubject)

addpath('./pre-processing');
addpath('./Functions');
addpath('./EEG/');

% which subjects you want to process?
[who_idx,code_patient,patient,RBD,PSG_sess1,PSG_sess2,Gr,match,newID,age,gender,center] = get_subjects_RBD(whichsubject);
addpath('/home/fosco/toolboxes/Functions/erplab_4.0.3.1')

for isub = 1:length(who_idx)
    
    % get cfgs
    cfg = get_cfg_RBD(code_patient{isub});
    EEG = [];
    
    % ---------------------------
    % Import parameters & path(s)
    % ---------------------------
    % Write a status message to the command line.
    fprintf('\n Now importing subject %s, (number %d of %d to process).\n\n',code_patient{isub},isub,length(who_idx));
    pause(1)
    
    % Create output directory if necessary.
    if ~isdir(cfg.dir_eeg)
        mkdir(cfg.dir_eeg);
    end
    
    %-------------
    % Import data
    %-------------
    eegData = [cfg.dir_raw code_patient{isub} '.edf'];
    
    if ~exist(eegData,'file')
        fprintf('Does not exist!\n',eegData)
    else
        if cfg.do_import_ref
            fprintf('Importing %s\n',eegData)
            EEG = pop_biosig(eegData,'ref',cfg.ImportReference);
        else
            EEG = pop_biosig(eegData);
        end
        
        if EEG.srate>200 && strcmp(center{isub},'Bern')
            EEG.srate = 200;
            EEG = eeg_checkset(EEG);
        elseif strcmp(center{isub},'Parma')
            EEG = pop_resample(EEG,200);
            EEG = eeg_checkset(EEG);
            EEG.chanlocs_orig = EEG.chanlocs;
            
            chanekg = find(~cellfun(@isempty,regexp({EEG.chanlocs.labels},'ECG1-ECG2|ekg|ECG|EKG')));
            EEG.chanlocs(chanekg).labels = 'EKG';
            
            % sometimes the ECG electrodes are inverted:
            if any(strcmpi(code_patient{isub},{'n14','n3','n4','n6','n8','rbd18','rbd5'}))
                
            else
                EEG.data(chanekg,:) = -EEG.data(chanekg,:);
            end
            
            chaneog = find(~cellfun(@isempty,regexp({EEG.chanlocs.labels},'ROC-LOC')));
            
            if ~isempty(chaneog)
                EEG.chanlocs(chaneog).labels = 'EOG E1';
            else         
               chaneogR = find(~cellfun(@isempty,regexp({EEG.chanlocs.labels},'EOG dx|LOC-A1|LOC|EOG-L'))); 
               chaneogL = find(~cellfun(@isempty,regexp({EEG.chanlocs.labels},'EOG sin|ROC-A2|ROC|EOG-R'))); 
               EEG.chanlocs(chaneogR).labels = 'EOG E1';
               EEG.data(chaneogR,:) = EEG.data(chaneogR,:) - EEG.data(chaneogL,:);

            end
            
            chanC4  = find(~cellfun(@isempty,regexp({EEG.chanlocs.labels},'C4-A1|C4A1')));
            chanC4b = find(~cellfun(@isempty,regexp({EEG.chanlocs.labels},'F4-C4')));
            
            chanC3  = find(~cellfun(@isempty,regexp({EEG.chanlocs.labels},'C3-A2|C3A2')));
            chanC3b = find(~cellfun(@isempty,regexp({EEG.chanlocs.labels},'F3-C3')));
            
            chanFp1  = find(~cellfun(@isempty,regexp({EEG.chanlocs.labels},'C3-A2|C3A2')));
            chanFp1b = find(~cellfun(@isempty,regexp({EEG.chanlocs.labels},'F3-C3')));
            
            chanFp2  = find(~cellfun(@isempty,regexp({EEG.chanlocs.labels},'C3-A2|C3A2')));
            chanFp2b = find(~cellfun(@isempty,regexp({EEG.chanlocs.labels},'F3-C3')));
            
            
            if ~isempty(chanC4)
                EEG.chanlocs(chanC4).labels = 'C4';
            elseif isempty(chanC4) && ~isempty(chanC4b) 
                EEG.chanlocs(chanC4b).labels = 'C4b';
            end
            
            if ~isempty(chanC3)
                EEG.chanlocs(chanC3).labels = 'C3';
            elseif isempty(chanC3) && ~isempty(chanC3b)
                fprintf('\n\n [ stage 0 ] finding C3 \n\n');
                EEG.chanlocs(chanC3b).labels = 'C3b';
            end
        end
        
        
        %----------------------------------------
        % Shorten the data (e.g. from 5min to 15min)
        %----------------------------------------
        t1 = 10;   % from minute; the first part of the recording is empty or noisy
        t2 = 15;   % till minute
        EEG.datashort = [];
        EEG.datashort = EEG.data(:,((t1*60)*EEG.srate):((t2*60)*EEG.srate));
        EEG.data = [];
        EEG.data = EEG.datashort;
        EEG = eeg_checkset(EEG);
        EEG.timeselectonset = t1;
        EEG.timeselectoffst = t2;
        
        %-----
        % Find R-peak etc.
        %------------ 
                     
        if cfg.genekgtrigger
            %% Process ECG
            ecgchan = [];
            ecgin   = [];
            ecgchan = strcmp({EEG.chanlocs.labels},'EKG');
            fprintf('\n\n [ stage 1 ] finding r-peaks in ECG \n\n');
            ecgin = EEG.data(ecgchan,:);
            
            % filter the EKG:
            % Filter order
            ord=2;
            ecgin = double(ecgin);
            
            % Filter
            Wn     = [3 30]/fix(EEG.srate/2);
            [B,A]  = butter (ord,Wn);
            ecgout = filtfilt(B,A,ecgin);
            ecgout = (ecgout-min(ecgout))/(max(ecgout)-min(ecgout));
            [qrspeaks,rpeaks] = findpeaks(zscore(ecgout),...
                'MinPeakProminence',3,'MinPeakDistance',0.2*EEG.srate);
            
            % calculate IBI:
            EEG.ekg_IBImean = [];
            EEG.ekg_IBIsd   = [];
            EEG.ekgsrate    = EEG.srate;
            EEG.ekg_IBIraw  = diff(rpeaks);
            EEG.ekg_IBImean = (mean(diff(rpeaks)))/EEG.ekgsrate; % interval in seconds
            EEG.ekg_IBIsd   = (std(diff(rpeaks)))/EEG.ekgsrate;
            %                 EEG.ekg_HR      = (length(rpeaks)/(length(EEG.times)/EEG.ekgsrate))*60;
            EEG.ekg_HR      = 60/EEG.ekg_IBImean; %beats per minute
            
            % calculate HRV
            rr_test_st2 = {};
            rr_test_st2.code = code_patient{isub};
            rr_test_st2.rr_interval = EEG.ekg_IBIraw./EEG.ekgsrate;
            rr_test_st2.rr_peaks    = rpeaks./EEG.ekgsrate;
            [hrv_feats_tb, hrv_feats_epochs_tb] = hrv_features(rr_test_st2);
            
            EEG.mean_NN   = hrv_feats_tb(:,2); % mean NN (normalised RR interval)
            EEG.SD_NN     = hrv_feats_tb(:,3); % standard deviation of NN
            EEG.VLF_power = hrv_feats_tb(:,4); % power in the very low frequency band (0.01 to 0.04 Hz)
            EEG.LF_power  = hrv_feats_tb(:,5); % power in the low frequency band (0.04 to 0.2 Hz)
            EEG.HF_power  = hrv_feats_tb(:,6); % power in the high frequency band (0.2 to 2 Hz)
            EEG.LF_HF_ratio = hrv_feats_tb(:,7); % LF_power:HF_power ratio
                    
%             figure
%             plot((1:length(ecgout))/1000,zscore(ecgout))
%             hold on
%             xlabel('Seconds')
%             plot(rpeaks/1000,qrspeaks,'ro')
%             keyboard;
%             
            len = length(EEG.event);
            for i=1:length(rpeaks)
                e = len + i;
                EEG.event(e).latency = rpeaks(i);
                EEG.event(e).duration = 0;
                EEG.event(e).type = 'r-peak';
            end
            EEG = eeg_checkset(EEG, 'eventconsistency');
            
        end
        
        %-------------
        % filter data:
        %-------------
        EEG = pop_eegfiltnew(EEG,'locutoff',1,'plotfreqz',0);
        EEG = pop_eegfiltnew(EEG,'hicutoff',45,'plotfreqz',0);
        
        %--------------------
        % Remove bad channels
        %--------------------
        % find cortical chans:
        eegchans  = [];
        chansbin  = [];
        if strcmp(center{isub},'Bern')
            eegchans  = {'C3','C4','F3','F4','F7','F8','Fp1','Fp2','Fpz','Fz','O1','O2','P3','P4','Pz','T3','T4','T5','T6'}; % Oz
        elseif strcmp(center{isub},'Parma')
            eegchans  = {'C3','C4'}; 
        end
        chansbin  = ismember({EEG.chanlocs.labels},eegchans);
        chans     = find(chansbin);
        
        % store the nbr of electrodes in the structure:
        EEG.chaneeg = sum(chansbin);
        
        EEGtmp = [];
        EEGtmp = pop_select(EEG,'channel',[chans]);
        
        if strcmp(center{isub},'Bern')
            EEGtmp = pop_chanedit(EEGtmp, 'lookup','/home/fosco/toolboxes/eeglab2022.0/plugins/dipfit/standard_BEM/elec/standard_1005.elc');
        end
        
        % save chanlocs for later interpol:
        name = ['/home/fosco/Git_epfl/hep_rbd/Code/matlab_code/pre-processing/electrode_n' num2str(EEGtmp.nbchan) '.mat'];
        chanlocs = EEGtmp.chanlocs;
        save(name,'chanlocs')
             
        
        %-----------
        % epoch data
        %-----------
        if cfg.genfaketriggerepoch
            EEGcont = [];
            EEGcont = EEG;
            if cfg.genfaketrigger
                fprintf(['\n\n [ stage 3 ] segmenting continous data \n\n']);
                t = cfg.epoch_tmax;
                n = 0;
                for l = 1:length(EEGcont.times)
                    if l == 1 || (((t*EEGcont.srate)* round(double(l)/(t*EEGcont.srate)) == l )) == 1
                        n = n+1;
                        EEGcont.event(n).type    = 'trigger';
                        EEGcont.event(n).latency = l;
                        EEGcont.event(n).urevent = n;
                        
                    elseif l == (((EEGcont.event(end).latency - EEGcont.event(1).latency)/EEGcont.srate)*60*EEGcont.srate)
                        break
                        
                    end
                end
            end
            
            [EEGcont, ~, com] = pop_epoch(EEGcont,{cfg.triggernumber},[cfg.epoch_tmin cfg.epoch_tmax], ...
                'newname', 'BDF file epochs', 'epochinfo', 'yes');
            EEGcont = eegh(com, EEGcont);
            
            % --------------------------------------------------------------
            % Save data.
            % --------------------------------------------------------------
            [EEGcont, com] = pop_editset(EEGcont, 'setname', [cfg.subject_name ' import_ekg_versionb_early']);
            EEGcont = eegh(com, EEGcont);
            pop_saveset(EEGcont,[cfg.subject_name  '_import_continous_versionb_early.set'] , cfg.dir_eeg);
            clear EEGcont
        end
        
        % convert to fieldtrip to use their toolbox for visual insepction:
        dataft = [];
        dataft = eeglab2fieldtrip(EEG,'preprocessing');
        cfg          = [];
        cfg.method   = 'summary';
        dummy        = ft_rejectvisual(cfg,dataft);

        %
        pause(3)
        
            
     
    end
end
fprintf('Done.\n')
