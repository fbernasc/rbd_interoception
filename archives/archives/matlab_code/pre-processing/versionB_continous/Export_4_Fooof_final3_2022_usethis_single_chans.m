%% Matlab PreProcessing - Multiple PSDs
clear all

% which freq method:
dowelch    = 0;
dofft      = 1;

% Load Data
whichsubject = [];
psds         = [];
subjects     = [];
sub_c        = [];
chans_c      = {};
psds_c       = [];
RBD_c        = [];
PSG_sess1_c  = [];
PSG_sess2_c  = [];
Gr_c         = [];
nbrChan_c    = [];

cnt   = 0;
cnt2  = 0;

[who_idx,code_patient,patient,RBD,PSG_sess1,PSG_sess2,Gr] = get_subjects_RBD(whichsubject);

for isub = 1:length(who_idx)
    
    % get cfgs
    cfg = get_cfg_RBD(code_patient{isub});
    
    
    
    % ----------------
    % Prepare data.
    % ----------------
    cnt = cnt+1;
    % Load data set.
    EEG = [];
    EEG = pop_loadset('filename',[cfg.subject_name '_cleanica_wittecg_1hz_early.set'],...
        'filepath',cfg.dir_eeg,'loadmode','all');
    
    % remove eog:
    [EEG,~] = pop_select(EEG,'nochannel',{'HEOG' 'VEOG' 'ECG'});
    
    
    
    %% Calculate Power Spectra
    psds_tmp     = [];
    freqs        = [];
    
    if dowelch == 1
        % Calculate power spectra with Welch's method
        % https://groups.google.com/g/analyzingneuraltimeseriesdata/c/D7u1bcNvPNc
        % https://zzz.bwh.harvard.edu/luna/img/cf/matlab.html
        rsEEG    = [];
        fftWindowLength  = 2; %in seconds
        fftWindowOverlap = 0; %fraction of 1.
        fftFreqRange     = [1:0.5:45];
        
        % calculate power:
        for t = 1:size(EEG.data,3)
            rsEEG = EEG.data(:,:,t)';
            [psds_tmp(t,:,:),freqs] = pwelch(rsEEG,fftWindowLength*EEG.srate,fftWindowOverlap,fftFreqRange,EEG.srate); % 2sec win (2*EEG.srate) & zero overlap;
        end
        
    elseif dofft
        dataft = [];
        dataft = eeglab2fieldtrip(EEG,'preprocessing');
        dataft.label=dataft.label';
        
        % Get global Power Spectrum:
        cfg                         = [];
        cfg.method                  = 'mtmfft';
        cfg.taper                   = 'hanning';
        cfg.tapsmofrq               = 1; % smoothing
        cfg.foi                     = 1:0.5:50;
        cfg.keeptrials              = 'no';
        power = ft_freqanalysis(cfg,dataft);
        freqs = power.freq;
        
    end
    
    %%%%%%%%%%%%%%%%%%%%%
    % loop over electodes:
    for c = 1:size(EEG.data,1)
        
        
        % mean power over trials & cluster of electrodes:
        if dowelch == 1
            tmp_c    = [];
            tmp_c    = squeeze(mean(psds_tmp(:,:,c),1));
            psds_c   = [psds_c;tmp_c];
            
        elseif dofft == 1
            tmp_c    = [];
            tmp_c    = squeeze(power.powspctrm(c,:));
            psds_c   = [psds_c;tmp_c];
        end
        
        % creat ID vector (to use after):
        sub_c  = [sub_c;who_idx(isub)];
        
        % creat chan vector (to use after):
        cnt2  = cnt2 +1;
        chans_c{cnt2} = EEG.chanlocs(c).labels;
        
        % creat RBD vector (to use after):
        RBD_c       = [RBD_c;RBD(isub)];
        PSG_sess1_c = [ PSG_sess1_c; PSG_sess1(isub)];
        PSG_sess2_c = [ PSG_sess2_c; PSG_sess2(isub)];
        Gr_c        = [ Gr_c; Gr(isub)];
        nbrChan_c   = [nbrChan_c;EEG.nbchan];               
        
    end
end


% T1 = array2table([round(freqs*2)/2]);
T1 = array2table(freqs);
T1.Properties.VariableNames(1) = {'Hz'};
if dowelch == 1
    writetable(T1,'/home/fosco/Git_epfl/hep_rbd/EEG/freqs_welch.csv')
elseif dofft == 1
    writetable(T1,'/home/fosco/Git_epfl/hep_rbd/EEG/freqs_fft_all.csv')
end
% writetable(T1,'/home/fosco/Git_epfl/pd_mh_eeg/Data/EEG/freqs.csv')

% write csv for clusters of chans:
all = [];
all = zeros(1+size(psds_c',2),7+length(freqs));
all(1,8:end) = round(freqs,2);
all(2:end,8:end) = psds_c;
all(2:end,1) = sub_c';
all(2:end,3) = RBD_c';
all(2:end,4) = PSG_sess1_c';
all(2:end,5) = PSG_sess2_c';
all(2:end,6) = Gr_c';
all(2:end,7) = nbrChan_c';


all = num2cell(all);
all{1,1} = 'ID';
all{1,2} = 'Chan';
all{1,3} = 'RBD';
all{1,4} = 'RBD_ses1';
all{1,5} = 'RBD_ses2';
all{1,6} = 'Group';
all{1,7} = 'nbrchan';

bi = 1;
for i = 2:(size(psds_c',2)+1)
    all{i,2} = chans_c{bi};
    bi = bi +1;
end

if dowelch == 1
    writecell(all,'/home/fosco/Git_epfl/hep_rbd/EEG/power_spectra_RBD_all_chans_welch.csv')
elseif dofft == 1
    writecell(all,'/home/fosco/Git_epfl/hep_rbd/EEG/power_spectra_RBD_all_chans_fft_all.csv')
end

