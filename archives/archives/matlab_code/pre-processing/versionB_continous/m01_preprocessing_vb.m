function m01_preprocessing_vb(whichsubject)

addpath('./pre-processing');
addpath('./Functions');
addpath('./EEG/');

% which subjects you want to process?
[who_idx,code_patient,patient,RBD,PSG_sess1,PSG_sess2,Gr] = get_subjects_RBD(whichsubject);
addpath('/home/fosco/toolboxes/Functions/erplab_4.0.3.1')

for isub = 1:length(who_idx)
    
    % get cfgs
    cfg = get_cfg_RBD(code_patient{isub});
    EEG = [];
    
    % ---------------------------
    % Import parameters & path(s)
    % ---------------------------
    % Write a status message to the command line.
    fprintf('\n Now importing subject %s, (number %d of %d to process).\n\n',code_patient{isub},isub,length(who_idx));
    pause(1)
    
    % Create output directory if necessary.
    if ~isdir(cfg.dir_eeg)
        mkdir(cfg.dir_eeg);
    end
    
    %-------------
    % Import data
    %-------------
    eegData = [cfg.dir_raw code_patient{isub} '.edf'];
    
    if ~exist(eegData,'file')
        fprintf('Does not exist!\n',eegData)
    else
        if cfg.do_import_ref
            fprintf('Importing %s\n',eegData)
            EEG = pop_biosig(eegData,'ref',cfg.ImportReference);
        else
            EEG = pop_biosig(eegData);
        end
        
        if EEG.srate>200
            EEG.srate = 200;
            EEG = eeg_checkset(EEG);
        end
        
        
        %----------------------------------------
        % Shorten the data (e.g. from 5min to 15min)
        %----------------------------------------
        t1 = 10;   % from minute; the first part of the recording is empty or noisy
        t2 = 15;   % till minute
        EEG.datashort = [];
        EEG.datashort = EEG.data(:,((t1*60)*EEG.srate):((t2*60)*EEG.srate));
        EEG.data = [];
        EEG.data = EEG.datashort;
        EEG = eeg_checkset(EEG);
        EEG.timeselectonset = t1;
        EEG.timeselectoffst = t2;
        
        %-----
        % Find R-peak etc.
        %------------ 
                     
        if cfg.genekgtrigger
            %% Process ECG
            ecgchan = [];
            ecgin   = [];
            ecgchan = strcmp({EEG.chanlocs.labels},'EKG');
            fprintf('\n\n [ stage 1 ] finding r-peaks in ECG \n\n');
            ecgin = EEG.data(ecgchan,:);
            
            % filter the EKG:
            % Filter order
            ord=2;
            ecgin = double(ecgin);
            
            % Filter
            Wn     = [3 30]/fix(EEG.srate/2);
            [B,A]  = butter (ord,Wn);
            ecgout = filtfilt(B,A,ecgin);
            ecgout = (ecgout-min(ecgout))/(max(ecgout)-min(ecgout));
            [qrspeaks,rpeaks] = findpeaks(zscore(ecgout),...
                'MinPeakProminence',3,'MinPeakDistance',0.2*EEG.srate);
            
            % calculate IBI:
            EEG.ekg_IBImean = [];
            EEG.ekg_IBIsd   = [];
            EEG.ekgsrate    = EEG.srate;
            EEG.ekg_IBIraw  = diff(rpeaks);
            EEG.ekg_IBImean = (mean(diff(rpeaks)))/EEG.ekgsrate; % interval in seconds
            EEG.ekg_IBIsd   = (std(diff(rpeaks)))/EEG.ekgsrate;
            %                 EEG.ekg_HR      = (length(rpeaks)/(length(EEG.times)/EEG.ekgsrate))*60;
            EEG.ekg_HR      = 60/EEG.ekg_IBImean; %beats per minute
            
            % calculate HRV
            rr_test_st2 = {};
            rr_test_st2.code = code_patient{isub};
            rr_test_st2.rr_interval = EEG.ekg_IBIraw./EEG.ekgsrate;
            rr_test_st2.rr_peaks    = rpeaks./EEG.ekgsrate;
            [hrv_feats_tb, hrv_feats_epochs_tb] = hrv_features(rr_test_st2);
            
            EEG.mean_NN   = hrv_feats_tb(:,2); % mean NN (normalised RR interval)
            EEG.SD_NN     = hrv_feats_tb(:,3); % standard deviation of NN
            EEG.VLF_power = hrv_feats_tb(:,4); % power in the very low frequency band (0.01 to 0.04 Hz)
            EEG.LF_power  = hrv_feats_tb(:,5); % power in the low frequency band (0.04 to 0.2 Hz)
            EEG.HF_power  = hrv_feats_tb(:,6); % power in the high frequency band (0.2 to 2 Hz)
            EEG.LF_HF_ratio = hrv_feats_tb(:,7); % LF_power:HF_power ratio
                    
            %             figure
            %             plot((1:length(ecgout))/1000,zscore(ecgout))
            %             hold on
            %             xlabel('Seconds')
            %             plot(rpeaks/1000,qrspeaks,'ro')
            %             keyboard;
            
            len = length(EEG.event);
            for i=1:length(rpeaks)
                e = len + i;
                EEG.event(e).latency = rpeaks(i);
                EEG.event(e).duration = 0;
                EEG.event(e).type = 'r-peak';
            end
            EEG = eeg_checkset(EEG, 'eventconsistency');
            
        end
        
        %-------------
        % filter data:
        %-------------
        EEG = pop_eegfiltnew(EEG,'locutoff',0.1,'plotfreqz',0);
        EEG = pop_eegfiltnew(EEG,'hicutoff',45,'plotfreqz',0);
        
        %--------------------
        % Remove bad channels
        %--------------------
        % find cortical chans:
        eegchans  = [];
        chansbin  = [];
        eegchans  = {'C3','C4','F3','F4','F7','F8','Fp1','Fp2','Fpz','Fz','O1','O2','P3','P4','Pz','T3','T4','T5','T6'}; % Oz
        chansbin  = ismember({EEG.chanlocs.labels},eegchans);
        chans     = find(chansbin);
        
        % store the nbr of electrodes in the structure:
        EEG.chaneeg = sum(chansbin);
        
        EEGtmp = [];
        EEGtmp = pop_select(EEG,'channel',[chans]);
        EEGtmp = pop_chanedit(EEGtmp, 'lookup','/home/fosco/toolboxes/eeglab2022.0/plugins/dipfit/standard_BEM/elec/standard_1005.elc');
        
        % save chanlocs for later interpol:
        name = ['/home/fosco/Git_epfl/hep_rbd/Code/matlab_code/pre-processing/electrode_n' num2str(EEGtmp.nbchan) '.mat'];
        chanlocs = EEGtmp.chanlocs;
        save(name,'chanlocs')
        
        % https://eeglab.org/tutorials/06_RejectArtifacts/cleanrawdata.html
        % https://sccn.ucsd.edu/githubwiki/files/cleanRawData_30thEEGLABWorkshop.pdf
        EEGtmp = pop_clean_rawdata(EEGtmp,...
            'FlatlineCriterion',5,...
            'ChannelCriterion',0.6,...
            'LineNoiseCriterion',4,...
            'Highpass','off',...
            'BurstCriterion','off',...
            'WindowCriterion','off',...
            'BurstRejection','off',...
            'Distance','Euclidian');
        
        %
        pause(3)
        
        which_diff = [];
        which_rm   = [];
        rm_chanID  = [];
        
        which_diff = find(~ismember(eegchans,{EEGtmp.chanlocs.labels})); % since not same electr
        which_rm   = eegchans{[which_diff]};
        rm_chanID  = find(contains({EEG.chanlocs.labels},which_rm));
        EEG = pop_select(EEG,'nochannel',[rm_chanID]);
        EEG = eeg_checkset( EEG );
        
        % clear data using ASR - just the bad periods
        eegchans  = [];
        chansbin  = [];
        eegchans  = {'C3','C4','F3','F4','F7','F8','Fp1','Fp2','Fpz','Fz','O1','O2','P3','P4','Pz','T3','T4','T5','T6','EKG','EOG E1','EOG E2'}; % Oz
        chansbin  = ismember({EEG.chanlocs.labels},eegchans);
        chans     = find(chansbin);
        
        EEG = pop_select(EEG,'channel',[chans]);
        EEG = pop_clean_rawdata(EEG, 'FlatlineCriterion','off',...
            'ChannelCriterion','off',...
            'LineNoiseCriterion','off',...
            'Highpass','off',...
            'BurstCriterion',20,...   % lower values are more agressive criteria
            'WindowCriterion',0.25,... % 0.05 (very aggressive) to 0.3 (very lax)
            'BurstRejection','on',...
            'Distance','Euclidian',...
            'WindowCriterionTolerances',[-Inf 10]);
        
        EEG = eeg_checkset( EEG );
        
        %-----------
        % epoch data
        %-----------
        if cfg.genfaketriggerepoch
            EEGcont = [];
            EEGcont = EEG;
            if cfg.genfaketrigger
                fprintf(['\n\n [ stage 3 ] segmenting continous data \n\n']);
                t = cfg.epoch_tmax;
                n = 0;
                for l = 1:length(EEGcont.times)
                    if l == 1 || (((t*EEGcont.srate)* round(double(l)/(t*EEGcont.srate)) == l )) == 1
                        n = n+1;
                        EEGcont.event(n).type    = 'trigger';
                        EEGcont.event(n).latency = l;
                        EEGcont.event(n).urevent = n;
                        
                    elseif l == (((EEGcont.event(end).latency - EEGcont.event(1).latency)/EEGcont.srate)*60*EEGcont.srate)
                        break
                        
                    end
                end
            end
            
            [EEGcont, ~, com] = pop_epoch(EEGcont,{cfg.triggernumber},[cfg.epoch_tmin cfg.epoch_tmax], ...
                'newname', 'BDF file epochs', 'epochinfo', 'yes');
            EEGcont = eegh(com, EEGcont);
            
            % --------------------------------------------------------------
            % Save data.
            % --------------------------------------------------------------
            [EEGcont, com] = pop_editset(EEGcont, 'setname', [cfg.subject_name ' import_ekg_versionb_early']);
            EEGcont = eegh(com, EEGcont);
            pop_saveset(EEGcont,[cfg.subject_name  '_import_continous_versionb_early.set'] , cfg.dir_eeg);
            clear EEGcont
        end
            
        if cfg.genekgtriggerepoch
            
            fprintf(['\n\n [ stage 4 ] epoching R-peaks \n\n']);
            
            [EEG, ~, com] = pop_epoch(EEG,{'r-peak'},[cfg.epoch_ekg_tmin cfg.epoch_ekg_tmax], ...
                'newname', 'BDF file epochs', 'epochinfo', 'yes');
            EEG = eegh(com, EEG);
            
            fprintf(['\n\n [ stage 5 ] I have ' num2str(EEG.nbchan)  ' electrodes \n\n']);
            pause(3)
            
            % --------------------------------------------------------------
            % Save data.
            % --------------------------------------------------------------
            [EEG, com] = pop_editset(EEG, 'setname', [cfg.subject_name ' import_ekg_versionb_early']);
            EEG = eegh(com, EEG);
            pop_saveset(EEG,[cfg.subject_name  '_import_ekg_versionb_early.set'] , cfg.dir_eeg);
            clear EEG
        end
    end
end
fprintf('Done.\n')
