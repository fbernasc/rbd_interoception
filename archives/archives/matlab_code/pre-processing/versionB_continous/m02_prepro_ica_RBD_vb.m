clear all

whichsubject = [];
whichdata = '_import_continous_versionb_early.set'; % select between _import_ekg and _import_continous
whichdataout = '_ica_continous_1hz_vb_early.set';

[who_idx,code_patient,patient,RBD,PSG_sess1,PSG_sess2,Gr] = get_subjects_RBD(whichsubject);

for isub = 1:length(who_idx)
    
    % Load CFG file. I know, eval is evil, but this way we allow the user
    % to give the CFG function any arbitrary name, as defined in the EP
    % struct.
    % get cfgs
    cfg = get_cfg_RBD(code_patient{isub});
    EEG = [];
    
    if exist([cfg.dir_eeg '/' cfg.subject_name whichdata],'file')
        
        fprintf('\nNow importing subject %s, (number %d of %d to process).\n\n',code_patient{isub},isub,length(who_idx));
        % --------------
        % Load data set.
        % --------------
        EEG = pop_loadset('filename', [cfg.subject_name whichdata] , ...
            'filepath', cfg.dir_eeg, 'loadmode', 'all');
        EEG = pop_chanedit(EEG, 'lookup','/home/fosco/toolboxes/eeglab2022.0/plugins/dipfit/standard_BEM/elec/standard_1005.elc');
        
        
        % interpolate electrodes that have been removed:
        if EEG.chaneeg == 19       
            load('electrode_n19.mat')
        elseif EEG.chaneeg == 6      
            load('electrode_n6.mat')
        end
        EEG = pop_interp(EEG,chanlocs,'spherical');
        pause(3)
        
        %---------------------------------------------------------
        % Average reference (only using EEG channel, i.e. no EXT)
        %---------------------------------------------------------
        % https://eeglab.org/tutorials/11_Scripting/automated_pipeline.html
        % https://github.com/LIMO-EEG-Toolbox/limo_meeg/blob/master/resources/from_bids2stats.m
        % center data first
        %         which_keep = find(contains({EEG.chanlocs.labels},{electrodes(3:end).labels})); % since not same electr
        % center data
        if cfg.do_recenter
            fprintf('\n\n [ stage 1 ] centering the epochs \n\n');
            EEG = pop_rmbase(EEG,[EEG.xmin*1000 EEG.xmax*1000]);
        end
        
        eegchans  = {'C3','C4','F3','F4','F7','F8','Fp1','Fp2','Fpz','Fz','O1','O2','Oz','P3','P4','Pz','T3','T4','T5','T6'};
        chans     = find(ismember({EEG.chanlocs.labels},eegchans));
        fprintf('\n\n [ stage 2 ] doing avg reference \n\n');
        EEG = pop_reref(EEG,{EEG.chanlocs(chans).labels},'keepref','on');
        
        
        % remove trials that have external stimuli:
        %         [EEG,com] = pop_eegthresh(EEG,1,12:30,-600,600,EEG.xmin, EEG.xmax,[],1);
        
        
        % filter the data at 1hz - better for ica
        EEG2 = pop_eegfiltnew(EEG,'locutoff',1,'plotfreqz',0);
        
        % ECG, HEOG and VEOG are going to be a nuisance for now. Save 1hz
        % HP-filtered version for later
        ecg  = squeeze(EEG2.data(strcmp({EEG2.chanlocs.labels},'EKG'),:,:));
        veog = squeeze(EEG2.data(strcmp({EEG2.chanlocs.labels},'EOG E1'),:,:));
        heog = squeeze(EEG2.data(strcmp({EEG2.chanlocs.labels},'EOG E2'),:,:));
        
        EEG.save_ecg   = ecg;
        EEG.save_heog  = heog;
        EEG.save_veog  = veog;
        EEG2.save_ecg  = ecg;
        EEG2.save_heog = heog;
        EEG2.save_veog = veog;
        
        
        EEG  = pop_select(EEG, 'channel',{EEG.chanlocs(chans).labels});
        EEG2 = pop_select(EEG2,'channel',{EEG2.chanlocs(chans).labels});
        
        
        %--------------
        % ICA with all
        %--------------
        
        % The following lines will first check that your data is of sufficient rank so as not to try to extract too many independent components. The next three lines are optional, but you are encouraged to use them.
        fprintf(['\n\n [ stage 3 ] ICA on ' num2str(EEG.nbchan)  ' electrodes \n\n'])
        pause(3)
        
        datsvd = svd(reshape(EEG2.data,[EEG.nbchan,EEG.trials*EEG.pnts]));
        % figure;plot(datsvd);
        n = sum(datsvd > 100);
        n = n-1; % beacause ECG removed for ICA but taking into account for RANK calcution so far
        [EEG2 com] = pop_runica(EEG2, 'icatype','runica', 'extended', 1, 'chanind',1:EEG.nbchan, 'pca',n);
        
        % pca
        %         fprintf('ICA based on variance.\n')
        %         [COEFF,SCORE,latent,tsquare] = pca(EEG2.data');
        %         % keep components corresponding to 99% of variance
        %         EEG2.num_components_to_keep = find(cumsum(latent) ./ sum(latent)>cfg.ica_kepvarianceperc,1)+1;   %change the value from 0.98-0.99-0.995-0.998-1, if sometimes the ICA is not done
        %         % run BINICA
        %         EEG2 = pop_runica(EEG2, 'icatype', 'binica' ,'pca', EEG2.num_components_to_keep, 'chanind', 1:length(EEG2.chanlocs));%, 'pca', length(EEG.chanlocs));,'pca', 8,
        
        
        
        %copy weight & sphere to original data
        EEG.icaweights  = EEG2.icaweights;
        EEG.icasphere   = EEG2.icasphere;
        EEG.icawinv     = EEG2.icawinv;
        EEG.icachansind = EEG2.icachansind;
        EEG.icaact      = EEG2.icaact;
        EEG = eeg_checkset(EEG);
        
        
        % Create output directory if necessary.
        if ~isdir(cfg.dir_eeg)
            system(sprintf('mkdir -p %s',cfg.dir_eeg));
        end
        EEG2 = pop_saveset(EEG2,[cfg.subject_name whichdataout], cfg.dir_eeg);
        EEG  = pop_saveset(EEG,[cfg.subject_name  whichdataout], cfg.dir_eeg);
    end
end
