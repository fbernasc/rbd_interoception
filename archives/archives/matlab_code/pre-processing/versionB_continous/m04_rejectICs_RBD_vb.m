% function m04_rejectICs_PD(whichsubject)

clear all
visual_ica   = 0;
whichsubject = [];
whichdata    = '_ica_continous_1hz_vb_early.set'; % select between _import_ekg and _import_continous
whichdataout = '_cleanica_wittecg_1hz_early.set';

[who_idx,code_patient,patient,RBD,PSG_sess1,PSG_sess2,Gr] = get_subjects_RBD(whichsubject);

for isub = 1:length(who_idx)
    
    % -------------
    % Prepare data.
    % -------------
    
    cfg = get_cfg_RBD(code_patient{isub});
    EEG = [];
    
    
    % Write a status message to the command line.
    EEG = pop_loadset('filename',[cfg.subject_name whichdata],'filepath',cfg.dir_eeg,'loadmode','all');
    
    % link electrodes position
    EEG = pop_chanedit(EEG, 'lookup','/home/fosco/toolboxes/eeglab2022.0/plugins/dipfit/standard_BEM/elec/standard_1005.elc');
    EEG.data(EEG.nbchan+1,:,:) = EEG.save_ecg;
    EEG.data(EEG.nbchan+2,:,:) = EEG.save_heog;
    EEG.data(EEG.nbchan+3,:,:) = EEG.save_veog;
    
    EEG.chanlocs(EEG.nbchan+1).labels = 'ECG';
    EEG.chanlocs(EEG.nbchan+2).labels = 'HEOG'; 
    EEG.chanlocs(EEG.nbchan+3).labels = 'VEOG'; 
    
    if visual_ica
        [EEG, com] = SASICA(EEG);
        keyboard
        EEG = evalin('base','EEG'); %SASICA stores the results in base workspace via assignin. So we have to use this workaround...
        EEG = eegh(com,EEG);
        
        fprintf(['\n I will remove ' num2str(sum(EEG.reject.gcompreject)) ' components \n']);
        ncomps = sum(EEG.reject.gcompreject);
        [EEG,com] = pop_subcomp(EEG,find(EEG.reject.gcompreject),1);
        
    else
        [EEG,com]= eeg_SASICA(EEG,'MARA_enable',0,...
            'FASTER_enable',1,'FASTER_blinkchanname','VEOG',...
            'ADJUST_enable',0,...
            'chancorr_enable',0,'chancorr_channames','ECG','chancorr_corthresh',0.6,... % 'auto4'
            'EOGcorr_enable',1,'EOGcorr_Heogchannames','HEOG','EOGcorr_corthreshH',0.6,... % 'auto4'
            'EOGcorr_Veogchannames','VEOG','EOGcorr_corthreshV',0.6,...  % 'auto4'
            'resvar_enable',0,'resvar_thresh',15,...
            'SNR_enable',0,'SNR_snrcut',1,'SNR_snrBL',[-Inf 0] ,'SNR_snrPOI',[0 Inf],...
            'trialfoc_enable',1,'trialfoc_focaltrialout','auto',...  
            'focalcomp_enable',0,'focalcomp_focalICAout','auto',...
            'autocorr_enable',1,'autocorr_autocorrint',20,...
            'autocorr_dropautocorr','auto','opts_noplot',1,...
            'opts_nocompute',0,'opts_FontSize',14);
        
        ncomps = sum(EEG.reject.gcompreject);
        
        %     keyboard;
        %     EEG = evalin('base','EEG');
        %     EEG = eegh(com,EEG);
        
        fprintf(['\n I will remove ' num2str(ncomps) ' components\n']);
        
        V_Components_to_remove = find(EEG.reject.gcompreject == 1);
        [EEG, com] = pop_subcomp(EEG, V_Components_to_remove);
        
    end
    if isempty(com)
        return
    end
    % ----------
    % Save data.
    % ----------
    EEG = pop_editset(EEG,'setname',[cfg.subject_name  whichdataout]);
    EEG = pop_saveset( EEG, [cfg.subject_name  whichdataout] , cfg.dir_eeg);
end
% fclose(resultfileData);
fprintf('Done.\n')
