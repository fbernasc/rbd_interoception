clear all

whichsubject = [];
whichdata = '_import_ekg.set'; % select between _import_ekg and _import_continous
[who_idx,code_patient,patient,IRB,PSG_sess] = get_subjects_RBD(whichsubject);


for isub = 1:length(who_idx)
    
    % Load CFG file. I know, eval is evil, but this way we allow the user
    % to give the CFG function any arbitrary name, as defined in the EP
    % struct.
    % get cfgs
    cfg = get_cfg_RBD(code_patient{isub});
    EEG = [];
        
    if exist([cfg.dir_eeg '/' cfg.subject_name(1:end-4) '_eeg_import_ekg.set'],'file')
        
        fprintf('\nNow importing subject %s, (number %d of %d to process).\n\n',code_patient{isub},isub,length(who_idx));
        % --------------
        % Load data set.
        % --------------
        EEG = pop_loadset('filename', [cfg.subject_name whichdata] , ...
            'filepath', cfg.dir_eeg, 'loadmode', 'all');
        
        % remove eogs, ekg and M1:
        [EEG_tmpdat,com] = pop_select(EEG,'nochannel',{'EOG E1' 'EOG E2' 'EKG' 'M1' 'M2'});
        
        % remove bad chanels:
        if cfg.detectbadchanels
            Index_veog = find(contains({EEG_tmpdat.chanlocs.labels},'VEOG'));
            Index_heog = find(contains({EEG_tmpdat.chanlocs.labels},'HEOG'));
            Index_veog2 = find(contains({EEG_tmpdat.chanlocs.labels},'Fp1'));
            Index_heog2 = find(contains({EEG_tmpdat.chanlocs.labels},'Fp2'));
            [badsensorsIndex,badepochIndex] = reject_chan(cfg,EEG_tmpdat,Index_veog,Index_heog,Index_veog2,Index_heog2);
            fprintf(['\n\n I found ' num2str(length(badsensorsIndex)) ' bad electrodes \n\n']);
        end
        
        if cfg.rejectbadepochs
            [EEG,com] = pop_select(EEG,'notrial',badepochIndex');
            EEG = eegh(com,EEG);
            EEG = eeg_checkset(EEG);
        end
        
        if cfg.rejectbadchanels
            if ~isempty(badsensorsIndex)
                [EEG,com]  = pop_select(EEG,'nochannel',{EEG.chanlocs(badsensorsIndex).labels});
                EEG = eegh(com, EEG);
                EEG = eeg_checkset(EEG);
            end
        end
        
        pause(2)
        
        % visual inspection:
        if cfg.do_visual_inspection_preICA
            
            col = cell(1,length(EEG_tmp.chanlocs));
            for ichan=1:length(EEG_tmp.chanlocs)
                col{ichan} = [0 0 0];
            end
            
            for ichan = badsensorsIndex'
                col{ichan} = [1 0 0];
            end
            
            global eegrej
            
            mypop_eegplot(EEG_tmp,1,1,1,'winlength',8,'color',col,'command','global eegrej, eegrej = TMPREJ');
            
            disp('Interrupting function now. Waiting for you to press')
            disp('"Update marks", and hit "Continue" in Matlab editor menu')
            
            keyboard
            
            % eegplot2trial cannot deal with multi-rejection
            if ~isempty(eegrej)
                trials_to_delete = [];
                badChnXtrl       = [];
                rejTime          = [];
                
                rejTime = eegrej(:,1:2);
                [~,firstOccurences,~] = unique(rejTime,'rows');
                eegrej = eegrej(firstOccurences,:);
                
                [badtrls, badChnXtrl] = eegplot2trial(eegrej,EEG.pnts,length(EEG.epoch));
                trials_to_delete = find(badtrls);
                clear eegrej;
                
                % -------------------------------------
                %  Execute SELECTIVE interpolation and rejection
                % -------------------------------------
                EEG = pop_selectiveinterp(EEG,badChnXtrl);
                [EEG, com] = pop_rejepoch(EEG, trials_to_delete, 1);
                EEG = eegh(com,EEG);
                
            end
            
            % ----------------------------------------
            %  Execute on all data set interpolation
            % ----------------------------------------
            %         % do you want to interpolate extra chans?
            %         interpchan = inputdlg('Channels to interpolate [numeric values]','Interpolate?');
            %         if~isempty(str2num(interpchan{:}))
            %             [EEG,com]  = pop_interp(EEG,str2num(interpchan{:}),'spherical');
            %             EEG = eegh(com, EEG);
            %         end
        end
        
        if cfg.rejectbadchanels
%             if ~isempty(badsensorsIndex)
%                 [EEG,com]  = pop_select(EEG,'nochannel',{EEG.chanlocs(badsensorsIndex).labels});
%                 EEG = eegh(com, EEG);
%                 EEG = eeg_checkset(EEG);
%             end
            if cfg.do_visual_inspection_preICA
                % do you want to reject more chans after visual inspection?
                [indx,tf] = listdlg('ListString',{EEG.chanlocs.labels});
                if~isempty(indx)
                    [EEG,com]  = pop_select(EEG,'nochannel',indx);
                    EEG = eegh(com, EEG);
                    EEG = eeg_checkset(EEG);
                end
            end
        end
        
        pause(3)
        
        
        % remove bad chanels:
        if cfg.do_reref_before_ica
            Index_veog = find(contains({EEG.chanlocs.labels},'VEOG'));
            Index_heog = find(contains({EEG.chanlocs.labels},'HEOG'));
            Index_veog2 = find(contains({EEG.chanlocs.labels},'EOG E1'));
            Index_heog2 = find(contains({EEG.chanlocs.labels},'EOG E2'));
            Index_m1 = find(contains({EEG.chanlocs.labels},'M1'));
            Index_m2 = find(contains({EEG.chanlocs.labels},'M2'));
            Index_ecg = find(contains({EEG.chanlocs.labels},'EKG'));
            [EEG, com] = pop_reref(EEG,[],'keepref','on','exclude',[Index_veog Index_heog Index_veog2 Index_heog2 Index_m1 Index_m2 Index_ecg]);
        end
        
        
        clear sensorvar level trial badtrials badsensorsIndex
        
        
        EEG = pop_editset(EEG, 'setname', [cfg.subject_name '_'  whichdata(8:11) '_CleanBeforeICA.set']);
        EEG = pop_saveset(EEG, [cfg.subject_name '_'  whichdata(8:11) '_CleanBeforeICA.set'], cfg.dir_eeg);
    end
end
