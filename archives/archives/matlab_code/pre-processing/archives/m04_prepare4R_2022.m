clear all; close all; clc;

addpath([pwd '/Functions/']);
% inpath   = ['/media/fbernasc/fosco.bernasconi/HEP_schizophrenia/preprocessing/cleanica5/'];
inpath = ['/media/fosco/fosco.bernasconi/HEP_schizophrenia/preprocessing_newb/cleanica_2022/']; % working version is cleanica5

outpath1 = ['/media/fosco/fosco.bernasconi/HEP_schizophrenia/preprocessing_newb/4R_2022/SE/'];
outpath2 = ['/media/fosco/fosco.bernasconi/HEP_schizophrenia/preprocessing_newb/4R_2022/SZ/'];
micromed.chanlocs = readlocs( 'micromed_cap.ced');

for group = 1:2
    if group == 1
        dirname_EEG = ([inpath 'SZ/']);
        finale_filename = '*_wecg*.set';
        filenames_EEG = dir([dirname_EEG finale_filename]);
        outpath = outpath2;
        patient = 1;
        
    elseif group == 2
        dirname_EEG = ([inpath 'SE/']);
        finale_filename = '*_wecg*.set';
        filenames_EEG = dir([dirname_EEG finale_filename]);
        outpath = outpath1;
        patient = 0;
    end
    
    for filenumber=1:length(filenames_EEG)
        
              
        % Create output directory if necessary.
        if ~isdir(outpath)
            system(sprintf('mkdir -p %s',outpath));
        end
        
        EEG = [];
        EEG = pop_loadset([dirname_EEG filenames_EEG(filenumber).name]);
        
%         EEG = pop_resample(EEG,EEG.srate/2);
        
        % remove EOG:
        veog = find(contains({EEG.chanlocs.labels},'EOG'));
        ecg1 = find(contains({EEG.chanlocs.labels},'ECG'));
        ecg2 = find(contains({EEG.chanlocs.labels},'ECG2+'));
        ext1 = find(contains({EEG.chanlocs.labels},'A1'));
        ext2 = find(contains({EEG.chanlocs.labels},'A2'));
        EEG  = pop_select(EEG,'nochannel',[veog ecg2 ext1 ext2]);
        
        
        EEG.chanlocs(1).type   = '';
        EEG.chanlocs(1).theta  = 0;
        EEG.chanlocs(1).radius = 0;
        EEG.chanlocs(1).X = 0;
        EEG.chanlocs(1).Y = 0;
        EEG.chanlocs(1).Z = 0;
        EEG.chanlocs(1).sph_theta  = 0;
        EEG.chanlocs(1).sph_phi    = 0;
        EEG.chanlocs(1).sph_radius = 0;
        EEG.chanlocs(1).urchan = 1;
        EEG.chanlocs(1).sph_theta_besa = 0;
        EEG.chanlocs(1).sph_phi_besa   = 0;
        
%         EEG.chanlocs = rmfield(EEG.chanlocs,'x');
%         EEG.chanlocs = rmfield(EEG.chanlocs,'y');
%         EEG.chanlocs = rmfield(EEG.chanlocs,'z');
        
        for e=1:length(EEG.epoch)
           
            EEG.newepoch(e).event = EEG.epoch(e).event(1);
            
            if iscell(EEG.epoch(e).eventtype) == 1
                EEG.newepoch(e).eventtype = str2num(EEG.epoch(e).eventtype{1});
            else
                EEG.newepoch(e).eventtype = str2num(EEG.epoch(e).eventtype(1));
            end
            
                        
            if iscell(EEG.epoch(e).eventlatency) == 1
                EEG.newepoch(e).eventlatency = EEG.epoch(e).eventlatency{1};
            else
                EEG.newepoch(e).eventlatency = EEG.epoch(e).eventlatency(1);
            end
            
            if iscell(EEG.epoch(e).eventurevent) == 1
                EEG.newepoch(e).eventurevent = EEG.epoch(e).eventurevent{1};
            else
                EEG.newepoch(e).eventurevent = EEG.epoch(e).eventurevent(1);
            end
            
            if iscell(EEG.epoch(e).eventduration) == 1
                EEG.newepoch(e).eventduration = EEG.epoch(e).eventduration{1};
            else
                EEG.newepoch(e).eventduration = EEG.epoch(e).eventduration(1);
            end
            
        end
        
        EEG.epoch = EEG.newepoch;
        EEG = rmfield(EEG,'newepoch');
        EEG = eeg_checkset(EEG);
        EEG = pop_editset(EEG,'setname',[filenames_EEG(filenumber).name(1:end-4) '_wecg_clean4R.set']);
        EEG = pop_saveset( EEG,[filenames_EEG(filenumber).name(1:end-4) '_wecg_clean4R.set'], outpath);
        
    end
end
