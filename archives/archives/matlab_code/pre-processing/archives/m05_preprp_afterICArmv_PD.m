function m05_preprp_afterICArmv_PD(whichsubject)

% filename=['media/fbernasc/Elements/Parkinson/rsEEG/Preprocessed/Number_of_rej_trials_subjects_v2.csv'];
% if exist(filename,'file')==2,delete(filename),end
% resultfileData = fopen(filename,'at');

[who_idx,patient,ID,MH,MCI,MCIR1,MCIR3,Agonist,LEDD,Passage,...
     PH,Tactile,Visual1,Visual2,Audio,Olfactory,Hallucinations,reject] = get_subjects_PD(whichsubject);

for isub = 1:length(who_idx)
    
    EEG = [];    
    
    % get cfgs
    cfg = get_cfg_Parkinson(ID{isub});    
    % --------------------------------------------------------------
    % Prepare data.
    % --------------------------------------------------------------
    fprintf('\nNow importing subject %s, (number %d of %d to process).\n\n',ID{isub},isub,length(who_idx));   
    
    % Load data set.
    EEG = pop_loadset('filename',[cfg.subject_name '_ICArej.set'],...
        'filepath',cfg.dir_eeg,'loadmode','all');
    
    % add if is a patient:
    if patient{isub}
        for itrial = 1:EEG.trials
            EEG.epoch(itrial).patient = patient{isub};
            EEG.epoch(itrial).MH  = MH{isub};
            EEG.epoch(itrial).MCI = MCI{isub};
            EEG.epoch(itrial).MCIR1 = MCIR1{isub};
            EEG.epoch(itrial).MCIR3 = MCIR3{isub};            
        end
    end
    EEG = eeg_checkset(EEG);    
    trinum1 = EEG.trials;
    
    % remove EOGs:
     [EEG,com] = pop_select(EEG,'nochannel',{'VEOG' 'HEOG'});

    % Interpolate channels:
    if cfg.interpolMissingChan
        EEGorig = pop_loadset('filename',[cfg.subject_name '_import.set'],...
        'filepath',[cfg.dir_eeg],'loadmode','all');
        originalEEG.chanlocs = EEGorig.chanlocs(1:19);
        [EEG,com] = pop_interp(EEG,originalEEG.chanlocs,'spherical');
        EEG = eegh(com, EEG);
    end
    
    [EEG,com] = pop_eegthresh(EEG,1,1:EEG.nbchan,-100,100,EEG.xmin, EEG.xmax, [], 1);
    trinum2 = EEG.trials;
    trinum3 = trinum1 - trinum2;
    
    pause(2)
    
    
    if cfg.do_visual_inspection_postICA
        
        %visual inspection:
        global eegrej

        mypop_eegplot(EEG,1,1,1,'winlength',8,'command','global eegrej, eegrej = TMPREJ'); 

        disp('Interrupting function now. Waiting for you to press')
        disp('"Update marks", and hit "Continue" in Matlab editor menu')        
        keyboard

        % eegplot2trial cannot deal with multi-rejection
        if ~isempty(eegrej)
            trials_to_delete = [];
            badChnXtrl       = [];
            rejTime          = [];
            
            rejTime = eegrej(:,1:2);
            [~,firstOccurences,~] = unique(rejTime,'rows');
            eegrej = eegrej(firstOccurences,:);

            [badtrls, badChnXtrl] = eegplot2trial(eegrej,EEG.pnts,length(EEG.epoch));
            trials_to_delete = find(badtrls);
            clear eegrej;

            % -------------------------------------
            %  Execute SELECTIVE interpolation and rejection
            % -------------------------------------
            EEG = pop_selectiveinterp(EEG,badChnXtrl);
            [EEG, com] = pop_rejepoch(EEG, trials_to_delete, 1);
            EEG = eegh(com,EEG);

        end
        
        % ----------------------------------------
        %  Execute on all data set interpolation
        % ----------------------------------------
        [indx,tf] = listdlg('ListString',{EEG.chanlocs.labels});
        if~isempty(indx)            
            [EEG,com]  = pop_interp(EEG,indx,'spherical');
            EEG = eegh(com, EEG);
        end
    end
    
 
%     if cfg.do_reref_after_ica        
%         [EEG, com] = pop_reref(EEG,[],'keepref','on');
%         EEG = eegh(com, EEG);
%     end
    
    EEG = pop_editset(EEG, 'setname', [cfg.subject_name '_VisCleanAfterIca.set']);
    EEG = pop_saveset(EEG,[cfg.subject_name '_VisCleanAfterIca.set'] ,cfg.dir_eeg);
    
    clear trinum1 trinum2 trinum3 EEG
    pause(0.4)
end
% fclose(resultfileData);
fprintf('Done.\n')