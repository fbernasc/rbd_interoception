% function m04_rejectICs_PD(whichsubject)

clear all

whichsubject = [];
whichdata = '_ica_1hz_vb.set.set'; % select between _import_ekg and _import_continous
[who_idx,code_patient,patient,IRB,PSG_sess] = get_subjects_RBD(whichsubject);

for isub = 1:length(who_idx)

    % -------------
    % Prepare data.
    % -------------
    
    cfg = get_cfg_RBD(code_patient{isub});
    EEG = [];
    

    % Write a status message to the command line.
    EEG = pop_loadset('filename',[cfg.subject_name '_ica_1hz_vb.set'],'filepath',cfg.dir_eeg,'loadmode','all');
    
    % link electrodes position
    EEG=pop_chanedit(EEG, 'lookup','/home/fosco/toolboxes/eeglab2022.0/plugins/dipfit/standard_BEM/elec/standard_1005.elc');
    
    % change label for ECG
    Index_ecg = find(contains({EEG.chanlocs.labels},'EKG'));
    EEG.chanlocs(Index_ecg).labels = 'ECG';  
    Index_eog1 = find(contains({EEG.chanlocs.labels},'EOG E1'));
    EEG.chanlocs(Index_eog1).labels = 'eog1';  
    Index_eog2 = find(contains({EEG.chanlocs.labels},'EOG E2'));
    EEG.chanlocs(Index_eog2).labels = 'eog2';  
    
    % Run SASICA
    whichHEOG = find(strcmp({EEG.chanlocs.labels},'HEOG'));
    whichVEOG = find(strcmp({EEG.chanlocs.labels},'VEOG'));

    [EEG,com] = SASICA(EEG);
%     [EEG,com]= eeg_SASICA(EEG,'MARA_enable',0,'FASTER_enable',0,'FASTER_blinkchanname','No channel','ADJUST_enable',1,...
%         'chancorr_enable',1,'chancorr_channames',1,'chancorr_corthresh','auto 4',...
%         'EOGcorr_enable',1,'EOGcorr_Heogchannames','HEOG','EOGcorr_corthreshH','auto 4',...
%         'EOGcorr_Veogchannames','VEOG','EOGcorr_corthreshV','auto 4',...
%         'resvar_enable',0,'resvar_thresh',15,...
%         'SNR_enable',0,'SNR_snrcut',1,'SNR_snrBL',[-Inf 0] ,'SNR_snrPOI',[0 Inf],...
%         'trialfoc_enable',1,'trialfoc_focaltrialout','auto','focalcomp_enable',1,...
%         'focalcomp_focalICAout','auto','autocorr_enable',1,'autocorr_autocorrint',20,...
%         'autocorr_dropautocorr','auto','opts_noplot',1,'opts_nocompute',0,'opts_FontSize',14);
    
    ncomps = sum(EEG.reject.gcompreject);

    keyboard; 
    EEG = evalin('base','EEG'); 
    EEG = eegh(com,EEG);

    fprintf(['\n I will remove ' num2str(ncomps) ' components\n']);

    V_Components_to_remove = find(EEG.reject.gcompreject == 1);
    [EEG, com] = pop_subcomp(EEG, V_Components_to_remove);
    if isempty(com)
        return
    end
    EEG = eegh(com,EEG);
    EEG = pop_editset(EEG, 'setname', [cfg.subject_name '_'  whichdata(8:11)  '_ica_clean.set']);
    EEG = pop_saveset( EEG, [cfg.subject_name '_'  whichdata(8:11)  '_ica_clean.set'],cfg.dir_eeg);

    clear ncomps

end
% fclose(resultfileData);
fprintf('Done.\n')   
