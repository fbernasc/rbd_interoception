function m01_preprocessing(whichsubject)

addpath('./pre-processing');
addpath('./Functions');
addpath('/home/fosco/Git_epfl/RBD/EEG/');

% which subjects you want to process?
[who_idx,code_patient,patient,IRB,PSG_sess] = get_subjects_RBD(whichsubject);
addpath('/home/fosco/toolboxes/Functions/erplab_4.0.3.1')

for isub = 1:length(who_idx)
    
    % get cfgs
    cfg = get_cfg_RBD(code_patient{isub});
    EEG = [];
    
    % ---------------------------
    % Import parameters & path(s)
    % ---------------------------
    % Write a status message to the command line.
    fprintf('\n Now importing subject %s, (number %d of %d to process).\n\n',code_patient{isub},isub,length(who_idx));
    fprintf(['\n PSG session: ' num2str(PSG_sess(isub)) '\n\n']);
    pause(1)
    
    % Create output directory if necessary.
    if ~isdir(cfg.dir_eeg)
        mkdir(cfg.dir_eeg);
    end
    
    %-------------
    % Import data
    %-------------
    eegData = [cfg.dir_raw code_patient{isub} '.edf'];
    
    if ~exist(eegData,'file')
        fprintf('Does not exist!\n',eegData)
    else
        if cfg.do_import_ref
            fprintf('Importing %s\n',eegData)
            EEG = pop_biosig(eegData,'ref',cfg.ImportReference);
        else
            EEG = pop_biosig(eegData);
        end
        
%         if EEG.srate>200
%             [EEG] = pop_resample(EEG,200);
%         end
        
        
        %----------------------------------------
        % Shorten the data (e.g. from 5min to 15min)
        %----------------------------------------
        t1 = 15;  % from minute; the first part of the recording is empty or noisy
        t2 = 20;  % till minute
        EEG.datashort = [];
        EEG.datashort = EEG.data(:,((t1*60)*EEG.srate):((t2*60)*EEG.srate));
        EEG.data = [];
        EEG.data = EEG.datashort;
        EEG = eeg_checkset(EEG);
        
        
        %--------------------------------
        %
        %--------------------------------
        
        %----------------------------------------
        % Preprocessing (filtering,epoching etc.)
        %----------------------------------------
        d = code_patient{isub};
        EEG = func_prepareEEG(EEG,cfg,d,patient);
        keyboard; 
        
        % --------------------------------------------------------------
        % Save data.
        % --------------------------------------------------------------
        [EEG, com] = pop_editset(EEG, 'setname', [cfg.subject_name ' import_ekg']);
        EEG = eegh(com, EEG);
        pop_saveset( EEG, [cfg.subject_name  '_import_ekg.set'] , cfg.dir_eeg);
        
    end
end
fprintf('Done.\n')
