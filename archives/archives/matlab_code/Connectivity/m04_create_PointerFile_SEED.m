% difference between conditions:
clear all;
close all;

subjects = {'S1002_LS_CTRL_L380';'S1003_LS_CTRL_L379';'S1004_LS_CTRL_L378';'S1005_LS_CTRL_L384';'S1009_LS_CTRL_L360';...
            'S1010_LS_CTRL_L385';'S1011_LS_CTRL_L386';'S1012_LS_CTRL_L383';'S1013_LS_CTRL_L387';'S1016_LS_CTRL_L395';'S1017_LS_CTRL_L391';...
            'S1018_LS_CTRL_L398';'S1020_LS_CTRL_L394';'S1022_LS_CTRL_L399';'S1024_LS_CTRL_L402';...
            'S1026_LS_CTRL_L409';'S1028_LS_CTRL_L396';'S1032_LS_CTRL_L410';'S1038_LS_CTRL_L439';...
            'S2003_LS_Ctrl_L454';'S2005_LS_Ctrl_L460';'S2008_LS_Ctrl_L455';'S2009_LS_Ctrl_L462';'S2010_LS_Ctrl_L466';'S2013_LS_Ctrl_L470';...            
            'LNAC45';'s2001_F130';'s2003_F136';'s2004_F139';'s2005_F138';'s2006_F135';'s2007_F142';'s2008_F145';...
            's2009_F147';'s2013_F151';'s2014_F152';'s2015_F154';'s2016_F153';'s2017_F155';'S1001_F110';'S1002_F113';...
            'S1003_F112';'S1005_F117';'S1006_F121';'S1007_F115';'S1008_F125';'S1009_F120';'S2018_F156';'S2020_F159';'S2022_F161'};
        
voi = struct('subjnr',[],'condnr',[],'filenames',[],'pathnames',[],'coords',[],'subjectmaxtf',[]);

%%%% Change THIS:
band = {'31to45Hz'};
   
for isubjects = 1:length(subjects)
        load(['/media/sv/New Volume/Dropbox/FoP_EEG/FoP_Connectivity/Connectivity_NM/Schizophrenics/all2all/seed/' ...
              's_beamtf_ccohere_' subjects{isubjects} '_band' band{1} '_seedTemporalSupLZ.mat']);
        voi.pathnames{isubjects} = ['/media/sv/New Volume/Dropbox/FoP_EEG/FoP_Connectivity/Connectivity_NM/Schizophrenics/all2all/seed/' ...
                                    's_beamtf_ccohere_' subjects{isubjects} '_band' band{1} '_seedTemporalSupLZ'];
        voi.filenames{isubjects} = ['s_beamtf_ccohere_' subjects{isubjects} '_band' band{1} '_seedTemporalSupLZ'];
end

voi.condnr = [ones(1,length(subjects))];
voi.condnr((length(subjects)/2+1):length(subjects)) = 2*voi.condnr((length(subjects)/2+1):length(subjects));
voi.subjnr = 1:length(subjects);
voi.subjectmaxtf = zeros(1,length(subjects));
save(['Connectivity_' band{1} '_PointerFile_seed_Temporal_Sup_Left'],'voi');


