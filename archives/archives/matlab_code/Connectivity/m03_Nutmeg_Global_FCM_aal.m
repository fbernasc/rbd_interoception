%% Compute the GLOBAL FCM based on AAL atlas
% here we compute the functional connectivity for each voxel (to the rest
% of the brain), and then avarage the connectivity values on 80 AAL atlas;
% for specific frequency bands independently. 
% fosco.bernasconi@gmail.com

function m03_Nutmeg_Global_FCM_aal(whichsubject)

inpath = ['/media/sv/Elements/Parkinson/Preprocessed_eeg2/SessionFiles/'];

[who_idx,patient,ID,MH,MCI,MCIR1,MCIR3,Agonist,LEDD,Passage,PH,...
    Tactile,Visual1,Visual2,Audio,Olfactory,Hallucinations,reject] = get_subjects(whichsubject);

for isub = 1:length(who_idx)
    
    % get cfgs
    cfg = get_cfg(who_idx(isub),ID{isub}); 
      
    global nuts fuse
    fcm_start AAccohere31MeanZ        
    fcm_start ROI_config.mat

%     if isub == 1
%         R = fcm_voxel2roi(nuts.voxels,nuts.coreg);
%         save roi_tfm_80Roi R
%     end
    
    % If you already have the tfm calculated, you can simply activate it with:
    fcm_useroi('roi_tfm_80Roi');      

    bands   = [1 3; 4 7;8 12;13 30; 31 45]; % frequency of interest:
    isbands = [1 47];  % frequency of the is:

    % load session file:
    session = [inpath,cfg.subject_name];
    nut_opensession(session,1)
    session2 = [cfg.subject_name];

    % calculate all bands
    for b=1:size(bands,1)
        fst  = sprintf('%dto%d',bands(b,:)); 
        fst2 = sprintf('%dto%d',isbands(1,:)); 
        fcm_sourceconnectivity(['ccohere_' session2 '_band' fst 'Hz'],['filtdata_' session2 '_ellipauto_' fst2 'Hz'],...
            ['W_' session2 '_ellipauto_ScalarMN_' fst2 'Hz_0to1998ms_MinNorm_Scalar'],fcm_conndef,'b',bands(b,1),bands(b,2),512,'roi','roi_tfm_80Roi')
%         fcm_sourceconnectivity(['ccohere_' session2 '_band' fst 'Hz'],['filtdata_' session2 '_ellipauto_' fst2 'Hz'],...
%             ['W_' session2 '_ellipauto_SAM4EEGcn_' fst2 'Hz_0to1998ms_SAM'],fcm_conndef,'b',bands(b,1),bands(b,2),512,'roi','roi_tfm_80Roi')

        fcm_comcoh2beam(['ccohere_' session2 '_band' fst 'Hz'])  
    end

%    fcm_comcoh2beam(['ccohere_' session2 '_band' fst 'Hz'])    
end
fprintf('Done.\n')% compute inverse solutions nutmeg

