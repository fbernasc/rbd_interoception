%% Compute the GLOBAL FCM
% here we compute the functional connectivity for each voxel (to the rest
% of the brain), for specific frequency bands 
% fosco.bernasconi@gmail.com

function m03_Nutmeg_Global_FCM(whichsubject)

inpath = ['/media/sv/Elements/Parkinson/Preprocessed_eeg_final/SessionFiles/'];

[who_idx,patient,ID,MH,MCI,MCIR1,MCIR3,Agonist,LEDD,Passage,...
    PH,Tactile,Visual1,Visual2,Audio,Olfactory,Hallucinations,reject] = get_subjects_PD(whichsubject);

for isub = 1:length(who_idx)
    
    % get cfgs
    cfg = get_cfg_Parkinson(ID{isub});
      
    global nuts fuse
    fcm_start AAccohere31MeanZ        

    bands   = [1 3; 4 7;8 12;13 30; 31 45]; % frequency of interest:
    isbands = [1 47];  % frequency of the is:

    % load session file:
    session = [inpath,cfg.subject_name];
    nut_opensession(session,1)
    session2 = [cfg.subject_name];

    % calculate all bands
    for b=1:size(bands,1)
        fst  = sprintf('%dto%d',bands(b,:)); 
        fst2 = sprintf('%dto%d',isbands(1,:)); 
        fcm_sourceconnectivity(['ccohere_' session2 '_band' fst 'Hz'],['filtdata_' session2 '_ellipauto_' fst2 'Hz'],...
            ['W_' session2 '_ellipauto_ScalarMN_' fst2 'Hz_0to1998ms_MinNorm_Scalar'],fcm_conndef,'b',bands(b,1),bands(b,2),512)
        fcm_comcoh2beam(['ccohere_' session2 '_band' fst 'Hz'])  
    end

%     fcm_comcoh2beam(['ccohere_' session2 '_band' fst 'Hz'])    
end
fprintf('Done.\n')% compute inverse solutions nutmeg

