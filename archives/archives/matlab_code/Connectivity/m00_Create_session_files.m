function m00_Create_session_files(whichsubject)

[who_idx,patient,ID,MH,MCI,MCIR1,MCIR3,Agonist,LEDD,Passage,...
    PH,Tactile,Visual1,Visual2,Audio,Olfactory,Hallucinations,reject,pdcrs,moca] = get_subjects_PD(whichsubject);

for isub = 1:length(who_idx)

    EEG = [];
    
    % get cfgs
    cfg = get_cfg_Parkinson(ID{isub});  
    
    inpath  = ['/media/sv/Elements/Parkinson/Preprocessed_eeg_final/EEG2NM/'];
    outpath = ['/media/sv/Elements/Parkinson/Preprocessed_eeg_final/SessionFiles/'];
    
    % Create output directory if necessary.
    if ~isdir(outpath)
        mkdir(outpath);
    end 
    
    % -------------
    % Prepare data:
    % -------------
    fprintf('\nNow importing subject %s, (number %d of %d to process).\n\n',ID{isub},isub,length(who_idx));      
            
    CC = load('/home/sv/Matlabtoolboxes/Scripts4Analysis/Parkinson/Code/Connectivity/PD_templateSessionFile.mat');
    data1 = load([inpath, cfg.subject_name '.mat']);

    CC.meg.data = permute(data1.data,[2 1 3]);
    sessionfile = [cfg.subject_name '.mat'];
    CC.sessionfile = sessionfile;
    save([outpath, sessionfile],'-struct','CC')

    clear data1
    clear CC

end
fprintf('Done.\n')