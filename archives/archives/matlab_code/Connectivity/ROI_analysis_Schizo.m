% Analyse ROI matrix:
clear all
close all

subjects = {'S1002_LS_CTRL_L380';'S1003_LS_CTRL_L379';'S1004_LS_CTRL_L378';'S1005_LS_CTRL_L384';'S1009_LS_CTRL_L360';...
            'S1010_LS_CTRL_L385';'S1011_LS_CTRL_L386';'S1012_LS_CTRL_L383';'S1013_LS_CTRL_L387';'S1016_LS_CTRL_L395';'S1017_LS_CTRL_L391';...
            'S1018_LS_CTRL_L398';'S1020_LS_CTRL_L394';'S1022_LS_CTRL_L399';'S1024_LS_CTRL_L402';...
            'S1026_LS_CTRL_L409';'S1028_LS_CTRL_L396';'S1032_LS_CTRL_L410';'S1038_LS_CTRL_L439';...
            'S2003_LS_Ctrl_L454';'S2005_LS_Ctrl_L460';'S2008_LS_Ctrl_L455';'S2009_LS_Ctrl_L462';'S2010_LS_Ctrl_L466';'S2013_LS_Ctrl_L470';...            
            'LNAC45';'s2001_F130';'s2003_F136';'s2004_F139';'s2005_F138';'s2006_F135';'s2007_F142';'s2008_F145';...
            's2009_F147';'s2013_F151';'s2014_F152';'s2015_F154';'s2016_F153';'s2017_F155';'S1001_F110';'S1002_F113';...
            'S1003_F112';'S1005_F117';'S1006_F121';'S1007_F115';'S1008_F125';'S1009_F120';'S2018_F156';'S2020_F159';'S2022_F161'};
        
        
conditions = {'sync';'async'};
ROIs       = {'Insula_R' 'Temporal_Sup_R' 'Parietal_Sup_R' 'Insula_L' 'Temporal_Sup_L' 'Parietal_Sup_L'};
load(['/media/sv/New Volume/Dropbox/FoP_EEG/FoP_Connectivity/Connectivity_NM/Final/FCM/all2all/ROI/roi_tfm_80Roi.mat']);

sch = 0;
h   = 0;

for isub = 1:length(subjects)
    
    load(['/media/sv/New Volume/Dropbox/FoP_EEG/FoP_Connectivity/Connectivity_NM/Schizophrenics/all2all/ROIs/ccohere_' subjects{isub} ...
      '_band31to45Hz']);     
    
    if isub < length(subjects)/2 +1
        sch = sch +1;
        M_schizo{sch} = fcm_roimatrix(CC,1); % this containes all 80 Rois
        R.selectedROI_id = find(ismember(R.roilabel,ROIs));
        M_sel_schizo{sch}= M_schizo{sch}.coh(R.selectedROI_id,R.selectedROI_id);
    else
        h = h+1;
        M_healthy{h} = fcm_roimatrix(CC,1); % this containes all 80 Rois
        R.selectedROI_id = find(ismember(R.roilabel,ROIs));
        M_sel_healthy{h} = M_healthy{h}.coh(R.selectedROI_id,R.selectedROI_id);

    end    
        

%     % compute mean from one ROI to all other:
%     Mn=mean(M_sel{is,c},2);
%     Normcoherence{is,c} = zscore(Mn); % normalize data

    clear CC    
end


%%
% compute stat for each selected ROI:
for x = 1:length(ROIs)
    for y = 1:length(ROIs)
        for s = 1:(length(subjects)/2)            
            syn2(s) = M_sel_healthy{s}(x,y);
            asy2(s) = M_sel_schizo{s}(x,y);            
        end
        [hs,ps,ci,stats] = ttest2(syn2,asy2);
        pvals(x,y)  = ps;
        hvals(x,y)  = hs;
        stat1(x,y) = stats.tstat;        
        clear syn2 asy2
    end    
end

[FDRs, Q] = mafdr(reshape(pvals,1,36));
pp = reshape(FDRs,6,6);

pvals(pvals > 0.05) = 0;
pvalh(pvalh > 0.05) = 0;


% pp  = triu(pp);  % schizo get upper part of matrix:
% pp2 = tril(pp2); % healthy
% tp = pp + pp2;

pvals = tril(pvals);
pvalh = tril(pvalh);
% tp    = pvalh + pvals;

% plot the stats:
figure('Color','w')
subplot(1,2,1)
    title('Schizo data') 
    imagesc(pvals)
    set(gca,'XTick',[1:length(ROIs)])
    set(gca,'XTickLabel',R.roilabel(R.selectedROI_id))
    set(gca,'YTick',[1:length(ROIs)])
    set(gca,'YTickLabel',R.roilabel(R.selectedROI_id))
    
subplot(1,2,2)
    title('FoP data')
    imagesc(pvalh)
    set(gca,'XTick',[1:length(ROIs)])
    set(gca,'XTickLabel', R.roilabel(R.selectedROI_id))
    set(gca,'YTick',[1:length(ROIs)])
    set(gca,'YTickLabel', R.roilabel(R.selectedROI_id))


% compute stat for each all ROI:
for x = 1:length(R.goodroi)
    for y = 1:length(R.goodroi)
        for s = 1:length(subjects)            
            syn3(s) = M_schizo{s}.coh(x,y);
            asy3(s) = M_healthy{s}.coh(x,y);            
        end
        [h,p3,ci,stats3] = ttest2(syn3,asy3);
        pval3(x,y) = p3;
        stat3(x,y) = stats3.tstat;        
        clear syn3 asy3
    end    
end

pval3(pval3 > 0.05) = 0;  

% plot the stats:
figure('Color','w')
    imagesc(tril(pval3))
%     set(gca,'XTick',[1:length(R.goodroi)])
%     set(gca,'XTickLabel',R.roilabel(R.goodroi))
    xticklabel_rotate([1:length(R.goodroi)],90,R.roilabel(R.goodroi));
    set(gca,'YTick',[1:length(R.goodroi)])
    set(gca,'YTickLabel',R.roilabel(R.goodroi))
figure('Color','w')
    imagesc(tril(pval4))
%     set(gca,'XTick',[1:length(R.goodroi)])
%     set(gca,'XTickLabel',R.roilabel(R.goodroi))
    xticklabel_rotate([1:length(R.goodroi)],90,R.roilabel(R.goodroi));
    set(gca,'YTick',[1:length(R.goodroi)])
    set(gca,'YTickLabel',R.roilabel(R.goodroi))
figure('Color','w')
    pval3(pval3 > 0) = 1;
    pval4(pval4 > 0) = 1;
    overlap = tril(pval3) + tril(pval4);
    imagesc(tril(overlap))
%     set(gca,'XTick',[1:length(R.goodroi)])
%     set(gca,'XTickLabel',R.roilabel(R.goodroi))
    xticklabel_rotate([1:length(R.goodroi)],90,R.roilabel(R.goodroi));
    set(gca,'YTick',[1:length(R.goodroi)])
    set(gca,'YTickLabel',R.roilabel(R.goodroi))







