% nutmeg FCM: Remove "external" from SPM( folder

clear all 
close all

subjects = {'s2003_F136';'s2017_F155';'S2018_F156';'S2023_F163';'s2006_F135';'s2008_F145';'s2009_F147';'s2013_F151';'S1001_F110';'S1002_F113';...
            'S1006_F121';'S1007_F115';'S1008_F125';'S1009_F120';...
            'LNAC32';'LNAC45';'s2001_F130';'s2004_F139';'s2007_F142';'s2015_F154';'s2016_F153';'S1003_F112';...
            'S2020_F159';'S2022_F161';'s2005_F138';'s2010_F146';'s2014_F152';'S1005_F117';...
            'L395';'L379';'L380';'L469';'L460';'L394';'L398';'L469';'L402';'L452';'L399';'L439';'L360';'L462';...
            'L466';'L391';'L396';'L383';'L409';'L454';'L378';'L410';'L384';'L466';'L387';'L403';'L404';'L470'};
        
        
inpath = ['/media/sv/New Volume/Dropbox/Feeling of a Presence (FoP)/EEG experiments/FoP_EEG/FoP_Connectivity/Schizo_connectivity/SessonFiles/']; 

for isubjects = 1:length(subjects)         
        
        % load session file:
        session = [inpath, subjects{isubjects}];
        nut_opensession(session,1)
        session2 = [subjects{isubjects}];    
    
        global nuts fuse
        fcm_start ROI_config.mat
        R = fcm_voxel2roi2(nuts.voxels,nuts.coreg);
        save roi_tfm2 R
       
        % If you already have the tfm calculated, you can simply activate it with:
        fcm_useroi('roi_tfm2');  % ou autre filename
                       
        bands   = [31 45]; % frequency of interest:
        isbands = [3 47];  % frequency of the is:
        
        % calculate all bands
        for b=1:size(bands,1)
            fst  = sprintf('%dto%d',bands(b,:)); 
            fst2 = sprintf('%dto%d',isbands(b,:)); 
            fcm_sourceconnectivity(['ccohere_' session2 '_band' fst 'Hz'],['filtdata_' session2 '_ellipauto_' fst2 'Hz'],...
                ['W_' session2 '_ellipauto_SAM4EEGcn_' fst2 'Hz_0to1998ms_SAM'],fcm_conndef,'b',bands(b,1),bands(b,2),512,'roi','roi_tfm2')
        end

        fcm_comcoh2beam(['ccohere_' session2 '_band' fst 'Hz'])
end




