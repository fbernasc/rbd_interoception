function m01_InverseSolutions(whichsubject)

[who_idx,patient,ID,MH,MCI,MCIR1,MCIR3,Agonist,LEDD,Passage,...
     PH,Tactile,Visual1,Visual2,Audio,Olfactory,Hallucinations,reject] = get_subjects_PD(whichsubject);

for isub = 1:length(who_idx)

    EEG = [];
    
    % get cfgs
    cfg = get_cfg_Parkinson(ID{isub}); 
    inpath = ['/media/sv/Elements/Parkinson/Preprocessed_eeg_final/SessionFiles/'];
    
    % -------------
    % Prepare data:
    % -------------
    fprintf('\nNow importing subject %s, (number %d of %d to process).\n\n',ID{isub},isub,length(who_idx));
    
    global nuts
    session = [inpath, cfg.subject_name];
    nut_opensession(session,1)
    tfbf(session,'2sec',1,47,'ellipauto','ScalarMN'); % 'SAM4EEGcn' / 'ScalarMN'
end
fprintf('Done.\n')% compute inverse solutions nutmeg