function Export_Nutmeg2R(whichsubject)

% export connectivity to R for plotting:
[who_idx,patient,ID,MH,MCI,MCIR1,MCIR3,Agonist,LEDD,Passage,PH,Tactile,Visual1,Visual2,Audio,Olfactory,Hallucinations,reject,pdcrs,moca,...
    age,udprs3,depress,apati]= get_subjects_PD(whichsubject);

%% load original results from FoP in healthy:
mainpath = ['/media/sv/New Volume/Dropbox/Feeling of a Presence (FoP)/EEG experiments/FoP_EEG/FoP_Connectivity/Connectivity_NM/Final/FCM/all2all'];
load([mainpath, '/Diff_of_FCM/s_beamtf1_ttest_Gamma.mat']);
inpath     = [mainpath, 'Diff_of_FCM/'];
% conditions = {'Left_Cluster';'Right_Cluster'}; 
% get clusters:
positiveclust = find(snpm.p_cluster_corr_pos<1);
negativeclust = find(snpm.p_cluster_corr_neg<1);

%%
filename=['/media/sv/New Volume/Dropbox/Parkinson/Experiment(s)/Results/Connectivity/Connectivity_PD_MN_final.csv'];
if exist(filename,'file')==2,delete(filename),end
resultfileData = fopen(filename,'at');

% inpathPDdata = '/media/sv/Elements/Parkinson/Preprocessed_eeg2/FCM_MinNorm/voxels/';
inpathPDdata = '/media/sv/Elements/Parkinson/Preprocessed_eeg2/FCM_MN/';
bands       = [4 7;8 12;13 30; 31 45]; % frequency of interest:
         
for isub = 1:length(who_idx)
    % get cfgs
    cfg = get_cfg_Parkinson(ID{isub});  
    
    for b=1:size(bands,1)    
        for c =1:2
            dataZ = load([inpathPDdata 's_beamtf_ccohere_' cfg.subject_name '_band' num2str(bands(b,1)) 'to' num2str(bands(b,2)) 'HzZ.mat']);
            data  = load([inpathPDdata 's_beamtf_ccohere_' cfg.subject_name '_band' num2str(bands(b,1)) 'to' num2str(bands(b,2)) 'Hz.mat']);
            if c == 1
                clustZ = mean(dataZ.beam.s{1}(positiveclust));
                clust  = mean(data.beam.s{1}(positiveclust));
            elseif c == 2
                clustZ = mean(dataZ.beam.s{1}(negativeclust));
                clust  = mean(data.beam.s{1}(negativeclust));
            end
            
            switch c
                case 1
                    c1 = 'left';
                case 2
                    c1 = 'right';
            end
            
            switch b
                case 1
                    b1 = 'theta';
                case 2
                    b1 = 'alpha';
                case 3
                    b1 = 'beta';
                case 4
                    b1 = 'gamma';
            end
            
            
            fprintf(resultfileData,'%f %f %d %s %s %d %d %d %d %d %d %d %d %d %d %d %d %d %d %s %s %d %d %d %d %d %d %d\n',clustZ,clust,isub,c1,b1,MH{isub},...
                MCI{isub},MCIR1{isub},MCIR3{isub},...
                LEDD{isub},PH{isub},Passage{isub},Tactile{isub},Visual1{isub},...
                Visual2{isub},Audio{isub},Olfactory{isub},Hallucinations{isub},reject{isub},patient{isub},ID{isub},pdcrs{isub},moca{isub},...
                age{isub},udprs3{isub},depress{isub},apati{isub},Agonist{isub});
            
            clear clust c c1 cond
        end
        clear b1
    end
end
fprintf('   Done!  \n')
fclose(resultfileData);