% difference between conditions:
clear all;
close all;

% Good data (based on schizo):
subjects = {'s2003_F136';'s2017_F155';'S2018_F156';'S2023_F163';'s2006_F135';'s2008_F145';'s2009_F147';'s2013_F151';'S1001_F110';'S1002_F113';...
            'S1006_F121';'S1007_F115';'S1008_F125';'S1009_F120';...
            'LNAC32';'LNAC45';'s2001_F130';'s2004_F139';'s2007_F142';'s2015_F154';'s2016_F153';'S1003_F112';...
            'S2020_F159';'S2022_F161';'s2005_F138';'s2010_F146';'s2014_F152';'S1005_F117'};
        
voi = struct('subjnr',[],'condnr',[],'filenames',[],'pathnames',[],'coords',[],'subjectmaxtf',[]);

%%%% Change THIS:
band = {'31to45HzZ'};
wher = {'all2all'};
    
for isubjects = 1:length(subjects)
        load(['/media/sv/New Volume/Dropbox/Feeling of a Presence (FoP)/EEG experiments/FoP_EEG/FoP_Connectivity/Schizo_connectivity/FCM_ROI/' ...
              's_beamtf_ccohere_' subjects{isubjects} '_band' band{1} '.mat']);
        voi.pathnames{isubjects} = ['/media/sv/New Volume/Dropbox/Feeling of a Presence (FoP)/EEG experiments/FoP_EEG/FoP_Connectivity/Schizo_connectivity/FCM_ROI/' ...
                                    's_beamtf_ccohere_' subjects{isubjects} '_band' band{1}];
        voi.filenames{isubjects} = ['s_beamtf_ccohere_' subjects{isubjects} '_band' band{1}];
end

voi.condnr = [ones(1,length(subjects))];
% voi.condnr((length(subjects)/2+1):length(subjects)) = 2*voi.condnr((length(subjects)/2+1):length(subjects));
voi.condnr(15:28) = 2;
voi.subjnr = 1:length(subjects);
voi.subjectmaxtf = zeros(1,length(subjects));
save(['ROIs_Connectivity_Schizo_' band{1} '_PointerFile'],'voi');


