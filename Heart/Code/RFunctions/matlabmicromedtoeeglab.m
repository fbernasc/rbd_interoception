function outputeeglab = matlabmicromedtoeeglab(matlabfile)
load (matlabfile)
load /home/fbernasc/Git/HEP_schizophrenia/Marcos/homemademicromedcap
EEG=[]
EEG.setname=matlabfile(1:end-4)
EEG.filename=matlabfile
EEG.filepath=[]
EEG.trials=1
EEG.pnts= subject.Num_Samples
EEG.nbchan=subject.Num_Chan
EEG.srate= subject.Rate_Min
EEG.xmin=0
EEG.xmax=length(subject.data)/subject.Rate_Min
EEG.times=linspace(0,length(subject.data)/subject.Rate_Min,length(subject.data))
EEG.ref='Cz' %number 21    
EEG.history=[]
EEG.comments=[]
EEG.etc =[]
EEG.saved='No'
EEG.data = subject.data
EEG.icawinv=[]
EEG.icasphere=[]
EEG.icaweights=[]
EEG.icaact=[]
EEG.event=[]
EEG.epoch=[]
EEG.chanlocs=homemademicromedcap
EEG.reject=[]
EEG.stats=[]
EEG.splinefile=[]
EEG.urevent=[]

EEG.rt=[]
EEG.eventdescription={}
EEG.epochdescription={}
EEG.specdata=[]
EEG.specicaact=[]

outputeeglab = EEG
end

