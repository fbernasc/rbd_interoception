---
title: "Spectral parameterization for studying minor hallucinations in PD"
author: "Fosco Bernasconi"
date: "6/22/2021"
output: html_document
editor_options: 
  chunk_output_type: console
---

```{r setup, include=FALSE}
 knitr::knit_engines$set(python = reticulate::eng_python)
```

### Load R libraries
```{r, message= FALSE}
library(tidyverse)   # Access a collection of packages for data management (e.g., dplyr, ggplot2, readr)
library(reticulate)  # Interface Python and R Studio
library(magick)      # Load and adjust .PNG files
library(gridExtra)   # Arrange multiple plots
library(psych)       # Toolbox for data analysis
library(sjstats)     # Toolbox for data analysis
library(ggpubr)      # Additional tools for data visualization
library(cowplot)
library(here)
library(broom)
library(MASS)
library(sfsmisc)
library(permuco)
library(kableExtra)
library(tidyr)
library(dplyr)
library(purrr)
library(lme4)
library(lmerTest)
library(sjPlot)
library(predictmeans)
```

### Load Python libraries
Load the Python modules we need for this tutorial.
```{python, message = TRUE}
# Import some useful standard library modules
import os
from pathlib import Path

# Import some general scientific python libraries
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
plt.clf()
plt.cla()
plt.close()

# Import the parameterization model group object
from fooof import FOOOF,FOOOFGroup

# Import useful parameterization related utilities and plot functions
from fooof.bands import Bands
from fooof.analysis import get_band_peak_fg
from fooof.utils import trim_spectrum
from fooof.data import FOOOFSettings
from fooof.plts import plot_spectrum
from fooof.plts.periodic import plot_peak_fits, plot_peak_params
from fooof.plts.aperiodic import plot_aperiodic_params, plot_aperiodic_fits

# Import functions to examine frequency-by-frequency error of model fits
from fooof.analysis.error import compute_pointwise_error_fg
```

### Check specparam version
```{python, message = FALSE}
import fooof
print(fooof.__version__)
```

### Load data
```{r, message= FALSE}
dataname = "~/Git_epfl/hep_rbd/EEG/power_spectra_RBD_all_chans_fft_all.csv"
```

### Load group PSDs
```{python, message= FALSE}
dataname = "~/Git_epfl/hep_rbd/EEG/power_spectra_RBD_all_chans_fft_all.csv"
freqs   = np.ravel(pd.read_csv("~/Git_epfl/hep_rbd/EEG/freqs_fft_all.csv"))
spectra = np.array(pd.read_csv(dataname))[:,6:(6+len(freqs))] 

print(freqs.shape)
print(spectra.shape)
```

### Fit power spectra parameters
```{python, message = FALSE}
# Define `peak_width_limit` setting
peak_width = [1,12]
 
## Define `max_n_peaks` setting
n_peaks = 12

## Define `min_peak_height` setting
peak_height = 0 
peak_thr    = 1 

# Define frequency range
PSD_range = [2,40]

#### Sub-select frequency ranges
m_freqs, m_spectra = trim_spectrum(freqs, spectra, PSD_range)

# Initialize a model object for spectral parameterization, with some settings
fg = FOOOFGroup(peak_width_limits = peak_width, 
max_n_peaks = n_peaks, 
min_peak_height = peak_height,
peak_threshold = peak_thr, 
verbose = True)

# Fit group PSD over 
fg.fit(m_freqs,m_spectra, PSD_range)
```

### Underfitting
```{python, message = FALSE}
# Extract all fits that are above some error threshold, for further examination.
underfit_error_threshold = 0.1
underfit_check = []
cnt = 0
for ind, res in enumerate(fg):
    if res.error > underfit_error_threshold:
        cnt = cnt+1
        underfit_check.append(fg.get_fooof(ind, regenerate = True))

# print('Number of underfitting: ', cnt)
print(cnt)        
```

### Overfitting
```{python, message = FALSE}
# Extract all fits that are below some error threshold, for further examination.
overfit_error_threshold = 0.02
overfit_check = []
cnt2 = 0
for ind, res in enumerate(fg):
    if res.error < overfit_error_threshold:
        cnt2 = cnt2 +1
        overfit_check.append(fg.get_fooof(ind, regenerate = True))
        
print(cnt2) 
```

### Extract periodic and aperiodic parameters
```{python, message = FALSE}
# Extract aperiodic and periodic parameters
aps = fg.get_params('aperiodic_params')
per = fg.get_params('peak_params')

# Extract group fit information
err = fg.get_params('error')
r2s = fg.get_params('r_squared')
```
 
 
```{python, message = FALSE}
# Define canonical frequency bands
bands = Bands({'delta' : [2, 4], 
               'theta' : [4, 8], 
               'alpha' : [8, 13], 
               'alphatheta': [4,13],
               'beta'  : [13, 30],
               'gamma' : [31, 40]})
```


```{python, message = FALSE}
# Extract band-limited peaks information
deltas   = get_band_peak_fg(fg, bands.delta)
thetas   = get_band_peak_fg(fg, bands.theta)
alphas   = get_band_peak_fg(fg, bands.alpha)
alphathetas = get_band_peak_fg(fg, bands.alphatheta)
betas    = get_band_peak_fg(fg, bands.beta)
gammas   = get_band_peak_fg(fg, bands.gamma)
```


### Calling python objects into R
```{r}
# Transfer periodic parameters to R data frame
per <- as.data.frame(py$per) %>% 
         dplyr::rename(CF = 1, PW = 2, BW = 3, index = 4) %>% 
         group_by(index) %>% 
         mutate(peak_num = seq_along(CF), index = index + 1) %>% 
         tidyr::pivot_wider(id_col = index, names_from = peak_num, values_from = c(CF, PW, BW)) %>% 
         dplyr::select(index,
                CF_1, PW_1, BW_1, CF_2, PW_2, BW_2, CF_3, PW_3, BW_3,
                CF_4, PW_4, BW_4, CF_5, PW_5, BW_5, CF_6, PW_6, BW_6,CF_7, PW_7, BW_7,CF_8, PW_8, BW_8) #  ,CF_7, PW_7, BW_7,CF_8, PW_8, BW_8
```


```{r}
# Transfer band-limited identified peaks to R data frame
deltas <- as.data.frame(py$deltas) %>% 
            rename(CF_delta = 1, PW_delta = 2, BW_delta = 3) %>%
            mutate_at(vars(1:3), ~ifelse(. == "NaN", NA, .)) %>% 
            mutate(index = row_number())

thetas <- as.data.frame(py$thetas) %>% 
            rename(CF_theta = 1, PW_theta = 2, BW_theta = 3) %>%
            mutate_at(vars(1:3), ~ifelse(. == "NaN", NA, .)) %>% 
            mutate(index = row_number())

alphas <- as.data.frame(py$alphas) %>% 
            rename(CF_alpha = 1, PW_alpha = 2, BW_alpha = 3) %>% 
            mutate_at(vars(1:3), ~ifelse(. == "NaN", NA, .)) %>% 
            mutate(index = row_number())

alphathetas <- as.data.frame(py$alphathetas) %>% 
            rename(CF_alphathetas = 1, PW_alphathetas = 2, BW_alphathetas = 3) %>% 
            mutate_at(vars(1:3), ~ifelse(. == "NaN", NA, .)) %>% 
            mutate(index = row_number())
betas  <- as.data.frame(py$betas) %>% 
            rename(CF_beta = 1, PW_beta = 2, BW_beta = 3) %>% 
            mutate_at(vars(1:3), ~ifelse(. == "NaN", NA, .)) %>% 
            mutate(index = row_number())

gammas  <- as.data.frame(py$gammas) %>% 
            rename(CF_gamma = 1, PW_gamma = 2, BW_gamma = 3) %>% 
            mutate_at(vars(1:3), ~ifelse(. == "NaN", NA, .)) %>% 
            mutate(index = row_number())
```


```{r}
# Transfer aperiodic parameters to R data frame
aps <- as.data.frame(py$aps) %>% 
         rename(offset = 1, exponent = 2) %>% 
         mutate(index = row_number())

# Transfer group fit information to R data frame
r2s <- as.data.frame(py$r2s) %>% 
         rename(r2s = 1) %>% 
         mutate(index = row_number())

err <- as.data.frame(py$err) %>% 
         rename(err = 1) %>% 
         mutate(index = row_number())

```


```{r}
# Read in IDs 
IDs <- read.csv(dataname, header = TRUE) %>% 
         dplyr::select(ID,Chan,RBD,RBD_ses1,RBD_ses2,Group,nbrchan) %>% 
         mutate(index = row_number())  

# Join data frames
dat <- full_join(IDs, thetas, by = "index") %>%
  full_join(deltas, by = "index") %>%       
  full_join(alphas, by = "index") %>%
  full_join(alphathetas, by = "index") %>%
  full_join(betas, by = "index")  %>%
  full_join(gammas, by = "index")  %>%
  full_join(aps, by = "index") %>%
  full_join(r2s, by = "index") %>% 
  full_join(err, by = "index") %>% 
  full_join(per, by = "index") %>% 
  dplyr::select(-index) %>% arrange(ID)

```

### Group comparisons AND correlation as a function of cognitive scores of periodic and aperiodic activity
```{r , warning= FALSE}
dat$Group[dat$Group == 1] <- 'RBD_Sess1'
dat$Group[dat$Group == 2] <- 'RBD_Sess2'
dat$Group[dat$Group == 3] <- 'HC_Sess1'

tmp1 = dat %>% dplyr::filter(!Group == 'RBD_Sess2')

# offset:
offset = tmp1 %>% dplyr::filter(Chan %in% c("C3","C4","F3","F4","O1","O2")) %>% group_by(Chan) %>% do(tidy(t.test(offset ~Group, paired = FALSE, var.equal = FALSE, data = .)))
p.adjust(offset$p.value,method = 'fdr')

# exponent:
exponent = tmp1 %>% dplyr::filter(Chan %in% c("C3","C4","F3","F4","O1","O2")) %>% group_by(Chan) %>% do(tidy(t.test(exponent ~ Group, paired = FALSE, var.equal = FALSE, data = .)))
p.adjust(exponent$p.value,method = 'fdr')

# delta:
delta = tmp1 %>% dplyr::filter(Chan %in% c("C3","C4","F3","F4","O1","O2")) %>% group_by(Chan) %>% do(tidy(t.test(PW_delta ~ Group, paired = FALSE, var.equal = FALSE, data = .)))
p.adjust(delta$p.value,method = 'fdr')

# theta:
theta = tmp1 %>% dplyr::filter(Chan %in% c("C3","C4","F3","F4","O1","O2")) %>% group_by(Chan) %>% do(tidy(t.test(PW_theta ~ Group, paired = FALSE, var.equal = FALSE, data = .)))
p.adjust(theta$p.value,method = 'fdr')

# alpha:
alpha = tmp1 %>% dplyr::filter(Chan %in% c("C3","C4","F3","F4","O1","O2")) %>% group_by(Chan) %>% do(tidy(t.test(PW_alpha ~ Group, paired = FALSE, var.equal = FALSE, data = .)))
p.adjust(alpha$p.value,method = 'fdr')

# beta:
beta = tmp1 %>% dplyr::filter(Chan %in% c("C3","C4","F3","F4","O1","O2")) %>% group_by(Chan) %>% do(tidy(t.test(PW_beta ~ Group, paired = FALSE, var.equal = FALSE, data = .)))
p.adjust(beta$p.value,method = 'fdr')

# gamma:
gamma = tmp1 %>% dplyr::filter(Chan %in% c("C3","C4","F3","F4","O1","O2")) %>% group_by(Chan) %>% do(tidy(t.test(PW_gamma ~ Group, paired = FALSE, var.equal = FALSE, data = .)))
p.adjust(gamma$p.value,method = 'fdr')



set.seed(55)
# offset:
toff = list()
toff = tmp1 %>% 
  dplyr::select(offset,Chan,Group) %>% 
  group_by(Chan) %>% 
  nest() %>%  
  mutate(lme_out = map(data,~permuco::aovperm(offset ~ Group, np = 5000, data = .))) %>%
  mutate(perf = map(lme_out,"table")) %>%
  mutate(pperm = map(perf,'resampled P(>F)')) %>%
  mutate(pparam = map(perf,'parametric P(>F)')) %>%
  mutate(F= map(perf,'F')) %>%
  dplyr::select(-perf,-data,-lme_out) %>%
  ungroup() %>%
  as.data.frame()

allres=bind_rows(toff)
Fs=as_tibble(matrix(unlist(allres$F),ncol=2,byrow = T)); names(Fs) = c('Gr','NA')
ps=as_tibble(matrix(unlist(allres$pparam),ncol=2,byrow = T)); names(ps) = c('pGr','NA')
widestats = allres %>% dplyr::select(-F,-pparam) %>% as_tibble()
widestats = cbind(widestats,Fs,ps)

longstats = widestats  %>% pivot_longer(names_to = 'effect', cols =  c(Gr)) %>% dplyr::select(Chan,effect,F=value)
tmp  = widestats %>% pivot_longer(names_to = 'effect', cols =  c(pGr))
longstats$p=tmp$value
longstats = longstats %>% 
  group_by(effect) %>% mutate(pfdr = p.adjust(p,method='fdr')) %>%
  arrange(effect) %>% mutate(significant = ifelse(pfdr<= 0.05,1,0))
longstats
rm(longstats)
# longstats = longstats %>% mutate_if(is.numeric, round,2)
# write.table(longstats, file = "~/Git_epfl/pd_mh_eeg/Figures_Tables/Tables/Offset_interaction_pdcrsf.txt", sep = ",", quote = FALSE, row.names = F)


# exponent
texp = list()
texp = tmp1 %>% 
  dplyr::select(exponent,Chan,Group) %>% 
  group_by(Chan) %>% 
  nest() %>%  
  mutate(lme_out = map(data,~permuco::aovperm(exponent ~Group, np = 2000, data = .))) %>%
  mutate(perf = map(lme_out,"table")) %>%
  mutate(pperm = map(perf,'resampled P(>F)')) %>%
  mutate(pparam = map(perf,'parametric P(>F)')) %>%
  mutate(F= map(perf,'F')) %>%
  dplyr::select(-perf,-data,-lme_out) %>%
  ungroup() %>%
  as.data.frame()

allres=bind_rows(texp)
Fs=as_tibble(matrix(unlist(allres$F),ncol=2,byrow = T)); names(Fs) = c('Gr','NA')
ps=as_tibble(matrix(unlist(allres$pperm),ncol=2,byrow = T)); names(ps) = c('pGr','NA')
widestats = allres %>% dplyr::select(-F,-pperm) %>% as_tibble()
widestats = cbind(widestats,Fs,ps)

longstats = widestats  %>% pivot_longer(names_to = 'effect', cols =  c(Gr)) %>% dplyr::select(Chan,effect,F=value)
tmp  = widestats %>% pivot_longer(names_to = 'effect', cols =  c(pGr))
longstats$p=tmp$value
longstats = longstats %>% 
  group_by(effect) %>% mutate(pfdr = p.adjust(p,method='fdr')) %>%
  arrange(effect) %>% mutate(significant = ifelse(pfdr<= 0.05,1,0))
longstats
# longstats = longstats %>% mutate_if(is.numeric, round,2)
# write.table(longstats, file = "~/Git_epfl/pd_mh_eeg/Figures_Tables/Tables/Offset_interaction_pdcrsf.txt", sep = ",", quote = FALSE, row.names = F)
rm(longstats)

# Power
tmp1 %>% dplyr::filter(Chan %in% c("C3","C4","F3","F4","O1","O2")) %>% group_by(Chan) %>% do(tidy(t.test(PW_theta~Group, paired = FALSE, var.equal = FALSE, data = .)))

ttheta = list()
ttheta = tmp1 %>% 
  dplyr::select(PW_theta,Chan,Group) %>% 
  dplyr::filter(Chan %in% c("C3","C4","F3","F4","O1","O2")) %>%
  group_by(Chan) %>% 
  nest() %>%  
  mutate(lme_out = map(data,~permuco::aovperm(PW_theta~Group, np = 5000, data = .))) %>%
  mutate(perf = map(lme_out,"table")) %>%
  mutate(pperm = map(perf,'resampled P(>F)')) %>%
  mutate(pparam = map(perf,'parametric P(>F)')) %>%
  mutate(F= map(perf,'F')) %>%
  dplyr::select(-perf,-data,-lme_out) %>%
  ungroup() %>%
  as.data.frame()

allres=bind_rows(ttheta)
Fs=as_tibble(matrix(unlist(allres$F),ncol=2,byrow = T)); names(Fs) = c('Gr','NA')
ps=as_tibble(matrix(unlist(allres$pparam),ncol=2,byrow = T)); names(ps) = c('pGr','NA')
widestats = allres %>% dplyr::select(-F,-pparam) %>% as_tibble()
widestats = cbind(widestats,Fs,ps)

longstats = widestats  %>% pivot_longer(names_to = 'effect', cols =  c(Gr)) %>% dplyr::select(Chan,effect,F=value)
tmp  = widestats %>% pivot_longer(names_to = 'effect', cols =  c(pGr))
longstats$p=tmp$value
longstats = longstats %>% 
  group_by(effect) %>% mutate(pfdr = p.adjust(p,method='fdr')) %>%
  arrange(effect) %>% mutate(significant = ifelse(pfdr<= 0.05,1,0))
longstats

colors <- c('darkviolet','darkorange')
tmp1$RBD[tmp1$RBD == 1] = 'iRBD'
tmp1$RBD[tmp1$RBD == 0] = 'HC'
tmp1 %>%
  dplyr::filter(Chan %in% c("C3","C4","F3","F4","O1","O2")) %>%
  ggplot(aes(y = PW_theta, x = RBD, color=RBD)) +
  stat_summary(fun.data = "mean_cl_boot") + 
  stat_summary(fun = mean,geom = "line") +
  # geom_jitter(aes(alpha = 0.3),height = 0) +
  scale_color_manual(values = colors)  + 
  # ylab('Frontal-subcortical cognitve decline') + 
  ylab('Theta power [uV2]')  + 
  # coord_cartesian(ylim = c(40,75))  +
  facet_wrap(~Chan) +
  theme_cowplot(font_size = 12,line_size = 0.5, font_family = "sans-serif")


# alpha
talpha = list()
talpha = tmp1 %>% 
  dplyr::select(PW_alpha,Chan,Group) %>% 
  group_by(Chan) %>% 
  nest() %>%  
  mutate(lme_out = map(data,~permuco::aovperm(PW_alpha~Group, np = 5000, data = .))) %>%
  mutate(perf = map(lme_out,"table")) %>%
  mutate(pperm = map(perf,'resampled P(>F)')) %>%
  mutate(pparam = map(perf,'parametric P(>F)')) %>%
  mutate(F= map(perf,'F')) %>%
  dplyr::select(-perf,-data,-lme_out) %>%
  ungroup() %>%
  as.data.frame()

allres=bind_rows(talpha)
Fs=as_tibble(matrix(unlist(allres$F),ncol=2,byrow = T)); names(Fs) = c('Gr','NA')
ps=as_tibble(matrix(unlist(allres$pparam),ncol=2,byrow = T)); names(ps) = c('pGr','NA')
widestats = allres %>% dplyr::select(-F,-pparam) %>% as_tibble()
widestats = cbind(widestats,Fs,ps)

longstats = widestats  %>% pivot_longer(names_to = 'effect', cols =  c(Gr)) %>% dplyr::select(Chan,effect,F=value)
tmp  = widestats %>% pivot_longer(names_to = 'effect', cols =  c(pGr))
longstats$p=tmp$value
longstats = longstats %>% 
  group_by(effect) %>% mutate(pfdr = p.adjust(p,method='fdr')) %>%
  arrange(effect) %>% mutate(significant = ifelse(pfdr<= 0.05,1,0))
longstats




# beta
tbeta = list()
tbeta = tmp1 %>% 
  dplyr::select(PW_beta,Chan,Group) %>% 
  group_by(Chan) %>% 
  nest() %>%  
  mutate(lme_out = map(data,~permuco::aovperm(PW_beta~Group, np = 5000, data = .))) %>%
  mutate(perf = map(lme_out,"table")) %>%
  mutate(pperm = map(perf,'resampled P(>F)')) %>%
  mutate(pparam = map(perf,'parametric P(>F)')) %>%
  mutate(F= map(perf,'F')) %>%
  dplyr::select(-perf,-data,-lme_out) %>%
  ungroup() %>%
  as.data.frame()

allres=bind_rows(tbeta)
Fs=as_tibble(matrix(unlist(allres$F),ncol=2,byrow = T)); names(Fs) = c('Gr','NA')
ps=as_tibble(matrix(unlist(allres$pparam),ncol=2,byrow = T)); names(ps) = c('pGr','NA')
widestats = allres %>% dplyr::select(-F,-pparam) %>% as_tibble()
widestats = cbind(widestats,Fs,ps)

longstats = widestats  %>% pivot_longer(names_to = 'effect', cols =  c(Gr)) %>% dplyr::select(Chan,effect,F=value)
tmp  = widestats %>% pivot_longer(names_to = 'effect', cols =  c(pGr))
longstats$p=tmp$value
longstats = longstats %>% 
  group_by(effect) %>% mutate(pfdr = p.adjust(p,method='fdr')) %>%
  arrange(effect) %>% mutate(significant = ifelse(pfdr<= 0.05,1,0))
longstats