rm
options(warn=-1)
library(R.utils)
library(lme4);library(lmerTest)         # mixed model stuff
library(Hmisc)
library(tidyverse); theme_set(theme_bw(base_size = 10)); 
library(afex)
library(multidplyr)
library(patchwork)
library(zoo)
library(data.table)
library(RColorBrewer)
# library(mvtboost)
library(tidyr)
library(sjmisc)
library(eegUtils) 
library(here)
library(cowplot)

dopreproc = 1 # do preprocessing or just load preprocessed data
dozscore  = 1
dostat    = 0

myRdBu=c(brewer.pal(3,'Greens'), brewer.pal(3,'Reds'))
rootdir = "/home/fosco/Git_epfl/hep_rbd/EEG/Preprocessed/"
sourceDirectory('/home/fosco/Git_epfl/hep_rbd/Code/RFunctions')

setwd(rootdir)
eeglist = list.files(path=paste(rootdir),pattern = '4R.set', recursive = TRUE)

# Preprocessing -----------------------------------------------------------
if (dopreproc == 1) {
  atmp=list()
  # read preprocessed files and append to a
  for (f in 1:length(eeglist)) {
    print(paste('Importing subject: ', eeglist[f], sep = ''))
    EEG = my_import_set_HEP(paste(rootdir, eeglist[f], sep = '/'))
    beh = EEG[[2]] %>% mutate(epoch = 1:n()) %>% mutate(suj = paste('s', f, sep = ''))
    tmp = EEG[[1]] %>% tidyr::gather(chan, amp, names(EEG[[1]])[1:(length(EEG[[1]])-2)])
    atmp[[f]] = full_join(tmp, beh, by = 'epoch')
    atmp[[f]]$suj = f
    atmp[[f]]$GR = beh$group[f] 
  }
  
  a=bind_rows(atmp) # concatenate the list atmp
  rm(atmp)
  names(a)[names(a) == "time.x"] <- "time"
  
  ## reencode time variable in ms
  a$time = round(a$time)
  save(a,file='data_clean_HEP.RData')
  
  ## read chan locs
  elec = EEG[[3]] %>% select(electrode,x=cart_x,y=cart_y) %>% arrange(electrode)
  save(elec,file='elec.RData')
}

# if (dopreproc == 0) {
#   if (dozscore == 1) {
#     # load('data_clean_zscore.RData')
#   }
#   else if (dozscore == 0) {
#     load('data_avg_HEP.RData.RData')
#   }
# }

dat = a
rm(a)
gc()


if (dozscore == 1) {
    dat <- dat %>% group_by(suj,chan,epoch) %>% 
      mutate(amp = (amp - mean(amp))/sd(amp)) %>% ungroup()
  }
  
  
# do bsl correction:
# dat <- left_join(dat, dat %>% filter(time>= -200 & time<= 0) %>%
#                          group_by(chan,epoch,suj) %>%
#                          summarise(baseline = mean(amp))) %>%
#         mutate(amp_bslcorr= amp - baseline) %>% ungroup()

## parallel version
# if memory stack issue, use "ulimit -s 16000" on linux
# dat <- dat %>% filter(suj %in% c(1,2,55,56))
# dat <- dat %>% filter(time>= 200 & time<= 600)
# 
# dat = dat %>% filter(!chan %in% c('ECG1+'))

rm(beh)
rm(EEG)
rm(tmp)
gc()


# dat <- dat %>% filter(chan %in% c('C3','C4','Cz','F3','F4','F7','F8','Fp1','Fp2','Fz','P3','P4','Pz')) %>% droplevels()
# dat <- dat %>% filter(chan %in% c('Fz','Cz')) %>% droplevels()

fitControl <- trainControl(method = "repeatedcv",number = 5,repeats=5,returnData=F,returnResamp='final',savePredictions='final',preProcOptions = list(thresh = 0.8))
# fitControl <- trainControl(method = "cv",number = 10,returnData=F,returnResamp='final',savePredictions='final',preProcOptions = list(thresh = 0.8))
# fitControl <- trainControl(method = "LOOCV",returnData=F,returnResamp='final',savePredictions='final',preProcOptions = list()) # not enoght MEMORY

clust = multidplyr::new_cluster(2)
clust = clust %>% cluster_library(c('dplyr','afex','purrr','caret'))
multidplyr::cluster_assign(clust, train = 'train')
multidplyr::cluster_assign(clust, map = 'map')
multidplyr::cluster_assign(clust, one_of = 'one_of')
multidplyr::cluster_copy(clust,'fitControl')

dat = dat %>% dplyr::filter(!GR == 2)
dat$GR[dat$GR == 1] = 'RBD'
dat$GR[dat$GR == 3] = 'nRBD'

# sigle trials:
    classify_data = dat %>% 
      # filter(time>= 200 & time<= 600) %>% 
      dplyr::filter(chan %in% c('C3')) %>% 
      dplyr::select(chan,amp,time,epoch,suj,GR) %>%
      arrange(suj,epoch,chan,time) %>%
      group_by(suj,epoch,chan) %>%
      mutate(amp2 = rollmean(amp,10,fill=NA)) %>%
      dplyr::select(-amp) %>%
      filter(!is.na(amp2)) %>%
      tidyr::spread(chan,amp2) %>% ungroup() %>%
      dplyr::select(-epoch,-suj) %>%
      tidyr::nest(-time)
    
    gc()
    m1 = classify_data %>%
      partition(clust) %>%
      mutate(fit = map(data, ~train(GR ~., data = .x, preProcess = c("center", "scale"),
                                    method = "glmnet", family = "binomial", trControl = fitControl))) %>%
                                    # tuneGrid = expand.grid(alpha = 1,lambda = 0.001)))) %>%
      collect()
    
    options(warn=0)
    


## classification
save(m1,file='./m1_HEP_decoding_LOOCV_100msbsl.RData')

m1res <- m1 %>% mutate(kappa = map(fit,"resample") %>%
                         map("Kappa") %>%
                         map_dbl(~mean(.x)))

allres=data.frame(time=m1res$time,kappa=m1res$kappa)
allres %>%
  ggplot(aes(x=time, y=kappa))+
  stat_summary(fun.data = mean_cl_normal, geom = "ribbon", alpha = 0.2) +
  stat_summary(fun = mean, geom = "line")

m1res <- m1 %>% mutate(kappa = map(fit,"resample") %>%
                         map("Kappa") %>%
                         map_dbl(~mean(.x)))


m1res <- m1 %>% mutate(Accuracy = map(fit,"resample") %>%
                         map("Accuracy") %>%
                         map_dbl(~mean(.x)))
allres=data.frame(time=m1res$time,accuracy=m1res$Accuracy)
allres %>%
  ggplot(aes(x=time, y=accuracy))+
  stat_summary(fun.data = mean_cl_normal, geom = "ribbon", alpha = 0.2) +
  stat_summary(fun = mean, geom = "line")

dostats = 0

if (dostats == 1) {
  load('/LOCALSCRATCH/fbernasc/SCZ/m1_HEP_decoding_pca_99.RData')
  
  # ### permuation:
  # set.seed(2)
  # limit <- length(dat$Group)
  # myindex <- c(rep("SE",33), rep("SZ",15))
  # endDim <- 4000
  # permutations<-sample(myindex)
  # 
  # # generate permutations
  # while(is.null(dim(unique(permutations))) || dim(unique(permutations))[1]!=endDim) {
  #   permutations <- rbind(permutations,sample(myindex))
  # }
  # 
  # # Resulting permutations:
  # pb <- unique(permutations)
  # nperm <- 2000
  # permKappaint <- numeric(nperm)
  # 
  # for (i in 1:nperm){
  #   
  #   options(warn=-1)
  #   dat$Group  <- pb[i,]  # shoufle order of patients
  #   classify_data = dat %>%
  #     dplyr::select(chan,amp_bslcorr,time,epoch,suj,Group) %>%
  #     arrange(suj,epoch,chan,time) %>%
  #     group_by(suj,epoch,chan) %>%
  #     mutate(amp = rollmean(amp_bslcorr,10,fill=NA)) %>% 
  #     dplyr::select(-amp_bslcorr) %>% 
  #     filter(!is.na(amp)) %>%
  #     spread(chan,amp) %>% ungroup()  %>%
  #     dplyr::select(-epoch,-suj) %>% 
  #     tidyr::nest(-time) # probably suj should not be there as we do between
  #   options(warn=0)
  #   
  #   
  #   mpermu = classify_data %>% 
  #     partition(clust) %>%
  #     mutate(fit = map(data, ~train(Group ~., data = .x, preProcess = "scale",
  #                                   method = "glmnet", family = "binomial", trControl = fitControl)))  %>%   
  #     collect()
  #   
  #   permKappaint[i] <- mpermu$results$Kappa
  # }
  # 
  # 
  # # load('/LOCALSCRATCH/fbernasc/SCZ/mres_HEP_decoding.RData')
  # 
  # m1 <- mutate(m1[[s]], perf = map(fit,"resample") %>%
  #                    map("Kappa") %>%
  #                    map_dbl(~mean(.x)))
  
  
  m1 <- mutate(m1, kappa = map(fit,"resample") %>%
                 map("Kappa") %>%
                 map_dbl(~mean(.x)))
  
  # ## regression
  # # m1[[s]] <- mutate(m1[[s]], perf = map(fit,'resample') %>%
  # #                        map("Rsquared") %>%
  # #                        map_dbl(~mean(.x)))
  # 
  
  allres=data.frame(time=m1$time,kappa=m1$kappa)
  allres %>%
    ggplot(aes(x=time, y=kappa))+
    stat_summary(fun.data = mean_cl_normal, geom = "ribbon", alpha = 0.2) +
    stat_summary(fun = mean, geom = "line")
  
  # ggsave("HEP_decoding.pdf")
  
  # p2=allres %>%
  #   ggplot(aes(x=time, y=perf,color=suj,group=suj))+ geom_path()
  # 
  # grid.arrange(p1,p2)
  
}


##################################################
# # mgaussian glmnet -----------------------------
# 
# require(glmnet)
# require(doMC)
# 
# 
# chans = unique(a$chan) %>% sort()
# times=unique(a$time[a$time> -200 & a$time <600])
# sujs=unique(a$suj)
# g = a %>% filter(is.element(sdt,c('hit','miss'))) %>% select(chan,amp,sdt,conf,suj,time,epoch)%>%  spread(chan,amp)
# g$sdt = ifelse(g$sdt=='hit',1,0)
# 
# registerDoMC(cores=10)
# 
# counter=1
# betas=list()
# eoi=c('conf','sdt')
# for (s in sujs[1:2]) {
#   for (t in times) {
#     message(paste(s,' time: ',t,sep=''))
#     tmp = g %>% filter(suj==s,time==t)  %>% select(-epoch,-suj,-time) 
#     x = tmp %>% select(-conf,-sdt) %>% as.matrix(ncol=length(chans))
#     y = matrix(cbind(tmp$conf,tmp$sdt),ncol=2)
#     m = cv.glmnet(x,y,family="mgaussian",alpha=0,parallel=T,nfolds=10)
#     coefs=coef(m,s='lambda.min')
#     for (c in 1:length(coefs)) {
#       betatmp = as.data.frame(as.matrix(coefs[[c]])) 
#       betas[[counter]] = betatmp %>%  mutate(effect=eoi[c],chan=rownames(betatmp),suj=s,time=t,lambda=m$lambda.min)
#       counter=counter+1
#     }     
#   }
# }
# 
# save(betas,file='./models/betas_glmnet_mgaussian.RData')
# load('./models/betas_glmnet_mgaussian.RData')
# 
# betas = bind_rows(betas) %>% as_tibble() %>% filter(chan!='(Intercept)')
# names(betas)[1] = 'val'
# 
# betas  %>% group_by(suj) %>% #mutate(val=scale(val)) %>% 
#   ggplot(aes(time,val,color=effect)) + stat_summary(fun.y = sum,geom='line') + facet_wrap(~suj)
# 
# 
# avg = betas %>% group_by(time,effect,suj) %>% summarise(val=mean(val)) 
# 
# avg %>% ggplot(aes(time,val,color=effect,fill=effect)) + stat_summary(fun.data = mean_cl_normal,geom='ribbon',color=NA,alpha=.1) +
#   stat_summary(fun.y = mean,geom='line')+facet_wrap(~effect,scale='free')
# 









