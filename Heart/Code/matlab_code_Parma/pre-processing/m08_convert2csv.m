% function m04_rejectICs_PD(whichsubject)

clear all
whichsubject = [];
whichdata    = '_cleanica_wtecg_1hz_early.set'; % select between _import_ekg and _import_continous
whichdataout = '_hep_witecg_early.csv';

[who_idx,code_patient,patient,RBD,PSG_sess1,PSG_sess2,Gr,match,newID] = get_subjects_RBD(whichsubject);

for isub = 1:length(who_idx)
    
    % -------------
    % Prepare data.
    % -------------
    
    cfg = get_cfg_RBD(code_patient{isub});
    EEG = [];
    
    
    % Write a status message to the command line.
    EEG = pop_loadset('filename',[cfg.subject_name whichdata ],'filepath',cfg.dir_eeg,'loadmode','all');
    
    % avg trials:
    EEG.data = mean(EEG.data,3);
    EEG = eeg_checkset(EEG);
    
    EEG.data(EEG.nbchan+1,:) = repelem(isub,size(EEG.data,2));
    EEG.chanlocs(EEG.nbchan+1).labels = 'ID'; 
    
    EEG.data(EEG.nbchan+2,:) = repelem(patient(isub),size(EEG.data,2));
    EEG.chanlocs(EEG.nbchan+2).labels = 'Group'; 
    
    EEG.data(EEG.nbchan+3,:) = repelem(RBD(isub),size(EEG.data,2));
    EEG.chanlocs(EEG.nbchan+3).labels = 'RBD'; 
    
    EEG.data(EEG.nbchan+4,:) = repelem(PSG_sess1(isub),size(EEG.data,2));
    EEG.chanlocs(EEG.nbchan+4).labels = 'Session1'; 
    
    EEG.data(EEG.nbchan+5,:) = repelem(PSG_sess2(isub),size(EEG.data,2));
    EEG.chanlocs(EEG.nbchan+5).labels = 'Session2'; 
    
    EEG.data(EEG.nbchan+6,:) = repelem(EEG.ekg_IBImean,size(EEG.data,2));
    EEG.chanlocs(EEG.nbchan+6).labels = 'IBI'; 
    
    EEG.data(EEG.nbchan+7,:) = repelem(Gr(isub),size(EEG.data,2));
    EEG.chanlocs(EEG.nbchan+7).labels = 'Gr'; 
    
    EEG.data(EEG.nbchan+8,:) = repelem(EEG.chaneeg,size(EEG.data,2));
    EEG.chanlocs(EEG.nbchan+8).labels = 'nbchan'; 
    
    EEG.data(EEG.nbchan+9,:) = repelem(table2array(EEG.LF_power),size(EEG.data,2));
    EEG.chanlocs(EEG.nbchan+9).labels = 'LFpower'; 
    
    EEG.data(EEG.nbchan+10,:) = repelem(table2array(EEG.HF_power),size(EEG.data,2));
    EEG.chanlocs(EEG.nbchan+10).labels = 'HFpower';    
    
    EEG.data(EEG.nbchan+11,:) = repelem(newID(isub),size(EEG.data,2));
    EEG.chanlocs(EEG.nbchan+11).labels = 'newID'; 
    
    EEG.data(EEG.nbchan+12,:) = repelem(match(isub),size(EEG.data,2));
    EEG.chanlocs(EEG.nbchan+12).labels = 'match'; 
    
    EEG.data(EEG.nbchan+13,:) = repelem(table2array(EEG.LF_HF_ratio),size(EEG.data,2));
    EEG.chanlocs(EEG.nbchan+13).labels = 'HRV_ratio'; 
    
    EEG = eeg_checkset(EEG);
    
    pop_export(EEG,[cfg.dir_eeg cfg.subject_name  whichdataout],'erp','off','transpose','on','precision',4);
end
% fclose(resultfileData);

