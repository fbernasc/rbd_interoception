function m01_preprocessing_vb_new(whichsubject)

addpath('./pre-processing');
addpath('./Functions');
addpath('./EEG/');
addpath('/home/fosco/toolboxes/eeglab2022.0')
eeglab nogui
addpath('/home/fosco/toolboxes/fieldtrip-master')
ft_defaults

% which subjects you want to process?
[who_idx,code_patient,patient,RBD,PSG_sess1,PSG_sess2,Gr,match,newID,age,gender,center,converted] = get_subjects_RBD(whichsubject);
addpath('/home/fosco/toolboxes/Functions/erplab_4.0.3.1')

for isub = 1:length(who_idx)

    % get cfgs
    cfg = get_cfg_RBD(code_patient{isub});
    EEG = [];

    % ---------------------------
    % Import parameters & path(s)
    % ---------------------------
    % Write a status message to the command line.
    fprintf('\n Now importing subject %s, (number %d of %d to process).\n\n',code_patient{isub},isub,length(who_idx));
    pause(1)

    % Create output directory if necessary.
    if ~isdir(cfg.dir_eeg)
        mkdir(cfg.dir_eeg);
    end

    %-------------
    % Import data
    %-------------
    eegData = [cfg.dir_raw code_patient{isub} '.edf'];

    if ~exist(eegData,'file')
        fprintf('Does not exist!\n',eegData)
    else

        if cfg.do_import_ref
            fprintf('Importing %s\n',eegData)
            EEG = pop_biosig(eegData,'ref',cfg.ImportReference);
        else
            data = [];
            [data, event] = edf2fieldtrip(eegData);
            %             [hdr] = ft_read_header(eegData);


            % data has one extrachan
            selchan = ft_channelselection({'all' '-DHR'}, data.label);
            cfg1 = [];
            cfg1.channel = selchan;
            data = ft_selectdata(cfg1,data);

            % add chan info
            fprintf('\n\n [stage 0] add channels info \n\n')
            %             chaninfo = load("/home/fosco/Git_epfl/hep_rbd/Chaninfo.mat");
            EEG = fieldtrip2eeglab(data);
            EEG.chanlocs = struct(EEG.chanlocs);

            for i = 1:size(data.label,1)
                EEG.chanlocs(i).labels = data.label{i};
                EEG.chanlocs(i).ref = [];
                EEG.chanlocs(i).X = [];
                EEG.chanlocs(i).Y = [];
                EEG.chanlocs(i).Z = [];
            end

            tmp = EEG.data;
            EEG.data = [];
            EEG.data = tmp;
            EEG = eeg_checkset(EEG);
        end


        if strcmp(center{isub},'Bern')
            chanekg = find(~cellfun(@isempty,regexp({EEG.chanlocs.labels},'ECG1-ECG2|ekg|ECG|EKG')));
            EEG.chanlocs(chanekg).labels = 'ECG';
            EEG = eeg_checkset(EEG);
        elseif strcmp(center{isub},'Parma')

            chanekg = find(~cellfun(@isempty,regexp({EEG.chanlocs.labels},'ECG1-ECG2|ekg|ECG|EKG')));

            EEG.chanlocs(chanekg).labels = 'ECG';
            EEG = eeg_checkset(EEG);
            EEG.chanlocs_orig = EEG.chanlocs;

            % sometimes the ECG electrodes are inverted:
            if any(strcmpi(code_patient{isub},{'n14','n3','n4','n6','n8','rbd18','rbd5'}))

            else
                EEG.data(chanekg,:) = -EEG.data(chanekg,:);
            end

            chaneog = find(~cellfun(@isempty,regexp({EEG.chanlocs.labels},'ROC-LOC')));

            if ~isempty(chaneog)
                EEG.chanlocs(chaneog).labels = 'EOG E1';
            else
                chaneogR = find(~cellfun(@isempty,regexp({EEG.chanlocs.labels},'EOG dx|LOC-A1|LOC|EOG-L')));
                chaneogL = find(~cellfun(@isempty,regexp({EEG.chanlocs.labels},'EOG sin|ROC-A2|ROC|EOG-R')));
                EEG.chanlocs(chaneogR).labels = 'EOG E1';
                EEG.data(chaneogR,:) = EEG.data(chaneogR,:) - EEG.data(chaneogL,:);

            end

            if ~isempty(chanC4)
                EEG.chanlocs(chanC4).labels = 'C4';
            elseif isempty(chanC4) && ~isempty(chanC4b)
                EEG.chanlocs(chanC4b).labels = 'C4';
            end

        end

        % downsample the data:
        if EEG.srate ~= 200
            EEG = pop_resample(EEG,200);
        end



        % --------------------------------------------------------------
        % Save data.
        % --------------------------------------------------------------
        [EEG, com] = pop_editset(EEG, 'setname', [cfg.subject_name ' import_ekg_versionb_early']);
        EEG = eegh(com, EEG);
        pop_saveset(EEG,[cfg.subject_name  '_import_ekg_versionb_early.set'] , cfg.dir_eeg);
        clear EEG
    end
end
fprintf('Done.\n')
