function [] = findecgpeak(EEG)

chanekg = find(~cellfun(@isempty,regexp({EEG.chanlocs.labels},'ECG1-ECG2|ekg|ECG|EKG')));
EEG.chanlocs(chanekg).labels = 'ECG';

ecgchan = [];
ecgin   = [];

ecgchan = strcmp({EEG.chanlocs.labels},'ECG');
ecgin   = EEG.data(ecgchan,:);

% adaptive win
rpeaks   = [];
qrspeaks = [];
minProminences = [];

wt = modwt(ecgin,5);
wtrec = zeros(size(wt));
wtrec(4:5,:) = wt(4:5,:);
y = imodwt(wtrec,'sym4');
y = abs(y).^2;

localPeakIndices_adjust = [];

for i = 1:numWindows % for sliding win use: numDataPoints-windowSize+1
    window = [];
    windowStart = (i-1)*windowSize + 1;
    windowEnd   = i*windowSize;
    timepoints  = ([windowStart:windowEnd]);
    window      = (y(:,windowStart:windowEnd));

    % remove outliers in the window:
    minProminences(i) = mean(window)+3*std(window);

    if minProminences(i) < 0.001 % sometime EKG signal is lost

    else

        % Find peaks and troughs using the updated minimum prominences
        [localPeaks_R,localPeakIndices_R] = findpeaks(window,'MinPeakHeight',minProminences(i),'MinPeakDistance',0.2*EEG.srate);

        % re-encode the time to alligne to whole resp signal
        localPeakIndices_adjust_R = timepoints(localPeakIndices_R);
        rpeaks    = [rpeaks localPeakIndices_adjust_R];
        qrspeaks  = [qrspeaks localPeaks_R];
    end
end


figure
subplot(1,2,1)
plot(EEG.times,ecgin)
title('Raw ECG')
subplot(1,2,2)
plot(EEG.times,y)
title('R-Waves Localized by Wavelet Transform')
hold on
hwav = plot(rpeaks/EEG.srate,qrspeaks,'ro');
% hexp = plot(tm,y,'k*');
% xlabel('Seconds')



% figure
% subplot(1,2,2)
% plot((1:length(ecgout))./EEG.srate,zscore(ecgout))
% hold on
% xlabel('Seconds')
% plot(rpeaks./EEG.srate,qrspeaks,'ro')
% title('EKG');

EEG.ekgsrate         = EEG.srate;
EEG.RRinterval_tpoin = diff(rpeaks);
EEG.RRinterval_sec   = diff(rpeaks)./EEG.ekgsrate;
EEG.ekg_IBImean      = (mean(diff(rpeaks)))/EEG.ekgsrate; % interval in seconds
EEG.ekg_HR           = 60/EEG.ekg_IBImean; %beats per minute

fprintf(['\n\n Heart-rate before outliers removal:' num2str(EEG.ekg_HR) '\n\n']);
EEG.ekg_IBImean = [];
EEG.ekg_HR = [];

% remove outliers intervals:
% Define the outlier threshold
% threshold = 1.5 * iqr(diff(rpeaks));

% Find the indices of data points that are outliers
% outliers_below = diff(rpeaks) < (median(diff(rpeaks)) - threshold);
% outliers_above = diff(rpeaks) > (median(diff(rpeaks)) + threshold);
outliers_below = (diff(rpeaks)./EEG.ekgsrate) < 0.4;  %60/0.4 = 150bpm
outliers_above = (diff(rpeaks)./EEG.ekgsrate) > 1.5;  %60/1.6 = 37bpm
outliers = outliers_below | outliers_above;

% Remove outliers from the original dataset
rpeaks_no_outliers   = rpeaks(~outliers);
qrspeaks_no_outliers = qrspeaks(~outliers);

% store values in EEG:
EEG.qrspeaks = qrspeaks_no_outliers;
EEG.rpeaks   = rpeaks_no_outliers;

% calculate IBI:
EEG.ekg_IBImean = [];
EEG.ekg_IBIsd   = [];
EEG.ekgsrate    = EEG.srate;
EEG.ekg_IBIraw  = diff(rpeaks_no_outliers);
EEG.ekg_IBImean = (mean(diff(rpeaks_no_outliers)))/EEG.ekgsrate; % interval in seconds
EEG.ekg_IBIsd   = (std(diff(rpeaks_no_outliers)))/EEG.ekgsrate;
EEG.ekg_HR      = 60/EEG.ekg_IBImean; %beats per minute
% EEG.ekg_HR      = (length(rpeaks)/(length(EEG.times)/EEG.ekgsrate))*60;
% if EEG.srate defined in the peakfind
% EEG.ekg_IBImean = (mean(diff(rpeaks_no_outliers))); % interval in seconds
% EEG.ekg_IBIsd   = (std(diff(rpeaks_no_outliers)));
% EEG.ekg_HR      = 60/EEG.ekg_IBImean;


fprintf(['\n\n Heart-rate after outliers removal:' num2str(EEG.ekg_HR) '\n\n']);

% HRV
RR = EEG.ekg_IBIraw/EEG.srate;
EEG.rmssd    = HRV.RMSSD(RR,60);
EEG.hrv_sdsd = SDSD(RR,60);
EEG.hrv_sdnn = SDNN(RR,60);
EEG.rrhrv    = HRV.rrHRV(RR,60);
[EEG.pLF,EEG.pHF,EEG.LFHFratio,EEG.VLF,EEG.LF,EEG.HF] = fft_val(RR,60,EEG.srate);

