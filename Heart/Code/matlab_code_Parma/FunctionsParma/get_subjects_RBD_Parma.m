function [who_idx,code_patient,gender,age,patient,RBD] = get_subjects_RBD_Parma(whichsubject)


if isempty(whichsubject)
    %     [~,num,Raw] = xlsread('/media/fbernasc/Elements/Parkinson/Info/Patients_info_amica_noHC.xlsx','Sheet1');
    T = readtable('/mnt/nas/Users/Fosco/RBD/RBD_Parma/RBD_patients_info_Parma.csv');
    who_idx = 1:size(T(:,1),1);
    code_patient = table2array(T(:,2));
    gender = table2array(T(:,3));
    age    = table2array(T(:,4));
    patient  = table2array(T(:,5));
    RBD      = table2array(T(:,6));
    
elseif isnumeric(whichsubject) % If who is just a numeric index.
    T = readtable('/mnt/nas/Users/Fosco/RBD/RBD_Parma/RBD_patients_info_Parma.csv');
    who_idx = 1:size(T(whichsubject,1),1);
    code_patient = table2array(T(whichsubject,2));
    gender = table2array(T(whichsubject,3));
    age    = table2array(T(whichsubject,4));
    patient  = table2array(T(whichsubject,5));
    RBD      = table2array(T(whichsubject,6));

end