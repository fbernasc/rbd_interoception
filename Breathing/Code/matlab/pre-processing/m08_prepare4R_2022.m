clear all
visual_ica   = 0;
whichsubject = [];
whichdata    = '_cleanica_early_origref.set'; % select between _import_ekg and _import_continous
% whichdata    = '_import_ekg_versionb_early.set'; % for Parma data
whichdataout = '_cleanica_wittecg_1hz_early4R_origref.set';

[who_idx,code_patient,patient,RBD,PSG_sess1,PSG_sess2,Gr,match,newID,age,gender,center,converted] = get_subjects_RBD(whichsubject);

for isub = 1:length(who_idx)
    
    % -------------
    % Prepare data.
    % -------------
    
    cfg = get_cfg_RBD(code_patient{isub});
    EEG = [];
    
    
    % Write a status message to the command line.
    EEG = pop_loadset('filename',[cfg.subject_name whichdata],'filepath',cfg.dir_eeg,'loadmode','all');
    
    %         EEG = pop_resample(EEG,EEG.srate/2);
    
    % remove EOG:
    heog = find(~cellfun(@isempty,regexp({EEG.chanlocs.labels},'HEOG|EOG E1')));
    veog = find(~cellfun(@isempty,regexp({EEG.chanlocs.labels},'VEOG')));
    
%     if  strcmp(center{isub},'Parma')
%         ecg = find(~cellfun(@isempty,regexp({EEG.chanlocs.labels},'EKG')));
%         EEG.chanlocs(ecg).labels = 'ECG';
%     end
    
    EEG  = pop_select(EEG,'nochannel',[heog veog]);
        
    EEG.chanlocs(1).type   = '';
    EEG.chanlocs(1).theta  = 0;
    EEG.chanlocs(1).radius = 0;
    EEG.chanlocs(1).X = 0;
    EEG.chanlocs(1).Y = 0;
    EEG.chanlocs(1).Z = 0;
    EEG.chanlocs(1).sph_theta  = 0;
    EEG.chanlocs(1).sph_phi    = 0;
    EEG.chanlocs(1).sph_radius = 0;
    EEG.chanlocs(1).urchan = 1;
    EEG.chanlocs(1).sph_theta_besa = 0;
    EEG.chanlocs(1).sph_phi_besa   = 0;
    
    
    for e=1:length(EEG.epoch)
        
        EEG.newepoch(e).event = EEG.epoch(e).event(1);
        
        if iscell(EEG.epoch(e).eventtype) == 1
            EEG.newepoch(e).eventtype = 1;
        end
                

            EEG.newepoch(e).eventlatency = EEG.epoch(e).eventlatency(1);


            if strcmp(center{isub},'Bern')
                center2 = 1;
            elseif strcmp(center{isub},'Parma')
                center2 = 2;
            end
            EEG.newepoch(e).eventduration = EEG.epoch(e).eventduration(1);
            EEG.newepoch(e).center    = center2;
            EEG.newepoch(e).group     = Gr(isub);
            EEG.newepoch(e).ID        = who_idx(isub);
            EEG.newepoch(e).age       = age(isub);
            EEG.newepoch(e).gender    = gender(isub);
            EEG.newepoch(e).converted = converted(isub);
            EEG.newepoch(e).nbrtrials = EEG.trials;
            EEG.newepoch(e).nbrtrials = EEG.trials;
            EEG.newepoch(e).nbrtrials = EEG.trials;
        
    end
    
    EEG.epoch = EEG.newepoch;
    EEG = rmfield(EEG,'newepoch');
    EEG = eeg_checkset(EEG);
    EEG = pop_editset(EEG,'setname',[cfg.subject_name  whichdataout]);
    EEG = pop_saveset( EEG, [cfg.subject_name  whichdataout] , cfg.dir_eeg);
    
end
