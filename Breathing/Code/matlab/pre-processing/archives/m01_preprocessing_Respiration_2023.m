% function m01_preprocessing_Respiration(whichsubject)

clear all
close all
addpath(genpath('./matlab_code_Respiration/'));
addpath(genpath('./pre-processing/versionB/Respiration/'));
addpath(genpath('./matlab_code_Respiration/Functions/'));
addpath('./EEG/');
addpath('/home/fosco/toolboxes/eeglab2022.0')
eeglab nogui
addpath('/home/fosco/toolboxes/fieldtrip-master')
ft_defaults

whichsubject = [1:37];

% which subjects you want to process?
[who_idx,code_patient,patient,RBD,PSG_sess1,PSG_sess2,Gr,match,newID,age,gender,center,converted] = get_subjects_RBD(whichsubject);

for isub = 1:length(who_idx)

    % get cfgs
    cfg = get_cfg_RBD(code_patient{isub});
    EEG = [];

    % ---------------------------
    % Import parameters & path(s)
    % ---------------------------
    % Write a status message to the command line.
    fprintf('\n Now importing subject %s, (number %d of %d to process).\n\n',code_patient{isub},isub,length(who_idx));
    pause(1)

    %-------------
    % Import data
    %-------------
    if PSG_sess2(isub) == 1 % exlude the second session of patients that have 2

    else

        eegData = [cfg.dir_eeg code_patient{isub} '_import.set'];

        if ~exist(eegData,'file')
            fprintf('Does not exist!\n',eegData)
        else

            % Create output directory if necessary.
            if ~isdir(cfg.dir_eeg)
                mkdir(cfg.dir_eeg);
            end

            EEG = pop_loadset('filename',[code_patient{isub} '_import.set'],'filepath',cfg.dir_eeg);


            % add sleep staging:
            fprintf('\n\n [stage 1] add sleep stating to the data \n\n')
            EEG.sleepstages = NaN(1, EEG.pnts); % Create a vector of NaN values with the same length as the EEG data
            output_path = ['/media/fosco/T7/RBD/EEG/Preprocessed/' code_patient{isub} '/' code_patient{isub} '_mne_sleep_staging.csv'];
            sleep_stage_info = readmatrix(output_path);
            num_rows = size(sleep_stage_info, 1);
            sleep_stage_labels = sleep_stage_info(:, 2);

            if size(sleep_stage_labels,1) ~= size(EEG.times,2)
                fprintf('\n\n size sleep and data are different legnth !!! \n\n')
                keyboard
            else
                fprintf('\n\n GOOD size sleep and data are same legnth \n\n')
                pause(2)
            end

            % add sleepstage into data as channel:
            EEG.data(EEG.nbchan+1,:) = sleep_stage_labels';
            EEG.chanlocs(EEG.nbchan+1).labels = 'Sleep';
            EEG.sleep = sleep_stage_labels';
            EEG = eeg_checkset(EEG);


            %% Respiration
            [EEG] = findRespiration(EEG,0);
            % keyboard;

            %%
            % get the r-peak:
            fprintf('\n\n [ stage 5 ] finding r-peaks in ECG \n\n');
            
            chanekg = find(~cellfun(@isempty,regexp({EEG.chanlocs.labels},'ECG1-ECG2|ekg|ECG|EKG')));
            EEG.chanlocs(chanekg).labels = 'ECG';
            
            ecgchan = [];
            ecgin   = [];
            
            ecgchan = strcmp({EEG.chanlocs.labels},'ECG');
            ecgin   = EEG.data(ecgchan,:);

            % window = EEGresp.data(:,i:i+windowSize-1); % sliding win
            win_secs = 60;  % Adjust the window size in secs
            windowSize = round(win_secs*EEG.srate); % convert to timepoints
            numDataPoints = numel(ecgin);
            numWindows = floor(numDataPoints / windowSize);  % Number of non-overlapping windows

            [rpeaks_no_outliers,qrspeaks_no_outliers,ekg_IBIraw,ekg_IBImean,...
            ekg_IBIsd,ekg_HR,RR,rmssd,hrv_sdsd,hrv_sdnn,rrhrv,pLF,pHF,LFHFratio,VLF,LF,HF] = findECGpeaks(EEG,ecgin,numWindows,windowSize,0);

            % add values to EEG structure
            EEG.qrspeaks_wout_outliers  = rpeaks_no_outliers;
            EEG.rpeaks_wout_outliers    = qrspeaks_no_outliers;
            EEG.ekg_IBIraw              = ekg_IBIraw;
            EEG.ekg_IBImean             = ekg_IBImean;
            EEG.ekg_IBIsd               = ekg_IBIsd;
            EEG.ekg_HR                  = ekg_HR;
            EEG.RR                      = RR;
            EEG.rmssd                   = rmssd;
            EEG.hrv_sdsd                = hrv_sdsd;
            EEG.hrv_sdnn                = hrv_sdnn;
            EEG.rrhrv                   = rrhrv;
            EEG.pLF                     = pLF;
            EEG.pHF                     = pHF;
            EEG.LFHFratio               = LFHFratio;
            EEG.VLF                     = VLF;
            EEG.LF                      = LF;
            EEG.HF                      = HF;


            % keyboard;

            close all
            %%
            % identify reversals in sleep stages:
            sleepchan      = squeeze(EEG.data(strcmp({EEG.chanlocs.labels},'Sleep'),:,:));
            changedIndexes = diff(sleepchan)~=0;
            reasultcycles  = find(changedIndexes==1);
            awakefirst     = zeros(1,length(sleepchan));
            awakefirst(1:reasultcycles(1)) = 1; % from first time-point to last awake in the first part of the recording
            EEG.awakefirst = awakefirst;

            len = length(EEG.event);
            for i=1:length(rpeaks_no_outliers)
                e = len + i;
                EEG.event(e).latency = rpeaks_no_outliers(i);
                EEG.event(e).duration = 0;
                % EEG.event(e).type = i;
                EEG.event(e).type = 'r-peak';
                EEG.event(e).sleep = sleep_stage_labels(rpeaks_no_outliers(i));
                EEG.event(e).awake = awakefirst(EEG.event(i).latency);
            end

            EEG = eeg_checkset(EEG);
            EEG = eeg_checkset(EEG, 'eventconsistency');

            % for i=1:length(EEG.event)
            %     EEG.event(i).awake = awakefirst(EEG.event(i).latency);
            % end

            % ad heart variables per sleep stage
            lsleep = unique([EEG.event.sleep]);
            lsleep = lsleep +1; % to differentiate awake and wake  

            for s = 1:length(lsleep)
                if s == 1
                    selected_indices   = [];
                    selected_latencies = [];
                    A = [EEG.sleep;EEG.awakefirst];
                    % Define the conditions for column 1 and column 2
                    condition_column1 = A(1,:) == 0;  % Select values Wake/sleep stage
                    condition_column2 = A(2,:) == 1;  % Select values Awake
                    selected_indices = find(condition_column1 & condition_column2);
                    EEGtmp = [];
                    EEGtmp = EEG;
                    EEGtmp.data = [];
                    EEGtmp.data = EEG.data(:,selected_indices);
                    EEGtmp = eeg_checkset(EEGtmp);
                else
                    selected_indices   = [];
                    selected_latencies = [];
                    A = [EEG.sleep;EEG.awakefirst];
                    % Define the conditions for column 1 and column 2
                    condition_column1 = A(1,:) == lsleep(s)-1;  % Select values Wake
                    condition_column2 = A(2,:) == 0;  % Select values Awake
                    selected_indices = find(condition_column1 & condition_column2);
                    EEGtmp = [];
                    EEGtmp = EEG;
                    EEGtmp.data = [];
                    EEGtmp.data = EEG.data(:,selected_indices);
                    EEGtmp = eeg_checkset(EEGtmp);
                end

                % sometime eeg_check changes the timescale from s to ms
                % (which messes up with the code below):

                if EEGtmp.times(2) > 1
                    EEGtmp.times = EEGtmp.times./1000;
                end


                ecgchan = [];
                ecgin   = [];
                ecgchan = strcmp({EEGtmp.chanlocs.labels},'ECG');
                ecgin   = EEGtmp.data(ecgchan,:);

                % window = EEGresp.data(:,i:i+windowSize-1); % sliding win
                numWindows = [];
                win_secs   = 30;  % Adjust the window size in secs
                windowSize    = round(win_secs*EEGtmp.srate); % convert to timepoints
                numDataPoints = numel(ecgin);
                numWindows    = floor(numDataPoints / windowSize);  % Number of non-overlapping windows

                [rpeaks_no_outliers,qrspeaks_no_outliers,ekg_IBIraw,ekg_IBImean,...
                ekg_IBIsd,ekg_HR,RR,rmssd,hrv_sdsd,hrv_sdnn,rrhrv,pLF,pHF,LFHFratio,VLF,LF,HF] = findECGpeaks(EEGtmp,ecgin,numWindows,windowSize,1);

                % add values to EEG structure
                EEG.qrspeaks_wout_outliers_sleepstage{s,:}  = rpeaks_no_outliers;
                EEG.rpeaks_wout_outliers_sleepstage{s,:}    = qrspeaks_no_outliers;
                EEG.ekg_IBIraw_sleepstage{s,:}              = ekg_IBIraw;
                EEG.ekg_IBImean_sleepstage{s,:}             = ekg_IBImean;
                EEG.ekg_IBIsd_sleepstage{s,:}               = ekg_IBIsd;
                EEG.ekg_HR_sleepstage{s,:}                  = ekg_HR;
                EEG.RR_sleepstage{s,:}                      = RR;
                EEG.rmssd_sleepstage{s,:}                   = mean(rmssd,'omitnan');
                EEG.hrv_sdsd_sleepstage{s,:}                = mean(hrv_sdsd,'omitnan');
                EEG.hrv_sdnn_sleepstage{s,:}                = mean(hrv_sdnn,'omitnan');
                EEG.rrhrv_sleepstage{s,:}                   = mean(rrhrv,'omitnan');
                EEG.pLF_sleepstage{s,:}                     = mean(pLF,'omitnan');
                EEG.pHF_sleepstage{s,:}                     = mean(pHF,'omitnan');
                EEG.LFHFratio_sleepstage{s,:}               = mean(LFHFratio,'omitnan');
                EEG.VLF_sleepstage{s,:}                     = mean(VLF,'omitnan');
                EEG.LF_sleepstage{s,:}                      = mean(LF,'omitnan');
                EEG.HF_sleepstage                           = mean(HF,'omitnan');
            end

            % keyboard;
            pause(2)
            close all

            pop_saveset(EEG,[code_patient{isub} '_import_w_physio.set'], cfg.dir_eeg);

            clear EEG bmObjTrx bmObjAb ecgout ecgin EEGresp EEGresp_up
            close all

        end
    end
end
% end
 
