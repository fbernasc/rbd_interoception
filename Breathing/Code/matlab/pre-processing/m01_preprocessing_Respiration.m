% function m01_preprocessing_Respiration(whichsubject)

clear all
close all
addpath(genpath('./matlab_code_Respiration/'));
addpath(genpath('./pre-processing/versionB/Respiration/'));
addpath(genpath('./matlab_code_Respiration/Functions/'));
addpath('./EEG/');
addpath('/home/fosco/toolboxes/eeglab2022.0')
eeglab nogui
addpath('/home/fosco/toolboxes/fieldtrip-master')
ft_defaults

whichsubject = [1:37];

% which subjects you want to process?
[who_idx,code_patient,patient,RBD,PSG_sess1,PSG_sess2,Gr,match,newID,age,gender,center,converted] = get_subjects_RBD(whichsubject);

for isub = 1:length(who_idx)

    % get cfgs
    cfg = get_cfg_RBD(code_patient{isub});
    EEG = [];

    % ---------------------------
    % Import parameters & path(s)
    % ---------------------------
    % Write a status message to the command line.
    fprintf('\n Now importing subject %s, (number %d of %d to process).\n\n',code_patient{isub},isub,length(who_idx));
    pause(1)

    %-------------
    % Import data
    %-------------
    if PSG_sess2(isub) == 1 % exlude the second session of patients that have 2

    else

        eegData = [cfg.dir_eeg code_patient{isub} '_import.set'];

        if ~exist(eegData,'file')
            fprintf('Does not exist!\n',eegData)
        else

            % Create output directory if necessary.
            if ~isdir(cfg.dir_eeg)
                mkdir(cfg.dir_eeg);
            end

            EEG = pop_loadset('filename',[code_patient{isub} '_import.set'],'filepath',cfg.dir_eeg);


            % add sleep staging:
            fprintf('\n\n [stage 1] add sleep stating to the data \n\n')
            EEG.sleepstages = NaN(1, EEG.pnts); % Create a vector of NaN values with the same length as the EEG data
            output_path = ['/media/fosco/T7/RBD/EEG/Preprocessed/' code_patient{isub} '/' code_patient{isub} '_mne_sleep_staging.csv'];
            sleep_stage_info = readmatrix(output_path);
            num_rows = size(sleep_stage_info, 1);
            sleep_stage_labels = sleep_stage_info(:, 2);

            if size(sleep_stage_labels,1) ~= size(EEG.times,2)
                fprintf('\n\n size sleep and data are different legnth !!! \n\n')
                keyboard
            else
                fprintf('\n\n GOOD size sleep and data are same legnth \n\n')
                pause(2)
            end

            % add sleepstage into data as channel:
            EEG.data(EEG.nbchan+1,:) = sleep_stage_labels';
            EEG.chanlocs(EEG.nbchan+1).labels = 'Sleep';
            EEG = eeg_checkset(EEG);


            % check if figure is same as for data in fieldtrip to ensure no
            % changes in sampling etc when going from FT to EEGLAB
            % Select interval from 4 seconds to 10 seconds
            start_time = 500;  % Start time in seconds
            end_time   = 650;  % End time in seconds

            % Create logical index for the selected interval
            positions1 = find(EEG.times == start_time);
            positions2 = find(EEG.times == end_time);


            chanRespAbdomen  = find(strcmp({EEG.chanlocs.labels},'Abdomen'));
            chanRespThorax   = find(strcmp({EEG.chanlocs.labels},'Thorax'));
            chanResp_flow    = find(strcmp({EEG.chanlocs.labels},'Flow_DR'));


            % figure;
            % subplot(2,2,1)
            % plot(EEG.times(positions1:positions2),EEG.data(chanRespAbdomen,positions1:positions2)); hold on;
            % title('Abdomen')
            % subplot(2,2,2)
            % plot(EEG.times(positions1:positions2),EEG.data(chanRespThorax,positions1:positions2)); hold on;
            % title('Thorax')
            % subplot(2,2,3)
            % plot(EEG.times(positions1:positions2),EEG.data(chanResp_flow,positions1:positions2)); hold on;
            % title('Flow DR')
            %keyboard;

            % get resipratio features:
            fprintf('\n\n [stage 1.a] get respiration features from Belt \n\n')
            dataType = 'humanBB';
            bmObjAb  = [];
            bmObjAb  = breathmetrics(EEG.data(chanRespAbdomen,:),EEG.srate,dataType);
            bmObjAb.estimateAllFeatures();
            % fig = bmObjAb.plotFeatures();

            EEG.inhaleOnsetsAbdo = bmObjAb.inhaleOnsets;
            EEG.exhaleOnsetsAbdo = bmObjAb.exhaleOnsets;
            EEG.Average_Inter_Breath_Interval = (mean(diff([EEG.inhaleOnsetsAbdo])))/EEG.srate; % interval in seconds
            EEG.IBIsd            = (std(diff([EEG.inhaleOnsetsAbdo])))/EEG.srate;
            EEG.breathing_rate   = 60./[EEG.Average_Inter_Breath_Interval];

            % get info also from thorax belt
            bmObjTrx = [];
            bmObjTrx = breathmetrics(EEG.data(chanRespThorax,:),EEG.srate,dataType);
            bmObjTrx.estimateAllFeatures();
            % fig = bmObjTrx.plotFeatures();

            EEG.inhaleOnsetsTrx = bmObjTrx.inhaleOnsets;
            EEG.exhaleOnsetsTrx = bmObjTrx.exhaleOnsets;

            fprintf('\n\n [stage 1.b] get breathing phase interpolation from breathing belt \n\n')

            chanresp = chanRespAbdomen; % chanRespThorax or chanResp_flow or chanRespAbdomen
            indices = sort([EEG.inhaleOnsetsAbdo, EEG.exhaleOnsetsAbdo]);
            interp_respiration = [];
            for j = 1:2:length(indices)-2
                if indices(j) < indices(j+1) && indices(j+2) <= indices(end)
                    interp_respiration(indices(j):indices(j+1)) = linspace(-pi, 0, indices(j+1)-indices(j)+1);
                    interp_respiration(indices(j+1):indices(j+2)) = linspace(0, pi, indices(j+2)-indices(j+1)+1);
                end
            end

            figure; hold all
            subplot(2,2,1)
            plot(EEG.times(positions1:positions2),EEG.data(chanresp,positions1:positions2)); hold on;
            plot(EEG.times(positions1:positions2),interp_respiration(:,positions1:positions2)); hold on;
            title('Breathing belt')
            legend('Raw Respiration Belt', 'Respiration phase Belt');


            % filter respiratory signal:
            fprintf('\n\n [stage 2.a] get respiration features from nasal flow \n\n')

            dataType = 'humanAirflow';
            bmObjNose  = [];
            bmObjNose = breathmetrics(EEG.data(chanResp_flow,:),EEG.srate,dataType);
            bmObjNose.estimateAllFeatures();

            EEG.inhaleOnsetsNose   = bmObjNose.inhaleOnsets;
            EEG.exhaleOnsetsNose   = bmObjNose.exhaleOnsets;
            EEG.bmObjNose          = bmObjNose;
            % fig = bmObjNose.plotFeatures();

            fprintf('\n\n [stage 2.b] get breathing phase interpolation from nasal flow \n\n')
            indices = sort([EEG.inhaleOnsetsNose,EEG.exhaleOnsetsNose]);
            interp_respiration = [];
            for j = 1:2:length(indices)-2
                if indices(j) < indices(j+1) && indices(j+2) <= indices(end)
                    interp_respiration(indices(j):indices(j+1)) = linspace(-pi, 0, indices(j+1)-indices(j)+1);
                    interp_respiration(indices(j+1):indices(j+2)) = linspace(0, pi, indices(j+2)-indices(j+1)+1);
                end
            end

            % store data in EEG
            EEG.respphaseNose = interp_respiration;

            % figure
            subplot(2,2,2)
            plot(EEG.times(positions1:positions2),EEG.data(chanresp,positions1:positions2)); hold on;
            plot(EEG.times(positions1:positions2),interp_respiration(:,positions1:positions2)); hold on;
            title('Nasal flow');
            legend('Raw Nasal Respiration', 'Interpolated Respiration');


            fprintf('\n\n [stage 3.a] get breathing phase interpolation respiration belt \n\n')
            EEGresp  = [];
            chanresp = chanRespAbdomen; % chanRespThorax or chanResp_flow or chanRespAbdomen
            EEGresp  = pop_select(EEG, 'channel',chanresp);
            % EEGresp.data = zscore(EEGresp.data);
            % EEGresp.data = sgolayfilt(double(EEGresp.data),9,21);
            EEGresp = pop_eegfiltnew(EEGresp, 'locutoff',0.2,'hicutoff',0.8,'plotfreqz',0);

            % calculate respiratory phase
            resp_phase = angle(hilbert(EEGresp.data)); %phase of the respiration signal
            EEG.resp_phase = resp_phase;

            % store data in EEG
            EEG.respphaseHilbert = resp_phase;

            subplot(2,2,3)
            % plot(EEG.times(positions1:positions2),EEG.data(chanresp,positions1:positions2)); hold on;
            plot(EEG.times(positions1:positions2),EEGresp.data(:,positions1:positions2)); hold on;
            plot(EEG.times(positions1:positions2),resp_phase(:,positions1:positions2)); hold on;
            title('Hilbert on Breathing Belt')


            fprintf('\n\n [stage 4.a] get breathing phase interpolation respiration belt with sliding window \n\n')

            % Parameters for sliding window
            win_secs = 60;  % Adjust the window size in secs
            windowSize = round(win_secs*EEG.srate); % convert to timepoints
            numDataPoints = numel(EEGresp.data);
            numWindows = floor(numDataPoints / windowSize);  % Number of non-overlapping windows

            EEGresp  = [];
            chanresp = chanRespAbdomen; % chanRespThorax or chanResp_flow or chanRespAbdomen
            EEGresp  = pop_select(EEG, 'channel',chanresp);
            % EEGresp.data = zscore(EEGresp.data);

            peakIndices = [];
            peakValues  = [];
            troughsIndices = [];
            troughsValues  = [];
            localPeakIndices_adjust = [];
            localPeakIndices_adjust = [];

            % Find peaks within each sliding window
            for i = 1:numWindows % for sliding win use: numDataPoints-windowSize+1
                window = [];
                % window = EEGresp.data(:,i:i+windowSize-1); % sliding win
                windowStart = (i-1)*windowSize + 1;
                windowEnd   = i*windowSize;
                timepoints  = ([windowStart:windowEnd]);
                window      = (EEGresp.data(:,windowStart:windowEnd));
                minProminences(i) = std(window) - (std(window)*0.2);

                % Find peaks and troughs using the updated minimum prominences
                [localPeaks,localPeakIndices]     = findpeaks(window,'MinPeakProminence', minProminences(i),'MinPeakDistance',0.6*EEG.srate);
                [localtroughs,locatroughsIndices] = findpeaks(-window,'MinPeakProminence', minProminences(i),'MinPeakDistance',0.6*EEG.srate);

                % re-encode the time to alligne to whole resp signal
                localPeakIndices_adjust = timepoints(localPeakIndices);
                peakIndices = [peakIndices localPeakIndices_adjust];
                peakValues  = [peakValues localPeaks];

                localtroughsIndices_adjust = timepoints(locatroughsIndices);
                troughsIndices = [troughsIndices localtroughsIndices_adjust];
                troughsValues  = [troughsValues localtroughs];

                % update raw respiration to zscore by window:
                EEGresp_up.data(:,windowStart:windowEnd) = window;
                EEG.peakIndices = peakIndices;
                EEG.peakValues  = peakValues;
                EEG.peakIndices = peakIndices;
                EEG.troughsIndices = troughsIndices;
                EEG.troughsValues  = troughsValues;
            end

            % Find peaks and troughs using the updated minimum prominences
            indices = [];
            indices = sort([peakIndices troughsIndices]);
            interp_respiration = [];
            for j = 1:2:length(indices)-2
                if indices(j) < indices(j+1) && indices(j+2) <= indices(end)
                    interp_respiration(indices(j):indices(j+1))   = linspace(-pi, 0, indices(j+1)-indices(j)+1);
                    interp_respiration(indices(j+1):indices(j+2)) = linspace(0, pi, indices(j+2)-indices(j+1)+1);
                end
            end

            % store data in EEG
            EEG.respphaseFindpeak = interp_respiration;

            subplot(2,2,4)
            % plot(EEG.times(positions1:positions2),EEG.data(chanresp,positions1:positions2)); hold on;
            plot(EEG.times(positions1:positions2),EEGresp.data(positions1:positions2)); hold on;
            % plot(EEGresp_up.data); hold on;
            % plot(peakIndices,peakValues,"o");hold on;
            % plot(troughsIndices2,troughsValues.*-1,"o");hold on;
            plot(EEG.times(positions1:positions2),interp_respiration(positions1:positions2)); hold on;
            title('Peakfind - andaptive win on normalized Breathing Belt')
            % keyboard;

            % get the r-peak:
            fprintf('\n\n [ stage 5 ] finding r-peaks in ECG \n\n');
            chanekg = find(~cellfun(@isempty,regexp({EEG.chanlocs.labels},'ECG1-ECG2|ekg|ECG|EKG')));
            EEG.chanlocs(chanekg).labels = 'ECG';

            ecgchan = [];
            ecgin   = [];

            ecgchan = strcmp({EEG.chanlocs.labels},'ECG');
            ecgin = EEG.data(ecgchan,:);

            % filter the EKG:
            % Filter order
            % ord=2;
            % ecgin = double(ecgin);
            %
            % % % Filter
            % Wn     = [3 30]/fix(EEG.srate/2);
            % [B,A]  = butter (ord,Wn);
            % ecgout = filtfilt(B,A,ecgin);
            % [qrspeaks,rpeaks] = findpeaks(zscore(ecgout),EEG.srate,'MinPeakProminence',4,'MinPeakDistance',0.2);

            % adaptive win ----Y NOT working
            rpeaks   = [];
            qrspeaks = [];
            minProminences = [];

            wt = modwt(ecgin,5);
            wtrec = zeros(size(wt));
            wtrec(4:5,:) = wt(4:5,:);
            y = imodwt(wtrec,'sym4');
            y = abs(y).^2;

            localPeakIndices_adjust = [];

            for i = 1:numWindows % for sliding win use: numDataPoints-windowSize+1
                window = [];
                windowStart = (i-1)*windowSize + 1;
                windowEnd   = i*windowSize;
                timepoints  = ([windowStart:windowEnd]);
                window      = (y(:,windowStart:windowEnd));

                % remove outliers in the window:
                minProminences(i) = mean(window)+3*std(window);

                if minProminences(i) < 0.001 % sometime EKG signal is lost

                else

                    % Find peaks and troughs using the updated minimum prominences
                    [localPeaks_R,localPeakIndices_R] = findpeaks(window,'MinPeakHeight',minProminences(i),'MinPeakDistance',0.2*EEG.srate);

                    % re-encode the time to alligne to whole resp signal
                    localPeakIndices_adjust_R = timepoints(localPeakIndices_R);
                    rpeaks    = [rpeaks localPeakIndices_adjust_R];
                    qrspeaks  = [qrspeaks localPeaks_R];
                end
            end


            figure
            subplot(1,2,1)
            plot(EEG.times,ecgin)
            title('Raw ECG')
            subplot(1,2,2)
            plot(EEG.times,y)
            title('R-Waves Localized by Wavelet Transform')
            hold on
            hwav = plot(rpeaks/EEG.srate,qrspeaks,'ro');
            % hexp = plot(tm,y,'k*');
            % xlabel('Seconds')



            % figure
            % subplot(1,2,2)
            % plot((1:length(ecgout))./EEG.srate,zscore(ecgout))
            % hold on
            % xlabel('Seconds')
            % plot(rpeaks./EEG.srate,qrspeaks,'ro')
            % title('EKG');

            EEG.ekgsrate         = EEG.srate;
            EEG.RRinterval_tpoin = diff(rpeaks);
            EEG.RRinterval_sec   = diff(rpeaks)./EEG.ekgsrate;
            EEG.ekg_IBImean      = (mean(diff(rpeaks)))/EEG.ekgsrate; % interval in seconds
            EEG.ekg_HR           = 60/EEG.ekg_IBImean; %beats per minute

            fprintf(['\n\n Heart-rate before outliers removal:' num2str(EEG.ekg_HR) '\n\n']);
            EEG.ekg_IBImean = [];
            EEG.ekg_HR = [];

            % remove outliers intervals:
            % Define the outlier threshold
            % threshold = 1.5 * iqr(diff(rpeaks));

            % Find the indices of data points that are outliers
            % outliers_below = diff(rpeaks) < (median(diff(rpeaks)) - threshold);
            % outliers_above = diff(rpeaks) > (median(diff(rpeaks)) + threshold);
            outliers_below = (diff(rpeaks)./EEG.ekgsrate) < 0.4;  %60/0.4 = 150bpm
            outliers_above = (diff(rpeaks)./EEG.ekgsrate) > 1.5;  %60/1.6 = 37bpm
            outliers = outliers_below | outliers_above;

            % Remove outliers from the original dataset
            rpeaks_no_outliers   = rpeaks(~outliers);
            qrspeaks_no_outliers = qrspeaks(~outliers);

            % store values in EEG:
            EEG.qrspeaks = qrspeaks_no_outliers;
            EEG.rpeaks   = rpeaks_no_outliers;

            % calculate IBI:
            EEG.ekg_IBImean = [];
            EEG.ekg_IBIsd   = [];
            EEG.ekgsrate    = EEG.srate;
            EEG.ekg_IBIraw  = diff(rpeaks_no_outliers);
            EEG.ekg_IBImean = (mean(diff(rpeaks_no_outliers)))/EEG.ekgsrate; % interval in seconds
            EEG.ekg_IBIsd   = (std(diff(rpeaks_no_outliers)))/EEG.ekgsrate;
            EEG.ekg_HR      = 60/EEG.ekg_IBImean; %beats per minute
            % EEG.ekg_HR      = (length(rpeaks)/(length(EEG.times)/EEG.ekgsrate))*60;
            % if EEG.srate defined in the peakfind
            % EEG.ekg_IBImean = (mean(diff(rpeaks_no_outliers))); % interval in seconds
            % EEG.ekg_IBIsd   = (std(diff(rpeaks_no_outliers)));
            % EEG.ekg_HR      = 60/EEG.ekg_IBImean;


            fprintf(['\n\n Heart-rate after outliers removal:' num2str(EEG.ekg_HR) '\n\n']);


            % HRV
            RR = EEG.ekg_IBIraw/EEG.srate;
            EEG.rmssd    = HRV.RMSSD(RR,60);
            EEG.hrv_sdsd = SDSD(RR,60);
            EEG.hrv_sdnn = SDNN(RR,60);
            EEG.rrhrv    = HRV.rrHRV(RR,60);
            [EEG.pLF,EEG.pHF,EEG.LFHFratio,EEG.VLF,EEG.LF,EEG.HF] = fft_val(RR,60,EEG.srate);


            % % calculate HRV
            % rr_test_st2 = {};
            % rr_test_st2.code = code_patient{isub};
            % rr_test_st2.rr_interval = EEG.ekg_IBIraw./EEG.srate;
            % rr_test_st2.rr_peaks    = rpeaks_no_outliers./EEG.ekgsrate;
            % [hrv_feats_tb] = hrv_features(rr_test_st2);
            %
            % EEG.mean_NN   = hrv_feats_tb(:,2); % mean NN (normalised RR interval)
            % EEG.SD_NN     = hrv_feats_tb(:,3); % standard deviation of NN
            % EEG.VLF_power = hrv_feats_tb(:,4); % power in the very low frequency band (0.01 to 0.04 Hz)
            % EEG.LF_power  = hrv_feats_tb(:,5); % power in the low frequency band (0.04 to 0.2 Hz)
            % EEG.HF_power  = hrv_feats_tb(:,6); % power in the high frequency band (0.2 to 2 Hz)
            % EEG.LF_HF_ratio = hrv_feats_tb(:,7); % LF_power:HF_power ratio


            keyboard;

            % identify reversals in sleep stages:
            sleepchan      = squeeze(EEG.data(strcmp({EEG.chanlocs.labels},'Sleep'),:,:));
            changedIndexes = diff(sleepchan)~=0;
            reasultcycles  = find(changedIndexes==1);
            awakefirst      = zeros(1,length(sleepchan));
            awakefirst(1:reasultcycles(1)) = 1; % from first time-point to last awake in the first part of the recording


            len = length(EEG.event);
            for i=1:length(rpeaks_no_outliers)
                e = len + i;
                EEG.event(e).latency = rpeaks_no_outliers(i);
                EEG.event(e).duration = 0;
                EEG.event(e).type = i;
                EEG.event(e).heart = 'r-peak';
                EEG.event(e).sleep = sleep_stage_labels(rpeaks_no_outliers(i));
                EEG.event(e).awake = awakefirst(EEG.event(i).latency);
            end
            
            EEG = eeg_checkset(EEG);
            EEG = eeg_checkset(EEG, 'eventconsistency');

            % for i=1:length(EEG.event)
            %     EEG.event(i).awake = awakefirst(EEG.event(i).latency);
            % end

            % ad heart variables per sleep stage
            lsleep = unique([EEG.event.sleep]);
            lsleep = lsleep +1; % to differentiate awake and wake

            for s = 1:length(lsleep)
                if s == 1
                    A = [EEG.event.sleep;EEG.event.awake];
                    % Define the conditions for column 1 and column 2
                    condition_column1 = A(1,:) == lsleep(s);  % Select values in column 1 greater than 2
                    condition_column2 = A(2,:) == 1; % Select values in column 2 less than 40
                    selected_indices = find(condition_column1 & condition_column2);
                    EEGtmp = [];
                    EEGtmp = pop_selectevent(EEG,'type',[selected_indices']);
                else
                    % Define the conditions for column 1 and column 2
                    condition_column1 = A(1,:) == lsleep(s)-1;  % Select values in column 1 greater than 2
                    condition_column2 = A(2,:) == 1; % Select values in column 2 less than 40
                    selected_indices = find(condition_column1 & condition_column2);
                    EEGtmp = [];
                    EEGtmp = pop_selectevent(EEG,'type',[selected_indices']);
                end

                % get RR interval for time of interest:
                rpeaks_no_outliers_tmp      = [];
                rpeaks_no_outliers_tmp      = rpeaks_no_outliers();

                % calculate IBI:
                EEG.ekg_IBImean_sleep_stage = [];
                EEG.ekg_IBIsd_sleep_stage   = [];
                EEG.ekgsrate_sleep_stage    = EEG.srate;
                EEG.ekg_IBIraw_sleep_stage  = sdiff(rpeaks_no_outliers_tmp);
                EEG.ekg_IBImean_sleep_stage = (mean(diff(rpeaks_no_outliers_tmp)))/EEG.ekgsrate_sleep_stage; % interval in seconds
                EEG.ekg_IBIsd_sleep_stage   = (std(diff(rpeaks_no_outliers_tmp)))/EEG.ekgsrate_sleep_stage;
                EEG.ekg_HR_sleep_stage      = 60/EEG.ekg_IBImean; %beats per minute

                % HRV
                RR = EEG.ekg_IBImean_sleep_stage/EEG.ekgsrate_sleep_stage;
                EEG.rmssd    = HRV.RMSSD(RR,60);
                EEG.hrv_sdsd = SDSD(RR,60);
                EEG.hrv_sdnn = SDNN(RR,60);
                EEG.rrhrv    = HRV.rrHRV(RR,60);
                [EEG.pLF,EEG.pHF,EEG.LFHFratio,EEG.VLF,EEG.LF,EEG.HF] = fft_val(RR,60,EEG.srate);
           end



            pop_saveset(EEG,[code_patient{isub} '_import_w_physio.set'], cfg.dir_eeg);

            clear EEG bmObjTrx bmObjAb ecgout ecgin EEGresp EEGresp_up
            close all

        end
    end
end
% end

